import { configure } from '@storybook/react'
import { addDecorator } from '@storybook/react'
import { withThemesProvider } from 'storybook-addon-styled-component-theme'
import { withA11y } from '@storybook/addon-a11y'

const themes = [{ name: 'default' }]

addDecorator(withThemesProvider(themes))
addDecorator(withA11y)

configure(() => {
  const req = require.context('../src', true, /\.stories\.tsx$/)
  req.keys().forEach(filename => req(filename))
}, module)
