import React from 'react'
import { Route } from 'react-router'

import { ROUTES } from '~/constants'
import { Pages } from '~/pages'

// prettier-ignore
export const Routes = () => (
  <>
    <Route exact path="/_playground" component={Pages.Playground} />
    <Route exact path="/_signalr" component={Pages.SignalR} />

    <Route exact path={ROUTES.LABELS} component={Pages.Labels} />
    <Route exact path={ROUTES.LABEL_NEW} component={Pages.LabelsForm} />
    <Route exact path={ROUTES.LABEL_EDIT} component={Pages.LabelsForm} />

    <Route exact path={ROUTES.RECERTIFICATIONS} component={Pages.Recertifications} />
    <Route exact path={ROUTES.RECERTIFICATION_NEW} component={Pages.RecertificationsForm} />
    <Route exact path={ROUTES.RECERTIFICATION_EDIT} component={Pages.RecertificationsForm} />

    <Route exact path={ROUTES.JOB_POSITIONS} component={Pages.JobPositions} />
    <Route exact path={ROUTES.JOB_POSITION_NEW} component={Pages.JobPositionsForm} />
    <Route exact path={ROUTES.JOB_POSITION_EDIT} component={Pages.JobPositionsForm} />

    <Route exact path={ROUTES.LABEL_RESTRICTED_PAIRS} component={Pages.LabelRestrictedPairs} />
    <Route exact path={ROUTES.LABEL_RESTRICTED_PAIR_NEW} component={Pages.LabelRestrictedPairsForm} />
    <Route exact path={ROUTES.LABEL_RESTRICTED_PAIR_EDIT} component={Pages.LabelRestrictedPairsForm} />

    <Route exact path={ROUTES.ORGANIZATIONAL_UNITS} component={Pages.OrganizationalUnits} />
    <Route exact path={ROUTES.ORGANIZATIONAL_UNIT_NEW} component={Pages.OrganizationalUnitsForm} />
    <Route exact path={ROUTES.ORGANIZATIONAL_UNIT_EDIT} component={Pages.OrganizationalUnitsForm} />

    <Route exact path={ROUTES.RECERTIFICATION_UNITS} component={Pages.RecertificationUnits} />
    <Route exact path={ROUTES.RECERTIFICATION_UNIT_EDIT} component={Pages.RecertificationUnitsForm} />
  </>
)
