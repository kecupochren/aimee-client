import { theme } from '~/constants'
import { css } from '~/utils'

export const globalCSS = css`
  && {
    html {
      overflow-y: scroll;
    }

    html,
    body {
      font-family: ${theme.font.family.regular};
      font-size: ${theme.font.size.default};
      font-weight: 400;
      color: ${theme.color.text};
    }

    svg {
      display: block;
    }

    img {
      max-width: 100%;
      height: auto;
    }

    html a:hover {
      color: #00affa;
    }

    strong {
      font-weight: 500;
    }
  }
`
