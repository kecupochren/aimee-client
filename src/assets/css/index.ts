import { createGlobalStyle } from '~/utils'

import { resetCSS } from './reset'
import { globalCSS } from './globals'

export const GlobalStyle = createGlobalStyle`
  ${resetCSS}
  ${globalCSS}
`
