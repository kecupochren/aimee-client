import React from 'react'

import { styled, css } from '~/utils'

interface IProps {
  size?: number
  inline?: boolean
  fill?: string
  className?: string
}

export const IconCheck = ({ className, size, fill, inline }: IProps) => (
  <SVG
    size={size}
    className={className}
    style={inline ? { display: 'inline' } : {}}
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24"
  >
    <path d="M0 0h24v24H0z" fill="none" />
    <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z" fill={fill || 'currentcolor'} />
  </SVG>
)

const SVG = styled.svg<IProps>`
  ${props => css`
    ${props.size &&
      css`
        width: ${props.size}px;
        height: ${props.size}px;
      `}
  `}
`
