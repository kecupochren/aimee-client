import React from 'react'

import { styled, css } from '~/utils'

interface IProps {
  fill?: string
  className?: string
  left?: boolean
  right?: boolean
  up?: boolean
}

export const IconCaret = ({ className, fill }: IProps) => (
  <SVG
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24"
  >
    <path d="M7 10l5 5 5-5z" fill={fill || 'currentColor'} />
    <path d="M0 0h24v24H0z" fill="none" />
  </SVG>
)

const SVG = styled.svg<IProps>`
  ${props => css`
    ${props.left &&
      css`
        transform: rotate(90deg);
      `}

    ${props.right &&
      css`
        transform: rotate(-90deg);
      `}

    ${props.up &&
      css`
        transform: rotate(180deg);
      `}
  `}
`
