import { defineMessages } from 'react-intl'

export const sharedMessages = defineMessages({
  /**
   * Locales
   */
  'en-US': {
    id: 'english',
    defaultMessage: 'English'
  },
  'cs-CZ': {
    id: 'czech',
    defaultMessage: 'Czech'
  },

  /**
   * Actions
   */
  Add: {
    id: 'Glossary.Add',
    defaultMessage: 'Add'
  },
  Save: {
    id: 'Glossary.Save',
    defaultMessage: 'Save'
  },
  Delete: {
    id: 'Glossary.Delete',
    defaultMessage: 'Delete'
  },
  Remove: {
    id: 'Glossary.Remove', // missing
    defaultMessage: 'Remove'
  },
  Cancel: {
    id: 'Glossary.Cancel',
    defaultMessage: 'Cancel'
  },
  Edit: {
    id: 'Glossary.Edit',
    defaultMessage: 'Edit'
  },
  Confirm: {
    id: 'Glossary.Confirm',
    defaultMessage: 'Confirm'
  },

  /**
   * Generic
   */
  Name: {
    id: 'BusinessRole.Name',
    defaultMessage: 'Name'
  },
  Description: {
    id: 'Glossary.Description',
    defaultMessage: 'Description'
  },
  Created: {
    id: 'Glossary.Created',
    defaultMessage: 'Created at'
  },
  CreatedBy: {
    id: 'Glossary.CreatedBy',
    defaultMessage: 'Created by'
  },
  Changed: {
    id: 'Glossary.Changed',
    defaultMessage: 'Updated at'
  },
  ChangedBy: {
    id: 'Glossary.ChangedBy',
    defaultMessage: 'Updated by'
  },
  Action: {
    id: 'Glossary.Action',
    defaultMessage: 'Action'
  },
  Search: {
    id: 'Glossary.Search',
    defaultMessage: 'Search...'
  },
  Yes: {
    id: 'Glossary.Yes',
    defaultMessage: 'Yes'
  },
  No: {
    id: 'Glossary.No',
    defaultMessage: 'No'
  },
  NoResultsFound: {
    id: 'Glossary.NoResultsFound', // missing
    defaultMessage: 'No results found'
  },
  CannotDelete: {
    id: 'Glossary.CannotDelete', // missing
    defaultMessage: 'Cannot delete'
  },
  CannotDeleteSelectedRecords: {
    id: 'Glossary.CannotDeleteSelectedRecords', // missing
    defaultMessage: 'None of the selected records can be deleted'
  },
  ConfirmDelete: {
    id: 'Glossary.ConfirmDelete', // missing
    defaultMessage: 'Confirm delete'
  },
  ConfirmDeleteRecord: {
    id: 'Glossary.ConfirmDeleteRecord', // missing
    defaultMessage: 'Are you sure you want to delete this record?'
  },
  ConfirmDeleteRecords: {
    id: 'Glossary.ConfirmDeleteRecords', // missing
    defaultMessage: 'Are you sure you want to delete these {count} records?'
  },
  RecordsCannotBeDeleted: {
    id: 'Glossary.RecordsCannotBeDeleted', // missing
    defaultMessage: '\n {count} cannot be deleted.'
  },
  DeleteSuccess: {
    id: 'Glossary.deleteSuccess',
    defaultMessage: 'Record successfully deleted'
  },
  CreateSuccess: {
    id: 'Glossary.createSuccess',
    defaultMessage: 'Record successfully created'
  },
  EditSuccess: {
    id: 'Glossary.editSuccess',
    defaultMessage: 'Record successfully edited'
  },
  MissingPermission: {
    id: 'Glossary.missingPermission',
    defaultMessage: 'Your account does not have necessary permission to view this page'
  },
  CancelImpersonate: {
    id: 'Employee.CancelImpersonate',
    defaultMessage: 'Cancel impersonation'
  },
  UploadFile: {
    id: 'Glossary.UploadFile',
    defaultMessage: 'Upload file'
  },
  UnknownErrorOccured: {
    id: 'ErrorMessages.UnknownErrorOccured',
    defaultMessage: 'Unknown error occured.'
  },

  /**
   * Entities
   */
  BusinessRoles: {
    id: 'Navigation.BusinessRoles',
    defaultMessage: 'Business roles'
  },
  BusinessRole: {
    id: 'SodCompensation.BusinessRole',
    defaultMessage: 'Business role'
  },
  Companies: {
    id: 'Navigation.Companies',
    defaultMessage: 'Business roles'
  },
  Company: {
    id: 'SodCompensation.Company',
    defaultMessage: 'Business role'
  },
  DataSets: {
    id: 'Navigation.DataSets',
    defaultMessage: 'Data sets'
  },
  DataSet: {
    id: 'SodCompensation.DataSet',
    defaultMessage: 'Data set'
  },
  Departments: {
    id: 'Navigation.Departments',
    defaultMessage: 'Departments'
  },
  Department: {
    id: 'JobPosition.Department',
    defaultMessage: 'Department'
  },
  Employees: {
    id: 'Navigation.Employees',
    defaultMessage: 'Employees'
  },
  Employee: {
    id: 'Glossary.Employee',
    defaultMessage: 'Employee'
  },
  Identities: {
    id: 'Navigation.Identities',
    defaultMessage: 'Identities'
  },
  Identity: {
    id: 'JobPosition.Identity',
    defaultMessage: 'Identity'
  },
  JobPositions: {
    id: 'Navigation.JobPositions',
    defaultMessage: 'Job positions'
  },
  JobPosition: {
    id: 'Employee.JobPosition',
    defaultMessage: 'Job position'
  },
  LabelRestrictedPairs: {
    id: 'Navigation.LabelRestrictedPairs',
    defaultMessage: 'Label restricted pairs'
  },
  LabelRestrictedPair: {
    id: 'Glossary.LabelRestrictedPair', // missing
    defaultMessage: 'Label restricted pair'
  },
  Labels: {
    id: 'Navigation.Labels',
    defaultMessage: 'Labels'
  },
  Label: {
    id: 'SodCompensation.Label',
    defaultMessage: 'Label'
  },
  OrgUnits: {
    id: 'Navigation.OrganizationalUnits',
    defaultMessage: 'Organizational units'
  },
  OrgUnit: {
    id: 'SodCompensation.OrganizationalUnit',
    defaultMessage: 'Organizational unit'
  },
  Permissions: {
    id: 'Navigation.Permissions',
    defaultMessage: 'Permissions'
  },
  Permission: {
    id: 'SodCompensation.Permission',
    defaultMessage: 'Permission'
  },
  PreapprovedSodCompensations: {
    id: 'Navigation.PreapprovedSodCompensations',
    defaultMessage: 'Preapproved SoD compensations'
  },
  PreapprovedSodCompensation: {
    id: 'SodCompensation.PreapprovedSodCompensation',
    defaultMessage: 'Preapproved SoD compensation'
  },
  SodConflicts: {
    id: 'Navigation.SodConflicts',
    defaultMessage: 'SoD konflikty'
  },
  SodConflict: {
    id: 'SodCompensation.SodConflict',
    defaultMessage: 'SoD konflikt'
  },
  SystemApplications: {
    id: 'Navigation.SystemApplications',
    defaultMessage: 'System apps'
  },
  SystemPermissions: {
    id: 'Glossary.SystemPermissions',
    defaultMessage: 'System permissions'
  },
  SystemRoles: {
    id: 'Navigation.SystemRoles',
    defaultMessage: 'System roles'
  },
  WorkflowReport: {
    id: 'Navigation.WorkflowReport',
    defaultMessage: 'Workflow report'
  },
  RecertificationList: {
    id: 'Navigation.Recertification',
    defaultMessage: 'Recertifications list'
  },
  RecertificationUnitsList: {
    id: 'Navigation.RecertificationUnitsList',
    defaultMessage: 'Recertifications unit list'
  },
  MyRecertificationUnits: {
    id: 'Navigation.MyRecertificationUnits',
    defaultMessage: 'My recertification units'
  },

  /**
   * Business specific
   */
  Dashboard: {
    id: 'Glossary.Dashboard',
    defaultMessage: 'Dashboard'
  },
  SystemAndApps: {
    id: 'Homepage.SystemAndApps',
    defaultMessage: 'System and applications'
  },
  BusinessModel: {
    id: 'Homepage.BusinessModel',
    defaultMessage: 'Business model'
  },
  Recertification: {
    id: 'Homepage.Recertification',
    defaultMessage: 'Recertification'
  },
  RecertificationUnit: {
    id: 'Homepage.RecertificationUnit',
    defaultMessage: 'Recertification unit'
  },
  IsSoD: {
    id: 'Label.IsSoDCaption',
    defaultMessage: 'Is SoD'
  },
  Workflow: {
    id: 'Glossary.Workflow',
    defaultMessage: 'Workflow'
  },
  FirstLabel: {
    id: 'LabelRestrictedPair.FirstLabelCaption',
    defaultMessage: 'First label'
  },
  SecondLabel: {
    id: 'LabelRestrictedPair.SecondLabelCaption',
    defaultMessage: 'Second label'
  },
  IsConflictCompensable: {
    id: 'LabelRestrictedPair.IsConflictCompensable',
    defaultMessage: 'Is conflict compensable'
  },
  RecertificationStateInitial: {
    id: 'Recertification.StateInitial',
    defaultMessage: 'Initial'
  },
  RecertificationStateOpened: {
    id: 'Recertification.StateOpened',
    defaultMessage: 'Opened'
  },
  RecertificationStateConfirmed: {
    id: 'Recertification.StateConfirmed',
    defaultMessage: 'Confirmed'
  },
  RecertificationStateFinalized: {
    id: 'Recertification.StateFinalized',
    defaultMessage: 'Finalized'
  },
  RecertificationUnitStateInitial: {
    id: 'RecertificationUnit.StateInitial',
    defaultMessage: 'Initial'
  },
  RecertificationUnitStateDraft: {
    id: 'RecertificationUnit.StateDraft',
    defaultMessage: 'Opened'
  },
  RecertificationUnitStateConfirmed: {
    id: 'RecertificationUnit.StateConfirmed',
    defaultMessage: 'Confirmed'
  },
  RecertificationUnitStateUnlocked: {
    id: 'RecertificationUnit.StateUnlocked',
    defaultMessage: 'Unlocked'
  }
})

export const messages = sharedMessages
