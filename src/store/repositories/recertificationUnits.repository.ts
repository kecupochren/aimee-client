import { Repository } from '../modules'
import { RecertificationCreateVM } from '~/api/apimodels/Recertifications'
import { action } from 'mobx'
import { IRecertificationUnit, RecertificationUnitModel } from '../models'

export interface IRecertificationUnitsFilters {
  [key: string]: string[]
  Text: string[]
  RecertificationId: string[]
}

export class RecertificationUnitsRepository extends Repository<
  number,
  IRecertificationUnit,
  RecertificationUnitModel,
  IRecertificationUnitsFilters
> {
  constructor(app: any) {
    super(app, {
      createModel: (data: IRecertificationUnit) => new RecertificationUnitModel(this.helpers, data),
      fetch: query => this.helpers.api.getRecertificationUnits(query),
      fetchOne: id => this.helpers.api.getRecertificationUnit(id)
    })
  }

  @action.bound async create(props: RecertificationCreateVM) {
    return this.handleCreate(props)
  }
}
