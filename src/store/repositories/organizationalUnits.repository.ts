import { action } from 'mobx'

import { IOrganizationalUnit, IOrganizationalUnitForSaveVM } from '~/api/models'

import { Repository } from '../modules'
import { OrganizationalUnitModel } from '../models'

export class OrganizationalUnitsRepository extends Repository<
  number,
  IOrganizationalUnit,
  OrganizationalUnitModel
> {
  // ===================================
  // Constructor
  // ===================================
  constructor(app: any) {
    super(app, {
      createModel: (data: IOrganizationalUnit) => new OrganizationalUnitModel(this.helpers, data),
      hydrateModels: (data: IOrganizationalUnit, model: OrganizationalUnitModel) => {
        if (data.labels.length) {
          model.labels.replace(this.repositories.labels.addItems(data.labels, true))
        } else {
          model.labels.clear()
        }

        return model
      },
      fetch: query => this.helpers.api.getOrganizationalUnits(query),
      fetchOne: id => this.helpers.api.getOrganizationalUnit(id),
      create: props => this.helpers.api.createOrganizationalUnit(props as IOrganizationalUnitForSaveVM), // prettier-ignore
      edit: (id, props) => this.helpers.api.editOrganizationalUnit(id, props as IOrganizationalUnitForSaveVM), // prettier-ignore
      delete: id => this.helpers.api.deleteOrganizationalUnit(id)
    })
  }

  // ===================================
  // Mutations
  // ===================================
  @action.bound async create(props: IOrganizationalUnitForSaveVM) {
    return this.handleCreate(props)
  }

  @action.bound async edit(id: number, props: IOrganizationalUnitForSaveVM) {
    return this.handleEdit(id, props)
  }
}
