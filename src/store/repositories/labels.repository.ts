import { action } from 'mobx'

import { ILabel, ILabelForSaveVM } from '~/api/models'

import { Repository } from '../modules'
import { LabelModel } from '../models'

export class LabelsRepository extends Repository<number, ILabel, LabelModel> {
  // ===================================
  // Constructor
  // ===================================
  constructor(app: any) {
    super(app, {
      createModel: (data: ILabel) => new LabelModel(this.helpers, data),
      fetch: query => this.helpers.api.getLabels(query),
      fetchOne: id => this.helpers.api.getLabel(id),
      create: props => this.helpers.api.createLabel(props as ILabelForSaveVM),
      edit: (id, props) => this.helpers.api.editLabel(id, props as ILabelForSaveVM),
      delete: id => this.helpers.api.deleteLabel(id)
    })
  }

  // ===================================
  // Mutations
  // ===================================
  @action.bound async create(data: ILabelForSaveVM) {
    return this.handleCreate(data)
  }

  @action.bound async edit(id: number, data: ILabelForSaveVM) {
    return this.handleEdit(id, data)
  }
}
