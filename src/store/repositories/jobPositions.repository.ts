import { action } from 'mobx'

import { IJobPosition, IJobPositionForSaveVM } from '~/api/models'

import { Repository } from '../modules'
import { JobPositionModel } from '../models'

export interface IJobPositionFilters {
  Text?: string
  DepartmentId?: number
}

export class JobPositionsRepository extends Repository<
  number,
  IJobPosition,
  JobPositionModel,
  IJobPositionFilters
> {
  // ===================================
  // Constructor
  // ===================================
  constructor(app: any) {
    super(app, {
      createModel: (data: IJobPosition) => new JobPositionModel(this.helpers, data),
      hydrateModels: (data: IJobPosition, model: JobPositionModel) => {
        const { businessRoles } = data

        if (businessRoles && businessRoles.length) {
          const roles = data.businessRoles.map(x => ({
            ...x,
            id: x.businessRoleId
          }))
          model.businessRoles.replace(this.repositories.businessRoles.addItems(roles, true))
        } else {
          model.businessRoles.clear()
        }

        return model
      },
      fetch: query => this.helpers.api.getJobPositions(query),
      fetchOne: id => this.helpers.api.getJobPosition(id),
      create: props => this.helpers.api.createJobPosition(props),
      edit: (id, props) => this.helpers.api.editJobPosition(id, props),
      delete: id => this.helpers.api.deleteJobPosition(id)
    })
  }

  // ===================================
  // Mutations
  // ===================================
  @action.bound async create(props: IJobPositionForSaveVM) {
    return this.handleCreate(props)
  }

  @action.bound async edit(id: number, props: IJobPositionForSaveVM) {
    return this.handleEdit(id, props)
  }
}
