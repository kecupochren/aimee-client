/**
 * Data stores hold instances of models
 */

import { RootStore } from '../rootStore'

import { BusinessRolesRepository } from './businessRoles.repository'
import { DepartmentsRepository } from './departments.repository'
import { JobPositionsRepository } from './jobPositions.repository'
import { RecertificationsRepository } from './recertifications.repository'
import { RecertificationUnitsRepository } from './recertificationUnits.repository'
import { LabelsRepository } from './labels.repository'
import { LabelRestrictedPairsRepository } from './labelRestrictedPairs.repository'
import { OrganizationalUnitsRepository } from './organizationalUnits.repository'
import { SystemApplicationsRepository } from './systemApplications.repository'

export class RepositoriesStore {
  root: RootStore

  businessRoles: BusinessRolesRepository
  departments: DepartmentsRepository
  recertifications: RecertificationsRepository
  recertificationUnits: RecertificationUnitsRepository
  systemApplications: SystemApplicationsRepository
  jobPositions: JobPositionsRepository
  labels: LabelsRepository
  labelRestrictedPairs: LabelRestrictedPairsRepository
  organizationalUnits: OrganizationalUnitsRepository

  constructor(root: RootStore) {
    this.root = root

    this.businessRoles = new BusinessRolesRepository(root)
    this.departments = new DepartmentsRepository(root)
    this.recertifications = new RecertificationsRepository(root)
    this.recertificationUnits = new RecertificationUnitsRepository(root)
    this.systemApplications = new SystemApplicationsRepository(root)
    this.jobPositions = new JobPositionsRepository(root)
    this.labels = new LabelsRepository(root)
    this.labelRestrictedPairs = new LabelRestrictedPairsRepository(root)
    this.organizationalUnits = new OrganizationalUnitsRepository(root)
  }
}
