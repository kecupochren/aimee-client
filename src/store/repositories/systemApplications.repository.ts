import { Repository } from '../modules'
import { SystemApplicationModel, ISystemApplication } from '../models'

export class SystemApplicationsRepository extends Repository<
  number,
  ISystemApplication,
  SystemApplicationModel
> {
  // ===================================
  // Constructor
  // ===================================
  constructor(app: any) {
    super(app, {
      createModel: (data: ISystemApplication) => new SystemApplicationModel(this.helpers, data),
      hydrateModels: (_data: ISystemApplication, model: SystemApplicationModel) => {
        // TODO:

        // console.log(data, 'sss')
        return model
      },
      fetch: query => this.helpers.api.getSystemApplications(query)
      // fetchOne: id => this.helpers.api.getRecertification(id),
      // create: props => this.helpers.api.createRecertification(props),
      // edit: (id, props) => this.helpers.api.editRecertification(id, props)
    })
  }

  // ===================================
  // Mutations
  // ===================================
  // @action.bound async create(props: RecertificationCreateVM) {
  //   return this.handleCreate(props)
  // }

  // @action.bound async edit(id: number, props: RecertificationForSaveVM) {
  //   return this.handleEdit(id, props)
  // }
}
