import { action } from 'mobx'

import { ILabelRestrictedPair, ILabelRestrictedPairForSaveVM } from '~/api/models'

import { Repository } from '../modules'
import { LabelRestrictedPairModel } from '../models'

export class LabelRestrictedPairsRepository extends Repository<
  number,
  ILabelRestrictedPair,
  LabelRestrictedPairModel
> {
  // ===================================
  // Constructor
  // ===================================
  constructor(app: any) {
    super(app, {
      createModel: (data: ILabelRestrictedPair) => new LabelRestrictedPairModel(this.helpers, data),
      hydrateModels: (data: ILabelRestrictedPair, model: LabelRestrictedPairModel) => {
        const { firstLabelId, firstLabelName, secondLabelId, secondLabelName } = data

        if (firstLabelId) {
          const label = { id: firstLabelId, name: firstLabelName }
          model.firstLabel = this.repositories.labels.addItem(label, true)
        }

        if (secondLabelId) {
          const label = { id: secondLabelId, name: secondLabelName }
          model.secondLabel = this.repositories.labels.addItem(label, true)
        }

        return model
      },
      fetch: query => this.helpers.api.getLabelRestrictedPairs(query),
      fetchOne: id => this.helpers.api.getLabelRestrictedPair(id),
      create: props => this.helpers.api.createLabelRestrictedPair(props),
      edit: (id, props) => this.helpers.api.editLabelRestrictedPair(id, props),
      delete: id => this.helpers.api.deleteLabelRestrictedPair(id)
    })
  }

  // ===================================
  // Mutations
  // ===================================
  @action.bound async create(props: ILabelRestrictedPairForSaveVM) {
    return this.handleCreate(props)
  }

  @action.bound async edit(id: number, props: ILabelRestrictedPairForSaveVM) {
    return this.handleEdit(id, props)
  }
}
