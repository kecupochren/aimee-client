import { IBusinessRole } from '~/api/models'

import { Repository } from '../modules'
import { BusinessRoleModel } from '../models'

export class BusinessRolesRepository extends Repository<number, IBusinessRole, BusinessRoleModel> {
  // ===================================
  // Constructor
  // ===================================
  constructor(app: any) {
    super(app, {
      createModel: (data: IBusinessRole) => new BusinessRoleModel(this.helpers, data),
      fetch: query => this.helpers.api.getBusinessRoles(query)
    })
  }
}
