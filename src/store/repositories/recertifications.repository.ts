import { IRecertification, RecertificationModel } from './../models/Recertification.model'

import { Repository } from '../modules'
import { RecertificationCreateVM, RecertificationForSaveVM } from '~/api/apimodels/Recertifications'
import { action } from 'mobx'
import { messages } from '~/pages/RecertificationsForm/RecertificationsForm.messages'

export interface IRecertificationFilters {
  [key: string]: string[] | number[]
  Text: string[]
  DepartmentId: number[]
}

export class RecertificationsRepository extends Repository<
  number,
  IRecertification,
  RecertificationModel,
  IRecertificationFilters
> {
  // ===================================
  // Constructor
  // ===================================
  constructor(app: any) {
    super(app, {
      createModel: (data: IRecertification) => new RecertificationModel(this.helpers, data),
      hydrateModels: (data: IRecertification, model: RecertificationModel) => {
        if (data.systemApplications?.length) {
          const apps = data.systemApplications.map(x => ({ ...x, id: x.systemApplicationId }))
          const models = this.repositories.systemApplications.addItems(apps, true)
          model.systemApplications.replace(models)
        }

        return model
      },
      fetch: query => this.helpers.api.getRecertifications(query),
      fetchOne: id => this.helpers.api.getRecertification(id),
      create: props => this.helpers.api.createRecertification(props),
      edit: (id, props) => this.helpers.api.editRecertification(id.toString(), props)
    })
  }

  // ===================================
  // Mutations
  // ===================================
  @action.bound async create(props: RecertificationCreateVM) {
    return this.handleCreate(props)
  }

  @action.bound async edit(id: number, props: RecertificationForSaveVM) {
    return this.handleEdit(id, props)
  }

  @action.bound async open(id: string) {
    try {
      await this.helpers.api.openRecertification(id)
      const model = this.fetchOne(Number(id))

      this.helpers.notification.success({ message: messages.RecertificationOpened })

      return model
    } catch (error) {
      throw error
    }
  }

  @action.bound async finalize(id: string) {
    try {
      await this.helpers.api.finalizeRecertification(id)
      const model = this.fetchOne(Number(id))

      this.helpers.notification.success({ message: messages.RecertificationFinalized })

      return model
    } catch (error) {
      throw error
    }
  }
}
