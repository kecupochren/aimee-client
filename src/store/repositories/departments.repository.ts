import { IDepartment } from '~/api/models'

import { Repository } from '../modules'
import { DepartmentModel } from '../models'

export class DepartmentsRepository extends Repository<number, IDepartment, DepartmentModel> {
  // ===================================
  // Constructor
  // ===================================
  constructor(app: any) {
    super(app, {
      createModel: (data: IDepartment) => new DepartmentModel(this.helpers, data),
      fetch: query => this.helpers.api.getDepartments(query)
    })
  }
}
