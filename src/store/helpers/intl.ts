import { addLocaleData, IntlProvider, FormattedMessage } from 'react-intl'
import { action, observable, computed } from 'mobx'
import get from 'lodash/get'

import { TLocale, TLocaleShort } from '~/constants'

import { ApiHelper } from './api'

const DEFAULT_LOCALE = 'cs-CZ'

class Dictionaries {
  @observable cs = observable<string, string>(new Map())
  @observable en = observable<string, string>(new Map())
}

export class IntlHelper {
  // ===================================
  // Model
  // ===================================
  @observable locale: TLocale = DEFAULT_LOCALE
  @observable dictionaries: Dictionaries

  // ===================================
  // Constructor
  // ===================================
  constructor(private readonly apiHelper: ApiHelper) {
    this.dictionaries = new Dictionaries()
  }

  // ===================================
  // Views
  // ===================================
  // Return key in format react-intl expects, "cs-CZ" -> "cs"
  @computed get localeShortKey(): TLocaleShort {
    return this.locale.split('-')[0] as TLocaleShort
  }

  @computed get dictionary() {
    return this.dictionaries[this.localeShortKey].toJSON()
  }

  @computed get intlProvider() {
    const provider = new IntlProvider({
      locale: this.locale,
      messages: this.dictionary
    })

    return provider.getChildContext().intl
  }

  // ===================================
  // Actions
  // ===================================
  @action.bound async initialize(data: IAnyObject) {
    this.locale = get(data, 'language')

    await this.loadLocale(this.locale)
    await this.loadClientDictionary(this.locale)
    this.loadServerDictionary(JSON.parse(data.resources))
  }

  @action.bound async loadLocale(locale: TLocale) {
    if (locale !== 'en-US') {
      const localeData = await import(`react-intl/locale-data/${this.localeShortKey}`)
      addLocaleData(localeData.default)
    }
  }

  @action.bound async loadClientDictionary(locale: TLocale) {
    if (locale !== 'en-US') {
      const dictionary = await import(`../../translations/locales/${this.localeShortKey}.json`)
      this.mergeDictionary(dictionary.default)
    }
  }

  // Transform server provided dictionary to react-intl style and merges it to our default one.
  @action.bound loadServerDictionary(dictionary: IServerDictionary) {
    if (!dictionary) return

    // Flatten dictionary into single key/value pairs
    const dict = Object.entries(dictionary).reduce((map: IAnyObject, [section, phrases]) => {
      Object.entries(phrases).forEach(([phrase, translation]) => {
        // Transform interpolation style to use react-intl format
        // i.e. instead of ${name} on the API side we need {name}
        const translationFinal = translation.replace('${', '{')

        map[`${section}.${phrase}`] = translationFinal
      })

      return map
    }, {})

    this.mergeDictionary(dict)
  }

  @action.bound async setLocale(locale: TLocale) {
    await this.apiHelper.api.switchLanguage(locale)
    window.location.reload()
  }

  @action.bound formatMessage(
    message?: string | FormattedMessage.MessageDescriptor,
    values: IAnyObject = {}
  ) {
    if (!message) {
      return ''
    }

    if (typeof message === 'string') {
      return message
    }

    return this.intlProvider.formatMessage(message, values)
  }

  @action.bound mergeDictionary(dictionary: IAnyObject) {
    if (this.locale === 'cs-CZ') {
      this.dictionaries.cs.merge(dictionary)
    } else {
      this.dictionaries.en.merge(dictionary)
    }
  }
}

interface IServerDictionary {
  [section: string]: {
    [phrase: string]: string
  }
}
