import { computed } from 'mobx'

import { createApi, IApi } from '~/api'

import { EnvHelper } from './env'

export class ApiHelper {
  constructor(private readonly env: EnvHelper) {}

  @computed get api(): IApi {
    return createApi(this.env.config.apiURL)
  }
}
