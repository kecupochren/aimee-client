import { FormattedMessage } from 'react-intl'
import { action } from 'mobx'
import { notification } from 'antd'
import { ArgsProps as NotificationProps } from 'antd/es/notification'
import get from 'lodash/get'

import { theme } from '~/constants'

// notification.config({ placement: 'bottomRight', duration: 4000 })

import { IntlHelper } from './intl'

type TMessage = string | FormattedMessage.MessageDescriptor

export class NotificationHelper {
  // ===================================
  // Constructor
  // ===================================
  constructor(private readonly intl: IntlHelper) {}

  // ===================================
  // Helpers
  // ===================================
  @action.bound getMessages(props: INotificationProps) {
    return {
      message: this.intl.formatMessage(props.message, props.messageValues),
      description: this.intl.formatMessage(props.description, props.descriptionValues)
    }
  }

  // ===================================
  // Public
  // ===================================
  @action.bound info(messages: INotificationProps) {
    notification.info({
      ...this.getMessages(messages),
      style: { border: `2px solid ${theme.color.info}` }
    })
  }

  @action.bound warning(messages: INotificationProps) {
    notification.error({
      ...this.getMessages(messages),
      style: { border: `2px solid ${theme.color.warning}` }
    })
  }

  @action.bound success(messages: INotificationProps) {
    notification.success({
      ...this.getMessages(messages),
      style: { border: `2px solid ${theme.color.success}` }
    })
  }

  @action.bound error(error: INotificationProps) {
    notification.error({
      ...this.getMessages(error),
      style: { border: `2px solid ${theme.color.error}` }
    })
  }

  @action.bound handleError(error: Error) {
    if (process.env.NODE_ENV !== 'production') {
      console.log(error)
    }

    const message = get(error, 'response.data.message') || error.message

    // TODO: Logging...

    this.error({ message })
  }
}

// Extend default interface to support react-intl style messages
interface INotificationProps extends NotificationProps {
  message: TMessage
  messageValues?: IAnyObject
  description?: TMessage
  descriptionValues?: IAnyObject
}
