import { action } from 'mobx'
import get from 'lodash/get'

import { IntlHelper } from './intl'
import { UserHelper } from './user'
import { EnvHelper } from './env'

/**
 * Handles server rendered data exposed on the window object
 */
export class ServerDataHelper {
  // ===================================
  // Constructor
  // ===================================
  constructor(
    private readonly intl: IntlHelper,
    private readonly user: UserHelper,
    private readonly env: EnvHelper
  ) {}

  // ===================================
  // Actions
  // ===================================
  @action.bound load() {
    const serverData = get(window, 'serverData')

    if (serverData) {
      const data = JSON.parse(serverData)

      this.env.initialize(data)
      this.user.initialize(data)
      this.intl.initialize(data)
    }
  }
}

export type IServerData = Permissions
