import { action, observable, computed } from 'mobx'
import get from 'lodash/get'

import { TEnv, ENVIRONMENTS, ENVIRONMENT_KEYS, IEnvironment } from '~/constants'

export class EnvHelper {
  // ===================================
  // Model
  // ===================================
  @observable env: TEnv = 'server'

  @observable serverApiURL: string
  @observable serverEnvName: string

  // ===================================
  // Views
  // ===================================
  @computed get config(): IEnvironment {
    if (this.env === 'server') {
      return {
        apiURL: this.serverApiURL,
        name: this.serverEnvName
      }
    }

    return ENVIRONMENTS[this.env]
  }

  // ===================================
  // Actions
  // ===================================
  @action.bound initialize(serverData: IAnyObject) {
    this.setServerData(serverData)
    this.load()
  }

  @action.bound setServerData(data: IAnyObject) {
    const webApiUrl = get(data, 'webApiUrl')
    if (webApiUrl) {
      this.serverApiURL = webApiUrl
    }

    const envName = get(data, 'environmentName')
    if (envName) {
      this.serverEnvName = envName
    }
  }

  @action.bound load() {
    const env = window.localStorage.getItem('env') as any
    this.env = ENVIRONMENT_KEYS.includes(env) ? env : 'server'
  }

  @action.bound setEnv(env: TEnv) {
    window.localStorage.setItem('env', env)
    window.location.reload()
  }

  // ===================================
  // Private
  // ===================================
}
