import { RouterStore } from 'mobx-react-router'
import { action, observable, set, computed } from 'mobx'
import get from 'lodash/get'

import { PERMISSIONS } from '~/constants'

import { UserModel } from '../models'

import { ApiHelper } from './api'

export class UserHelper {
  // ===================================
  // Model
  // ===================================
  @observable profile?: UserModel
  @observable permissions: Permissions
  @observable isImpersonated = false

  // ===================================
  // Constructor
  // ===================================
  constructor(private readonly apiHelper: ApiHelper, private readonly router: RouterStore) {}

  // ===================================
  // Views
  // ===================================
  @computed get name() {
    if (!this.profile) {
      return 'User'
    }

    if (this.profile.userDisplayAs) {
      return this.profile.userDisplayAs
    }

    return this.profile.userName
  }

  hasPermission(permission?: PERMISSIONS): boolean {
    if (!permission) return true

    if (this.permissions) {
      // @ts-ignore
      return this.permissions[permission]
    }

    return false
  }

  // ===================================
  // Actions
  // ===================================
  @action.bound async initialize(data: any) {
    await this.checkImpersonation(data)

    this.profile = new UserModel(data)
    this.permissions = new Permissions(data)
  }

  @action.bound async checkImpersonation(data: any) {
    const isImpersonated = get(data, 'isImpersonated')
    const impersonatedUserId = get(data, 'impersonatedUserId')

    if (isImpersonated && impersonatedUserId) {
      this.isImpersonated = true
      await this.apiHelper.api.impersonate(impersonatedUserId)
    }
  }

  @action.bound async cancelImpersonation() {
    await this.apiHelper.api.unimpersonate()
    window.location.href = `/Impersonate/Unset/${window.btoa(this.router.location.pathname)}`
  }
}

export class Permissions {
  // ===================================
  // Model
  // ===================================
  @observable canCreateDepartment = false
  @observable canCreateJobPosition = false
  @observable canCreateLabel = false
  @observable canCreateLabelRestrictedPair = false
  @observable canCreateOrganizationalUnit = false
  @observable canCreateSystemSecurityFlag = false
  @observable canFullExportReports = false
  @observable canReadBusinessRole = false
  @observable canReadCompany = false
  @observable canReadDataSet = false
  @observable canReadDepartment = false
  @observable canReadEmployee = false
  @observable canReadEmployeeAll = false
  @observable canReadIdentityOrganizationalUnitReport = false
  @observable canReadJobPosition = false
  @observable canReadLabel = false
  @observable canReadLabelRestrictedPair = false
  @observable canReadOrganizationalUnit = false
  @observable canReadPreapprovedSodCompensation = false
  @observable canReadReportApplicationsWithRoleSecurityFlags = false
  @observable canReadReportEmployeeSodConflicts = false
  @observable canReadReportIdentityLabels = false
  @observable canReadReports = false
  @observable canReadSystemApplication = false
  @observable canReadSystemRole = false
  @observable canReadSystemSecurityFlag = false
  @observable canReadWorkflowSummary = false
  @observable canCreateRecertification = false
  @observable canReadRecertification = false
  @observable canReadRecertificationUnit = false

  // ===================================
  // Constructor
  // ===================================
  constructor(data: any) {
    set(this, get(data, 'authorization'))
  }
}
