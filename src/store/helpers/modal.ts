import { Modal } from 'antd'
import { ModalFuncProps } from 'antd/es/modal'
import { action, observable } from 'mobx'

export class ModalHelper {
  // ===================================
  // Model
  // ===================================
  // Array lets us have layered modals
  @observable openIDs: string[] = []

  // ===================================
  // Public
  // ===================================
  @action.bound open(id: string) {
    this.openIDs.push(id)
  }

  @action.bound info(props: ModalFuncProps) {
    Modal.info(props)
  }

  @action.bound error(props: ModalFuncProps) {
    Modal.error(props)
  }

  @action.bound confirm(props: ModalFuncProps) {
    Modal.confirm(props)
  }
}
