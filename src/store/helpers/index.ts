import { computed } from 'mobx'
import { RouterStore } from 'mobx-react-router'

import { EnvHelper } from './env'
import { IntlHelper } from './intl'
import { ModalHelper } from './modal'
import { NotificationHelper } from './notification'
import { ServerDataHelper } from './serverData'
import { UserHelper } from './user'
import { ApiHelper } from './api'

export class HelpersStore {
  _api: ApiHelper
  env: EnvHelper
  intl: IntlHelper
  modal: ModalHelper
  notification: NotificationHelper
  serverData: ServerDataHelper
  user: UserHelper
  router: RouterStore

  constructor(router: RouterStore) {
    this.env = new EnvHelper()
    this.router = router
    this._api = new ApiHelper(this.env)
    this.intl = new IntlHelper(this._api)
    this.modal = new ModalHelper()
    this.notification = new NotificationHelper(this.intl)
    this.user = new UserHelper(this._api, this.router)
    this.serverData = new ServerDataHelper(this.intl, this.user, this.env)
  }

  @computed get api() {
    return this._api.api
  }
}

export type THelpersStore = typeof HelpersStore
