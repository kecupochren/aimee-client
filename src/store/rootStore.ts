// Root Store

import React, { useContext } from 'react'
import { action, observable, computed } from 'mobx'
import { createBrowserHistory } from 'history'
import { RouterStore as ReactRouterStore, syncHistoryWithStore } from 'mobx-react-router'

import { RepositoriesStore } from './repositories'
import { HelpersStore } from './helpers'

export class RootStore {
  // ===================================
  // Model
  // ===================================
  @observable initialized = false

  repositories = new RepositoriesStore(this)
  helpers: HelpersStore

  constructor(RouterStore: ReactRouterStore) {
    this.helpers = new HelpersStore(RouterStore)
  }

  // ===================================
  // Views
  // ===================================
  @computed get config() {
    return this.helpers.env.config
  }

  // ===================================
  // Actions
  // ===================================
  @action.bound async initialize() {
    this.helpers.serverData.load()
    setTimeout(() => this.setInitialized(true), 50) // await loading server dictionary
  }

  @action.bound async setInitialized(initialized: boolean) {
    this.initialized = initialized
  }
}

export let rootStore: RootStore

export const createRootStore = () => {
  // Create RouterStore with synced history
  const browserHistory = createBrowserHistory()
  const RouterStore = new ReactRouterStore()
  const history = syncHistoryWithStore(browserHistory, RouterStore)

  rootStore = new RootStore(RouterStore)

  return { store: rootStore, history }
}

export const useRootStore = (): RootStore => useContext(React.createContext(rootStore))
