import { action, computed, observable } from 'mobx'

import { uuid } from '~/utils'

import { Collection } from '../Collection'

export class Selection<TItem = string> {
  // ===================================
  // Model
  // ===================================
  readonly selected = observable<string, TItem>(new Map())

  // ===================================
  // Constructor
  // ===================================
  constructor(
    private readonly collection?: Collection<any, any, any, any>,
    readonly opts?: { disabled: () => boolean; disabledMessage: string }
  ) {}

  // ===================================
  // Views
  // ===================================
  @computed get ids() {
    return Array.from(this.selected.keys())
  }

  @computed get numIDs() {
    return this.ids.map(x => Number(x))
  }

  @computed get items() {
    if (!this.selected.size) return []
    return [...this.selected.values()]
  }

  @computed get count() {
    return this.ids.length
  }

  @computed get hasSelected() {
    return this.count > 0
  }

  @computed get hasMultipleSelected() {
    return this.count > 1
  }

  @computed get props() {
    return {
      selectedRowKeys: this.ids,
      onChange: (_ids: any[], items: TItem[]) => this.set(items)
    }
  }

  // ===================================
  // Actions
  // ===================================
  get(id: number | string) {
    return this.selected.get(id.toString())
  }

  isID(item: TItem | number | string) {
    return typeof item === 'string' || typeof item === 'number'
  }

  getID(item: TItem | number | string) {
    if (typeof item === 'string' || typeof item === 'number') {
      return item.toString()
    }

    if ((item as any).id) {
      return (item as any).id.toString()
    }

    return uuid()
  }

  @action.bound add(item: TItem | number | string) {
    const id = this.getID(item)

    if (this.selected.has(id)) {
      this.selected.delete(id)
    } else {
      this.selected.set(id, id)
    }
  }

  @action.bound addItem(item: TItem) {
    const id = this.getID(item)

    if (this.selected.has(id)) {
      this.selected.delete(id)
    } else {
      this.selected.set(id, item)
    }
  }

  @action.bound remove(item: TItem | number | string) {
    const id = this.getID(item)
    this.selected.delete(id)
  }

  @action.bound set(items: TItem[] | number[] | string[]) {
    this.selected.clear()

    items.forEach((item: TItem | number | string) => {
      const id = this.getID(item)
      const itemFinal = this.isID(item) ? (item as any).toString() : item

      this.selected.set(id, itemFinal)
    })
  }

  @action.bound selectOne(item: TItem) {
    const id = this.getID(item)

    this.selected.clear()
    this.selected.set(id, item)
  }

  @action.bound selectAll() {
    if (!this.collection) return

    if (this.hasSelected) {
      this.selected.clear()
    } else {
      // TODO: Generic
      this.collection.results.forEach(item => this.selected.set(item.id, item.id))
    }
  }

  @action.bound clear() {
    this.selected.clear()
  }
}
