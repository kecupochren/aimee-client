import { observable, computed, action } from 'mobx'
import { PaginationConfig, SorterResult } from 'antd/lib/table'
import { Location } from 'history'
import qs from 'query-string'
import omit from 'lodash/omit'

export type TDirection = SorterResult<any>['order']

export type TAPIDirection = 'Ascending' | 'Descending' | undefined

export const PAGINATION_PROPS = ['orderBy', 'direction', 'page', 'pageSize']

export interface IAPIPagination {
  OrderByColumn?: string
  OrderByDirection?: TAPIDirection
  PageNumber?: number
  PageSize?: number
}

export interface IPaginationProps {
  orderBy?: string
  direction?: SorterResult<any>['order']
  page?: number
  pageSize?: number
  total?: number
}

export class Pagination implements IPaginationProps {
  // ===================================
  // Model
  // ===================================
  @observable orderBy?: string
  @observable direction?: TDirection
  @observable page = 1
  @observable pageSize = 30

  @observable total = 0

  // ===================================
  // Constructor
  // ===================================
  constructor(orderBy?: string, direction?: TDirection, pageSize = 30) {
    this.orderBy = orderBy
    this.direction = direction
    this.pageSize = pageSize
  }

  // ===================================
  // Views
  // ===================================
  @computed get props() {
    return {
      orderBy: this.orderBy,
      direction: this.direction,
      page: this.page,
      pageSize: this.pageSize,
      total: this.total
    }
  }

  @computed get asURLParams() {
    return omit(this.props, 'total')
  }

  @computed get asParams(): IAPIPagination {
    let direction: TAPIDirection

    if (this.direction === 'ascend') {
      direction = 'Ascending'
    }

    if (this.direction === 'descend') {
      direction = 'Descending'
    }

    return {
      OrderByColumn: this.orderBy,
      OrderByDirection: direction,
      PageNumber: this.page,
      PageSize: this.pageSize
    }
  }

  @computed get tablePaginationProps(): PaginationConfig {
    return {
      current: this.page,
      total: this.total,
      pageSize: this.pageSize
    }
  }

  @computed get offset() {
    return this.pageSize * (this.page - 1)
  }

  @computed get cursors() {
    return [this.offset, this.offset + this.pageSize]
  }

  // ===================================
  // Actions
  // ===================================
  @action.bound setOrderBy(orderBy?: string) {
    const changed = this.orderBy !== orderBy
    this.orderBy = orderBy
    return changed
  }

  @action.bound setDirection(direction: TDirection) {
    const changed = this.direction !== direction
    this.direction = direction
    return changed
  }

  @action.bound setSorting(sorter: SorterResult<any>) {
    const orderBy = sorter.order ? sorter.field : undefined

    const hasOrderChanged = this.setOrderBy(orderBy)
    const hasDirectionChanged = this.setDirection(sorter.order)

    return hasOrderChanged || hasDirectionChanged
  }

  @action.bound setPage(page: number = 1) {
    const changed = this.page !== page
    this.page = page
    return changed
  }

  @action.bound setTotal(total: number) {
    this.total = total
  }

  @action.bound setFromLocation(location: Location) {
    const params: any = qs.parse(location.search)

    this.orderBy = params.orderBy || this.orderBy
    this.direction = params.direction || this.direction
    this.page = Number(params.page) || this.page
    this.pageSize = Number(params.pageSize) || this.pageSize
  }

  @action.bound setFromTable(pagination: PaginationConfig, sorter: SorterResult<any>) {
    const hasOrderChanged = this.setOrderBy(sorter.field)
    const hasDirectionChanged = this.setDirection(sorter.order)
    const hasSortingChanged = hasOrderChanged || hasDirectionChanged

    return {
      hasPageChanged: this.setPage(pagination.current || 1),
      hasOrderChanged,
      hasDirectionChanged,
      hasSortingChanged
    }
  }

  @action.bound getPrev() {
    this.page = this.page - 1 || 1
  }

  @action.bound getNext() {
    this.page++
  }

  @action.bound incrementTotal() {
    this.total++
  }
}
