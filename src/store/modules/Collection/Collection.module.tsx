/**
 * Collection manages some paginated data. It holds singletons of MobX models
 */

import axios, { CancelTokenSource } from 'axios'
import { observable, computed, action, ObservableMap } from 'mobx'
import { PaginationConfig, SorterResult } from 'antd/es/table'
import { animateScroll } from 'react-scroll'
import qs from 'query-string'
import { OptionProps } from 'antd/es/select'
import { Location } from 'history'

import { BaseStore } from '../BaseStore'
import { Filters } from '../Filters'
import { Pagination, TDirection } from '../Pagination'
import { IRepositoryModelData, IRepositoryModel, IFetchParams } from '../Repository'
import { Selection } from '../Selection'
import { ITableProps } from '~/components'

type ID = string | number

export interface ICollectionFetchParams<IFilters> extends IFetchParams<IFilters> {
  initialized: boolean
  filtersChanged: boolean
  sortingChanged: boolean
  pageChanged: boolean
}

export interface ICollectionOptions<
  IDType extends ID,
  IItem extends IRepositoryModelData<IDType>,
  _Item extends IRepositoryModel<IDType, IItem>,
  IFilters extends IAnyObject = {}
> {
  // Should sync state to URL
  syncURL?: boolean

  // Override default order column
  orderBy?: string

  // Override default order direction
  direction?: TDirection

  // Override default page size
  pageSize?: number

  // Default filters params
  filters?: IFilters | undefined

  // Do not clear selected values when searching
  preserveSelected?: boolean

  // Exclude selected items from results
  excludeSelected?: boolean

  // Render only selected items
  onlySelected?: boolean
}

export class Collection<
  IDType extends ID,
  IItem extends IRepositoryModelData<IDType>,
  Item extends IRepositoryModel<IDType, IItem>,
  IFilters extends IAnyObject = {}
> extends BaseStore {
  // ===================================
  // Model
  // ===================================
  readonly data = observable<Item>([])

  readonly pagination: Pagination
  readonly selection: Selection<Item>
  readonly filters: Filters<IFilters>

  @observable initialized = false

  @observable loading = true
  @observable loaded = false

  @observable searching = false
  @observable searched = false

  @observable onlySelected = false

  @observable filtersChanged = false
  @observable sortingChanged = false
  @observable pageChanged = false

  private cancelToken: CancelTokenSource

  constructor(
    app: any,
    readonly fetchData?: (
      params: ICollectionFetchParams<IFilters>
    ) => Promise<{ data: Item[]; total: number } | void>,
    readonly options: ICollectionOptions<IDType, IItem, Item, IFilters> = {}
  ) {
    super(app)

    const { onlySelected = false, filters = {}, orderBy, direction, pageSize = 30 } = options

    this.pagination = new Pagination(orderBy, direction, pageSize)
    this.filters = new Filters<IFilters>(filters as IFilters)
    this.selection = new Selection(this)

    this.onlySelected = onlySelected

    this.cancelToken = axios.CancelToken.source()
  }

  // ===================================
  // Views
  // ===================================
  @computed get results() {
    if (this.options.excludeSelected) {
      return this.data.filter(x => !this.selection.get(x.id))
    }

    if (this.onlySelected) {
      return this.data.filter(x => this.selection.get(x.id))
    }

    return this.data
  }

  @computed get map() {
    return this.results.reduce((map, item) => {
      map.set(item.id, item)
      return map
    }, new ObservableMap<IDType, Item>())
  }

  @computed get count() {
    return this.results.length
  }

  @computed get hasData() {
    return this.count > 0
  }

  @computed get hasMore() {
    return this.count < this.pagination.total
  }

  @computed get isFirstLoad() {
    return this.loading && !this.loaded
  }

  @computed get stateAsString() {
    const state = {
      ...this.pagination.asURLParams,
      ...this.filters.asObject
    }

    return qs.stringify(state, { sort: false })
  }

  @computed get selected(): Item[] {
    return this.selection.items
  }

  @computed get validToDelete() {
    return this.selected.filter(x => x && x.canBeDeleted)
  }

  @computed get asSelectOptions(): OptionProps[] {
    return this.results.map(x => {
      const title = (x as any).name || x.id

      return {
        title,
        value: x.id
      }
    })
  }

  @computed get fetchParams(): ICollectionFetchParams<IFilters> {
    return {
      cancelToken: this.cancelToken,
      pagination: this.pagination,
      filters: this.filters,
      initialized: this.initialized,
      filtersChanged: this.filtersChanged,
      sortingChanged: this.sortingChanged,
      pageChanged: this.pageChanged
    }
  }

  @computed get tableProps(): ITableProps<Item> {
    return {
      dataSource: this.results,
      onChange: this.handleTableChange,
      pagination: this.pagination.props,
      rowSelection: this.selection.props,
      selection: this.selection,
      loading: this.loading || this.searching
    }
  }

  // ===================================
  // Helpers
  // ===================================
  @action.bound _syncURL() {
    if (this.options.syncURL) {
      window.history.replaceState('', '', `${window.location.pathname}?${this.stateAsString}`)
    }
  }

  // ===================================
  // Actions
  // ===================================

  // Initializes state from location (if defined) and performs fetch
  @action.bound async init(location?: Location) {
    if (location) {
      this.pagination.setFromLocation(location)
      this.filters.setFromLocation(location)
    }

    await this.handleFetch()
  }

  // Performs fetch using collection state
  @action.bound async handleFetch() {
    if (!this.fetchData) {
      return
    }

    this.loading = true

    try {
      const res = await this.fetchData(this.fetchParams)

      if (res) {
        this.pagination.setTotal(res.total)
        this.data.replace(res.data)
      }

      this._syncURL()
      this.loaded = true
    } catch (error) {
      this.helpers.notification.handleError(error)
    } finally {
      this.loading = false
      this.searching = false
    }
  }

  // Performs fetch with provided filters
  @action.bound async fetchWithFilters(filters: IFilters) {
    this.filters.merge(filters)
    this.handleFetch()
  }

  // Performs search, which is just fetch with Text param
  @action.bound async search(query: string) {
    this.searching = true

    if (query) this.filters.set('Text', [query])
    else this.filters.delete('Text')

    if (!this.options.preserveSelected) {
      this.selection.clear()
    }

    return this.handleFetch()
  }

  // Adds idem
  @action.bound addItem(item: any, isPartial = false) {
    const oldItem = this.data.find(x => x.id === item.id)

    if (oldItem) {
      oldItem.setData(item, isPartial)
    } else {
      this.data.push(item)
    }
  }

  // Handles Table state change
  @action.bound handleTableChange(
    pagination: PaginationConfig,
    filters: Record<string, string[]>,
    sorter: SorterResult<Item>
  ) {
    this.loading = true

    animateScroll.scrollTo(0, { duration: 200 })

    this.sortingChanged = this.pagination.setSorting(sorter)
    this.pageChanged = this.pagination.setPage(pagination.current)
    this.filtersChanged = this.filters.setFromTable(filters)

    if (this.filtersChanged) {
      this.selection.clear()
    }

    this._syncURL()
    this.handleFetch()
  }

  @action.bound clear() {
    this.initialized = false
    this.loading = true
    this.loaded = false

    this.data.clear()
    this.filters.clear()
    this.selection.clear()
  }
}
