/**
 * Base repository store that abstracts away fluff around:
 *   - hodling single MobX model instances across the app
 *   - fetching/searching with Pagination module
 */

import { observable, action, computed, runInAction, ObservableMap } from 'mobx'
import { CancelTokenSource } from 'axios'
import qs from 'query-string'
import get from 'lodash/get'

import { IList, IResponse } from '~/api'
import { messages } from '~/messages'
import { DuplicateError, NotFoundError, NotImplementedError } from '~/utils'

import { BaseStore } from '../BaseStore'
import { Pagination } from '../Pagination'
import { Filters } from '../Filters'

// Specific domain repository store props
export interface IRepositoryProps<
  IDType extends ID,
  IModel extends IRepositoryModelData<IDType>,
  Model extends IRepositoryModel<IDType, IModel>
> {
  createModel: (data: IModel, isPartial?: boolean) => Model
  hydrateModels?: (data: IModel, model: Model) => Model
  fetch: (query: string) => IResponse<IList<IRepositoryModelData<IDType>>>
  fetchOne?: (id: IDType) => IResponse<IRepositoryModelData<IDType>>
  create?: (props: any) => IResponse<IRepositoryModelData<IDType>>
  edit?: (id: IDType, props: any) => IResponse<IRepositoryModelData<IDType>>
  delete?: (id: IDType) => IResponse<boolean>
}

// Shape of the params for the fetch method this repository exposes.
// Usually called by Collection module which passes them in.
export interface IFetchParams<IFilters> {
  // Calling cancelToken.cancel() aborts running request
  cancelToken?: CancelTokenSource
  pagination: Pagination
  filters?: Filters<IFilters>
}

export interface IRepositoryModelData<IDType> {
  id: IDType
}

export interface IRepositoryModel<IDType, IModel> {
  // All models stored inside repository must have an ID
  id: IDType

  // Method for updating data (outside of constructor), so instances can be updated
  setData: (data: IModel, isPartial: boolean) => void

  // i18n name, for notifications and such
  entityName: string

  // For this specific project we also need to know whether entities can be edited/deleted
  canBeEdited?: boolean
  canBeDeleted?: boolean
}

type ID = string | number

export abstract class Repository<
  // ID type
  IDType extends ID,
  // API response data shape
  IModel extends IRepositoryModelData<IDType>,
  // MobX model shape
  Model extends IRepositoryModel<IDType, IModel>,
  // API filters shape
  IFilters extends IAnyObject = {}
> extends BaseStore {
  // ===================================
  // Model
  // ===================================
  // Source of truth for all API data
  @observable data: ObservableMap<IDType, Model> = new ObservableMap()

  // ===================================
  // Constructor
  // ===================================
  constructor(app: any, private readonly props: IRepositoryProps<IDType, IModel, Model>) {
    super(app)
  }

  // ===================================
  // Views
  // ===================================
  get(id: IDType) {
    return this.data.get(id) || this.data.get(Number(id) as any)
  }

  @computed get all() {
    return Array.from(this.data.values())
  }

  // ===================================
  // Helpers
  // ===================================
  /**
   * Hydrates sibling models
   */
  @action.bound hydrateModels(data: IModel, model: Model) {
    if (this.props.hydrateModels) {
      this.props.hydrateModels(data, model)
    }

    return model
  }

  /**
   * Creates or updates MobX model instance, while also hydrating sibling models
   */
  @action.bound addItem(data: IRepositoryModelData<IDType>, isPartial = false) {
    const oldModel = this.data.get(data.id)
    if (oldModel) {
      oldModel.setData(data as IModel, isPartial)
      const hydratedOldModel = this.hydrateModels(data as IModel, oldModel)
      return hydratedOldModel
    }

    const model = this.props.createModel(data as IModel, isPartial)
    const hydratedModel = this.hydrateModels(data as IModel, model)
    this.data.set(model.id, hydratedModel)
    return hydratedModel
  }

  /**
   * Creates or updates MobX model instances
   */
  @action.bound addItems(data: IRepositoryModelData<IDType>[] = [], isPartial = false) {
    return data.map(x => this.addItem(x, isPartial))
  }

  /**
   * Build query string for the API request
   */
  @action.bound buildRequestQuery(fetchParams?: IFetchParams<IFilters>) {
    let params = {}

    if (fetchParams) {
      const { pagination, filters } = fetchParams

      if (pagination) {
        params = pagination.asParams
      }

      if (filters) {
        params = {
          ...params,
          ...filters.asParams
        }
      }
    }

    return qs.stringify(params, { sort: false })
  }

  // ===================================
  // Queries
  // ===================================
  @action.bound async fetch(params?: IFetchParams<IFilters>) {
    try {
      const res = await this.props.fetch(this.buildRequestQuery(params))

      return runInAction(() => {
        const { data, total } = res.data

        return {
          data: this.addItems(data),
          total
        }
      })
    } catch (error) {
      throw error
    }
  }

  @action.bound async fetchOne(id: IDType) {
    if (!this.props.fetchOne) {
      throw new NotImplementedError()
    }

    try {
      const res = await this.props.fetchOne(id)

      return runInAction(() => {
        const entity = res.data

        if (entity) {
          const [model] = this.addItems([entity])
          return model
        }

        throw new NotFoundError()
      })
    } catch (error) {
      throw error
    }
  }

  // ===================================
  // Mutations
  // ===================================
  @action.bound async handleCreate(props: IAnyObject) {
    if (!this.props.create) {
      throw new NotImplementedError()
    }

    try {
      const res = await this.props.create(props)

      return runInAction(() => {
        const model = this.addItem(res.data)
        this.helpers.notification.success({ message: messages.CreateSuccess })
        return model
      })
    } catch (error) {
      if (isDuplicateError(error)) {
        throw new DuplicateError()
      }

      throw error
    }
  }

  @action.bound async handleEdit(id: IDType, props: IAnyObject) {
    if (!this.props.edit) {
      throw new NotImplementedError()
    }

    try {
      const res = await this.props.edit(id, props)

      return runInAction(() => {
        const model = this.addItem(res.data)
        this.helpers.notification.success({ message: messages.EditSuccess })
        return model
      })
    } catch (error) {
      if (isDuplicateError(error)) {
        throw new DuplicateError()
      }

      throw error
    }
  }

  @action.bound async handleDelete(item: Model) {
    if (!this.props.delete) {
      throw new NotImplementedError()
    }

    try {
      await this.props.delete(item.id)
    } catch (error) {
      throw error
    }

    runInAction(() => {
      this.data.delete(item.id)
    })
  }
}

const isDuplicateError = (error: any) => {
  const status = get(error, 'response.status', 0)
  const message = get(error, 'response.data.message', '')

  return status === 422 && message.includes('duplicate')
}
