import { action, runInAction, reaction, computed } from 'mobx'

import { messages } from '~/messages'

import { IRepositoryModelData, IRepositoryModel, Repository } from '../Repository'
import { Collection, ICollectionOptions } from '../Collection'

export class RepositoryCollection<
  IDType extends string | number,
  IModel extends IRepositoryModelData<IDType>,
  Model extends IRepositoryModel<IDType, IModel>,
  IFilters extends IAnyObject = {}
> extends Collection<IDType, IModel, Model, IFilters> {
  // ===================================
  // Constructor
  // ===================================
  constructor(
    props: any,
    readonly repository: Repository<IDType, IModel, Model, IFilters>,
    readonly options: ICollectionOptions<IDType, IModel, Model, IFilters> = {}
  ) {
    // Iniitalize Collection using fetch method from the repoistory
    super(props, repository.fetch, options)

    // Trigger fetch anytime repository data count changes, e.g. new items created/deleted
    reaction(
      () => this.repository.data.size,
      () => (this.initialized = false)
    )
  }

  // ===================================
  // Views
  // ===================================
  @computed get selected(): Model[] {
    return this.selection.ids.map(id => this.repository.get(id as IDType)!)
  }

  // ===================================
  // Mutations
  // ===================================
  @action.bound handleDelete(item: Model) {
    const { modal, intl } = this.helpers

    modal.confirm({
      title: intl.formatMessage(messages.ConfirmDelete),
      content: intl.formatMessage(messages.ConfirmDeleteRecord),
      onOk: () => this.confirmDelete(item)
    })
  }

  async confirmDelete(item: Model) {
    this.loading = true

    // Delete from repository store, which also makes delete API call
    await this.repository.handleDelete(item)

    runInAction(() => {
      // Delete from this collection
      this.data.replace(this.data.filter(x => x.id !== item.id))
      this.pagination.total--

      // Go page back if deleted as last item on this page
      if (this.results.length === 0) {
        this.pagination.getPrev()
        this.handleFetch()
      } else {
        this.loading = false
      }

      this.helpers.notification.success({
        message: messages.DeleteSuccess,
        messageValues: { entity: item.entityName },
        duration: 0
      })
    })
  }

  @action.bound handleBulkDelete() {
    const { modal, intl } = this.helpers
    const { formatMessage } = intl

    const validCount = this.validToDelete.length
    const invalidCount = this.selected.length - this.validToDelete.length

    if (validCount === 0) {
      return modal.error({
        title: formatMessage(messages.CannotDelete),
        content: formatMessage(messages.CannotDeleteSelectedRecords)
      })
    }

    let message = formatMessage(messages.ConfirmDeleteRecord)

    if (validCount > 1) {
      message = formatMessage(messages.ConfirmDeleteRecords, { count: validCount })
    }

    if (invalidCount > 0) {
      const confirmMessage = formatMessage(messages.ConfirmDeleteRecords, { count: validCount })
      const invalidMessage = formatMessage(messages.RecordsCannotBeDeleted, { count: invalidCount })

      message = `${invalidMessage} ${confirmMessage}`
    }

    modal.confirm({
      title: intl.formatMessage(messages.ConfirmDelete),
      content: message,
      okText: intl.formatMessage(messages.Confirm),
      cancelText: intl.formatMessage(messages.Cancel),
      onOk: this.confirmBulkDelete
    })
  }

  @action.bound async confirmBulkDelete() {
    await Promise.all(this.validToDelete.map(model => this.confirmDelete(model!)))
    this.selection.clear()
  }
}
