import { observable, ObservableMap, computed, action, keys, isObservableArray } from 'mobx'
import qs from 'query-string'
import { Location } from 'history'
import omit from 'lodash/omit'
import isEqual from 'lodash/isEqual'

import { toObject } from '~/utils'

import { PAGINATION_PROPS } from '../Pagination'
import { ColumnFilterItem } from 'antd/lib/table'

export class Filters<IFilters extends Record<string, any>> {
  // ===================================
  // Model
  // ===================================
  readonly state = observable<string, any>(new Map())
  readonly defaults = observable<string, any>(new Map())
  readonly options = observable<string, ObservableMap<string, string>>(new Map())

  // ===================================
  // Constructor
  // ===================================
  constructor(defaults?: IFilters) {
    if (defaults) {
      this.defaults.replace(defaults)
      this.state.replace(defaults)
    }
  }

  // ===================================
  // Views
  // ===================================
  @computed get empty() {
    return this.state.size === 0
  }

  @computed get asObject(): IFilters {
    return toObject(this.state)
  }

  @computed get asStaticObject() {
    return Object.entries(this.asObject).reduce((map: Record<string, any>, [key, value]) => {
      map[key] = isObservableArray(value) ? (value as any).toJS() : value
      return map
    }, {})
  }

  @computed get asParams() {
    return Object.entries(this.asObject).reduce((map: Record<string, any>, [key, value]) => {
      const filterKey = `Filter.${key}`

      if (Array.isArray(value) && typeof value[0] !== 'undefined') {
        map[filterKey] = value[0]
      } else if (typeof value !== 'undefined') {
        map[filterKey] = value
      }

      return map
    }, {})
  }

  @computed get optionsForTable(): Record<string, ColumnFilterItem[]> {
    return keys(this.options).reduce((result: Record<string, ColumnFilterItem[]>, property) => {
      const options = this.options.get(property)

      if (options) {
        const items: ColumnFilterItem[] = []

        for (const option of options.keys()) {
          items.push({ text: option, value: option })
        }

        result[property] = items
      }

      return result
    }, {})
  }

  // ===================================
  // Actions
  // ===================================
  @action.bound set(property: string, value: any) {
    return this.state.set(property, value)
  }

  @action.bound setDefault(property: string, value: any) {
    this.state.set(property, value)
    this.defaults.set(property, value)
  }

  @action.bound delete(property: string) {
    this.state.delete(property)
  }

  @action.bound clear() {
    this.state.clear()
    this.state.replace(this.defaults)
  }

  @action.bound merge(filters: Partial<IFilters>) {
    this.state.merge(filters)
  }

  @action.bound replace(filters: Partial<IFilters> | Record<string, string[]>) {
    this.clear()

    Object.entries(filters).forEach(([prop, selected]) => {
      if (selected && selected.length) {
        this.state.set(prop, selected)
      }
    })
  }

  @action.bound setFromLocation(location: Location) {
    const params = qs.parse(location.search)
    const validParams: any = omit(params, PAGINATION_PROPS)
    this.merge(validParams)
  }

  @action.bound setFromTable(filters: Record<string, string[]>) {
    const changed = this.hasChangedFrom(filters)
    this.replace(filters)
    return changed
  }

  @action.bound hasChangedFrom(filters: Record<string, string[]>) {
    if (Object.keys(filters).length === 0) return false

    const nonEmptyFilters = Object.entries(filters).reduce(
      (map: Record<string, string[]>, [key, value]) => {
        if (Array.isArray(value) && value.length) {
          map[key] = value
        }

        return map
      },
      {}
    )

    return !isEqual(nonEmptyFilters, this.asStaticObject)
  }

  @action.bound addOption(property: string, option: string) {
    const propOptions = this.options.get(property)

    if (propOptions) {
      propOptions.set(option, option)
    } else {
      this.options.set(property, new ObservableMap({ [option]: option }))
    }
  }
}
