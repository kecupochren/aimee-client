/**
 * BaseStore simplifies accessing some state layers
 */

import { HelpersStore } from '../../helpers'
import { RepositoriesStore } from '../../repositories'

export abstract class BaseStore {
  constructor(private readonly app: any) {}

  get helpers() {
    return this.app.helpers as HelpersStore
  }

  get repositories() {
    return this.app.repositories as RepositoriesStore
  }
}
