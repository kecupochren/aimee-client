import { IDepartment } from '~/api/models'

import { RepositoryCollection } from '../modules'
import { DepartmentModel } from '../models'

export class IDepartmentsCollection extends RepositoryCollection<
  number,
  IDepartment,
  DepartmentModel
> {}
