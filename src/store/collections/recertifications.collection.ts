import { RepositoryCollection } from '../modules'
import { IRecertification, RecertificationModel } from '../models'
import { IRecertificationFilters } from '../repositories/recertifications.repository'

export class IRecertificationsCollection extends RepositoryCollection<
  number,
  IRecertification,
  RecertificationModel,
  IRecertificationFilters
> {}
