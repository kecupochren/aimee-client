import { RepositoryCollection } from '../modules'
import { IRecertificationUnit, RecertificationUnitModel } from '../models'
import { IRecertificationUnitsFilters } from '../repositories/recertificationUnits.repository'

export class IRecertificationUnitsCollection extends RepositoryCollection<
  number,
  IRecertificationUnit,
  RecertificationUnitModel,
  IRecertificationUnitsFilters
> {}
