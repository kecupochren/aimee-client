import { IJobPosition } from '~/api/models'

import { RepositoryCollection } from '../modules'
import { JobPositionModel } from '../models'
import { IJobPositionFilters } from '../repositories/jobPositions.repository'

export class IJobPositionsCollection extends RepositoryCollection<
  number,
  IJobPosition,
  JobPositionModel,
  IJobPositionFilters
> {}
