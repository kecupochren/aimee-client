/**
 * User model read from window.serverData object
 */

import { observable } from 'mobx'

import { IUser } from '~/api/models'

import { setData } from './utilts'

export class UserModel implements IUser {
  // ===================================
  // Model
  // ===================================
  @observable userName: string
  @observable userDisplayAs: string
  @observable isImpersonated: boolean
  @observable impersonatedUserId?: string

  // ===================================
  // Constructor
  // ===================================
  constructor(data: IUser) {
    this.setData(data)
  }

  // ===================================
  // Actions
  // ===================================
  setData(data: IUser) {
    setData(this, data)
  }
}
