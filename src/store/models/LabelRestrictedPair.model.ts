import { observable, computed } from 'mobx'

import { ILabelRestrictedPair } from '~/api/models'
import { messages } from '~/messages'
import { setModelID } from '~/utils'

import { HelpersStore } from '../helpers'

import { LabelModel } from './Label.model'
import { setData } from './utilts'

export class LabelRestrictedPairModel implements ILabelRestrictedPair {
  // ===================================
  // Model
  // ===================================
  // Entity
  @observable id: number
  @observable created: Date
  @observable createdBy: string
  @observable changed: Date | undefined
  @observable changedBy: string | undefined
  @observable canBeDeleted: boolean
  @observable canBeEdited: boolean

  // Shared
  @observable description: string
  @observable isCompensable: boolean
  @observable firstLabelId: number
  @observable firstLabelName: string
  @observable secondLabelId: number
  @observable secondLabelName: string
  @observable firstLabel: LabelModel
  @observable secondLabel: LabelModel

  // Internal state
  @observable isPartial = false

  // ===================================
  // Constructor
  // ===================================
  constructor(readonly helpers: HelpersStore, data: ILabelRestrictedPair, isPartial = false) {
    this.setData(data, isPartial)
  }

  // ===================================
  // Views
  // ===================================
  @computed get entityName() {
    return this.helpers.intl.formatMessage(messages.LabelRestrictedPair)
  }

  // ===================================
  // Actions
  // ===================================
  setData(data: ILabelRestrictedPair, isPartial = false) {
    setData(this, data)
    this.isPartial = isPartial
    this.id = setModelID(data.id)
  }
}
