import { observable, computed, IObservableArray } from 'mobx'

import {
  RecertificationUnitState,
  RecertificationUnitDetailVM,
  IRecertificationUnitRowVM,
  RecertificationState,
  RecertificationUnitColumnDisplayVM
} from '~/api/apimodels/ViewModels'
import {
  RecertificationUnitListVM,
  RecertificationUnitColumnInputVM,
  RecertificationUnitColumnSelectVM
} from '~/api/apimodels/RecertificationUnits'
import { messages } from '~/messages'
import { setModelID } from '~/utils'

import { HelpersStore } from '../helpers'

import { setData } from './utilts'

export type IRecertificationUnit = RecertificationUnitListVM &
  // TODO: Fix rows in RecertificationUnitDetailVM
  Omit<RecertificationUnitDetailVM, 'recertificationUnitState' | 'recertificationState'>

export type TRecertificationUnitColumn =
  | RecertificationUnitColumnDisplayVM
  | RecertificationUnitColumnInputVM
  | RecertificationUnitColumnSelectVM

export class RecertificationUnitModel implements IRecertificationUnit {
  // ===================================
  // Model
  // ===================================
  // Entity
  @observable id: number
  @observable createdBy: string
  @observable confirmedBy: string
  @observable canBeConfirmed: boolean
  @observable canBeEdited: boolean
  @observable canBeUnlocked: boolean
  @observable canBeDeleted: boolean = false

  // Shared
  @observable created: Date | string
  @observable unlocked: Date | string
  @observable unlockedBy: string
  @observable recertificationUnitTypeName: string

  // List VM
  @observable departmentId: number
  @observable departmentName: string
  @observable lastConfirmed: Date | string
  @observable recertificationUnitState: RecertificationUnitState
  @observable recertificationUnitTypeId: number
  @observable systemApplicationId: number
  @observable systemApplicationName: string

  // Detail VM
  @observable columnDefinitions: IObservableArray<TRecertificationUnitColumn> = observable([])
  @observable rows: IObservableArray<IRecertificationUnitRowVM> = observable([])
  @observable deletedRows: IObservableArray<IRecertificationUnitRowVM> = observable([])
  @observable confirmed: Date | string
  @observable recertificationId: number
  @observable recertificationName: string
  @observable recertificationState: RecertificationState
  @observable title: string

  // Internal state
  @observable isPartial = false

  // ===================================
  // Constructor
  // ===================================
  constructor(readonly helpers: HelpersStore, data: IRecertificationUnit, isPartial = false) {
    this.setData(data, isPartial)
  }

  // ===================================
  // Views
  // ===================================
  @computed get entityName() {
    return this.helpers.intl.formatMessage(messages.RecertificationUnit)
  }

  @computed get stateLabel() {
    switch (this.recertificationUnitState) {
      case RecertificationUnitState.Initial:
        return messages.RecertificationUnitStateInitial
      case RecertificationUnitState.Confirmed:
        return messages.RecertificationUnitStateConfirmed
      case RecertificationUnitState.Draft:
        return messages.RecertificationUnitStateDraft
      case RecertificationUnitState.Unlocked:
        return messages.RecertificationUnitStateUnlocked
      default:
        const exh: never = this.recertificationUnitState
        return exh
    }
  }

  // ===================================
  // Actions
  // ===================================
  setData(data: IRecertificationUnit, isPartial = false) {
    setData(this, data)
    this.isPartial = isPartial
    this.id = setModelID(data.id)
  }

  setUnitState(state: RecertificationUnitState) {
    this.recertificationUnitState = state
  }
}
