import { observable, computed, IObservableArray } from 'mobx'

import {
  RecertificationState,
  RecertificationListVM,
  RecertificationDetailVM
} from '~/api/apimodels/ViewModels'
import { messages } from '~/messages'
import { setModelID } from '~/utils'

import { HelpersStore } from '../helpers'

import { SystemApplicationModel } from './SystemApplication.model'
import { setData } from './utilts'

export type IRecertification = RecertificationDetailVM & RecertificationListVM

export class RecertificationModel implements IRecertification {
  // ===================================
  // Model
  // ===================================
  // Entity
  @observable id: number
  @observable created: Date | string
  @observable createdBy: string
  @observable canBeEdited: boolean
  @observable canBeDeleted: boolean = false

  // Shared
  @observable name: string
  @observable planOfMeasuresDate: Date
  @observable referenceDate: Date
  @observable finalizationDate: Date
  @observable recertificationState: RecertificationState

  // Detail VM
  @observable systemApplications: IObservableArray<SystemApplicationModel> = observable([])

  // Internal state
  @observable isPartial = false

  // ===================================
  // Constructor
  // ===================================
  constructor(readonly helpers: HelpersStore, data: IRecertification, isPartial = false) {
    this.setData(data, isPartial)
  }

  // ===================================
  // Views
  // ===================================
  @computed get entityName() {
    return this.helpers.intl.formatMessage(messages.Recertification)
  }

  @computed get stateLabel() {
    switch (this.recertificationState) {
      case RecertificationState.Initial:
        return messages.RecertificationStateInitial
      case RecertificationState.Confirmed:
        return messages.RecertificationStateConfirmed
      case RecertificationState.Finalized:
        return messages.RecertificationStateFinalized
      case RecertificationState.Opened:
        return messages.RecertificationStateOpened
      default:
        const exh: never = this.recertificationState
        return exh
    }
  }

  // ===================================
  // Actions
  // ===================================
  setData(data: IRecertification, isPartial = false) {
    setData(this, data)
    this.isPartial = isPartial
    this.id = setModelID(data.id)
  }
}
