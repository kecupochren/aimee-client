import { observable, computed } from 'mobx'

import { IDepartment } from '~/api/models'
import { messages } from '~/messages'
import { setModelID } from '~/utils'

import { HelpersStore } from '../helpers'

import { setData } from './utilts'

export class DepartmentModel implements IDepartment {
  // ===================================
  // Model
  // ===================================
  // Entity
  @observable id: number
  @observable created: Date
  @observable createdBy: string
  @observable changed: Date | undefined
  @observable changedBy: string | undefined
  @observable canBeDeleted: boolean
  @observable canBeEdited: boolean

  // Shared
  @observable name: string
  @observable shortcut: string
  @observable companyId: number
  @observable deleted: boolean

  // Internal state
  @observable isPartial = false

  // ===================================
  // Constructor
  // ===================================
  constructor(readonly helpers: HelpersStore, data: IDepartment, isPartial = false) {
    this.setData(data, isPartial)
  }

  // ===================================
  // Views
  // ===================================
  @computed get entityName() {
    return this.helpers.intl.formatMessage(messages.Department)
  }

  // ===================================
  // Actions
  // ===================================
  setData(data: IDepartment, isPartial = false) {
    setData(this, data)
    this.isPartial = isPartial
    this.id = setModelID(data.id)
  }
}
