import { observable, computed, IObservableArray } from 'mobx'

import { IJobPosition, IIdentity, IWorkflowAuditTrail } from '~/api/models'
import { messages } from '~/messages'
import { setModelID } from '~/utils'

import { HelpersStore } from '../helpers'

import { BusinessRoleModel } from './BusinessRole.model'
import { DepartmentModel } from './Department.model'
import { setData } from './utilts'

export class JobPositionModel implements IJobPosition {
  // ===================================
  // Model
  // ===================================
  // Entity
  @observable id: number
  @observable created: Date
  @observable createdBy: string
  @observable changed: Date | undefined
  @observable changedBy: string | undefined
  @observable canBeDeleted: boolean
  @observable canBeEdited: boolean

  // Shared
  @observable name: string
  @observable departmentId: number

  // List VM
  @observable departmentName: string
  @observable workflowStatus: number

  // Detail VM
  @observable department: DepartmentModel
  @observable businessRoles: IObservableArray<BusinessRoleModel> = observable([])
  @observable identities: IObservableArray<IIdentity> = observable([])
  @observable auditTrails: IObservableArray<IWorkflowAuditTrail> = observable([])

  // Internal state
  @observable isPartial = false

  // ===================================
  // Constructor
  // ===================================
  constructor(readonly helpers: HelpersStore, data: IJobPosition, isPartial = false) {
    this.setData(data, isPartial)
  }

  // ===================================
  // Views
  // ===================================
  @computed get entityName() {
    return this.helpers.intl.formatMessage(messages.JobPosition)
  }

  // ===================================
  // Actions
  // ===================================
  setData(data: IJobPosition, isPartial = false) {
    setData(this, data)
    this.isPartial = isPartial
    this.id = setModelID(data.id)
  }
}
