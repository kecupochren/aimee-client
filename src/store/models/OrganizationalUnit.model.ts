import { observable, computed, IObservableArray } from 'mobx'

import { IOrganizationalUnit } from '~/api/models'
import { messages } from '~/messages'
import { setModelID } from '~/utils'

import { HelpersStore } from '../helpers'

import { LabelModel } from './Label.model'
import { setData } from './utilts'

export class OrganizationalUnitModel implements IOrganizationalUnit {
  // ===================================
  // Model
  // ===================================
  // Entity
  @observable id: number
  @observable created: Date
  @observable createdBy: string
  @observable changed: Date | undefined
  @observable changedBy: string | undefined
  @observable canBeDeleted: boolean
  @observable canBeEdited: boolean

  // Shared
  @observable name: string
  @observable labels: IObservableArray<LabelModel> = observable([])

  // Internal state
  @observable isPartial = false

  // ===================================
  // Constructor
  // ===================================
  constructor(readonly helpers: HelpersStore, data: IOrganizationalUnit, isPartial = false) {
    this.setData(data, isPartial)
  }

  // ===================================
  // Views
  // ===================================
  @computed get entityName() {
    return this.helpers.intl.formatMessage(messages.OrgUnit)
  }

  @computed get labelIds() {
    return this.labels.map(x => x.id)
  }

  // ===================================
  // Actions
  // ===================================
  setData(data: IOrganizationalUnit, isPartial = false) {
    setData(this, data)
    this.isPartial = isPartial
    this.id = setModelID(data.id)
  }
}
