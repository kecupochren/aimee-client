import { observable, computed } from 'mobx'

import { IBusinessRole, WorkflowStatus } from '~/api/models'
import { messages } from '~/messages'
import { setModelID } from '~/utils'

import { HelpersStore } from '../helpers'

import { setData } from './utilts'

export class BusinessRoleModel implements IBusinessRole {
  // ===================================
  // Model
  // ===================================
  // Entity
  @observable id: number
  @observable created: Date
  @observable createdBy: string
  @observable changed: Date | undefined
  @observable changedBy: string | undefined
  @observable canBeDeleted: boolean = true
  @observable canBeEdited: boolean = true

  // Shared
  @observable name: string
  @observable description: string
  @observable departmentId: number
  @observable departmentName: string
  @observable workflowStatus: WorkflowStatus

  // IJobPositionBusinessRoleVM
  @observable businessRoleId: number
  @observable department: string

  // Internal state
  @observable isPartial = false

  // ===================================
  // Constructor
  // ===================================
  constructor(readonly helpers: HelpersStore, data: IBusinessRole, isPartial = false) {
    this.setData(data, isPartial)
  }

  // ===================================
  // Views
  // ===================================
  @computed get entityName() {
    return this.helpers.intl.formatMessage(messages.BusinessRole)
  }

  // ===================================
  // Actions
  // ===================================
  setData(data: IBusinessRole, isPartial = false) {
    setData(this, data)

    this.isPartial = isPartial
    this.id = setModelID([data.id, data.businessRoleId])

    const department = data.department || data.departmentName
    this.department = department
    this.departmentName = department
  }
}
