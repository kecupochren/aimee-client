import { observable, computed } from 'mobx'

import {
  SystemApplicationListVM,
  RecertificationSystemApplicationVM
} from '~/api/apimodels/ViewModels'
import { messages } from '~/messages'
import { setModelID } from '~/utils'

import { HelpersStore } from '../helpers'

import { setData } from './utilts'

export type ISystemApplication = SystemApplicationListVM & RecertificationSystemApplicationVM

export class SystemApplicationModel implements ISystemApplication {
  // ===================================
  // Model
  // ===================================
  // Entity
  @observable id: number
  @observable canBeEdited: boolean
  @observable canBeDeleted: boolean

  // Shared
  @observable name: string

  // RecertificationSystemApplicationVM
  @observable areImportedDataValid: boolean
  @observable hasImportedData: boolean
  @observable imported: Date | string
  @observable importedBy: string
  @observable missingEmployeesDkx: string[]
  @observable missingRoleCodes: string[]
  @observable systemApplicationId: number
  @observable systemApplicationName: string

  // Internal state
  @observable isPartial = false

  // ===================================
  // Constructor
  // ===================================
  constructor(readonly helpers: HelpersStore, data: ISystemApplication, isPartial = false) {
    this.setData(data, isPartial)
  }

  // ===================================
  // Views
  // ===================================
  @computed get entityName() {
    return this.helpers.intl.formatMessage(messages.SystemApplications)
  }

  // ===================================
  // Actions
  // ===================================
  setData(data: ISystemApplication, isPartial = false) {
    setData(this, data)
    this.isPartial = isPartial
    this.id = setModelID([data.id, data.systemApplicationId])
  }

  updateData(data: Partial<ISystemApplication>) {
    setData(this, data)
  }
}
