import { observable, computed } from 'mobx'

import { ILabel } from '~/api/models'
import { messages } from '~/messages'
import { setModelID } from '~/utils'

import { HelpersStore } from '../helpers'

import { setData } from './utilts'

export class LabelModel implements ILabel {
  // ===================================
  // Model
  // ===================================
  // Entity
  @observable id: number
  @observable created: Date
  @observable createdBy: string
  @observable changed: Date | undefined
  @observable changedBy: string | undefined
  @observable canBeDeleted: boolean
  @observable canBeEdited: boolean

  // Shared
  @observable name: string
  @observable description: string
  @observable isSoD: boolean

  // Internal state
  @observable isPartial = false

  // ===================================
  // Constructor
  // ===================================
  constructor(readonly helpers: HelpersStore, data: ILabel, isPartial = false) {
    this.setData(data, isPartial)
  }

  // ===================================
  // Views
  // ===================================
  @computed get entityName() {
    return this.helpers.intl.formatMessage(messages.Label)
  }

  // ===================================
  // Actions
  // ===================================
  setData(data: ILabel, isPartial = false) {
    setData(this, data)
    this.isPartial = isPartial
    this.id = setModelID(data.id)
  }
}
