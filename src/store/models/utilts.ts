import { set } from 'mobx'
import moment from 'moment'

export const setData = (model: any, data: { [key: string]: any }) => {
  const dataFinal: { [key: string]: any } = {}

  // TODO: Use reduce, my brain is fried now
  Object.entries(data).forEach(([key, value]) => {
    dataFinal[key] = isValueDate(value) ? new Date(value) : value
  })

  set(model, dataFinal)
}

const isValueDate = (value: any) => {
  if (typeof value !== 'string') return false

  const isValidISO = moment(value, moment.ISO_8601, true).isValid()
  const hasISOLength = value.length === 24

  return isValidISO && hasISOLength
}
