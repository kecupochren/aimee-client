// Root Store

import React, { useContext } from 'react'
import { computed } from 'mobx'

import { JobPositionsStore } from '~/pages/JobPositions/JobPositions.store'
import { JobPositionsFormStore } from '~/pages/JobPositionsForm/JobPositionsForm.store'

import { LabelRestrictedPairsStore } from '~/pages/LabelRestrictedPairs/LabelRestrictedPairs.store'
import { LabelRestrictedPairsFormStore } from '~/pages/LabelRestrictedPairsForm/LabelRestrictedPairsForm.store'

import { LabelsStore } from '~/pages/Labels/Labels.store'
import { LabelsFormStore } from '~/pages/LabelsForm/LabelsForm.store'

import { OrganizationalUnitsStore } from '~/pages/OrganizationalUnits/OrganizationalUnits.store'
import { OrganizationalUnitsFormStore } from '~/pages/OrganizationalUnitsForm/OrganizationalUnitsForm.store'

import { RecertificationsStore } from '~/pages/Recertifications/Recertifications.store'
import { RecertificationsFormStore } from '~/pages/RecertificationsForm/RecertificationsForm.store'

import { RecertificationUnitsStore } from '~/pages/RecertificationUnits/RecertificationUnits.store'
import { RecertificationUnitsFormStore } from '~/pages/RecertificationUnitsForm/RecertificationUnitsForm.store'

import { rootStore } from './rootStore'
import { HelpersStore } from './helpers'
import { RepositoriesStore } from './repositories'

export class AppStore {
  // ===================================
  // Model
  // ===================================
  root = rootStore

  JobPositionsStore = new JobPositionsStore(this)
  JobPositionsFormStore = new JobPositionsFormStore(this)

  LabelRestrictedPairsStore = new LabelRestrictedPairsStore(this)
  LabelRestrictedPairsFormStore = new LabelRestrictedPairsFormStore(this)

  LabelsStore = new LabelsStore(this)
  LabelsFormStore = new LabelsFormStore(this)

  OrganizationalUnitsStore = new OrganizationalUnitsStore(this)
  OrganizationalUnitsFormStore = new OrganizationalUnitsFormStore(this)

  RecertificationStore = new RecertificationsStore(this)
  RecertificationFormStore = new RecertificationsFormStore(this)

  RecertificationUnitsStore = new RecertificationUnitsStore(this)
  RecertificationUnitsFormStore = new RecertificationUnitsFormStore(this)

  // ===================================
  // Views
  // ===================================
  @computed get helpers(): HelpersStore {
    return this.root.helpers
  }

  @computed get repositories(): RepositoriesStore {
    return this.root.repositories
  }
}

export let appStore: AppStore

export const createStore = () => {
  appStore = new AppStore()

  return { store: appStore }
}

export const useStore = (): AppStore => useContext(React.createContext(appStore))
