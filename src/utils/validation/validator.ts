import get from 'lodash/get'
import set from 'lodash/set'
import { FormattedMessage } from 'react-intl'

type Rule<T = any> = (
  value: T,
  data: { [key: string]: T }
) => string | FormattedMessage.MessageDescriptor | undefined

const joinRules = <T = any>(rules: Rule<T>[]) => (value: T, data: { [key: string]: T }) =>
  rules
    // Test value against all rules
    .map(rule => rule(value, data))
    // Filter null errors & pick the first one
    .filter(error => Boolean(error))[0]

export function validator(rules: { [key: string]: Rule | Rule[] }) {
  return (data = {}) => {
    let errors = {}

    Object.keys(rules).forEach(key => {
      // concat enables both functions and arrays of functions
      const rule = joinRules(([] as Rule[]).concat(rules[key]))
      const error = rule(get(data, key), data)

      if (error) {
        errors = set(errors, key, error)
      }
    })

    return errors
  }
}
