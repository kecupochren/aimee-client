import get from 'lodash/get'
import moment from 'moment'
import { messages } from './rules.messages'
import { FormattedMessage } from 'react-intl'
// import moment from 'moment'

export function maxLines(limit: number, message: string) {
  return (value = '') => {
    const lines = value.trim().split(/[\n,]+/)
    if (lines.length > limit) {
      return message
    }
    return undefined
  }
}

export function required(value: string | string[] = '') {
  if (Array.isArray(value) && value.length === 0) {
    return 'Required'
  }

  if (typeof value !== 'string') {
    return undefined
  }

  if (!value.trim()) {
    return 'Required'
  }

  return undefined
}

export function email(value: string) {
  const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  const isValid = new RegExp(pattern).test(value)

  if (!isValid) {
    return 'Invalid email address'
  }

  return undefined
}

export function minLength(limit: number, message?: string) {
  return (value: string) => {
    if (value && value.length < limit) {
      return message || `Atleast ${limit} characters`
    }

    return undefined
  }
}

export function match(field: string) {
  return (value: string, data: object) => {
    // @ts-ignore
    if (value !== data[field]) {
      return "Doesn't match"
    }

    return undefined
  }
}

// export function isURL(value: string) {
//   return checkURL(value)
// }

// export function checkURLs(value: string) {
//   const urls = value && value.split(/[\n,]+/)
//   let error

//   if (urls && urls.length) {
//     const trimmed = urls.map(x => x.trim())

//     trimmed.forEach(url => {
//       if (!isURL(url)) {
//         error = 'Some of the URLs are invalid'
//       }
//     })

//     const set = new Set(trimmed)

//     if (set.size !== urls.length) {
//       error = 'There are duplicate URLs'
//     }
//   }

//   return error
// }

export function dateInFuture(date: string) {
  if (
    moment(new Date())
      .add(1, 'day')
      .isAfter(date, 'day')
  ) {
    return messages.ruleMessages.DateInFuture
  }

  return undefined
}

export function requiredIf(field: string, message: any) {
  return (value: string, data: object) => {
    if (!value && get(data, field)) {
      return message || `${field} is also required`
    }

    return undefined
  }
}

export function dateAfter(field: string, message: FormattedMessage.MessageDescriptor) {
  return (date: string, data: object) => {
    if (
      moment(get(data, field))
        .add(1, 'day')
        .isAfter(date, 'day')
    ) {
      return message
    }

    return undefined
  }
}

export function not(value: string, message?: any) {
  return (fieldValue: string) => {
    if (value === fieldValue) {
      return message || `This one is invalid`
    }

    return undefined
  }
}

export function min(limit: number) {
  return (value: string) => {
    if (Number(value) < Number(limit)) {
      return `Min ${limit}`
    }

    return undefined
  }
}

export function max(limit: number) {
  return (value: string) => {
    if (Number(value) > limit) {
      return `Max ${limit}`
    }

    return undefined
  }
}

// export function isDate(date: string) {
//   if (!date) {
//     return 'Required'
//   }

//   if (!moment(date).isValid()) {
//     return 'Invalid date'
//   }

//   return undefined
// }
