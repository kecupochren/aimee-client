import { defineMessages } from 'react-intl'

const ruleMessages = defineMessages({
  DateInFuture: {
    id: 'ValidationRules.DateInFuture',
    defaultMessage: 'Date must be in the future'
  }
})

export const messages = { ruleMessages }
