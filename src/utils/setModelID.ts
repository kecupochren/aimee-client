import { uuid } from './uuid'

export const setModelID = (sources: number[] | number) => {
  const id = Array.isArray(sources) ? sources.find(x => typeof x !== 'undefined') : sources

  if (id) {
    return id
  }

  return (uuid() as unknown) as number
}
