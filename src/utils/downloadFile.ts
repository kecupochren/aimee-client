export const downloadFile = (data: BlobPart, name: string, type: string) => {
  const blobData = new Blob([data as BlobPart], { type })
  const url = URL.createObjectURL(blobData)
  const link = document.createElement('a')

  link.setAttribute('href', url)
  link.setAttribute('download', name)
  document.body.appendChild(link)
  link.click()
}
