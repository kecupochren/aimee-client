import * as styledComponents from 'styled-components'

import { TTheme } from '~/constants'

const {
  default: styled,
  createGlobalStyle,
  css,
  keyframes,
  withTheme,
  ThemeProvider
} = styledComponents as styledComponents.ThemedStyledComponentsModule<TTheme>

export interface IWithTheme {
  theme: TTheme
}

export { styled, createGlobalStyle, css, keyframes, withTheme, ThemeProvider }
