import { RouteProps } from 'react-router'

type TLocation = RouteProps['location']

export function getIDParam(location: TLocation) {
  const parts = location && location.pathname.split('/').reverse()
  const id = parts && parts[0]

  if (id && id !== 'new') {
    return Number(id)
  }

  return undefined
}
