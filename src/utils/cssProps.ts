import {
  FlexProperty,
  JustifyContentProperty,
  AlignItemsProperty,
  WidthProperty,
  DisplayProperty,
  FlexBasisProperty
} from 'csstype'

import { ISpacingProps, applySpacingProps, getSpacingProps } from './spacingProps'

export interface ICSSProps extends ISpacingProps {
  width?: WidthProperty<number | string>
  display?: DisplayProperty
  flex?: FlexProperty<number | string>
  flexBasis?: FlexBasisProperty<number | string>
  flexGrow?: number
  flexShrink?: number
  justifyContent?: JustifyContentProperty
  alignItems?: AlignItemsProperty
}

export const applyCSSProps = (props: any) => {
  let css = applySpacingProps(props)

  if (props.display) {
    css += `display: ${props.display};`
  }

  if (props.width) {
    css += `width: ${props.width};`
  }

  if (props.flex) {
    css += `flex: ${props.flex};`
  }

  if (props.flexBasis) {
    css += `flex-basis: ${props.flexBasis};`
  }

  if (props.flexGrow) {
    css += `flex-grow: ${props.flexGrow};`
  }

  if (props.flexShrink) {
    css += `flex-shrink: ${props.flexShrink};`
  }

  if (props.justifyContent) {
    css += `justify-content: ${props.justifyContent};`
  }

  if (props.alignItems) {
    css += `align-items: ${props.justifyContent};`
  }

  return css
}

export const getCSSProps = (props: any) => {
  const keys: Array<keyof ICSSProps> = [
    'width',
    'display',
    'flex',
    'flexGrow',
    'flexShrink',
    'justifyContent',
    'alignItems',
    'flexBasis'
  ]

  const cssProps: any = getSpacingProps(props)

  keys.forEach(key => {
    cssProps[key] = props[key]
  })

  return cssProps
}
