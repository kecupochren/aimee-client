export class AppError extends Error {
  type: string

  constructor(type: string) {
    super()
    this.type = type
  }
}

export class NotFoundError extends AppError {
  constructor() {
    super('E_NOT_FOUND')
    this.message = 'Not found'
  }
}

export class DuplicateError extends AppError {
  constructor() {
    super('E_DUPLICATE')
  }
}

export class NotImplementedError extends AppError {
  constructor() {
    super('E_NOT_IMPLEMENTED')
  }
}
