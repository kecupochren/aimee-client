import capitalize from 'lodash/capitalize'
import lowerCase from 'lodash/lowerCase'

export const humanizeString = (str: string): string => {
  return capitalize(lowerCase(str).replace('_', ' '))
}
