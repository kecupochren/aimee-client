export const formatExactNumber = (x?: number) => {
  if (typeof x === 'undefined') return null
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
}
