import React from 'react'

import omit from 'lodash/omit'

// Helper for excluding custom props from getting passed to DOM.
// This is done by styled-component automatically for regular components but not for 3rd party.
export const withValidProps = (Component: any, custom: string[] = []) => (props: any) => (
  <Component
    {...omit(props, [
      // Exclude cssProps helper props, used by multiple components
      'width',
      'display',
      'flex',
      'flexGrow',
      'flexShrink',
      'justifyContent',
      'alignItems',
      'flexBasis',

      // Used by multiple components...
      'requiredPermission',
      'returnButton',

      ...custom
    ])}
  />
)
