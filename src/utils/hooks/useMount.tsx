import { useEffectOnce } from './useEffectOnce'

export const useMount = (mountFn: () => void, unmountFn?: () => void) => {
  useEffectOnce(() => {
    mountFn()

    if (unmountFn) return unmountFn

    return undefined
  })
}
