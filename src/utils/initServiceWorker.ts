export function initServiceWorker() {
  const isLocalhost = window.location.pathname.includes('localhost')
  const isProduction = process.env.NODE_ENV === 'production'
  const isLocalProd = process.env.LOCAL_PROD

  console.log('isLocalhost', isLocalhost)
  console.log('isLocalProd', isLocalProd)

  if ('serviceWorker' in navigator && isProduction && !isLocalhost && !isLocalProd) {
    window.addEventListener('load', () => {
      navigator.serviceWorker
        .register('/service-worker.js')
        .then(registration => console.log('SW registered: ', registration))
        .catch(registrationError => console.log('SW registration failed: ', registrationError))
    })
  }
}
