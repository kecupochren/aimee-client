import Loadable from 'react-loadable'

// Import directly to avoid cyclic import
import { PageLoader } from '~/components/ui/PageLoader/PageLoader'

export function lazyLoadPage(pageImport: () => any) {
  return Loadable({
    // @ts-ignore
    loading: PageLoader,
    loader: pageImport,
    delay: 1000
  })
}
