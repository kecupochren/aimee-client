import { ObservableMap, keys } from 'mobx'

export const toObject = (input: ObservableMap<string, any>) => {
  return keys(input).reduce((map, key) => ({ ...map, [key]: input.get(key) }), {} as any)
}

export const noop = () => undefined
