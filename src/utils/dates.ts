import moment, { Moment } from 'moment'

import { DATE_FORMATS } from '~/constants'

// With/without milliseconds
const ISO_DATE_LENGTH = [20, 24]

export const isValidDate = (date?: any): Moment | null => {
  if (!date) {
    return null
  }

  const momentObj = moment.utc(date)
  const isValid = momentObj.isValid()

  return isValid ? momentObj : null
}

export const isValidISODate = (date?: string): Moment | null => {
  if (!date) {
    return null
  }

  const iso = moment.utc(date, moment.ISO_8601)
  const isValid = iso.isValid() && ISO_DATE_LENGTH.includes(date.length)

  return isValid ? iso : null
}

export const isValidDefaultDate = (date?: string): Moment | null => {
  if (!date) return null

  const defaultDate = moment.utc(date, DATE_FORMATS.DEFAULT)
  const isValid = defaultDate.isValid() && date.length === DATE_FORMATS.DEFAULT.length

  return isValid ? defaultDate : null
}
