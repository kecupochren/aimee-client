import React from 'react'
import { FormattedMessage, InjectedIntl } from 'react-intl'

export function renderMessage(
  message?: FormattedMessage.MessageDescriptor | string | undefined | React.ReactElement | null,
  intl?: InjectedIntl
) {
  if (!message) {
    return null
  }

  if (typeof message === 'string') {
    return message
  }

  if (React.isValidElement(message)) {
    return message
  }

  if (message && message.id) {
    if (intl) {
      return intl.formatMessage(message)
    }

    return <FormattedMessage {...message} />
  }

  return null
}
