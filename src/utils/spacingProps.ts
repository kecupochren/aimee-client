import omit from 'lodash/omit'

export interface ISpacingProps {
  m?: number | string
  mx?: number | string
  my?: number | string
  mt?: number | string
  mr?: number | string
  mb?: number | string
  ml?: number | string

  p?: number | string
  px?: number | string
  py?: number | string
  pt?: number | string
  pr?: number | string
  pb?: number | string
  pl?: number | string
}

const getValue = (prop: any) => {
  const unit = typeof prop === 'string' ? '' : 'px'
  return `${prop}${unit}`
}

export const applySpacingProps = (props: any) => {
  let styles = ''

  if (typeof props.m !== 'undefined') {
    styles += `margin: ${getValue(props.m)};`
  }
  if (typeof props.mx !== 'undefined') {
    styles += `margin-left: ${getValue(props.mx)}; margin-right: ${getValue(props.mx)};`
  }
  if (typeof props.my !== 'undefined') {
    styles += `margin-top: ${getValue(props.my)}; margin-bottom: ${getValue(props.my)};`
  }
  if (typeof props.mt !== 'undefined') {
    styles += `margin-top: ${getValue(props.mt)};`
  }
  if (typeof props.mr !== 'undefined') {
    styles += `margin-right: ${getValue(props.mr)};`
  }
  if (typeof props.mb !== 'undefined') {
    styles += `margin-bottom: ${getValue(props.mb)};`
  }
  if (typeof props.ml !== 'undefined') {
    styles += `margin-left: ${getValue(props.ml)};`
  }

  if (typeof props.p !== 'undefined') {
    styles += `padding: ${getValue(props.p)};`
  }
  if (typeof props.px !== 'undefined') {
    styles += `padding-left: ${getValue(props.px)}; padding-right: ${getValue(props.px)};`
  }
  if (typeof props.py !== 'undefined') {
    styles += `padding-top: ${getValue(props.py)}; padding-bottom: ${getValue(props.py)};`
  }
  if (typeof props.pt !== 'undefined') {
    styles += `padding-top: ${getValue(props.pt)};`
  }
  if (typeof props.pr !== 'undefined') {
    styles += `padding-right: ${getValue(props.pr)};`
  }
  if (typeof props.pb !== 'undefined') {
    styles += `padding-bottom: ${getValue(props.pb)};`
  }
  if (typeof props.pl !== 'undefined') {
    styles += `padding-left: ${getValue(props.pl)};`
  }

  return styles
}

export const getSpacingProps = (props: any = {}) => {
  const spacingProps = {
    m: props.m,
    mx: props.mx,
    my: props.my,
    mt: props.mt,
    mr: props.mr,
    mb: props.mb,
    ml: props.ml,
    p: props.p,
    px: props.px,
    py: props.py,
    pt: props.pt,
    pr: props.pr,
    pb: props.pb,
    pl: props.pl
  }

  return { ...spacingProps }
}

export const omitSpacingProps = (props: any = {}) => {
  return omit(props, [
    'm',
    'mx',
    'my',
    'mt',
    'mr',
    'mb',
    'ml',
    'p',
    'px',
    'py',
    'pt',
    'pr',
    'pb',
    'pl'
  ])
}
