// prettier-ignore
type ValidateShape<T, Shape> = T extends Shape
    ? Exclude<keyof T, keyof Shape> extends never
        ? T
        : never
    : never

/**
 * Check if given object exactly match given type
 */
export const checkObjectShape = <T, M>(obj: ValidateShape<T, M>) => obj
