import React, { ReactElement } from 'react'
import { SynchronizedHistory } from 'mobx-react-router'
import { Router as RCRouter } from 'react-router'
import { IntlProvider } from 'react-intl'
import { render as testRender, queryByAttribute, RenderResult } from '@testing-library/react'
import { Provider } from 'mobx-react'

import { theme } from '~/constants'
import { RootStore } from '~/store/rootStore'
import { ThemeProvider } from '~/utils'

export const render = (
  comp: ReactElement,
  store?: { store: RootStore; history: SynchronizedHistory }
): RenderResult & { wrapper: Element } => {
  window.matchMedia = jest.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(),
    removeListener: jest.fn(),
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn()
  }))

  const renderResult = testRender(comp, {
    wrapper: ({ children }) => {
      const renderPage = (locale: string, dictionary: IAnyObject) => (
        <IntlProvider key={locale} locale={locale} messages={dictionary}>
          <ThemeProvider theme={theme}>
            <div>{children}</div>
          </ThemeProvider>
        </IntlProvider>
      )

      if (store) {
        const { localeShortKey, dictionary } = store.store.helpers.intl

        return (
          <RCRouter history={store.history}>
            <Provider store={store.store}>{renderPage(localeShortKey, dictionary)}</Provider>
          </RCRouter>
        )
      }

      return renderPage('en', {})
    }
  })

  const wrapper = renderResult.baseElement.children[0].children[0].children[0]

  return { ...renderResult, wrapper }
}

export const getByClassName = queryByAttribute.bind(null, 'class')
