import { format } from 'date-fns'

import { EMPTY_DASH } from '~/constants/misc'
import { DATE_FORMATS } from '~/constants'

export function formatDate(date: string) {
  if (!date) return EMPTY_DASH
  return format(date, DATE_FORMATS.DEFAULT)
}

export function formatDateLong(date: string) {
  if (!date) return EMPTY_DASH
  return format(date, DATE_FORMATS.LONG)
}
