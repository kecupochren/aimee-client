import { observable, action, computed } from 'mobx'
import { RouteProps } from 'react-router'

import { ILabelRestrictedPairForSaveVM, ILabel } from '~/api/models'
import { ROUTES } from '~/constants'
import { BaseStore, RepositoryCollection } from '~/store/modules'
import { LabelRestrictedPairModel, LabelModel } from '~/store/models'
import { getIDParam, DuplicateError } from '~/utils'

import { messages } from './LabelRestrictedPairsForm.messages'

export class LabelRestrictedPairsFormStore extends BaseStore {
  // ===================================-
  // Model
  // ===================================-
  // Entity to edit
  @observable labelRestrictedPair?: LabelRestrictedPairModel
  @observable id?: number

  // Options for dropdowns
  @observable labels: RepositoryCollection<number, ILabel, LabelModel>

  // State
  @observable loading = true
  @observable submitting = false

  // ===================================
  // Constructor
  // ===================================
  constructor(app: any) {
    super(app)

    this.labels = new RepositoryCollection(app, this.repositories.labels, {
      preserveSelected: true
    })
  }

  // ===================================
  // Views
  // ===================================
  @computed get pageTitle() {
    return this.id ? messages.EditLabelRestrictedPair : messages.AddNewLabelRestrictedPair
  }

  @computed get repository() {
    return this.repositories.labelRestrictedPairs
  }

  // ===================================-
  // Actions
  // ===================================-
  @action.bound async mount(props: RouteProps) {
    this.id = getIDParam(props.location)

    if (!this.id) {
      this.loading = false
      return
    }

    try {
      const model = await this.repository.fetchOne(Number(this.id))

      if (model.firstLabel) {
        this.labels.addItem(model.firstLabel, true)
      }

      if (model.secondLabel) {
        this.labels.addItem(model.secondLabel, true)
      }

      this.labelRestrictedPair = model
    } catch (error) {
      this.helpers.notification.handleError(error)
    } finally {
      this.loading = false
    }
  }

  @action.bound async handleSubmit(data: any) {
    const payload: ILabelRestrictedPairForSaveVM = {
      description: data.description,
      isCompensable: data.isCompensable,
      firstLabelId: data.firstLabelId,
      secondLabelId: data.secondLabelId
    }

    this.submitting = true

    try {
      if (this.labelRestrictedPair) {
        await this.repository.edit(this.labelRestrictedPair.id, payload)
      } else {
        await this.repository.create(payload)
      }

      this.helpers.router.push(ROUTES.LABEL_RESTRICTED_PAIRS)
    } catch (error) {
      if (error instanceof DuplicateError) {
        return this.helpers.notification.error({
          message: messages.ErrorDuplicate
        })
      }

      this.helpers.notification.handleError(error)
    } finally {
      this.submitting = false
    }
  }

  @action.bound unmount() {
    this.labelRestrictedPair = undefined
    this.loading = true
  }
}
