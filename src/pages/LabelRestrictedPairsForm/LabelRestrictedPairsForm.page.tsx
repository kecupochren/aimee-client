import React, { FC } from 'react'
import { Form, FormRenderProps } from 'react-final-form'
import { observer } from 'mobx-react-lite'
import { RouteProps } from 'react-router'

import { ILabelRestrictedPairForSaveVM } from '~/api/models'
import { ButtonGroup, Button, Card, Input, Page, Checkbox, Select } from '~/components'
import { messages } from '~/messages'
import { useMount } from '~/utils'
import { useStore } from '~/store'

export const LabelRestrictedPairsForm: React.FC<RouteProps> = props => {
  const {
    mount,
    unmount,
    labelRestrictedPair,
    handleSubmit
  } = useStore().LabelRestrictedPairsFormStore

  useMount(() => mount(props), unmount)

  return (
    <Form<ILabelRestrictedPairForSaveVM>
      onSubmit={handleSubmit}
      component={FormComponent}
      initialValues={labelRestrictedPair}
    />
  )
}

const FormComponent: FC<FormRenderProps<ILabelRestrictedPairForSaveVM>> = observer(props => {
  const { pageTitle, labels, loading } = useStore().LabelRestrictedPairsFormStore

  return (
    <form onSubmit={props.handleSubmit}>
      <Page title={pageTitle} width="sm" loading={loading} actions={<Actions {...props} />}>
        <Card mb={24}>
          <Select
            type="field"
            optionsFrom="collection"
            name="firstLabelId"
            required
            label={messages.FirstLabel}
            placeholder={messages.FirstLabel}
            collection={labels}
            mb={24}
          />

          <Select
            type="field"
            optionsFrom="collection"
            name="secondLabelId"
            required
            label={messages.SecondLabel}
            placeholder={messages.SecondLabel}
            collection={labels}
            mb={24}
          />

          <Input
            isTextarea
            name="description"
            placeholder={messages.Description}
            label={messages.Description}
            rows={5}
            mb={24}
          />

          <Checkbox name="isCompensable" label={messages.IsConflictCompensable} />

          <Actions {...props} />
        </Card>
      </Page>
    </form>
  )
})

const Actions: FC<FormRenderProps<ILabelRestrictedPairForSaveVM>> = observer(props => (
  <ButtonGroup align="end">
    <Button message={messages.Cancel} mr={6} returnButton />
    <Button
      message={messages.Save}
      type="primary"
      htmlType="submit"
      loading={props.submitting}
      disabled={props.pristine}
    />
  </ButtonGroup>
))

/* tslint:disable-next-line no-default-export */
export default observer(LabelRestrictedPairsForm)
