import { defineMessages } from 'react-intl'

import { sharedMessages } from '~/messages'

const pageMessages = defineMessages({
  ErrorDuplicate: {
    id: 'LabelsForm.ErrorDuplicate',
    defaultMessage: 'A label restricted pair with this name already exists'
  },
  AddNewLabelRestrictedPair: {
    id: 'LabelRestrictedPairsForm.AddNewLabelRestrictedPair',
    defaultMessage: 'Add new Label restricted pair'
  },
  EditLabelRestrictedPair: {
    id: 'LabelRestrictedPairsForm.EditLabelRestrictedPair',
    defaultMessage: 'Edit Label restricted pair'
  }
})

export const messages = { ...sharedMessages, ...pageMessages }
