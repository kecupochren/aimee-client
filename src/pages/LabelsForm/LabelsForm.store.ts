import { observable, action, computed } from 'mobx'
import { RouteProps } from 'react-router'

import { ILabelForSaveVM } from '~/api/models'
import { ROUTES } from '~/constants'
import { BaseStore } from '~/store/modules'
import { LabelModel } from '~/store/models'
import { getIDParam, DuplicateError } from '~/utils'

import { messages } from './LabelsForm.messages'

export class LabelsFormStore extends BaseStore {
  // ===================================-
  // Model
  // ===================================-
  @observable label?: LabelModel
  @observable id?: number
  @observable loading = true

  // ===================================
  // Views
  // ===================================
  @computed get isEditing() {
    return typeof this.id !== 'undefined'
  }

  @computed get pageTitle() {
    return this.isEditing ? messages.EditLabel : messages.AddNewLabel
  }

  @computed get repository() {
    return this.repositories.labels
  }

  // ===================================-
  // Actions
  // ===================================-
  @action.bound async mount(props: RouteProps) {
    this.id = getIDParam(props.location)

    if (!this.id) {
      this.loading = false
      return
    }

    try {
      this.label = await this.repository.fetchOne(this.id)
    } catch (error) {
      this.helpers.notification.handleError(error)
    } finally {
      this.loading = false
    }
  }

  @action.bound async handleSubmit(data: any) {
    const payload: ILabelForSaveVM = {
      name: data.name,
      description: data.description,
      isSoD: data.isSoD
    }

    try {
      if (this.label) {
        await this.repository.edit(this.label.id, payload)
      } else {
        await this.repository.create(payload)
      }

      this.helpers.router.push(ROUTES.LABELS)
    } catch (error) {
      if (error instanceof DuplicateError) {
        return this.helpers.notification.error({
          message: messages.ErrorDuplicate
        })
      }

      this.helpers.notification.handleError(error)
    }
  }

  @action.bound unmount() {
    this.label = undefined
    this.loading = true
  }
}
