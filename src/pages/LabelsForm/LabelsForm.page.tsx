import React, { FC } from 'react'
import { Form, FormRenderProps } from 'react-final-form'
import { observer } from 'mobx-react-lite'
import { RouteProps } from 'react-router'

import { ButtonGroup, Button, Input, Page, Checkbox, Card } from '~/components'
import { messages } from '~/messages'
import { useMount } from '~/utils'
import { useStore } from '~/store'

export const LabelsForm: React.FC<RouteProps> = props => {
  const { mount, unmount, label, handleSubmit } = useStore().LabelsFormStore

  useMount(() => mount(props), unmount)

  return <Form onSubmit={handleSubmit} component={FormComponent} initialValues={label} />
}

const FormComponent: FC<FormRenderProps> = observer(props => {
  const { pageTitle, loading } = useStore().LabelsFormStore

  return (
    <form onSubmit={props.handleSubmit}>
      <Page title={pageTitle} width="sm" loading={loading} actions={<Actions {...props} />}>
        <Card mb={24}>
          <Input name="name" required placeholder={messages.Name} label={messages.Name} mb={24} />

          <Input
            isTextarea
            required
            name="description"
            placeholder={messages.Description}
            label={messages.Description}
            rows={5}
            mb={24}
          />

          <Checkbox name="isSoD" label={messages.IsSoD} />

          <Actions {...props} />
        </Card>
      </Page>
    </form>
  )
})

const Actions: FC<FormRenderProps> = observer(props => (
  <ButtonGroup align="end">
    <Button message={messages.Cancel} mr={6} returnButton />
    <Button
      message={messages.Save}
      type="primary"
      htmlType="submit"
      loading={props.submitting}
      disabled={props.pristine}
    />
  </ButtonGroup>
))

/* tslint:disable-next-line no-default-export */
export default observer(LabelsForm)
