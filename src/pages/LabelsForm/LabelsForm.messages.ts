import { defineMessages } from 'react-intl'

import { sharedMessages } from '~/messages'

const pageMessages = defineMessages({
  ErrorDuplicate: {
    id: 'LabelsForm.ErrorDuplicate',
    defaultMessage: 'A label with this name already exists'
  },
  AddNewLabel: {
    id: 'LabelsForm.AddNewLabel',
    defaultMessage: 'Add new Label'
  },
  EditLabel: {
    id: 'LabelsForm.EditLabel',
    defaultMessage: 'Edit Label'
  }
})

export const messages = { ...sharedMessages, ...pageMessages }
