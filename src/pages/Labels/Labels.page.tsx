import React, { FC, useEffect } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { observer } from 'mobx-react-lite'

import { Page } from '~/components'
import { PERMISSIONS } from '~/constants'
import { useStore } from '~/store'
import { messages } from '~/messages'

import { LabelsFilters, LabelsActions, LabelsTable } from './components'

export const Labels: FC<RouteComponentProps> = props => {
  const { labels } = useStore().LabelsStore

  useEffect(() => {
    labels.init(props.location)
  }, [])

  return (
    <Page
      title={messages.Labels}
      actions={LabelsActions}
      loading={labels.isFirstLoad}
      requiredPermission={PERMISSIONS.canReadLabel}
    >
      <LabelsFilters />
      <LabelsTable />
    </Page>
  )
}

/* tslint:disable-next-line no-default-export */
export default observer(Labels)
