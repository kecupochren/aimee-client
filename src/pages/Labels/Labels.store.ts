import { observable } from 'mobx'

import { ILabel } from '~/api/models'
import { LabelModel } from '~/store/models'
import { BaseStore, RepositoryCollection } from '~/store/modules'

export class LabelsStore extends BaseStore {
  @observable labels: RepositoryCollection<number, ILabel, LabelModel>

  constructor(app: any) {
    super(app)
    this.labels = new RepositoryCollection(app, this.repositories.labels, { syncURL: true })
  }
}
