import React, { FC } from 'react'
import { observer } from 'mobx-react'

import { ILabel } from '~/api/models'
import { IconCheck } from '~/assets/icons'
import { theme, ROUTES, EMPTY_DASH } from '~/constants'
import { Text, RepositoryCollectionTable } from '~/components'
import { messages } from '~/messages'
import { LabelModel } from '~/store/models'
import { useStore } from '~/store'

export const LabelsTable: FC = observer(() => (
  <RepositoryCollectionTable<number, ILabel, LabelModel>
    collection={useStore().LabelsStore.labels}
    withBaseActions
    withBaseColumns
    editPageRoute={ROUTES.LABEL_EDIT}
    columns={[
      {
        title: <Text message={messages.Name} />,
        dataIndex: 'name',
        key: 'name',
        sorter: true
      },
      {
        title: <Text message={messages.IsSoD} />,
        dataIndex: 'isSod',
        key: 'isSod',
        sorter: true,
        align: 'center',
        render: (_text, label) =>
          label.isSoD ? (
            <IconCheck inline size={16} fill={theme.color.success} />
          ) : (
            <Text grayest>{EMPTY_DASH}</Text>
          )
      }
    ]}
  />
))
