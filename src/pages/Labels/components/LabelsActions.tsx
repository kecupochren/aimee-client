import React from 'react'

import { Button, Link } from '~/components'
import { ROUTES, PERMISSIONS } from '~/constants'
import { messages } from '~/messages'

export const LabelsActions: React.FC = () => (
  <>
    <Link to={ROUTES.LABEL_NEW}>
      <Button
        message={messages.Add}
        type="primary"
        requiredPermission={PERMISSIONS.canCreateLabel}
      />
    </Link>
  </>
)
