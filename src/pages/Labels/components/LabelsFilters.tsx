import React from 'react'
import { observer } from 'mobx-react'

import { Input, Box } from '~/components'
import { useStore } from '~/store'
import { messages } from '~/messages'

export const LabelsFilters: React.FC = observer(() => {
  const { labels } = useStore().LabelsStore

  return (
    <Box mb={12}>
      <Input
        id="input-search"
        placeholder={messages.Search}
        isSearch
        allowClear
        // value={labels.filters.get('Text')}
        onChange={query => labels.search(query)}
        debounce
      />
    </Box>
  )
})
