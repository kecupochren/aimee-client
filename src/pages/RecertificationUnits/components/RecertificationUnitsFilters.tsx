import React from 'react'
import { observer } from 'mobx-react'

import { Input, Box } from '~/components'
import { useStore } from '~/store'

import { messages } from '../RecertificationUnits.messages'

export const RecertificationUnitsFilters: React.FC<{ id: string }> = observer(() => {
  const { recertificationUnits } = useStore().RecertificationUnitsStore

  return (
    <Box mb={12} flex justifyContent="space-between">
      <Input
        onChange={query => recertificationUnits.search(query)}
        placeholder={messages.Search}
        isSearch
        allowClear
        debounce
        flexGrow={1}
      />
    </Box>
  )
})
