import React from 'react'
import { observer } from 'mobx-react'

import { ROUTES } from '~/constants'
import { Text, RepositoryCollectionTable, renderDateValue } from '~/components'
import { RecertificationUnitModel, IRecertificationUnit } from '~/store/models'
import { useStore } from '~/store'

import { messages } from '../RecertificationUnits.messages'

export const RecertificationUnitsTable: React.FC = observer(() => (
  <RepositoryCollectionTable<number, IRecertificationUnit, RecertificationUnitModel>
    collection={useStore().RecertificationUnitsStore.recertificationUnits}
    editPageRoute={ROUTES.RECERTIFICATION_UNIT_EDIT}
    withBaseActions
    scroll={{ x: 3200 }}
    columns={[
      {
        title: <Text message={messages.RecertificationUnitTypeName} />,
        dataIndex: 'recertificationUnitTypeName',
        key: 'recertificationUnitTypeName',
        sorter: true,
        ellipsis: true,
        width: 300
      },
      {
        title: <Text message={messages.SystemApplications} />,
        dataIndex: 'systemApplicationName',
        key: 'systemApplicationName',
        sorter: true,
        ellipsis: true,
        width: 200
      },
      {
        title: <Text message={messages.Department} />,
        dataIndex: 'departmentName',
        key: 'departmentName',
        sorter: true,
        ellipsis: true,
        width: 200
      },
      {
        title: <Text message={messages.RecertificationUnitState} />,
        dataIndex: 'recertificationUnitState',
        key: 'recertificationUnitState',
        sorter: true,
        ellipsis: true,
        width: 200,
        render: (_, recertModel) => <Text message={recertModel.stateLabel} />
      },
      {
        title: <Text message={messages.RecertificationUnitLastConfirmed} />,
        dataIndex: 'confirmed',
        key: 'confirmed',
        sorter: true,
        ellipsis: true,
        width: 250,
        render: (_, recertModel) =>
          recertModel.lastConfirmed ? renderDateValue(recertModel.lastConfirmed.toString()) : ''
      },
      {
        title: <Text message={messages.RecertificationUnitConfirmedBy} />,
        dataIndex: 'confirmedBy',
        key: 'confirmedBy',
        sorter: true,
        ellipsis: true,
        width: 200
      },
      {
        title: <Text message={messages.RecertificationUnlocked} />,
        dataIndex: 'unlocked',
        key: 'unlocked',
        sorter: true,
        ellipsis: true,
        width: 200,
        render: (_, recertModel) =>
          recertModel.unlocked ? renderDateValue(recertModel.unlocked.toString()) : ''
      },
      {
        title: <Text message={messages.RecertificationUnlockedBy} />,
        dataIndex: 'unlockedBy',
        key: 'unlockedBy',
        sorter: true,
        ellipsis: true,
        width: 200
      },
      {
        title: <Text message={messages.Created} />,
        dataIndex: 'created',
        key: 'created',
        sorter: true,
        ellipsis: true,
        width: 200,
        render: (_, recertModel) => renderDateValue(recertModel.created.toString())
      },
      {
        title: <Text message={messages.CreatedBy} />,
        dataIndex: 'createdBy',
        key: 'createdBy',
        sorter: true,
        ellipsis: true,
        width: 200
      }
    ]}
  />
))
