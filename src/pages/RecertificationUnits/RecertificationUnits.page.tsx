import React, { FC, useEffect } from 'react'
import { observer } from 'mobx-react-lite'
import { RouteComponentProps } from 'react-router'

import { Page } from '~/components'
import { messages } from '~/messages'
import { useStore } from '~/store'

import { RecertificationUnitsTable, RecertificationUnitsFilters } from './components'

export interface IProps extends RouteComponentProps<{ id: string }> {}

export const RecertificationUnits: FC<IProps> = props => {
  const { recertificationUnits, init } = useStore().RecertificationUnitsStore
  const { id } = props.match.params

  useEffect(() => init(props), [])

  return (
    <Page loading={recertificationUnits.isFirstLoad} title={messages.RecertificationUnitsList}>
      <RecertificationUnitsFilters id={id} />
      <RecertificationUnitsTable />
    </Page>
  )
}

/* tslint:disable-next-line no-default-export */
export default observer(RecertificationUnits)
