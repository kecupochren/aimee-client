import { defineMessages } from 'react-intl'

import { sharedMessages } from '~/messages'

const pageMessages = defineMessages({
  RecertificationUnitConfirmedBy: {
    id: 'RecertificationUnit.RecertificationUnitConfirmedBy',
    defaultMessage: 'Recertification confirmed by'
  },
  RecertificationUnitState: {
    id: 'RecertificationUnit.RecertificationUnitState',
    defaultMessage: 'Recertification unit state'
  },
  RecertificationUnlockedBy: {
    id: 'RecertificationUnit.RecertificationUnlockedBy',
    defaultMessage: 'Recertification unlocked by'
  },
  RecertificationUnlocked: {
    id: 'RecertificationUnit.RecertificationUnlocked',
    defaultMessage: 'Recertification unlocked'
  },
  RecertificationUnitLastConfirmed: {
    id: 'RecertificationUnit.RecertificationUnitLastConfirmed',
    defaultMessage: 'Recertification unit last confirmed'
  },
  RecertificationUnitTypeName: {
    id: 'RecertificationUnit.RecertificationUnitTypeName',
    defaultMessage: 'Recertification unit type name'
  },
  PlanOfMeasuresDate: {
    id: 'Recertification.PlanOfMeasuresDate',
    defaultMessage: 'Plan of measures date'
  },
  FinalizationDate: {
    id: 'Recertification.FinalizationDate',
    defaultMessage: 'Finalization date'
  },
  RecertificationState: {
    id: 'Recertification.RecertificationState',
    defaultMessage: 'Recertification state'
  },
  ReferenceDate: {
    id: 'Recertification.ReferenceDate',
    defaultMessage: 'Reference date'
  }
})

export const messages = { ...sharedMessages, ...pageMessages }
