import { observable, action } from 'mobx'

import { BaseStore, RepositoryCollection } from '~/store/modules'
import { IRecertificationUnitsCollection } from '~/store/collections'

import { IProps } from './RecertificationUnits.page'

export class RecertificationUnitsStore extends BaseStore {
  // ===================================
  // Model
  // ===================================
  // Table data
  @observable recertificationUnits: IRecertificationUnitsCollection

  // ===================================
  // Constructor
  // ===================================
  constructor(app: any) {
    super(app)

    this.recertificationUnits = new RepositoryCollection(
      app,
      this.repositories.recertificationUnits,
      { orderBy: 'recertificationUnitTypeName' }
    )
  }

  // ===================================
  // Actions
  // ===================================
  @action.bound init(props: IProps) {
    const { id } = props.match.params

    this.recertificationUnits.filters.setDefault('RecertificationId', [id])
    this.recertificationUnits.init(props.location)
  }
}
