import { observable, action } from 'mobx'
import { RouteComponentProps } from 'react-router'

import { BaseStore, RepositoryCollection } from '~/store/modules'
import { IJobPositionsCollection, IDepartmentsCollection } from '~/store/collections'

export class JobPositionsStore extends BaseStore {
  // ===================================
  // Model
  // ===================================
  // Table data
  @observable jobPositions: IJobPositionsCollection

  // Department filter options
  @observable departments: IDepartmentsCollection

  // ===================================
  // Constructor
  // ===================================
  constructor(app: any) {
    super(app)

    this.jobPositions = new RepositoryCollection(app, this.repositories.jobPositions)

    this.departments = new RepositoryCollection(app, this.repositories.departments, {
      preserveSelected: true
    })
  }

  // ===================================
  // Actions
  // ===================================
  @action.bound fetch(props: RouteComponentProps) {
    this.jobPositions.init(props.location)
  }
}
