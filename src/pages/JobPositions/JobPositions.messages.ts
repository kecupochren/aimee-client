import { defineMessages } from 'react-intl'

import { sharedMessages } from '~/messages'

const pageMessages = defineMessages({
  WorkflowStatus: {
    id: 'Workflow.WorkflowStatus',
    defaultMessage: 'Workflow status'
  },
  Approval: {
    id: 'Workflow.Approval',
    defaultMessage: 'Approved'
  },
  InProgress: {
    id: 'Workflow.InProgress',
    defaultMessage: 'In progress'
  },
  NotStarted: {
    id: 'Workflow.NotStarted',
    defaultMessage: 'Not started'
  },
  Rejected: {
    id: 'Workflow.Rejected',
    defaultMessage: 'Rejected'
  },
  Department: {
    id: 'JobPosition.Department',
    defaultMessage: 'Department'
  }
})

export const messages = { ...sharedMessages, ...pageMessages }
