import React, { FC, useEffect } from 'react'
import { observer } from 'mobx-react-lite'
import { RouteComponentProps } from 'react-router'

import { Page } from '~/components'
import { useStore } from '~/store'

import { JobPositionsFilters, JobPositionsActions, JobPositionsTable } from './components'
import { messages } from './JobPositions.messages'

export const JobPositions: FC<RouteComponentProps> = props => {
  const { jobPositions, fetch } = useStore().JobPositionsStore

  useEffect(() => fetch(props), [])

  return (
    <Page
      actions={JobPositionsActions}
      loading={jobPositions.isFirstLoad}
      title={messages.JobPositions}
    >
      <JobPositionsFilters />
      <JobPositionsTable />
    </Page>
  )
}

/* tslint:disable-next-line no-default-export */
export default observer(JobPositions)
