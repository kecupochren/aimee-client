import React from 'react'

import { Button, Link } from '~/components'
import { ROUTES } from '~/constants'

import { messages } from '../JobPositions.messages'

export const JobPositionsActions: React.FC = () => (
  <>
    <Link to={ROUTES.JOB_POSITION_NEW}>
      <Button message={messages.Add} type="primary" />
    </Link>
  </>
)
