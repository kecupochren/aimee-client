import React from 'react'
import { observer } from 'mobx-react'

import { IJobPosition, WorkflowStatus } from '~/api/models'
import { ROUTES, theme } from '~/constants'
import { Text, RepositoryCollectionTable, Icon, Tooltip, Link } from '~/components'
import { JobPositionModel } from '~/store/models'
import { useStore } from '~/store'

import { messages } from '../JobPositions.messages'

export const JobPositionsTable: React.FC = observer(() => (
  <RepositoryCollectionTable<number, IJobPosition, JobPositionModel>
    collection={useStore().JobPositionsStore.jobPositions}
    editPageRoute={ROUTES.JOB_POSITION_EDIT}
    withBaseActions
    withBaseColumns
    columns={[
      {
        title: <Text message={messages.WorkflowStatus} />,
        dataIndex: 'workflowStatus',
        key: 'workflowStatus',
        align: 'center',
        render: renderWorkFlowStatus,
        width: 125
      },
      {
        title: <Text message={messages.Name} />,
        dataIndex: 'name',
        key: 'name',
        sorter: true
      },
      {
        title: <Text message={messages.Department} />,
        dataIndex: 'departmentName',
        key: 'departmentName',
        sorter: true,
        render: (_, item) => {
          if (!item.departmentId) return item.departmentName

          return (
            <Link
              withUnderline
              color="gray"
              to={ROUTES.legacy.DEPARTMENT.replace(':id', item.departmentId.toString())}
            >
              <div>{item.departmentName}</div>
            </Link>
          )
        }
      }
    ]}
  />
))

const renderWorkFlowStatus = (_: any, jobPosition: JobPositionModel) => {
  switch (jobPosition.workflowStatus) {
    case WorkflowStatus.Approved:
      return (
        <Tooltip message={messages.Approval}>
          <Icon type="check" style={{ color: theme.color.success }} />
        </Tooltip>
      )

    case WorkflowStatus.InProgress:
      return (
        <Tooltip message={messages.InProgress}>
          <Icon type="clock-circle" theme="twoTone" />
        </Tooltip>
      )

    case WorkflowStatus.NotStarted:
      return (
        <Tooltip message={messages.NotStarted}>
          <Icon type="hourglass" theme="twoTone" />
        </Tooltip>
      )

    case WorkflowStatus.Rejected:
      return (
        <Tooltip message={messages.Rejected}>
          <Icon type="stop" theme="twoTone" />
        </Tooltip>
      )

    default:
      return null
  }
}
