import React from 'react'
import { observer } from 'mobx-react'

import { Input, Box, Select } from '~/components'
import { useStore } from '~/store'

import { messages } from '../JobPositions.messages'

export const JobPositionsFilters: React.FC = observer(() => {
  const { jobPositions, departments } = useStore().JobPositionsStore

  return (
    <Box mb={12} flex justifyContent="space-between">
      <Input
        onChange={query => jobPositions.search(query)}
        // value={jobPositions.filters.get('Text')}
        placeholder={messages.Search}
        isSearch
        allowClear
        debounce
        flexGrow={1}
        mr={12}
      />

      <Select<number>
        type="controlled"
        optionsFrom="collection"
        collection={departments}
        onChange={(DepartmentId?: number) => jobPositions.fetchWithFilters({ DepartmentId })}
        value={jobPositions.filters.state.get('DepartmentId')}
        placeholder={messages.Department}
        width="250px"
      />
    </Box>
  )
})
