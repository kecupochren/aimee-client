import React, { FC } from 'react'
import { observer } from 'mobx-react-lite'
import { RouteComponentProps } from 'react-router'

import { PageContainer } from '~/components'
import { useMount } from '~/utils'
import { useStore } from '~/store'

export class PlaygroundStore {}

const Playground: FC<RouteComponentProps> = () => {
  // @ts-ignore
  const store = useStore()

  useMount(() => {
    // store.init()
  })

  return <PageContainer>Foo</PageContainer>
}

/* tslint:disable-next-line */
export default observer(Playground)
