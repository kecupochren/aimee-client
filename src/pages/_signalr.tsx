/* tslint:disable no-default-export */

import React, { FC } from 'react'
import { Form, FormRenderProps } from 'react-final-form'

// import { SelectField } from '~/components'
import { useStore } from '~/store'
import { observer } from 'mobx-react-lite'
import * as signalR from '@microsoft/signalr'

const SignalR: FC<any> = () => {
  // const [value, setValue] = React.useState('')
  const store = useStore()

  const connection = new signalR.HubConnectionBuilder()
    // v URL je potřeba volat id recertifikační jednotky, aby server věděl, kterou recertifikační jednotku edituje
    .withUrl(`${store.root.config.apiURL}/recertification-units?recertificationUnitId=123`) // 'http://localhost:12503/recertification-units?recertificationUnitId=123'
    .configureLogging(signalR.LogLevel.Information)
    .withAutomaticReconnect()
    .build()

  // TODO: Types of arguments
  connection.on(
    'RecertificationUnitSavedAsync',
    (/*key: string, statements: any, inputs: any*/) => {
      console.log('signalR: Other user has just saved the recertification unit.')
    }
  )

  connection.on('ConfirmAsync', () => {
    console.log('signalR: Other user has just confirmed the recertification unit.')
  })

  connection.on('UnlockAsync', () => {
    console.log('signalR: Other user has just unlocked current recertification unit.')
  })

  connection.start().then(() => {
    console.log('signalR: connected to server')
    /*
        Podporované metody pro volání na serveru:
        connection.invoke('SaveAsync', key, statemsnts, inputs) // uloží řádek
        connection.invoke('ConfirmAsync') // potvrdí recertifikační jednotku
        connection.invoke('UnlockAsync') // odemkne recertifikační jednotku
      */
  })

  const renderForm = observer(({ handleSubmit }: FormRenderProps) => (
    <form onSubmit={handleSubmit} />
  ))

  return (
    <div>
      <Form
        onSubmit={values => console.log('values', values)}
        initialValues={{}}
        component={renderForm}
      />
    </div>
  )
}

export default observer(SignalR)
