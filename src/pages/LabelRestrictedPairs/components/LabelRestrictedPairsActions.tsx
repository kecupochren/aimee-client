import React from 'react'

import { Button, Link } from '~/components'
import { ROUTES } from '~/constants'
import { messages } from '~/messages'

export const LabelRestrictedPairsActions: React.FC = () => (
  <>
    <Link to={ROUTES.LABEL_RESTRICTED_PAIR_NEW}>
      <Button message={messages.Add} type="primary" />
    </Link>
  </>
)
