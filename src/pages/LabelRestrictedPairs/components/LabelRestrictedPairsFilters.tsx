import React from 'react'
import { observer } from 'mobx-react'

import { Input, Box } from '~/components'
import { messages } from '~/messages'
import { useStore } from '~/store'

export const LabelRestrictedPairsFilters: React.FC = observer(() => {
  const { labelRestrictedPairs } = useStore().LabelRestrictedPairsStore

  return (
    <Box mb={12}>
      <Input
        placeholder={messages.Search}
        isSearch
        allowClear
        // value={labelRestrictedPairs.filters.get('Text')}
        onChange={query => labelRestrictedPairs.search(query)}
        debounce
      />
    </Box>
  )
})
