import React, { FC } from 'react'
import { observer } from 'mobx-react'

import { ILabelRestrictedPair } from '~/api/models'
import { ROUTES } from '~/constants'
import { Text, RepositoryCollectionTable, Link, Tag } from '~/components'
import { messages } from '~/messages'
import { LabelRestrictedPairModel } from '~/store/models'
import { useStore } from '~/store'

export const LabelRestrictedPairsTable: FC = observer(() => (
  <RepositoryCollectionTable<number, ILabelRestrictedPair, LabelRestrictedPairModel>
    collection={useStore().LabelRestrictedPairsStore.labelRestrictedPairs}
    withBaseActions
    withBaseColumns
    editPageRoute={ROUTES.LABEL_RESTRICTED_PAIR_EDIT}
    columns={[
      {
        title: <Text message={messages.FirstLabel} />,
        dataIndex: 'firstLabelName',
        key: 'firstLabelName',
        sorter: true,
        render: (_, item) => (
          <Link to={ROUTES.LABEL_EDIT.replace(':id', item.firstLabelId.toString())}>
            <Tag color="blue">{item.firstLabelName}</Tag>
          </Link>
        )
      },
      {
        title: <Text message={messages.SecondLabel} />,
        dataIndex: 'secondLabelName',
        key: 'secondLabelName',
        sorter: true,
        render: (_, item) => (
          <Link to={ROUTES.LABEL_EDIT.replace(':id', item.secondLabelId.toString())}>
            <Tag color="blue">{item.secondLabelName}</Tag>
          </Link>
        )
      }
    ]}
  />
))
