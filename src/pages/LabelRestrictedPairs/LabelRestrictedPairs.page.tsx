import React, { FC, useEffect } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { observer } from 'mobx-react-lite'

import { Page } from '~/components'
import { useStore } from '~/store'
import { messages } from '~/messages'

import {
  LabelRestrictedPairsFilters,
  LabelRestrictedPairsActions,
  LabelRestrictedPairsTable
} from './components'

export const LabelRestrictedPairs: FC<RouteComponentProps> = props => {
  const { labelRestrictedPairs, fetch } = useStore().LabelRestrictedPairsStore

  useEffect(() => fetch(props), [])

  return (
    <Page
      title={messages.LabelRestrictedPairs}
      actions={LabelRestrictedPairsActions}
      loading={labelRestrictedPairs.isFirstLoad}
    >
      <LabelRestrictedPairsFilters />
      <LabelRestrictedPairsTable />
    </Page>
  )
}

/* tslint:disable-next-line no-default-export */
export default observer(LabelRestrictedPairs)
