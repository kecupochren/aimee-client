import { observable, action } from 'mobx'
import { RouteComponentProps } from 'react-router'

import { ILabelRestrictedPair } from '~/api/models'
import { LabelRestrictedPairModel } from '~/store/models'
import { BaseStore, RepositoryCollection } from '~/store/modules'

export class LabelRestrictedPairsStore extends BaseStore {
  // ===================================
  // Model
  // ===================================
  @observable labelRestrictedPairs: RepositoryCollection<
    number,
    ILabelRestrictedPair,
    LabelRestrictedPairModel
  >

  // ===================================
  // Constructor
  // ===================================
  constructor(app: any) {
    super(app)

    this.labelRestrictedPairs = new RepositoryCollection(
      app,
      this.repositories.labelRestrictedPairs,
      { orderBy: 'firstLabelName' }
    )
  }

  // ===================================
  // Actions
  // ===================================
  @action.bound fetch(props: RouteComponentProps) {
    this.labelRestrictedPairs.init(props.location)
  }
}
