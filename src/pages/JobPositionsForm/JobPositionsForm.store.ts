import { observable, action, computed } from 'mobx'
import { RouteProps } from 'react-router'

import { IJobPositionForSaveVM, IDepartment, IBusinessRole } from '~/api/models'
import { BaseStore, RepositoryCollection } from '~/store/modules'
import { JobPositionModel, DepartmentModel, BusinessRoleModel } from '~/store/models'
import { getIDParam, DuplicateError } from '~/utils'

import { messages } from './JobPositionsForm.messages'
import { ROUTES } from '~/constants'

export class JobPositionsFormStore extends BaseStore {
  // ===================================-
  // Model
  // ===================================-
  // Entity to edit
  @observable jobPosition?: JobPositionModel
  @observable id?: number

  // Options for dropdowns
  @observable departments: RepositoryCollection<number, IDepartment, DepartmentModel>
  @observable businessRoles: RepositoryCollection<number, IBusinessRole, BusinessRoleModel>

  // State
  @observable loading = true
  @observable submitted = false
  @observable completedWorkflowsVisible = false

  // ===================================
  // Constructor
  // ===================================
  constructor(app: any) {
    super(app)

    this.departments = new RepositoryCollection(app, this.repositories.departments)
    this.businessRoles = new RepositoryCollection(app, this.repositories.businessRoles, {
      preserveSelected: true,
      excludeSelected: true
    })
  }

  // ===================================
  // Views
  // ===================================
  @computed get isEditing() {
    return typeof this.id !== 'undefined'
  }

  @computed get pageTitle() {
    return this.id ? messages.EditJobPosition : messages.AddNewRecertification
  }

  @computed get auditTrails() {
    if (!this.jobPosition) return []

    if (this.completedWorkflowsVisible) {
      return this.jobPosition.auditTrails
    }

    return this.jobPosition.auditTrails.filter(x => !x.completed)
  }

  @computed get initialValues() {
    return {
      ...(this.jobPosition as any),
      someDate: new Date()
    }
  }

  // ===================================-
  // Actions
  // ===================================-
  @action.bound async mount(props: RouteProps) {
    this.id = getIDParam(props.location)

    if (!this.id) {
      this.loading = false
      return
    }

    try {
      await this.fetchDetail(Number(this.id))
    } catch (error) {
      this.helpers.notification.handleError(error)
    } finally {
      this.loading = false
    }

    this.completedWorkflowsVisible = false
  }

  @action.bound async fetchDetail(id: number) {
    const jobPosition = await this.repositories.jobPositions.fetchOne(id)

    if (jobPosition.department) {
      this.departments.addItem(jobPosition.department)
    }

    if (jobPosition.businessRoles.length) {
      this.businessRoles.selection.set(jobPosition.businessRoles.map(x => x.id))
    }

    this.jobPosition = jobPosition
  }

  @action.bound addBusinessRole(id: number) {
    this.businessRoles.selection.add(id)
  }

  @action.bound async handleSubmit(values: any) {
    const payload: IJobPositionForSaveVM = {
      name: values.name,
      departmentId: values.departmentId,
      businessRoleIds: this.businessRoles.selection.numIDs,
      workflowNote: '123'
    }

    try {
      if (this.jobPosition) {
        await this.repositories.jobPositions.edit(this.jobPosition.id, payload)
      } else {
        const { id } = await this.repositories.jobPositions.create(payload)

        this.id = id
        this.helpers.router.push(ROUTES.JOB_POSITION_EDIT.replace(':id', String(id)))
        this.fetchDetail(id)
      }
    } catch (error) {
      if (error instanceof DuplicateError) {
        return this.helpers.notification.error({
          message: messages.ErrorDuplicate
        })
      }

      this.helpers.notification.handleError(error)
    }
  }

  @action.bound toggleCompletedWorkflows() {
    this.completedWorkflowsVisible = !this.completedWorkflowsVisible
  }

  @action.bound unmount() {
    this.businessRoles.selection.clear()
    this.id = undefined
    this.jobPosition = undefined
    this.loading = true
  }
}
