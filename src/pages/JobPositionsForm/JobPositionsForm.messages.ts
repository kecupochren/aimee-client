import { defineMessages } from 'react-intl'

import { sharedMessages } from '~/messages'

const pageMessages = defineMessages({
  ErrorDuplicate: {
    id: 'JobPositionsForm.ErrorDuplicate',
    defaultMessage: 'A job position with this name already exists'
  },
  AddNewRecertification: {
    id: 'JobPositionsForm.AddNewJobPosition',
    defaultMessage: 'Add new Job position'
  },
  EditJobPosition: {
    id: 'JobPositionsForm.EditJobPosition',
    defaultMessage: 'Edit Job position'
  },
  BusinessRoles: {
    id: 'JobPosition.BusinessRoles',
    defaultMessage: 'Business roles'
  },
  IncludeComplete: {
    id: 'Workflow.IncludeComplete',
    defaultMessage: 'Include completed workflows'
  },
  Employees: {
    id: 'JobPosition.Employees',
    defaultMessage: 'Employees on this position'
  },
  AddBusinessRolesCaption: {
    id: 'JobPosition.AddBusinessRolesCaption',
    defaultMessage: 'Add business roles'
  },
  AddBusinessRolesPlaceholder: {
    id: 'JobPosition.AddBusinessRolesPlaceholder',
    defaultMessage: '-select business roles-'
  },
  IdentitiesDkxCodeCaption: {
    id: 'JobPosition.IdentitiesDkxCodeCaption',
    defaultMessage: 'DKX'
  },
  IdentitiesGivenNameCaption: {
    id: 'JobPosition.IdentitiesGivenNameCaption',
    defaultMessage: 'Name'
  },
  IdentitiesIdentityTypeCaption: {
    id: 'JobPosition.IdentitiesIdentityTypeCaption',
    defaultMessage: 'Identity type'
  },
  IdentitiesSureNameCaption: {
    id: 'JobPosition.IdentitiesSureNameCaption',
    defaultMessage: 'Surname'
  },
  WorkflowCreated: {
    id: 'Workflow.Created',
    defaultMessage: 'Workflow created'
  },
  WorkflowCurrentStep: {
    id: 'Workflow.CurrentStep',
    defaultMessage: 'Current step'
  },
  WorkflowStatus: {
    id: 'Workflow.Status',
    defaultMessage: 'Status'
  },
  WorkflowOperationsAccept: {
    id: 'WorkflowOperations.Accept',
    defaultMessage: 'Accepted'
  },
  WorkflowOperationsForward: {
    id: 'WorkflowOperations.Forward',
    defaultMessage: 'Forwarded'
  },
  WorkflowOperationsReject: {
    id: 'WorkflowOperations.Reject',
    defaultMessage: 'Rejected'
  },
  WorkflowOperationsReturn: {
    id: 'WorkflowOperations.Return',
    defaultMessage: 'Returned for further inspection'
  },
  WorkflowOperationsAcknowledge: {
    id: 'WorkflowOperations.Acknowledge',
    defaultMessage: 'Acknowledged'
  }
})

export const messages = { ...sharedMessages, ...pageMessages }
