import React, { FC } from 'react'
import { Form, FormRenderProps } from 'react-final-form'
import { observer } from 'mobx-react-lite'
import { RouteProps } from 'react-router'

import {
  IJobPositionForSaveVM,
  IIdentity,
  IWorkflowAuditTrail,
  WorkflowOperation,
  WorkflowStepAction
} from '~/api/models'
import {
  ButtonGroup,
  Button,
  Input,
  Page,
  Select,
  Switch,
  Text,
  ITableColumnProps,
  Table,
  Card,
  Link,
  Icon,
  Box,
  Tooltip,
  TableSelect
} from '~/components'
import { useMount, formatDateLong, formatDate } from '~/utils'
import { useStore } from '~/store'
import { BusinessRoleModel, JobPositionModel } from '~/store/models'

import { messages } from './JobPositionsForm.messages'
import { theme } from '~/constants'

export const JobPositionsForm: React.FC<RouteProps> = props => {
  const { mount, unmount, initialValues, handleSubmit } = useStore().JobPositionsFormStore

  useMount(() => mount(props), unmount)

  return (
    <Form<IJobPositionForSaveVM>
      onSubmit={handleSubmit}
      component={FormComponent}
      initialValues={initialValues}
    />
  )
}

const FormComponent: FC<FormRenderProps<IJobPositionForSaveVM>> = observer(props => {
  const store = useStore()

  const {
    jobPosition,
    departments,
    businessRoles,
    auditTrails,
    loading,
    pageTitle,
    completedWorkflowsVisible,
    toggleCompletedWorkflows,
    isEditing
  } = store.JobPositionsFormStore

  return (
    <form onSubmit={props.handleSubmit}>
      <Page loading={loading} title={pageTitle} actions={<Actions {...props} />}>
        <Card mb={12} title="Pracovní pozice">
          <Input name="name" required label={messages.Name} placeholder={messages.Name} mb={24} />

          <Select
            type="field"
            optionsFrom="collection"
            name="departmentId"
            required
            collection={departments}
            label={messages.Department}
            placeholder={messages.Department}
            mb={24}
          />
        </Card>

        <Card mb={12} title={messages.BusinessRoles.defaultMessage}>
          <TableSelect
            name="businessRoles"
            collection={businessRoles}
            columns={getBusinessRolesColumns()}
            selectLabel={messages.AddBusinessRolesCaption}
            selectPlaceholder={messages.AddBusinessRolesPlaceholder}
          />
        </Card>

        {Boolean(jobPosition && jobPosition.identities.length) && (
          <Card mb={12} title="Identity na této pozici">
            <Table
              dataSource={jobPosition!.identities}
              columns={identitiesColumns}
              noPagination
              size="small"
            />
          </Card>
        )}

        {isEditing && (
          <Card mb={36} title="Workflow">
            <>
              <Box flex justifyContent="space-between">
                <Switch
                  label={messages.IncludeComplete}
                  checked={completedWorkflowsVisible}
                  onChange={toggleCompletedWorkflows}
                  mb={12}
                />
              </Box>

              {Boolean(auditTrails.length) && (
                <Table
                  dataSource={auditTrails}
                  columns={getAuditTrailsColumns(jobPosition!)}
                  noPagination
                  size="small"
                  mb={24}
                />
              )}
            </>
          </Card>
        )}

        <Actions {...props} />
      </Page>
    </form>
  )
})

const Actions: FC<FormRenderProps<IJobPositionForSaveVM>> = observer(props => (
  <ButtonGroup align="end">
    <Button message={messages.Cancel} mr={6} returnButton />

    <Button
      message={messages.Save}
      type="primary"
      htmlType="submit"
      loading={props.submitting}
      disabled={props.pristine}
    />
  </ButtonGroup>
))

const getBusinessRolesColumns = (): ITableColumnProps<BusinessRoleModel>[] => {
  return [
    {
      title: <Text message={messages.Name} />,
      dataIndex: 'name',
      key: 'name'
    },
    {
      title: <Text message={messages.Department} />,
      dataIndex: 'departmentName',
      key: 'departmentName'
    },
    {
      title: <Text message={messages.Description} />,
      dataIndex: 'description',
      key: 'description',
      width: '50%'
    }
  ]
}

const identitiesColumns: ITableColumnProps<IIdentity>[] = [
  {
    title: <Text message={messages.IdentitiesDkxCodeCaption} />,
    dataIndex: 'dkxExternalCode',
    key: 'dkxExternalCode'
  },
  {
    title: <Text message={messages.IdentitiesGivenNameCaption} />,
    dataIndex: 'givenName',
    key: 'givenName'
  },
  {
    title: <Text message={messages.IdentitiesSureNameCaption} />,
    dataIndex: 'surname',
    key: 'surname'
  },
  {
    title: <Text message={messages.IdentitiesIdentityTypeCaption} />,
    dataIndex: 'identityType',
    key: 'identityType'
  }
]

const getAuditTrailsColumns = (
  jobPosition: JobPositionModel
): ITableColumnProps<IWorkflowAuditTrail>[] => [
  {
    title: <Text message={messages.WorkflowStatus} />,
    dataIndex: 'workflowStepAction',
    key: 'workflowStepAction',
    align: 'center',
    render: (_, item) => {
      switch (item.workflowOperationId) {
        case WorkflowOperation.AcceptId:
          return (
            <Tooltip message={messages.WorkflowOperationsAccept}>
              <Icon type="check" color={theme.color.success} />
            </Tooltip>
          )

        case WorkflowOperation.ForwardId:
          return (
            <Tooltip message={messages.WorkflowOperationsForward}>
              <Icon type="forward" color={theme.color.primaryLight} size={18} />
            </Tooltip>
          )

        case WorkflowOperation.ReturnId:
          return (
            <Tooltip message={messages.WorkflowOperationsReturn}>
              <Icon type="rollback" color={theme.color.primaryLight} />
            </Tooltip>
          )

        case WorkflowOperation.RejectId:
          return (
            <Tooltip message={messages.WorkflowOperationsReject}>
              <Icon type="minus-circle" theme="twoTone" />
            </Tooltip>
          )

        default:
          return (
            <Tooltip message={messages.WorkflowOperationsAcknowledge}>
              <Icon type="clock-circle" theme="twoTone" />
            </Tooltip>
          )
      }
    }
  },
  {
    title: <Text message={messages.Name} />,
    dataIndex: 'name',
    key: 'name'
  },
  {
    title: <Text message={messages.WorkflowCreated} />,
    dataIndex: 'created',
    key: 'created',
    render: (_, item) => (
      <Tooltip overlay={formatDateLong(item.created.toString())}>
        {formatDate(item.created.toString())} {item.createdBy}
      </Tooltip>
    )
  },
  {
    title: <Text message={messages.WorkflowCurrentStep} />,
    dataIndex: 'currentWorkflowStep',
    key: 'currentWorkflowStep'
  },
  {
    title: <Text message={messages.Action} />,
    key: 'action',
    align: 'right',
    render: (_, item) => {
      return (
        <Tooltip message={messages.Edit}>
          <Link noHoverStyling href={getWorkflowDetailUrl(item, jobPosition.id)}>
            <Icon type="edit" />
          </Link>
        </Tooltip>
      )
    }
  }
]

const getWorkflowDetailUrl = (trail: IWorkflowAuditTrail, jobPositionId: number) => {
  switch (trail.workflowStepAction) {
    case WorkflowStepAction.JobPositionCreate:
      return `/BusinessCodebooks/JobPosition/WorkflowDetailCreate/${jobPositionId}`
    case WorkflowStepAction.JobPositionModify:
      return `/BusinessCodebooks/JobPositionsModifyWorkflow/Index/${jobPositionId}/${trail.entityChangeSetId}` // prettier-ignore
    case WorkflowStepAction.JobPositionDelete:
      return `/BusinessCodebooks/JobPosition/WorkflowDetailDelete/${jobPositionId}`
  }
}

/* tslint:disable-next-line no-default-export */
export default observer(JobPositionsForm)
