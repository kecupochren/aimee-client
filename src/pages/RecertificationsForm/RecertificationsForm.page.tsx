import React, { FC } from 'react'
import { Form, FormRenderProps } from 'react-final-form'
import { observer } from 'mobx-react-lite'
import { RouteProps } from 'react-router'

import { ROUTES } from '~/constants'
import { ButtonGroup, Button, Input, Page, Card, DatepickerField } from '~/components'
import { useStore } from '~/store'
import { useMount, validator, dateInFuture, dateAfter } from '~/utils'

import { messages } from './RecertificationsForm.messages'
import { SystemAppsTable } from './components'

enum Fields {
  name = 'name',
  finalizationDate = 'finalizationDate',
  planOfMeasuresDate = 'planOfMeasuresDate',
  referenceDate = 'referenceDate'
}

export interface IFormFields {
  [key: string]: any
  [Fields.name]: string
  [Fields.finalizationDate]: Date | string
  [Fields.planOfMeasuresDate]: Date | string
  [Fields.referenceDate]: Date | string
}

const validate = validator({
  [Fields.referenceDate]: dateInFuture,
  [Fields.planOfMeasuresDate]: [
    dateInFuture,
    dateAfter(Fields.referenceDate, messages.PlanOfMeasuresDateAfterReferenceDate)
  ],
  [Fields.finalizationDate]: [
    dateAfter(Fields.planOfMeasuresDate, messages.FinalizationDateAfterPlanOfMeasuresDate)
  ]
})

// ===================================
// Main
// ===================================
export const RecertificationsForm: FC<RouteProps> = props => {
  const { mount, unmount, recertification, handleSubmit } = useStore().RecertificationFormStore

  useMount(() => mount(props), unmount)

  return (
    <Form<IFormFields>
      onSubmit={handleSubmit}
      component={FormComponent}
      initialValues={recertification}
      validate={validate}
    />
  )
}

// ===================================
// Components
// ===================================
const FormComponent: FC<FormRenderProps<IFormFields>> = observer(props => {
  const {
    loading,
    submitting,
    pageTitle,
    recertification,
    creatingState,
    isInInitialState
  } = useStore().RecertificationFormStore

  return (
    <form onSubmit={props.handleSubmit}>
      <Page
        loading={loading}
        loadingText={creatingState}
        title={pageTitle}
        actions={
          <Actions
            {...props}
            loading={loading}
            submitting={submitting}
            isInInitialState={isInInitialState}
          />
        }
      >
        <Card mb={24}>
          <Input
            name={Fields.name}
            required
            label={messages.Name}
            placeholder={messages.Name}
            mb={24}
          />

          <DatepickerField
            name={Fields.referenceDate}
            required
            label={messages.ReferenceDate}
            placeholder={messages.ReferenceDate}
            mb={24}
          />

          <DatepickerField
            name={Fields.planOfMeasuresDate}
            required
            label={messages.PlanOfMeasuresDate}
            placeholder={messages.PlanOfMeasuresDate}
            mb={24}
          />

          <DatepickerField
            name={Fields.finalizationDate}
            required
            label={messages.FinalizationDate}
            placeholder={messages.FinalizationDate}
            mb={24}
          />

          <SystemAppsTable />

          {!isInInitialState && (
            <Button
              message={messages.RecertificationUnitsList}
              to={`${ROUTES.RECERTIFICATION_UNITS.replace(':id', recertification!.id.toString())}`}
            />
          )}

          <Actions
            {...props}
            loading={loading}
            submitting={submitting}
            isInInitialState={isInInitialState}
          />
        </Card>
      </Page>
    </form>
  )
})

const Actions: FC<FormRenderProps<IFormFields> & {
  loading: boolean
  submitting: boolean
  isInInitialState: boolean
}> = observer(props => {
  const {
    canOpenRecertification,
    openRecertification,
    canFinalizeRecertification,
    finalizeRecertification,
    handleRecertificationExportChanges,
    handleRecertificationExportSummary,
    canExportChanges,
    canExportSummary,
    uploadedFiles
  } = useStore().RecertificationFormStore

  const hasChanges = !props.pristine || uploadedFiles

  return (
    <ButtonGroup align="end">
      {canExportSummary && (
        <Button
          message={messages.RecertificationSummaryExport}
          ml={12}
          onClick={handleRecertificationExportSummary}
          disabled={props.submitting || props.loading}
          loading={props.submitting}
        />
      )}

      {canExportChanges && (
        <Button
          message={messages.RecertificationChangeExport}
          ml={12}
          onClick={handleRecertificationExportChanges}
          disabled={props.submitting || props.loading}
          loading={props.submitting}
        />
      )}

      {canOpenRecertification && (
        <Button
          message={messages.OpenRecertification}
          type="primary"
          ml={12}
          onClick={openRecertification}
          loading={props.submitting}
        />
      )}

      {canFinalizeRecertification && (
        <Button
          message={messages.FinalizeRecertification}
          type="primary"
          ml={12}
          onClick={finalizeRecertification}
          loading={props.submitting}
        />
      )}

      <Button
        message={messages.Cancel}
        ml={12}
        returnButton
        disabled={props.submitting || props.loading}
        loading={props.submitting}
      />

      <Button
        message={messages.Save}
        type="primary"
        ml={12}
        htmlType="submit"
        disabled={props.submitting || props.loading || !hasChanges}
        loading={props.submitting}
      />
    </ButtonGroup>
  )
})

/* tslint:disable-next-line no-default-export */
export default observer(RecertificationsForm)
