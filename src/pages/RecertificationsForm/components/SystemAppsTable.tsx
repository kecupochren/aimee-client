import React, { FC } from 'react'
import { ColumnProps } from 'antd/lib/table'
import { observer } from 'mobx-react-lite'

import {
  RepositoryCollectionTable,
  Text,
  FileUpload,
  Box,
  Icon,
  Collapse,
  Panel,
  Table
} from '~/components'
import { useStore } from '~/store'
import { SystemApplicationModel } from '~/store/models'
import { formatDateLong } from '~/utils'

import { ISystemApplicationRow } from '../RecertificationsForm.store'
import { messages } from '../RecertificationsForm.messages'

interface IProps {}

// ===================================
// Main
// ===================================
export const SystemAppsTable: FC<IProps> = observer(() => {
  const {
    isInInitialState,
    recertification,
    systemApplications,
    isEditing
  } = useStore().RecertificationFormStore

  const tableProps = {
    withBackground: true,
    withBorder: true,
    noPagination: true,
    mb: 24
  }

  const columns: ColumnProps<ISystemApplicationRow>[] = [
    {
      title: <Text message={messages.Name} />,
      dataIndex: 'name',
      key: 'name'
    }
  ]

  if (isInInitialState) {
    if (isEditing) {
      columns.push({
        title: <Text message={messages.UploadFile} />,
        render: (_, row) => <FileUploadDataCell row={row} />
      })

      columns.push({
        title: <Text message={messages.UploadFile} />,
        render: (_, row) => <UploadDetailDataCell row={row} />
      })

      columns.push({
        title: <Text message={messages.ErrorsFound} />,
        render: (_, row) => <MissingEntitiesDataCell row={row} />
      })
    }

    return (
      <RepositoryCollectionTable<number, ISystemApplicationRow, SystemApplicationModel>
        collection={systemApplications}
        columns={columns}
        size="middle"
        noMeta
        {...tableProps}
      />
    )
  }

  if (!(systemApplications && recertification)) {
    return <></>
  }

  if (isEditing) {
    columns.push({
      title: <Text message={messages.UploadFile} />,
      render: (_, row) => <UploadDetailDataCell row={row} />
    })

    columns.push({
      title: <Text message={messages.ErrorsFound} />,
      render: (_, row) => <MissingEntitiesDataCell row={row} />
    })
  }

  return (
    <Table columns={columns} dataSource={recertification!.systemApplications} {...tableProps} />
  )
})

// ===================================
// Cells
// ===================================
interface ICellProps {
  row: ISystemApplicationRow
}

const FileUploadDataCell: FC<ICellProps> = observer(({ row }) => {
  const {
    handleFileUpload,
    systemApplications,
    isInInitialState
  } = useStore().RecertificationFormStore

  const isRowSelected = systemApplications.selection.get(row.id)
  const isFileUploadEnabled = isInInitialState ? isRowSelected : true

  return (
    <span style={{ marginRight: '40px' }}>
      <FileUpload
        disabled={!isFileUploadEnabled}
        onDrop={files => handleFileUpload(files, row)}
        accept=".xlsx,.xls"
        multiple={false}
        message={messages.UploadFile}
      />
    </span>
  )
})

const UploadDetailDataCell: FC<ICellProps> = observer(({ row }) => {
  const { systemApplications } = useStore().RecertificationFormStore

  if (!row.hasImportedData) return null

  const isRowSelected = systemApplications.selection.get(row.id)
  const iconType = row.areImportedDataValid ? 'check' : 'close'

  return (
    <span>
      {row.hasImportedData && isRowSelected && (
        <Box flex alignItems="center">
          <Icon inline type={iconType} />
          <Text ml={10} message={messages.FileInsertedBy} />
          &nbsp;{row.importedBy}, {formatDateLong(row.imported.toString())}
        </Box>
      )}
    </span>
  )
})

const MissingEntitiesDataCell: FC<ICellProps> = observer(({ row }) => {
  const { systemApplications } = useStore().RecertificationFormStore
  const isRowSelected = systemApplications.selection.get(row.id)

  if (!isRowSelected || !row.hasImportedData) return null

  const missingEmployees = row.missingEmployeesDkx
  const missingRoles = row.missingRoleCodes

  const hasMissingEmployees = missingEmployees.length > 0
  const hasMissingRoles = missingRoles.length > 0

  if (!hasMissingEmployees && !hasMissingRoles) {
    return <Text message={messages.FileInsertedNoErrors} ml="20px" />
  }

  const dataCellHeaderProps = {
    missingEmployees,
    missingRoles,
    hasMissingEmployees,
    hasMissingRoles
  }

  const panelStyle = {
    border: '0'
  }

  return (
    <Collapse bordered={false}>
      <Panel
        key="collapsePanel"
        header={<DataCellHeader {...dataCellHeaderProps} />}
        style={panelStyle}
      >
        {hasMissingEmployees && (
          <Text mr={40}>
            <EmployeesCount count={missingEmployees.length} />
            {missingEmployees.map((empl, index) => (
              <div key={index}>{empl}</div>
            ))}
          </Text>
        )}

        {hasMissingRoles && (
          <Text mr={40}>
            <RolesCount count={missingRoles.length} />
            {missingRoles.map((empl, index) => (
              <div key={index}>{empl}</div>
            ))}
          </Text>
        )}
      </Panel>
    </Collapse>
  )
})

interface IDataCellHeaderProps {
  missingEmployees: string[]
  missingRoles: string[]
  hasMissingEmployees: boolean
  hasMissingRoles: boolean
}

const DataCellHeader: FC<IDataCellHeaderProps> = observer(props => {
  const { missingEmployees, missingRoles, hasMissingEmployees, hasMissingRoles } = props

  if (hasMissingEmployees && hasMissingRoles) {
    return (
      <ErrorBox>
        <EmployeesCount count={missingEmployees.length} />
        <br />
        <RolesCount count={missingRoles.length} />
      </ErrorBox>
    )
  }

  if (hasMissingEmployees) {
    return (
      <ErrorBox>
        <EmployeesCount count={missingEmployees.length} />
      </ErrorBox>
    )
  }

  if (hasMissingRoles) {
    return (
      <ErrorBox>
        <RolesCount count={missingRoles.length} />
      </ErrorBox>
    )
  }

  return null
})

const EmployeesCount: FC<{ count: number }> = observer(({ count }) => (
  <>
    <Text message={messages.MissingEmployees} /> {count}
  </>
))

const RolesCount: FC<{ count: number }> = observer(({ count }) => (
  <>
    <Text message={messages.MissingRoles} /> {count}
  </>
))

const ErrorBox: FC = observer(({ children }) => (
  <Box mt={-12} mb={-12}>
    {children}
  </Box>
))
