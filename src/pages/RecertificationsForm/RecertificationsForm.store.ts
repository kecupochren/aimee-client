import { observable, action, computed, runInAction } from 'mobx'
import { RouteProps } from 'react-router'
import { FileWithPath } from 'file-selector'
import * as signalR from '@microsoft/signalr'

import { ROUTES } from '~/constants'
import {
  RecertificationSystemApplicationVM,
  RecertificationState
} from '~/api/apimodels/ViewModels'
import { RecertificationForSaveVM } from '~/api/apimodels/Recertifications'
import { BaseStore, RepositoryCollection } from '~/store/modules'
import { RecertificationModel, SystemApplicationModel, ISystemApplication } from '~/store/models'
import { getIDParam } from '~/utils'

import { messages } from './RecertificationsForm.messages'
import { IFormFields } from './RecertificationsForm.page'
import { sharedMessages } from '~/messages'
import { downloadFile } from '~/utils/downloadFile'

export class RecertificationsFormStore extends BaseStore {
  // ===================================-
  // Model
  // ===================================-
  @observable id?: number
  @observable recertification?: RecertificationModel
  @observable systemApplications: RepositoryCollection<
    number,
    ISystemApplication,
    SystemApplicationModel
  >

  @observable loading = true
  @observable submitted = false
  @observable uploadedFiles = false
  @observable creatingState: string = ''

  private signalRConnection: signalR.HubConnection

  // ===================================
  // Constructor
  // ===================================
  constructor(app: any) {
    super(app)

    this.systemApplications = new RepositoryCollection(app, this.repositories.systemApplications, {
      pageSize: 999
    })
  }

  // ===================================
  // Views
  // ===================================
  @computed get isEditing() {
    return typeof this.id !== 'undefined'
  }

  @computed get pageTitle() {
    return this.isEditing ? messages.EditRecertification : messages.AddNewRecertification
  }

  @computed get submitting() {
    return this.loading || this.submitted
  }

  @computed get canEditSystemApplications(): boolean {
    return this.recertification?.recertificationState === RecertificationState.Initial
  }

  @computed get canOpenRecertification(): boolean {
    if (this.recertification) {
      const allDataValid = this.recertification.systemApplications.every(
        sa => sa.hasImportedData && sa.areImportedDataValid
      )

      return allDataValid && this.canEditSystemApplications
    }

    return false
  }

  @computed get canFinalizeRecertification(): boolean {
    return this.recertification?.recertificationState === RecertificationState.Confirmed
  }

  @computed get canExportChanges(): boolean {
    if (this.recertification) {
      return this.recertification.recertificationState !== RecertificationState.Initial
    }
    return false
  }

  @computed get canExportSummary(): boolean {
    if (this.recertification) {
      return this.recertification.recertificationState === RecertificationState.Finalized
    }
    return false
  }

  @computed get isInInitialState(): boolean {
    return (
      this.recertification?.recertificationState == null ||
      this.recertification?.recertificationState === RecertificationState.Initial
    )
  }

  // ===================================
  // Helpers
  // ===================================
  @action.bound async fetchRecertification(id: number) {
    this.recertification = await this.repositories.recertifications.fetchOne(id)
    this.systemApplications.selection.set(this.recertification.systemApplications.map(x => x.id))
  }

  @action.bound async fetchSystemApps() {
    this.systemApplications.handleFetch()
  }

  @action.bound async connectoToSignalR() {
    this.signalRConnection = new signalR.HubConnectionBuilder()
      .withUrl(`${this.helpers.env.serverApiURL}/recertifications`)
      .configureLogging(signalR.LogLevel.Trace)
      .withAutomaticReconnect()
      .build()

    this.signalRConnection.onclose((error?: Error | undefined) => {
      if (error) {
        this.helpers.notification.handleError(error)
      }
    })

    this.signalRConnection.on('CreatingDone', (recertificationId: number) => {
      this.helpers.notification.success({ message: messages.RecertificationWasCreated })
      this.id = recertificationId
      this.helpers.router.push(ROUTES.RECERTIFICATION_EDIT.replace(':id', this.id!.toString()))
      this.creatingState = ''
      this.loading = false
      this.submitted = false
      try {
        this.fetchRecertification(this.id!)
      } catch (error) {
        this.helpers.notification.handleError(error)
      }
    })

    this.signalRConnection.on('Error', (error: string) => {
      this.helpers.notification.error({ message: error })
    })

    try {
      await this.signalRConnection.start()
      console.log('signalR: connected to server')
    } catch (error) {
      this.helpers.notification.error({ message: sharedMessages.UnknownErrorOccured })
    }
  }

  @action.bound async uploadSystemAppFile(formData: {
    recertificationId: number
    systemApplicationId: number
    file: any
  }) {
    if (formData.file == null) {
      return
    }

    try {
      const fd = new FormData()
      fd.append('file', formData.file)
      fd.append('systemApplicationId', String(formData.systemApplicationId))
      fd.append('recertificationId', String(formData.recertificationId))

      const { data } = await this.helpers.api.uploadSystemAppFile(fd)
      if (this.recertification) {
        runInAction(async () => {
          await this.fetchRecertification(this.recertification!.id)
          const index = this.recertification!.systemApplications.findIndex(
            x => x.systemApplicationId === data.systemApplicationId
          )

          runInAction(async () => {
            const sa = this.recertification!.systemApplications[index]
            if (sa) {
              sa.updateData(data)
            }
            this.uploadedFiles = true
          })
        })
      }
    } catch (error) {
      this.helpers.notification.handleError(error)
    } finally {
      this.loading = false
    }
  }

  // ===================================-
  // Actions
  // ===================================-
  @action.bound async handleFileUpload(files: FileWithPath[], row: ISystemApplicationRow) {
    this.uploadSystemAppFile({
      recertificationId: this.recertification!.id,
      systemApplicationId: row.id,
      file: files[0]
    })
  }

  @action.bound async handleRecertificationExportChanges() {
    try {
      this.submitted = true
      const { data } = await this.helpers.api.getRecertificationExportChanges(this.id!.toString())
      downloadFile(data, 'RecertificationChangeExport.xlsx', 'text/xlsx')
    } finally {
      runInAction(() => {
        this.submitted = false
      })
    }
  }

  @action.bound async handleRecertificationExportSummary() {
    try {
      this.submitted = true
      const { data } = await this.helpers.api.getRecertificationExportSummary(this.id!.toString())
      downloadFile(data, 'RecertificationSummaryExport.xlsx', 'text/xlsx')
    } finally {
      runInAction(() => {
        this.submitted = false
      })
    }
  }

  @action.bound async handleSubmit(data: IFormFields) {
    const payload: RecertificationForSaveVM = {
      name: data.name,
      referenceDate: new Date(data.referenceDate),
      planOfMeasuresDate: new Date(data.planOfMeasuresDate),
      finalizationDate: new Date(data.finalizationDate),
      systemApplicationIds: this.systemApplications.selection.numIDs
    }

    if (this.isEditing) {
      try {
        await this.repositories.recertifications.edit(this.recertification!.id, payload)
        await this.fetchRecertification(this.recertification!.id)
      } catch (error) {
        this.helpers.notification.handleError(error)
      }
    } else {
      runInAction(() => {
        this.loading = true
        this.submitted = true
      })

      try {
        this.signalRConnection.stream('CreateAsync', payload).subscribe({
          next: (state: string) => {
            this.creatingState = state
          },
          complete: () => {
            runInAction(() => {
              this.loading = false
              this.submitted = false
              this.creatingState = ''
            })
          },
          error: err => {
            this.helpers.notification.handleError(err)
          }
        })
      } catch (error) {
        this.helpers.notification.handleError(error)
      }
    }
  }

  @action.bound async openRecertification() {
    try {
      this.submitted = true
      this.loading = true
      this.recertification = await this.repositories.recertifications.open(
        this.recertification!.id.toString()
      )
    } finally {
      runInAction(() => {
        this.submitted = false
        this.loading = false
      })
    }
  }

  @action.bound async finalizeRecertification() {
    try {
      this.submitted = true
      this.loading = true
      this.recertification = await this.repositories.recertifications.finalize(
        this.recertification!.id.toString()
      )
    } finally {
      runInAction(() => {
        this.submitted = false
        this.loading = false
      })
    }
  }

  // ===================================
  // Lifecycle
  // ===================================
  @action.bound async mount(props: RouteProps) {
    this.id = getIDParam(props.location)

    try {
      await this.fetchSystemApps()

      if (this.isEditing) {
        await this.fetchRecertification(this.id!)
      } else {
        await this.connectoToSignalR()
      }
    } catch (error) {
      this.helpers.notification.handleError(error)
    } finally {
      this.loading = false
    }
  }

  @action.bound unmount() {
    this.id = undefined
    this.loading = true
    this.uploadedFiles = false
    this.recertification = undefined
    this.signalRConnection?.stop()
    this.systemApplications.selection.clear()
  }
}

export interface ISystemApplicationRow extends SystemApplicationModel {
  details?: RecertificationSystemApplicationVM
}
