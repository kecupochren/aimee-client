import { defineMessages } from 'react-intl'

import { sharedMessages } from '~/messages'

const pageMessages = defineMessages({
  RecertificationSummaryExport: {
    id: 'Recertification.ChangeSummary',
    defaultMessage: 'Recertification summary export'
  },
  RecertificationChangeExport: {
    id: 'Recertification.ChangeExport',
    defaultMessage: 'Recertification change export'
  },
  PlanOfMeasuresDate: {
    id: 'Recertification.PlanOfMeasuresDate',
    defaultMessage: 'Plan of measures date'
  },
  FinalizationDate: {
    id: 'Recertification.FinalizationDate',
    defaultMessage: 'Finalization date'
  },
  ReferenceDate: {
    id: 'Recertification.ReferenceDate',
    defaultMessage: 'Reference date'
  },
  AddNewRecertification: {
    id: 'RecertificationForm.AddNewRecertification',
    defaultMessage: 'Add new recertification'
  },
  EditRecertification: {
    id: 'RecertificationsForm.EditRecertification',
    defaultMessage: 'Edit recertification'
  },
  PlanOfMeasuresDateAfterReferenceDate: {
    id: 'Recertification.ErrorReferenceDateCannotBeGraterThanPlanOfMeasuresDate',
    defaultMessage: 'Reference date cannot be smaller than Plan of measures date'
  },
  FinalizationDateAfterPlanOfMeasuresDate: {
    id: 'Recertification.ErrorPlanOfMeasuresDateCannotBeGraterThanFinalizationDate',
    defaultMessage: 'Plan of measures date cannot be smaller than Finalization date'
  },
  FileInsertedNoErrors: {
    id: 'Recertification.FileInsertedNoErrors',
    defaultMessage: 'No errors'
  },
  FileInsertedBy: {
    id: 'Recertification.FileInsertedBy',
    defaultMessage: 'File inserted by '
  },
  MissingRoles: {
    id: 'Recertification.MissingRoles',
    defaultMessage: 'Missing roles'
  },
  MissingEmployees: {
    id: 'Recertification.MissingEmployees',
    defaultMessage: 'Missing employees'
  },
  ErrorsFound: {
    id: 'Recertification.ErrorsFound',
    defaultMessage: 'Errors found'
  },
  OpenRecertification: {
    id: 'Recertification.OpenRecertification',
    defaultMessage: 'Open recertification'
  },
  FinalizeRecertification: {
    id: 'Recertification.FinalizeRecertification',
    defaultMessage: 'Finish recertification'
  },
  RecertificationWasCreated: {
    id: 'Recertification.RecertificationWasCreated',
    defaultMessage: 'Recertification was created'
  },
  RecertificationOpened: {
    id: 'Recertification.RecertificationOpened',
    defaultMessage: 'Recertification was opened.'
  },
  RecertificationFinalized: {
    id: 'Recertification.RecertificationFinalized',
    defaultMessage: 'Recertification was finished.'
  }
})

export const messages = { ...sharedMessages, ...pageMessages }
