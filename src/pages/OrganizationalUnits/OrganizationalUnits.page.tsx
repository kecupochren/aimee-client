import React, { FC, useEffect } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { observer } from 'mobx-react-lite'

import { Page } from '~/components'
import { useStore } from '~/store'
import { messages } from '~/messages'

import {
  OrganizationalUnitsFilters,
  OrganizationalUnitsActions,
  OrganizationalUnitsTable
} from './components'

export const OrganizationalUnits: FC<RouteComponentProps> = props => {
  const { organizationalUnits, fetch } = useStore().OrganizationalUnitsStore

  useEffect(() => fetch(props), [])

  return (
    <Page
      title={messages.OrgUnits}
      actions={OrganizationalUnitsActions}
      loading={organizationalUnits.isFirstLoad}
    >
      <OrganizationalUnitsFilters />
      <OrganizationalUnitsTable />
    </Page>
  )
}

/* tslint:disable-next-line no-default-export */
export default observer(OrganizationalUnits)
