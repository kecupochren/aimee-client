import { observable, action } from 'mobx'
import { RouteComponentProps } from 'react-router'

import { IOrganizationalUnit } from '~/api/models'
import { OrganizationalUnitModel } from '~/store/models'
import { BaseStore, RepositoryCollection } from '~/store/modules'

export class OrganizationalUnitsStore extends BaseStore {
  // ===================================
  // Model
  // ===================================
  @observable organizationalUnits: RepositoryCollection<
    number,
    IOrganizationalUnit,
    OrganizationalUnitModel
  >

  // ===================================
  // Constructor
  // ===================================
  constructor(app: any) {
    super(app)
    this.organizationalUnits = new RepositoryCollection(app, this.repositories.organizationalUnits)
  }

  // ===================================
  // Actions
  // ===================================
  @action.bound fetch(props: RouteComponentProps) {
    this.organizationalUnits.init(props.location)
  }
}
