import { defineMessages } from 'react-intl'

import { sharedMessages } from '~/messages'

const pageMessages = defineMessages({})

export const messages = { ...sharedMessages, ...pageMessages }
