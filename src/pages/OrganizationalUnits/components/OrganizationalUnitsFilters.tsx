import React from 'react'
import { observer } from 'mobx-react'

import { Input, Box } from '~/components'
import { useStore } from '~/store'

import { messages } from '../OrganizationalUnits.messages'

export const OrganizationalUnitsFilters: React.FC = observer(() => {
  const { organizationalUnits } = useStore().OrganizationalUnitsStore

  return (
    <Box mb={12}>
      <Input
        placeholder={messages.Search}
        isSearch
        allowClear
        // value={organizationalUnits.filters.get('Text')}
        onChange={query => organizationalUnits.search(query)}
        debounce
      />
    </Box>
  )
})
