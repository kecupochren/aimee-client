import React from 'react'

import { Button, Link } from '~/components'
import { ROUTES } from '~/constants'

import { messages } from '../OrganizationalUnits.messages'

export const OrganizationalUnitsActions: React.FC = () => (
  <>
    <Link to={ROUTES.ORGANIZATIONAL_UNIT_NEW}>
      <Button message={messages.Add} type="primary" />
    </Link>
  </>
)
