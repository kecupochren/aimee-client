import React, { FC } from 'react'
import { observer } from 'mobx-react'

import { IOrganizationalUnit } from '~/api/models'
import { ROUTES } from '~/constants'
import { Text, Link, RepositoryCollectionTable, Tag } from '~/components'
import { OrganizationalUnitModel } from '~/store/models'
import { useStore } from '~/store'

import { messages } from '../OrganizationalUnits.messages'

/**
 * Main
 */
export const OrganizationalUnitsTable: FC = observer(() => (
  <RepositoryCollectionTable<number, IOrganizationalUnit, OrganizationalUnitModel>
    collection={useStore().OrganizationalUnitsStore.organizationalUnits}
    withBaseActions
    withBaseColumns
    editPageRoute={ROUTES.ORGANIZATIONAL_UNIT_EDIT}
    columns={[
      {
        title: <Text message={messages.Name} />,
        dataIndex: 'name',
        key: 'name',
        sorter: true
      },
      {
        title: <Text message={messages.Labels} />,
        dataIndex: 'labels',
        key: 'labels',
        width: '25%',
        render: (_, orgUnit) =>
          orgUnit.labels.map(label => {
            const id = label.id.toString()
            const route = `${ROUTES.LABEL_EDIT.replace(':id', id)}`

            return (
              <Link key={id} to={route}>
                <Tag color="blue">{label.name}</Tag>
              </Link>
            )
          })
      }
    ]}
  />
))
