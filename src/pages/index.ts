import { lazyLoadPage } from '~/utils'

// @ts-ignore
import * as api from '../api'

// prettier-ignore
export const Pages = {
  Playground: lazyLoadPage(() => import('./_playground')),
  SignalR: lazyLoadPage(() => import('./_signalr')),

  Labels: lazyLoadPage(() => import('./Labels/Labels.page')),
  LabelsForm: lazyLoadPage(() => import('./LabelsForm/LabelsForm.page')),

  JobPositions: lazyLoadPage(() => import('./JobPositions/JobPositions.page')),
  JobPositionsForm: lazyLoadPage(() => import('./JobPositionsForm/JobPositionsForm.page')),

  LabelRestrictedPairs: lazyLoadPage(() => import('./LabelRestrictedPairs/LabelRestrictedPairs.page')),
  LabelRestrictedPairsForm: lazyLoadPage(() => import('./LabelRestrictedPairsForm/LabelRestrictedPairsForm.page')),

  OrganizationalUnits: lazyLoadPage(() => import('./OrganizationalUnits/OrganizationalUnits.page')),
  OrganizationalUnitsForm: lazyLoadPage(() => import('./OrganizationalUnitsForm/OrganizationalUnitsForm.page')),

  Recertifications: lazyLoadPage(() => import('./Recertifications/Recertifications.page')),
  RecertificationsForm: lazyLoadPage(() => import('./RecertificationsForm/RecertificationsForm.page')),

  RecertificationUnits: lazyLoadPage(() => import('./RecertificationUnits/RecertificationUnits.page')),
  RecertificationUnitsForm: lazyLoadPage(() => import('./RecertificationUnitsForm/RecertificationUnitsForm.page')),

  // NotFound: lazyLoadPage(() => import('./NotFound/NotFound.page'))
}
