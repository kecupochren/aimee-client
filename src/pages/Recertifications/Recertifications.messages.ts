import { defineMessages } from 'react-intl'

import { sharedMessages } from '~/messages'

const pageMessages = defineMessages({
  PlanOfMeasuresDate: {
    id: 'Recertification.PlanOfMeasuresDate',
    defaultMessage: 'Plan of measures date'
  },
  FinalizationDate: {
    id: 'Recertification.FinalizationDate',
    defaultMessage: 'Finalization date'
  },
  RecertificationState: {
    id: 'Recertification.RecertificationState',
    defaultMessage: 'Recertification state'
  },
  ReferenceDate: {
    id: 'Recertification.ReferenceDate',
    defaultMessage: 'Reference date'
  }
})

export const messages = { ...sharedMessages, ...pageMessages }
