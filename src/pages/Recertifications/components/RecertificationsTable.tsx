import React from 'react'
import { observer } from 'mobx-react'

import { ROUTES } from '~/constants'
import { Text, RepositoryCollectionTable, renderDateValue } from '~/components'
import { RecertificationModel, IRecertification } from '~/store/models'
import { useStore } from '~/store'

import { messages } from '../Recertifications.messages'

export const RecertificationsTable: React.FC = observer(() => (
  <RepositoryCollectionTable<number, IRecertification, RecertificationModel>
    collection={useStore().RecertificationStore.recertifications}
    editPageRoute={ROUTES.RECERTIFICATION_EDIT}
    withBaseActions
    columns={[
      {
        title: <Text message={messages.Name} />,
        dataIndex: 'name',
        key: 'name',
        sorter: true
      },
      {
        title: <Text message={messages.ReferenceDate} />,
        dataIndex: 'referenceDate',
        key: 'referenceDate',
        sorter: true,
        render: (_, model) => renderDateValue(model.referenceDate.toString())
      },
      {
        title: <Text message={messages.PlanOfMeasuresDate} />,
        dataIndex: 'planOfMeasuresDate',
        key: 'planOfMeasuresDate',
        sorter: true,
        render: (_, model) => renderDateValue(model.planOfMeasuresDate.toString())
      },
      {
        title: <Text message={messages.FinalizationDate} />,
        dataIndex: 'finalizationDate',
        key: 'finalizationDate',
        sorter: true,
        render: (_, model) => renderDateValue(model.finalizationDate.toString())
      },
      {
        title: <Text message={messages.RecertificationState} />,
        dataIndex: 'recertificationState',
        key: 'recertificationState',
        sorter: true,
        render: (_, model) => <Text message={model.stateLabel} />
      },
      {
        title: <Text message={messages.Created} />,
        dataIndex: 'created',
        key: 'created',
        sorter: true,
        render: (_, model) =>
          renderDateValue(
            typeof model.created === 'string' ? model.created : model.created.toString()
          )
      },
      {
        title: <Text message={messages.CreatedBy} />,
        dataIndex: 'createdBy',
        key: 'createdBy',
        sorter: true
      }
    ]}
  />
))
