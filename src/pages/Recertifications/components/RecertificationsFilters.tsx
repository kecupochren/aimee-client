import React from 'react'
import { observer } from 'mobx-react'

import { Input, Box } from '~/components'
import { useStore } from '~/store'

import { messages } from '../Recertifications.messages'

export const RecertificationsFilters: React.FC = observer(() => {
  const { recertifications } = useStore().RecertificationStore

  return (
    <Box mb={12} flex justifyContent="space-between">
      <Input
        onChange={query => recertifications.search(query)}
        placeholder={messages.Search}
        isSearch
        allowClear
        debounce
        flexGrow={1}
      />
    </Box>
  )
})
