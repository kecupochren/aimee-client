import React from 'react'

import { Button, Link } from '~/components'
import { ROUTES } from '~/constants'

import { messages } from '../Recertifications.messages'

export const RecertificationsActions: React.FC = () => (
  <>
    <Link to={ROUTES.RECERTIFICATION_NEW}>
      <Button message={messages.Add} type="primary" />
    </Link>
  </>
)
