import React, { FC, useEffect } from 'react'
import { observer } from 'mobx-react-lite'
import { RouteComponentProps } from 'react-router'

import { Page } from '~/components'
import { messages } from '~/messages'
import { useStore } from '~/store'

import {
  RecertificationsActions,
  RecertificationsTable,
  RecertificationsFilters
} from './components'

export const Recertifications: FC<RouteComponentProps> = props => {
  const { recertifications, fetch } = useStore().RecertificationStore

  useEffect(() => fetch(props), [])

  return (
    <Page
      actions={RecertificationsActions}
      loading={recertifications.isFirstLoad}
      title={messages.RecertificationList}
    >
      <RecertificationsFilters />
      <RecertificationsTable />
    </Page>
  )
}

/* tslint:disable-next-line no-default-export */
export default observer(Recertifications)
