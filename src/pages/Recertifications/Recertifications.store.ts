import { observable, action } from 'mobx'
import { RouteComponentProps } from 'react-router'

import { BaseStore, RepositoryCollection } from '~/store/modules'
import { IRecertificationsCollection } from '~/store/collections'

export class RecertificationsStore extends BaseStore {
  // ===================================
  // Model
  // ===================================
  // Table data
  @observable recertifications: IRecertificationsCollection

  // ===================================
  // Constructor
  // ===================================
  constructor(app: any) {
    super(app)

    this.recertifications = new RepositoryCollection(app, this.repositories.recertifications, {
      orderBy: 'planOfMeasuresDate',
      direction: 'descend'
    })
  }

  // ===================================
  // Actions
  // ===================================
  @action.bound fetch(props: RouteComponentProps) {
    this.recertifications.init(props.location)
  }
}
