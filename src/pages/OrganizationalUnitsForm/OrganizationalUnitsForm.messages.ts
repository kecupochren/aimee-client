import { defineMessages } from 'react-intl'

import { sharedMessages } from '~/messages'

const pageMessages = defineMessages({
  ErrorDuplicate: {
    id: 'OrganizationalUnitsForm.ErrorDuplicate',
    defaultMessage: 'An organizational unit with this name already exists'
  }
})

export const messages = { ...sharedMessages, ...pageMessages }
