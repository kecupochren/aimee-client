import React, { FC } from 'react'
import { Form, FormRenderProps } from 'react-final-form'
import { observer } from 'mobx-react-lite'
import { RouteProps } from 'react-router'

import { ButtonGroup, Button, Input, Page, Select, Card } from '~/components'
import { messages } from '~/messages'
import { useMount } from '~/utils'
import { useStore } from '~/store'

export const OrganizationalUnitsForm: React.FC<RouteProps> = props => {
  const { mount, unmount, initialValues, handleSubmit } = useStore().OrganizationalUnitsFormStore

  useMount(() => mount(props), unmount)

  return <Form onSubmit={handleSubmit} component={FormComponent} initialValues={initialValues} />
}

const FormComponent: FC<FormRenderProps> = observer(props => {
  const { labels, loading } = useStore().OrganizationalUnitsFormStore

  return (
    <form onSubmit={props.handleSubmit}>
      <Page
        title={messages.OrgUnit}
        maxWidth={768}
        loading={loading}
        actions={<Actions {...props} />}
      >
        <Card>
          <Input name="name" placeholder={messages.Name} label={messages.Name} required mb={24} />

          <Select
            type="field"
            optionsFrom="collection"
            name="labelIds"
            label={messages.Labels}
            placeholder={messages.Labels}
            collection={labels}
            mode="multiple"
            mb={24}
          />

          <Actions {...props} />
        </Card>
      </Page>
    </form>
  )
})

const Actions: FC<FormRenderProps> = observer(props => (
  <ButtonGroup align="end">
    <Button message={messages.Cancel} mr={6} returnButton />
    <Button
      message={messages.Save}
      type="primary"
      htmlType="submit"
      loading={props.submitting}
      disabled={props.pristine}
    />
  </ButtonGroup>
))

/* tslint:disable-next-line no-default-export */
export default observer(OrganizationalUnitsForm)
