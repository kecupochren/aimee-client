import { observable, action, computed } from 'mobx'
import { RouteProps } from 'react-router'

import { IOrganizationalUnitForSaveVM, ILabel } from '~/api/models'
import { ROUTES } from '~/constants'
import { BaseStore, RepositoryCollection } from '~/store/modules'
import { OrganizationalUnitModel, LabelModel } from '~/store/models'
import { getIDParam, DuplicateError } from '~/utils'

import { messages } from './OrganizationalUnitsForm.messages'

export class OrganizationalUnitsFormStore extends BaseStore {
  // ===================================-
  // Model
  // ===================================-
  // Entity to edit
  @observable organizationalUnit?: OrganizationalUnitModel
  @observable id?: number

  // Options for dropdowns
  @observable labels: RepositoryCollection<number, ILabel, LabelModel>

  // State
  @observable loading = true

  // ===================================
  // Constructor
  // ===================================
  constructor(app: any) {
    super(app)
    this.labels = new RepositoryCollection(app, this.repositories.labels)
  }

  // ===================================
  // Views
  // ===================================
  @computed get initialValues() {
    if (!this.organizationalUnit) return {}

    return {
      ...this.organizationalUnit,
      labelIds: this.organizationalUnit.labels.map(x => x.id)
    }
  }

  // ===================================-
  // Actions
  // ===================================-
  @action.bound async mount(props: RouteProps) {
    this.id = getIDParam(props.location)

    if (!this.id) {
      this.loading = false
      return
    }

    try {
      const orgUnit = await this.repositories.organizationalUnits.fetchOne(this.id)

      if (orgUnit.labels.length) {
        orgUnit.labels.forEach(x => this.labels.addItem(x))
      }

      this.organizationalUnit = orgUnit
    } catch (error) {
      this.helpers.notification.handleError(error)
    } finally {
      this.loading = false
    }
  }

  @action.bound async handleSubmit(data: any) {
    const payload: IOrganizationalUnitForSaveVM = {
      name: data.name,
      labelIds: data.labelIds
    }

    try {
      if (this.organizationalUnit) {
        await this.repositories.organizationalUnits.edit(this.organizationalUnit.id, payload)
      } else {
        await this.repositories.organizationalUnits.create(payload)
      }

      this.helpers.router.push(ROUTES.ORGANIZATIONAL_UNITS)
    } catch (error) {
      if (error instanceof DuplicateError) {
        return this.helpers.notification.error({
          message: messages.ErrorDuplicate
        })
      }

      this.helpers.notification.handleError(error)
    }
  }

  @action.bound unmount() {
    this.organizationalUnit = undefined
    this.loading = true
  }
}
