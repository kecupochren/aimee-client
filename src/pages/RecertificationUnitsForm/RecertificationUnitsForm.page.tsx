import React, { FC } from 'react'
import { observer } from 'mobx-react-lite'
import { RouteProps } from 'react-router'

import { Page } from '~/components'
import { useStore } from '~/store'
import { useMount } from '~/utils'

import { RCUnitActions, RCUnitsTable, RCUnitsTableDeleted } from './components'
import { messages } from './RecertificationUnitsForm.messages'

export const RecertificationUnitsForm: FC<RouteProps> = observer(props => {
  const { mount, unmount, loading, unit } = useStore().RecertificationUnitsFormStore

  useMount(() => mount(props), unmount)

  return (
    <Page
      title={messages.RecertificationUnitTitle}
      loading={loading}
      titleVariable={unit?.title}
      actions={<RCUnitActions />}
    >
      <RCUnitsTable />
      <RCUnitsTableDeleted />
    </Page>
  )
})

/* tslint:disable-next-line no-default-export */
export default RecertificationUnitsForm
