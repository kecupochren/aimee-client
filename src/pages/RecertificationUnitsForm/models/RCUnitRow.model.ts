import { ObservableMap, computed, action, observable, set } from 'mobx'

import { RCUnitDisplayValueModel } from '../components/RCUnitDisplayValue'
import { RCUnitInputValueModel } from '../components/RCUnitInputValue'
import { RCUnitSelectValueModel } from '../components/RCUnitSelectValue'
import { RecertificationUnitRowForSaveVM } from '~/api/apimodels/RecertificationUnits'

export type TRCUnitRowValueModels =
  | RCUnitDisplayValueModel
  | RCUnitInputValueModel
  | RCUnitSelectValueModel

type TValues = ObservableMap<string, TRCUnitRowValueModels>

export interface IRCUnitRow {
  id: string
  key: string
  index: number
  values: TValues
  disabled?: boolean
  deleted?: boolean
}

export class RCUnitRowModel {
  // ===================================
  // Model
  // ===================================
  // ID is needed to satisfy Collection/Repository constraint - key is used
  @observable id: string
  // API generated key for this row
  @observable key: string

  // Row index
  @observable index: number
  // Default row index, as returned from the API
  @observable indexDefault: number

  // Data
  readonly values: TValues = new ObservableMap()

  // State
  @observable _disabled: boolean
  @observable deleted: boolean
  @observable saving: boolean

  // ===================================
  // Constructor
  // ===================================
  constructor(
    private readonly saveRow: (payload: RecertificationUnitRowForSaveVM) => Promise<any>,
    private readonly isUnitConfirmed: boolean,
    data: IRCUnitRow
  ) {
    this.setData(data)
    this.indexDefault = data.index
  }

  // ===================================
  // Views
  // ===================================
  @computed get entityName() {
    return 'Recertification unit row'
  }

  @computed get disabled() {
    return this._disabled || this.isUnitConfirmed
  }

  @computed get payload() {
    const payload = Array.from(this.values.values()).reduce(
      (map: Omit<RecertificationUnitRowForSaveVM, 'key'>, value: TRCUnitRowValueModels) => {
        const { inputForSave, statementForSave } = value as any

        if (inputForSave) {
          map.inputs.push(inputForSave)
        }

        if (statementForSave) {
          map.statements.push(statementForSave)
        }

        return map
      },
      { statements: [], inputs: [] }
    )

    return { key: this.key, ...payload }
  }

  // ===================================
  // Actions
  // ===================================
  @action.bound setData(data: IRCUnitRow) {
    set(this, data)
    this.id = data.key
  }

  @action.bound setDisabled(disabled: boolean) {
    this._disabled = disabled
  }

  @action.bound async save() {
    this.saveRow(this.payload)
  }
}
