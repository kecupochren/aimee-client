import React from 'react'
import { action, observable, computed, ObservableMap, runInAction } from 'mobx'
import { RouteProps } from 'react-router'
import { ColumnProps } from 'antd/lib/table'
import * as signalR from '@microsoft/signalr'
import debounce from 'lodash/debounce'

import {
  RecertificationUnitRowDisplayValueVM,
  RecertificationUnitRowStatementForSaveVM,
  RecertificationUnitRowInputForSaveVM,
  RecertificationUnitColumnSelectVM,
  RecertificationUnitColumnInputVM,
  RecertificationUnitRowInputValueVM,
  RecertificationUnitRowSelectValueVM,
  RecertificationStatementOption,
  RecertificationUnitRowForSaveVM,
  RecertificationUnitColumnOptionVM
} from '~/api/apimodels/RecertificationUnits'
import {
  RecertificationUnitState,
  RecertificationUnitColumnType,
  RecertificationUnitColumnDisplayVM,
  RecertificationState
} from '~/api/apimodels/ViewModels'
import { ITableProps } from '~/components/ui/Table'
import { BaseStore, Collection, ICollectionFetchParams } from '~/store/modules'
import { RecertificationUnitModel, TRecertificationUnitColumn } from '~/store/models'
import { getIDParam, downloadFile, sleep } from '~/utils'

import { IRCUnitRow, RCUnitRowModel, TRCUnitRowValueModels } from './models'
import { SIGNALR_EVENTS } from './RecertificationUnitsForm.constants'
import { messages } from './RecertificationUnitsForm.messages'
import { RCUnitDisplayValue, RCUnitDisplayValueModel } from './components/RCUnitDisplayValue'
import {
  RCUnitInputValueModel,
  RCUnitInputValue,
  IRCUnitInputValueProps
} from './components/RCUnitInputValue'
import {
  RCUnitSelectValueModel,
  RCUnitSelectValue,
  IRCUnitSelectValueProps
} from './components/RCUnitSelectValue'
import { RCUnitInputColumnFilter } from './components/RCUnitInputColumnFilter'

const CHECKBOX_COL_WIDTH = 60

type TFilters = IAnyObject

export class RecertificationUnitsFormStore extends BaseStore {
  // ===================================
  // Model
  // ===================================
  @observable id?: number
  @observable unit?: RecertificationUnitModel
  @observable signalRConnection: signalR.HubConnection

  @observable fetching = true
  @observable processing = true
  @observable unlocking = false
  @observable confirming = false
  @observable exporting = false
  // If true, there have been no changes to the data on the server
  @observable pristine = true

  readonly rows = new Collection<string, IRCUnitRow, RCUnitRowModel>(this, this._processData, {
    syncURL: true,
    pageSize: 20
  })

  readonly rowsDeleted = new Collection<string, IRCUnitRow, RCUnitRowModel>(this, undefined, {
    pageSize: 20
  })

  // ===================================
  // Views
  // ===================================
  @computed get loading() {
    if (!this.rows.initialized) {
      return this.fetching || this.processing
    }

    return this.fetching
  }

  @computed get submitting() {
    return this.unlocking || this.confirming || this.exporting || this.fetching
  }

  @computed get isConfirmed() {
    return this.unit?.recertificationUnitState === RecertificationUnitState.Confirmed
  }

  @computed get isOpen() {
    return !this.isConfirmed
  }

  @computed get canBeConfirmed() {
    return (
      this.unit?.canBeConfirmed &&
      !this.isConfirmed &&
      this.unit?.recertificationState === RecertificationState.Opened
    )
    // recertification SHOULD be opened, but because of testing we sometimes finish it without confirming RU
  }

  @computed get canBeUnlocked() {
    return (
      this.unit?.canBeUnlocked &&
      this.unit?.recertificationState === RecertificationState.Opened &&
      this.isConfirmed
    )
  }

  @computed get canBeEdited() {
    return (
      this.unit?.canBeEdited &&
      this.unit?.recertificationState === RecertificationState.Opened &&
      !this.isConfirmed
    )
  }

  @computed get columns(): TRecertificationUnitColumn[] {
    if (!this.unit) return []
    return this.unit.columnDefinitions
  }

  @computed get hasManyCols() {
    return this.columns.length > 4
  }

  @computed get columnProps(): Array<
    ColumnProps<RCUnitRowModel> & { type?: RecertificationUnitColumnType }
  > {
    if (!this.columns) return []

    return this.columns.map(col => {
      const isSelectCol = col.columnType === RecertificationUnitColumnType.Select
      const isInputCol = col.columnType === RecertificationUnitColumnType.Input
      const isDisplayCol = col.columnType === RecertificationUnitColumnType.Display

      const filterProps: ColumnProps<RCUnitRowModel> = {
        filters: []
      }

      if (isDisplayCol) {
        filterProps.filters = this.rows.filters.optionsForTable[col.propertyName]?.sort((a, b) =>
          a.value.localeCompare(b.value)
        )
      }

      if (isInputCol) {
        filterProps.filterDropdown = props => <RCUnitInputColumnFilter {...props} />
      }

      if (isSelectCol && 'availableOptions' in col) {
        filterProps.filters = col.availableOptions.map(o => ({
          text: o.text,
          value: String(o.value)
        }))
      }

      return {
        rowKey: 'id',
        type: col.columnType,
        title: col.headerTitle,
        dataIndex: col.propertyName,
        ...filterProps,
        sorter: true,
        ellipsis: true,
        fixed: this.hasManyCols && isSelectCol ? 'right' : undefined,
        width: approximateColWidth(col.headerTitle, filterProps.filters),
        render: (_, row: RCUnitRowModel) => {
          const value = row.values.get(col.propertyName)

          switch (col.columnType) {
            case RecertificationUnitColumnType.Display:
              return (
                <RCUnitDisplayValue
                  column={col as RecertificationUnitColumnDisplayVM}
                  value={value as RCUnitDisplayValueModel}
                />
              )

            case RecertificationUnitColumnType.Input:
              return (
                <RCUnitInputValue
                  row={row}
                  column={col as RecertificationUnitColumnInputVM}
                  value={value as RCUnitInputValueModel}
                  onChange={this.handleInputChange}
                  onBlur={this.syncServer}
                />
              )

            case RecertificationUnitColumnType.Select:
              return (
                <RCUnitSelectValue
                  row={row}
                  column={col as RecertificationUnitColumnSelectVM}
                  value={value as RCUnitSelectValueModel}
                  onChange={this.handleSelectChange}
                />
              )

            default:
              return null
          }
        }
      }
    })
  }

  @computed get allRowsHaveSelectValues() {
    return this.rows.data.every(row =>
      this.columns.every(col => {
        if (col.columnType === RecertificationUnitColumnType.Select) {
          const value = row.values.get(col.propertyName) as RCUnitSelectValueModel
          const hasValue = typeof value.stateValue !== 'undefined'
          const isSet = value.stateValue !== RecertificationStatementOption.NotSet

          return hasValue && isSet
        }

        return true
      })
    )
  }

  @computed get scrollProps() {
    if (!this.hasManyCols) {
      return undefined
    }

    const dataColsWidth = (this.columnProps.map(x => x.width) as unknown) as number
    const scrollWidth = dataColsWidth + CHECKBOX_COL_WIDTH // account for checkbox column

    return { x: scrollWidth }
  }

  @computed get sharedTableProps(): ITableProps<RCUnitRowModel> {
    return {
      scroll: this.scrollProps,
      columns: this.columnProps,
      loading: this.processing,
      tableLayout: 'auto'
    }
  }

  @computed get tableProps(): ITableProps<RCUnitRowModel> {
    return {
      ...this.rows.tableProps,
      ...this.sharedTableProps,
      onRowClick: row => this.rows.selection.addItem(row)
    }
  }

  @computed get tableDeletedProps(): ITableProps<RCUnitRowModel> {
    return {
      ...this.rowsDeleted.tableProps,
      ...this.sharedTableProps,
      columns: this.sharedTableProps.columns?.map(x => ({
        ...x,
        sorter: false,
        filters: undefined
      }))
    }
  }

  // ===================================
  // SignalR
  // ===================================
  @action.bound async _setupSignalR() {
    const { apiURL } = this.helpers.env.config
    const url = `${apiURL}/recertification-units?recertificationUnitId=${this.id}`

    this.signalRConnection = new signalR.HubConnectionBuilder()
      .withUrl(url)
      .configureLogging(signalR.LogLevel.Information)
      .withAutomaticReconnect()
      .build()

    this.signalRConnection.onclose((error?: Error | undefined) => {
      if (error) {
        this.helpers.notification.handleError(error)
      }
    })

    this.signalRConnection.on(SIGNALR_EVENTS.SAVED, (saved: RecertificationUnitRowForSaveVM) => {
      console.log('signalR: Other user has just saved the recertification unit.')
      this._updateRow(saved.key, saved.statements, saved.inputs)
    })

    this.signalRConnection.on(
      SIGNALR_EVENTS.SAVED_MULTIPLE,
      (savedMultiple: RecertificationUnitRowForSaveVM[]) => {
        console.log('signalR: Other user has just saved multiple of the recertification unit.')
        savedMultiple.forEach(saved => {
          this._updateRow(saved.key, saved.statements, saved.inputs)
        })
      }
    )

    this.signalRConnection.on(SIGNALR_EVENTS.CONFIRM, async () => {
      console.log('signalR: Other user has just confirmed the recertification unit.')
      await this._fetchDetail()
    })

    this.signalRConnection.on(SIGNALR_EVENTS.UNLOCK, async () => {
      console.log('signalR: Other user has just unlocked current recertification unit.')
      await this._fetchDetail()
    })

    await this.signalRConnection.start()
    console.log('signalR: connected to server')
  }

  @action.bound _updateRow(
    key: string,
    statements: RecertificationUnitRowStatementForSaveVM[],
    inputs: RecertificationUnitRowInputForSaveVM[]
  ) {
    this.pristine = false

    const row = this.rows.map.get(key)
    if (row) {
      row.setDisabled(true)
    }

    this.columns.forEach(col => {
      inputs.forEach(input => {
        if (col.propertyName === input.propertyName) {
          const value = row?.values.get(col.propertyName)

          if (value) {
            const { setValue } = value as any

            if (setValue) {
              const model = value as RCUnitInputValueModel
              model.setValue(input.value)
            }
          }
        }
      })

      statements.forEach(statement => {
        if (col.propertyName === statement.propertyName) {
          const value = row?.values.get(col.propertyName)

          if (value) {
            const { setSelectValue } = value as any

            if (setSelectValue) {
              const model = value as RCUnitSelectValueModel
              model.setSelectValue(statement.selectedOption)
            }
          }
        }
      })
    })
  }

  @action.bound async _reconnectSignalR() {
    if (this.signalRConnection.state !== signalR.HubConnectionState.Connected) {
      await this.signalRConnection.start()
    }
  }

  // ===================================
  // Helpers
  // ===================================
  @action.bound async _fetchDetail() {
    this._clear()

    this.unit = await this.repositories.recertificationUnits.fetchOne(this.id!)

    await this.rows.init()
    await this.rowsDeleted.init()

    this.fetching = false
  }

  @action.bound async _processData(params: ICollectionFetchParams<TFilters>) {
    const { initialized, filtersChanged, sortingChanged, pageChanged, pagination } = params
    const { direction, orderBy } = pagination

    if (initialized && pageChanged) {
      return
    }

    this.processing = true

    if (!this.pristine) {
      this.unit = await this.repositories.recertificationUnits.fetchOne(this.id!)
    }

    const sortingCleared = sortingChanged && !pagination.orderBy && !pagination.orderBy
    const shouldProcess = !initialized || filtersChanged

    const data: RCUnitRowModel[] = shouldProcess ? [] : [...this.rows.data]
    const dataDeleted: RCUnitRowModel[] = shouldProcess ? [] : [...this.rowsDeleted.data]

    setTimeout(() => {
      if (shouldProcess) {
        this.unit?.rows.forEach((row, index) => {
          let deleted = false
          let included = true

          const values = this.columns.reduce((map, col, colIndex) => {
            const valueData = row.values[colIndex]
            let value

            const colFilters: string[] = this.rows.filters.state.get(col.propertyName) || []

            if (colFilters.length) {
              included = false
            }

            if (col.columnType === RecertificationUnitColumnType.Display) {
              value = new RCUnitDisplayValueModel(
                valueData as RecertificationUnitRowDisplayValueVM,
                this.isConfirmed
              )

              // Check for deleted rows
              if (value.isDeleted && !deleted) {
                deleted = true
              }

              // Filter
              if (colFilters && colFilters.length) {
                included = colFilters.includes(value.valueFinal)
              }

              if (col.propertyName && value.valueFinal) {
                this.rows.filters.addOption(col.propertyName, value.valueFinal)
              }
            }

            if (col.columnType === RecertificationUnitColumnType.Input) {
              value = new RCUnitInputValueModel(valueData as RecertificationUnitRowInputValueVM)

              // Filter
              if (colFilters.length) {
                const valueFinal = value.valueFinal
                if (valueFinal) {
                  included = colFilters.some(filter => valueFinal.includes(filter))
                }
              }
            }

            if (col.columnType === RecertificationUnitColumnType.Select) {
              value = new RCUnitSelectValueModel(valueData as RecertificationUnitRowSelectValueVM)

              // Filter
              if (colFilters.length) {
                const valueFinal = value.valueFinal
                if (valueFinal) {
                  included = colFilters.some(filter => String(valueFinal) === filter)
                }
              }
            }

            if (value) {
              map.set(col.propertyName, value)
            }

            return map
          }, new ObservableMap<string, TRCUnitRowValueModels>())

          if (included) {
            const model = new RCUnitRowModel(this.saveRow, this.isConfirmed, {
              id: row.key,
              key: row.key,
              index,
              values,
              deleted
            })

            const arr = deleted ? dataDeleted : data

            arr.push(model)
          }
        })
      }

      // Sort data
      if (sortingCleared) {
        data.forEach(model => (model.index = model.indexDefault))
        data.sort((a, b) => a.index - b.index)
      } else if (sortingChanged && orderBy && direction) {
        data
          .sort((a, b) => {
            const first = direction === 'ascend' ? a : b
            const last = direction === 'ascend' ? b : a

            const valFirst = first.values.get(orderBy)
            const valLast = last.values.get(orderBy)

            if (valFirst?.valueFinal && valLast?.valueFinal) {
              if (valFirst.valueFinal < valLast.valueFinal) return -1
              if (valFirst.valueFinal > valLast.valueFinal) return 1
              return 0
            }

            return 0
          })

          // Update model index with sorted one
          .forEach((model, index) => (model.index = index))
      }

      // Write data to state (in one action to prevent rerenders/recomputations)
      runInAction(() => {
        this.rows.data.replace(data)
        this.rows.pagination.setTotal(data.length)
        this.rows.initialized = true

        this.rowsDeleted.data.replace(dataDeleted)
        this.rowsDeleted.pagination.setTotal(dataDeleted.length)
        this.rowsDeleted.initialized = true
      })

      this.processing = false
    }, 1)

    return Promise.resolve()
  }

  @action.bound _clear() {
    this.fetching = true
    this.processing = true
    this.unit = undefined
    this.rows.clear()
    this.rowsDeleted.clear()
  }

  // ===================================
  // Actions
  // ===================================
  @action.bound async saveRow(payload: RecertificationUnitRowForSaveVM) {
    if (this.canBeEdited) {
      await this.signalRConnection.invoke(SIGNALR_EVENTS.SAVE, payload)
    }
    this.pristine = false
  }

  @action.bound async saveRows(data: RecertificationUnitRowForSaveVM[]) {
    if (this.canBeEdited) {
      await this.signalRConnection.invoke(SIGNALR_EVENTS.SAVE_MULTIPLE, data)
    }
  }

  @action.bound syncServer = debounce((row: RCUnitRowModel) => {
    const rows = [row, ...this.rows.selection.items]
    const payload = rows.map(r => r.payload)
    this.saveRows(payload)
  }, 500)

  @action.bound syncClient(
    property: string,
    state: { input?: string; option?: RecertificationUnitColumnOptionVM }
  ) {
    this.rows.selection.items.forEach(item => {
      if (item.values) {
        const prop = item.values.get(property)

        if (prop) {
          if (prop instanceof RCUnitInputValueModel) {
            prop.setState(state.input!)
          }

          if (prop instanceof RCUnitSelectValueModel) {
            prop.setState(state.option!)
          }
        }
      }
    })
  }

  @action.bound handleInputChange({ row, value, column }: IRCUnitInputValueProps, input: string) {
    value.setState(input)
    this.syncClient(column.propertyName, { input })
    this.syncServer(row)
  }

  @action.bound handleSelectChange(
    { row, value, column }: IRCUnitSelectValueProps,
    option: RecertificationUnitColumnOptionVM
  ) {
    value.setState(option)
    this.syncClient(column.propertyName, { option })
    this.syncServer(row)
  }

  @action.bound async confirmRecertificationUnit() {
    if (!this.allRowsHaveSelectValues) {
      this.helpers.notification.error({ message: messages.RecertificationUnitRowsNotCompleted })
      return
    }

    if (!this.canBeConfirmed) {
      return
    }

    this.confirming = true

    try {
      await this._reconnectSignalR()
      await this.signalRConnection.invoke(SIGNALR_EVENTS.CONFIRM)
      await sleep(1000)
      this._fetchDetail()
    } catch (error) {
      this.helpers.notification.handleError(error)
    } finally {
      this.confirming = false
    }
  }

  @action.bound async unlockRecertificationUnit() {
    if (!this.canBeUnlocked) {
      return
    }

    this.unlocking = true

    try {
      await this._reconnectSignalR()
      await this.signalRConnection.invoke(SIGNALR_EVENTS.UNLOCK)
      await sleep(1000)
      this._fetchDetail()
    } catch (error) {
      this.helpers.notification.handleError(error)
    } finally {
      this.unlocking = false
    }
  }

  @action.bound async exportChanges() {
    if (!this.id) return

    this.exporting = true

    try {
      const { data } = await this.helpers.api.getRecertificationUnitExportChanges(
        this.id.toString()
      )
      downloadFile(data, 'RecertificationUnitChangeExport.xlsx', 'text/xlsx')
    } catch (error) {
      this.helpers.notification.handleError(error)
    } finally {
      this.exporting = false
    }
  }

  // ===================================
  // Lifecycle
  // ===================================
  @action.bound async mount(props: RouteProps) {
    this.id = getIDParam(props.location)

    if (!this.id) {
      this.fetching = false
      return
    }

    try {
      await this._fetchDetail()
      await this._setupSignalR()
    } catch (error) {
      this.helpers.notification.handleError(error)
    }
  }

  @action.bound unmount() {
    this._clear()
  }
}

const approximateColWidth = (title: string, filters: any) => {
  // Start with sort toggle width (all columns are sorters)
  let width = 35

  // Account for filters toggle
  if (filters) {
    width += 22
  }

  // Approximate title length in pixels, 7px is roughly 1 char
  width += title.length * 7

  // Return approximated value or minimum
  return Math.max(width, 150)
}
