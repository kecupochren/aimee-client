import { RecertificationEntityType } from '~/api/apimodels/ViewModels'
import { ROUTES } from '~/constants'

export const IGNORED_COLUMNS = ['SystemRoleCode', 'UserAccountName']

export enum SIGNALR_EVENTS {
  SAVED = 'RecertificationUnitSavedAsync',
  SAVED_MULTIPLE = 'RecertificationUnitSavedMultipleAsync',
  SAVE = 'SaveAsync',
  SAVE_MULTIPLE = 'SaveMultipleAsync',
  CONFIRM = 'ConfirmAsync',
  UNLOCK = 'UnlockAsync'
}

export const ENTITY_TO_ROUTE = {
  [RecertificationEntityType.BusinessRole]: ROUTES.legacy.BUSINESS_ROLE,
  [RecertificationEntityType.Company]: ROUTES.legacy.COMPANY,
  [RecertificationEntityType.Department]: ROUTES.legacy.DEPARTMENT,
  [RecertificationEntityType.Employee]: ROUTES.legacy.IDENTITY,
  [RecertificationEntityType.Label]: ROUTES.LABEL_EDIT,
  [RecertificationEntityType.SystemApplication]: ROUTES.legacy.SYSTEM_APP,
  [RecertificationEntityType.SystemRole]: ROUTES.legacy.SYSTEM_ROLE,
  [RecertificationEntityType.SystemSecurityFlag]: ROUTES.legacy.SYSTEM_PERMISSION,
  [RecertificationEntityType.NotSupported]: '' // we don't support -> no link
}
