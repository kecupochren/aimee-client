import { defineMessages } from 'react-intl'

import { sharedMessages } from '~/messages'

const pageMessages = defineMessages({
  RecertificationUnitChangeExport: {
    id: 'RecertificationUnit.ChangeExport',
    defaultMessage: 'Recertification unit change export'
  },
  RecertificationUnitTooltip: {
    id: 'RecertificationUnit.RecertificationUnitTooltip',
    defaultMessage: 'Original: '
  },
  RecertificationUnitNotSet: {
    id: 'RecertificationUnit.RecertificationUnitNotSet',
    defaultMessage: 'Not set'
  },
  RecertificationUnitTooltipCurrent: {
    id: 'RecertificationUnit.RecertificationUnitTooltipCurrent',
    defaultMessage: 'Current: '
  },
  RecertificationUnitNotSetFilterValue: {
    id: 'RecertificationUnit.RecertificationUnitNotSetFilterValue',
    defaultMessage: 'Not set: '
  },
  RecertificationUnitTableDeletedRows: {
    id: 'RecertificationUnit.RecertificationUnitTableDeletedRows',
    defaultMessage: 'Deleted rows'
  },
  RecertificationUnitToRecertificationList: {
    id: 'RecertificationUnit.RecertificationUnitToRecertificationList',
    defaultMessage: 'To recertification unit list'
  },
  RecertificationUnitTitle: {
    id: 'RecertificationUnit.RecertificationUnitTitle',
    defaultMessage: 'Recertification unit title'
  },
  RecertificationUnitConfirm: {
    id: 'RecertificationUnit.RecertificationUnitConfirm',
    defaultMessage: 'Confirm recertification unit'
  },
  RecertificationUnitUnlock: {
    id: 'RecertificationUnit.RecertificationUnitUnlock',
    defaultMessage: 'Unlock recertification unit'
  },
  RecertificationUnitRowsNotCompleted: {
    id: 'RecertificationUnit.ErrorRecertificationUnitCannotBeConfirmed_NotAllRowsSaved',
    defaultMessage: 'Recertification unit cannot be confirmed because some statement is not saved.'
  }
})

export const messages = { ...sharedMessages, ...pageMessages }
