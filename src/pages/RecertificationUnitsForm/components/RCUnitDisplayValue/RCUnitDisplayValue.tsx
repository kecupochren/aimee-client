import React, { FC } from 'react'
import { observer } from 'mobx-react'

import { Link } from '~/components/ui/Link'
import { Tooltip } from '~/components/ui/Tooltip'
import { Message } from '~/components/ui/Message'
import { RecertificationUnitColumnDisplayVM } from '~/api/apimodels/ViewModels'

import { ENTITY_TO_ROUTE } from '../../RecertificationUnitsForm.constants'
import { messages } from '../../RecertificationUnitsForm.messages'

import { RCUnitDisplayValueModel } from './RCUnitDisplayValue.model'
import { styled, css } from '~/utils'

export interface IRCUnitDisplayValue {
  column: RecertificationUnitColumnDisplayVM
  value?: RCUnitDisplayValueModel
}

export const RCUnitDisplayValue: FC<IRCUnitDisplayValue> = observer(({ column, value }) => {
  if (!value) {
    return null
  }

  const route = ENTITY_TO_ROUTE[column.entityType]
  if (route && value.entityId) {
    const isLegacy = !route.includes('spa')
    const entityURL = route.replace(':id', value.entityId.toString())
    const linkProps = isLegacy ? { href: entityURL } : { to: entityURL }

    return (
      <Link {...linkProps} target="_blank">
        <DisplayCellValue column={column} value={value} />
      </Link>
    )
  }

  return <DisplayCellValue column={column} value={value} />
})

const DisplayCellValue: FC<IRCUnitDisplayValue> = observer(({ value }) => {
  if (!value) return null

  const { valueFinal, generated, isChanged, isNew } = value

  return (
    <Tooltip
      content={
        <>
          {isChanged && !isNew && (
            <>
              <Message message={messages.RecertificationUnitTooltip} />
              {generated}
              <br />
              <Message message={messages.RecertificationUnitTooltipCurrent} />
            </>
          )}

          {valueFinal}
        </>
      }
    >
      <ValueDisplay isChanged={isChanged} isNew={isNew}>
        {valueFinal}
      </ValueDisplay>
    </Tooltip>
  )
})

const ValueDisplay = styled.div<{ isChanged?: boolean; isNew?: boolean }>`
  ${props => {
    if (props.isChanged) {
      return props.isNew
        ? css`
            border-bottom: green 1px solid;
          `
        : css`
            border-bottom: red 1px solid;
          `
    }

    return css``
  }}
`
