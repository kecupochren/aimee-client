import { observable, computed, set } from 'mobx'

import { RecertificationUnitRowDisplayValueVM } from '~/api/apimodels/RecertificationUnits'

export class RCUnitDisplayValueModel implements RecertificationUnitRowDisplayValueVM {
  @observable propertyName: string
  @observable entityId: number
  @observable generated: string
  @observable saved: string
  @observable current: string

  @computed get valueFinal() {
    if (this.isRCUnitConfirmed) {
      return this.saved
    }

    if (this.isDeleted) {
      // if the item was deleted, they don't have current item, so we will show generated value
      // but generated value may not exist (item could be created AFTER the recertification was created)
      // in this case, we will show saved value. It must exist, otherwise the item wouldn't be here
      return this.generated ? this.generated : this.saved
    }

    return this.current
  }

  @computed get isChanged(): boolean {
    if (this.isRCUnitConfirmed) {
      return this.saved !== this.current
    }

    if (!this.isDeleted) {
      return this.generated !== this.current
    }

    return false
  }

  @computed get isNew(): boolean {
    return !!this.current && !this.generated
  }

  @computed get isDeleted(): boolean {
    // deleted items are the items, that DONT have current value but HAVE generated value
    // = they existed when the recertification was created and exist now
    // deleted are also items, that DONT have current value, DONT have generated value (they didn't exist when the rec. was created)
    // but HAVE saved value. It means that item didn't exist when the rec. was created. Than entity was created, SAVED, and deleted.
    return (!this.current && !!this.generated) || (!this.current && !this.generated && !!this.saved)
  }

  @computed get entityURL() {
    if (this.entityId) {
      console.log('has entityId', this.entityId)
    }

    return ''
  }

  constructor(data: RecertificationUnitRowDisplayValueVM, readonly isRCUnitConfirmed: boolean) {
    set(this, data)
  }
}
