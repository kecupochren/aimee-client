import React, { FC } from 'react'
import { observer } from 'mobx-react-lite'
import { RouteProps } from 'react-router'
import { createGlobalStyle } from 'styled-components'

import { Table } from '~/components/ui/Table'
import { useStore } from '~/store'

import { RCUnitRowModel } from '../../models/RCUnitRow.model'

export const RCUnitsTable: FC<RouteProps> = observer(() => {
  const { tableProps } = useStore().RecertificationUnitsFormStore

  // Touch disabled props to force observer rerender
  // TODO: Globally somehow
  tableProps.dataSource?.forEach(x => x.disabled)

  return (
    <>
      <Styles />

      <Table<RCUnitRowModel>
        key="yo"
        withBackground
        withBorder
        rowKey={row => row.key}
        size="middle"
        {...tableProps}
      />
    </>
  )
})

const Styles = createGlobalStyle`
  .row--disabled {
    opacity: .3;
    position: relative;
  }
`
