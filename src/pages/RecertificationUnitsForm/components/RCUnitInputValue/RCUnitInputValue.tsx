import React, { FC } from 'react'
import { observer } from 'mobx-react'
import { Input } from 'antd'

import { RecertificationUnitColumnInputVM } from '~/api/apimodels/RecertificationUnits'

import { RCUnitRowModel } from '../../models'

import { RCUnitInputValueModel } from './RCUnitInputValue.model'

export interface IRCUnitInputValueProps {
  row: RCUnitRowModel
  column: RecertificationUnitColumnInputVM
  value: RCUnitInputValueModel
}

interface IRCUnitInputValuePropsFinal extends IRCUnitInputValueProps {
  onChange: (props: IRCUnitInputValueProps, state: string) => any
  onBlur: (row: RCUnitRowModel) => any
}

export const RCUnitInputValue: FC<IRCUnitInputValuePropsFinal> = observer(props => {
  const { row, value, onChange, onBlur } = props

  if (!value || row.deleted) {
    return null
  }

  return (
    <Input
      size="default"
      value={value.state}
      onChange={e => onChange(props, e.target.value)}
      onClick={e => e.stopPropagation()}
      onBlur={() => onBlur(row)}
      disabled={row.disabled}
    />
  )
})
