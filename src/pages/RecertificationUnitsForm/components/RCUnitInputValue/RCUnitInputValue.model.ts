import { action, computed, set, observable } from 'mobx'

import {
  RecertificationUnitRowInputValueVM,
  RecertificationUnitRowInputForSaveVM
} from '~/api/apimodels/RecertificationUnits'

export class RCUnitInputValueModel implements RecertificationUnitRowInputValueVM {
  @observable propertyName: string
  @observable value: string
  @observable state: string

  type = 'input'

  constructor(data: RecertificationUnitRowInputValueVM) {
    set(this, data)
    this.state = data.value || ''
  }

  @computed get valueFinal() {
    return this.value
  }

  @computed get inputForSave(): RecertificationUnitRowInputForSaveVM | undefined {
    if (this.state === null) return undefined

    return {
      propertyName: this.propertyName,
      value: this.state
    }
  }

  @action.bound setState(state: string) {
    this.state = state
  }

  @action.bound setValue(value?: string) {
    this.value = value || ''
    this.state = value || ''
  }
}
