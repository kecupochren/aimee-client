import React, { FC } from 'react'
import { observer } from 'mobx-react-lite'

import { FilterDropdownProps } from 'antd/lib/table'

// Use direct imports to prevent cyclic deps
import { Box } from '~/components/ui/Box'
import { ButtonGroup } from '~/components/ui/ButtonGroup'
import { Link } from '~/components/ui/Link'
import { Input } from '~/components/fields/Input'

import { styled } from '~/utils'

import { messages } from '../../RecertificationUnitsForm.messages'

export const RCUnitInputColumnFilter: FC<FilterDropdownProps> = observer(
  ({
    selectedKeys = [],
    setSelectedKeys = () => undefined,
    confirm,
    clearFilters = (_keys: any) => undefined
  }) => {
    const inputValue = selectedKeys[0] ? String(selectedKeys[0]) : ''

    return (
      <Box p={8}>
        <Input
          value={inputValue}
          onChange={value => setSelectedKeys(value ? [value] : [])}
          placeholder={messages.Search}
          size="default"
          mb={6}
        />

        <Actions>
          <Link onClick={confirm}>OK</Link>
          <Link onClick={() => clearFilters(selectedKeys as string[])}>Reset</Link>
        </Actions>
      </Box>
    )
  }
)

const Actions = styled(ButtonGroup)`
  justify-content: space-between;
  border-top: 1px solid ${props => props.theme.color.border};
  margin: 0 -8px;
  padding: 6px 8px 0;
`
