import React, { FC } from 'react'
import { observer } from 'mobx-react-lite'
import { RouteProps } from 'react-router'

import { Box } from '~/components/ui/Box'
import { Table } from '~/components/ui/Table'
import { Text } from '~/components/ui/Text'
import { useStore } from '~/store'

import { RCUnitRowModel } from '../../models/RCUnitRow.model'
import { messages } from '../../RecertificationUnitsForm.messages'

export const RCUnitsTableDeleted: FC<RouteProps> = observer(() => {
  const { tableDeletedProps } = useStore().RecertificationUnitsFormStore

  if (tableDeletedProps.dataSource?.length === 0) {
    return null
  }

  return (
    <Box mt={24}>
      <Text
        inlineBlock
        fontSize={17}
        mb={12}
        message={messages.RecertificationUnitTableDeletedRows}
      />

      <Table<RCUnitRowModel>
        withBackground
        withBorder
        rowKey={row => row.key}
        size="middle"
        {...tableDeletedProps}
      />
    </Box>
  )
})
