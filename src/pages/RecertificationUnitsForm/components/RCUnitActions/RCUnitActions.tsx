import React, { FC } from 'react'
import { observer } from 'mobx-react-lite'

import { useStore } from '~/store'
import { ROUTES } from '~/constants'
import { ButtonGroup, Button } from '~/components'

import { messages } from '../../RecertificationUnitsForm.messages'

export const RCUnitActions: FC = observer(() => {
  const {
    canBeConfirmed,
    canBeUnlocked,
    unlockRecertificationUnit,
    confirmRecertificationUnit,
    unit,
    submitting,
    exporting,
    confirming,
    unlocking,
    exportChanges
  } = useStore().RecertificationUnitsFormStore

  const recUnitsURL = unit
    ? ROUTES.RECERTIFICATION_UNITS.replace(':id', unit.recertificationId.toString())
    : ''

  return (
    <ButtonGroup align="end">
      <Button
        message={messages.RecertificationUnitToRecertificationList}
        ml={12}
        to={recUnitsURL}
      />

      <Button
        message={messages.RecertificationUnitChangeExport}
        ml={12}
        onClick={exportChanges}
        disabled={submitting}
        loading={exporting}
      />

      {canBeConfirmed && (
        <Button
          message={messages.RecertificationUnitConfirm}
          onClick={confirmRecertificationUnit}
          ml={12}
          type="primary"
          disabled={submitting}
          loading={confirming}
        />
      )}

      {canBeUnlocked && (
        <Button
          message={messages.RecertificationUnitUnlock}
          onClick={unlockRecertificationUnit}
          ml={12}
          disabled={submitting}
          type="primary"
          loading={unlocking}
        />
      )}
    </ButtonGroup>
  )
})
