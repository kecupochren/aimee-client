import { computed, action, set, observable } from 'mobx'

import {
  RecertificationUnitRowSelectValueVM,
  RecertificationUnitColumnOptionVM,
  RecertificationUnitRowStatementForSaveVM
} from '~/api/apimodels/RecertificationUnits'

export class RCUnitSelectValueModel implements RecertificationUnitRowSelectValueVM {
  @observable propertyName: string
  @observable value: RecertificationStatementOption
  @observable state: RecertificationUnitColumnOptionVM
  @observable stateValue: RecertificationStatementOption

  type = 'select'

  constructor(data: RecertificationUnitRowSelectValueVM) {
    set(this, data)
    this.stateValue = data.value
  }

  @computed get label() {
    return this.value
  }

  @computed get valueFinal() {
    return this.stateValue
  }

  @computed get statementForSave(): RecertificationUnitRowStatementForSaveVM | undefined {
    if (!this.state) return undefined

    return {
      propertyName: this.propertyName,
      selectedOption: this.state
    }
  }

  @action.bound setState(option: RecertificationUnitColumnOptionVM) {
    this.state = option
    this.stateValue = option.value
  }

  @action.bound setSelectValue(option: RecertificationUnitColumnOptionVM) {
    this.state = option
    this.value = option?.value as RecertificationStatementOption
    this.stateValue = option.value
  }
}

export enum RecertificationStatementOption {
  NotSet = 0,
  Correct = -1,
  NotCorrect = -2,
  CanBeRemoved = -3,
  NormalRole = -4,
  CriticalRole = -5,
  MyEmplyee = -6,
  NotMyEmployee = -7,
  ChangeOfDepartment = -8
}
