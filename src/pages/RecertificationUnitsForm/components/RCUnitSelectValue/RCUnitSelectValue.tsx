import React, { FC } from 'react'
import { observer } from 'mobx-react'
import { Select } from 'antd'

import {
  RecertificationUnitColumnSelectVM,
  RecertificationStatementOption,
  RecertificationUnitColumnOptionVM
} from '~/api/apimodels/RecertificationUnits'

import { RCUnitRowModel } from '../../models'

import { RCUnitSelectValueModel } from './RCUnitSelectValue.model'

const { Option } = Select

export interface IRCUnitSelectValueProps {
  row: RCUnitRowModel
  column: RecertificationUnitColumnSelectVM
  value: RCUnitSelectValueModel
}

interface IRCUnitSelectValuePropsFinal extends IRCUnitSelectValueProps {
  onChange: (props: IRCUnitSelectValueProps, option: RecertificationUnitColumnOptionVM) => any
}

export const RCUnitSelectValue: FC<IRCUnitSelectValuePropsFinal> = observer(props => {
  const { row, column, value, onChange } = props

  if (!value || row.deleted) {
    return null
  }

  const handleChange = (selected: RecertificationStatementOption) => {
    const option = column.availableOptions.find(x => x.value === selected)
    if (option) {
      onChange(props, option)
    }
  }

  return (
    <div onClick={e => e.stopPropagation()}>
      <Select<RecertificationStatementOption>
        size="default"
        value={value.stateValue}
        onChange={handleChange}
        style={{ minWidth: '160px', width: '100%' }}
        onBlur={row.save}
        disabled={row.disabled}
      >
        {column.availableOptions.map(option => (
          <Option key={option.value} value={option.value}>
            {option.text || <span style={{ opacity: 0 }}>Empty</span>}
          </Option>
        ))}
      </Select>
    </div>
  )
})
