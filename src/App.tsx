import React from 'react'
import { Router, Switch } from 'react-router'
import { IntlProvider } from 'react-intl'
import { observer } from 'mobx-react-lite'
import { History } from 'history'
import 'flag-icon-css/css/flag-icon.min.css'

import { GlobalStyle } from './assets/css'
import { theme } from './constants'
import { Layout, PageLoader } from './components'
import { Routes } from './routes'
import { useStore } from './store'
import { ThemeProvider } from './utils'

interface IAppProps {
  history: History<any>
}

const _App = (props: IAppProps) => {
  const store = useStore()

  if (!store.root.initialized) {
    return <PageLoader fullscreen />
  }

  const { localeShortKey, dictionary } = store.helpers.intl

  return (
    <IntlProvider key={localeShortKey} locale={localeShortKey} messages={dictionary}>
      <ThemeProvider theme={theme}>
        <>
          <GlobalStyle />

          <Router history={props.history}>
            <Layout>
              <Switch>
                <Routes />
              </Switch>
            </Layout>
          </Router>
        </>
      </ThemeProvider>
    </IntlProvider>
  )
}

export const App = observer(_App)
