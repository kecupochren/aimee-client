// React polyfills
import 'react-app-polyfill/ie11'
import 'react-app-polyfill/stable'

// Babel polyfills
import 'core-js/stable'
import 'regenerator-runtime/runtime'

import { mockServerData } from '~/constants'

const isLocal = process.env.NODE_ENV !== 'production'
const isLocalProd = process.env.LOCAL_PROD

// Mock server rendered data when in dev mode
if (isLocal || isLocalProd) {
  // For mocking through Cypress
  const serverData = window.localStorage.getItem('serverData')

  // @ts-ignore
  window.serverData = serverData || mockServerData
}

import React from 'react'
import ReactDOM from 'react-dom'
import moment from 'moment'

import { App } from './App'
import { initServiceWorker } from './utils'

import { createStore } from './store'
import { createRootStore, rootStore } from './store/rootStore'

moment.locale('cs')
// LR: toJSON overwrite Date's timezone to UTC
Date.prototype.toJSON = function() {
  return moment(this).format(moment.HTML5_FMT.DATE)
}

const { history } = createRootStore()
createStore()

rootStore.initialize()

ReactDOM.render(<App history={history} />, document.getElementById('app'))

initServiceWorker()

if ((module as any).hot) (module as any).hot.accept()
