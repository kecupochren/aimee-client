import React, { FC } from 'react'
import { FormattedMessage } from 'react-intl'
import { observer } from 'mobx-react-lite'
import { Field, FieldRenderProps } from 'react-final-form'

import { Table, ITableProps } from '~/components/ui/Table'
import { Select } from '~/components/fields/Select'
import { FieldLabel } from '~/components/ui/FieldLabel'
import { Text } from '~/components/ui/Text'
import { Tooltip } from '~/components/ui/Tooltip'
import { Link } from '~/components/ui/Link'
import { Icon } from '~/components/ui/Icon'
import { RepositoryCollection } from '~/store/modules'
import { messages } from '~/messages'

export interface IProps {
  // Field name
  name: string

  // Collection for options/selected state
  collection: RepositoryCollection<any, any, any>

  // Table columns
  columns: ITableProps<any>['columns']

  // Main label
  label?: FormattedMessage.MessageDescriptor

  // Select label
  selectLabel: FormattedMessage.MessageDescriptor

  // Select placeholder
  selectPlaceholder: FormattedMessage.MessageDescriptor
}

export const TableSelect: FC<IProps> = observer(props => {
  const { label, selectLabel, selectPlaceholder, collection, columns } = props

  const TableSelectComponent: FC<FieldRenderProps<string[], any>> = observer(fieldProps => {
    const handleChange = () => {
      fieldProps.input.onChange({ target: { value: collection.selection.ids } })
    }

    const handleAdd = (id?: number) => {
      if (id) {
        collection.selection.add(id)
        handleChange()
      }
    }

    const handleRemove = (id: number) => {
      collection.selection.remove(id)
      handleChange()
    }

    const columnsFinal = [
      // @ts-ignore
      ...columns,
      {
        title: <Text message={messages.Action} />,
        align: 'right' as any,
        render: (_text: any, entity: any) => (
          <Tooltip message={messages.Remove}>
            <Link noHoverStyling onClick={() => handleRemove(entity.id)}>
              <Icon type="delete" />
            </Link>
          </Tooltip>
        )
      }
    ]

    return (
      <>
        <Select
          type="observed"
          optionsFrom="collection"
          collection={collection}
          onChange={handleAdd}
          label={selectLabel}
          placeholder={selectPlaceholder}
          mb={24}
          noValue
        />

        {Boolean(collection.selection.count > 0) && (
          <>
            <FieldLabel message={label} />

            <Table
              dataSource={collection.selected}
              columns={columnsFinal}
              noPagination
              size="small"
              mb={24}
            />
          </>
        )}
      </>
    )
  })

  return <Field name={props.name} component={TableSelectComponent} />
})
