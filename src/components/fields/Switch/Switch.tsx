import React, { FC } from 'react'
import { FormattedMessage } from 'react-intl'
import { Field, FieldRenderProps } from 'react-final-form'
import { observer } from 'mobx-react-lite'
import capitalize from 'lodash/capitalize'

import { Switch as AntdSwitch } from 'antd'
import { SwitchProps as AntdSwitchProps } from 'antd/lib/Switch'
import 'antd/es/switch/style'

import { FieldLabel } from '~/components/ui/FieldLabel'
import {
  styled,
  createGlobalStyle,
  css,
  ISpacingProps,
  applySpacingProps,
  getSpacingProps
} from '~/utils'
import { theme } from '~/constants'

type TLabelPosition = 'left' | 'top' | 'right' | 'bottom'

// Use as controlled field
interface IControlledProps {
  type?: 'controlled'
  checked: boolean
  onChange: (checked: boolean) => void
}

// Use as react-final-form field, with optional listener
interface IFieldProps {
  type?: 'field'
  name: string
  onChange?: (checked: boolean) => void
}

export type ISwitchProps = AntdSwitchProps &
  ISpacingProps &
  (IControlledProps | IFieldProps) & {
    label: FormattedMessage.MessageDescriptor
    labelPosition?: TLabelPosition
  }

export const Switch: FC<ISwitchProps> = observer(props => {
  const { type = 'controlled', labelPosition = 'right', onChange, checked, label, ...rest } = props

  function handleChange(isChecked: boolean, fieldOnChange: (checked: boolean) => void) {
    if (type === 'field') {
      fieldOnChange(isChecked)
    }

    if (onChange) {
      onChange(isChecked)
    }
  }

  function getIsChecked(fieldProps: FieldRenderProps<boolean, any>) {
    if (type === 'field') {
      return fieldProps.input.value
    }

    return checked
  }

  function renderLabel() {
    return <FieldLabel message={label} mb={0} />
  }

  function renderSwitch(fieldProps: FieldRenderProps<boolean, any> = defaultFieldProps) {
    const renderLabelBefore = labelPosition === 'left' || labelPosition === 'top'
    const renderLabelAfter = labelPosition === 'right' || labelPosition === 'bottom'
    const isInline = labelPosition === 'left' || labelPosition === 'right'

    return (
      <Wrapper isInline={isInline} {...getSpacingProps(rest)}>
        <Styles />

        {renderLabelBefore && renderLabel()}

        <AntdSwitch
          {...rest}
          {...fieldProps.input}
          style={{ [`margin${capitalize(labelPosition)}`]: 6 }}
          onChange={isChecked => handleChange(isChecked, fieldProps.input.onChange)}
          checked={getIsChecked(fieldProps)}
        />

        {renderLabelAfter && renderLabel()}
      </Wrapper>
    )
  }

  if (props.type === 'field') {
    return <Field name={props.name} render={renderSwitch} />
  }

  return renderSwitch()
})

const defaultFieldProps = {
  input: {
    name: '',
    value: false,
    onChange: () => undefined,
    onBlur: () => undefined,
    onFocus: () => undefined
  },
  meta: {
    error: '',
    touched: false
  }
}

const Wrapper = styled.div<{ isInline: boolean }>`
  ${props => css`
    ${applySpacingProps(props)}

    ${props.isInline &&
      css`
        display: flex;
      `}
  `}
`

const Styles = createGlobalStyle`
  .ant-switch-checked {
    background: ${theme.color.primary};
  }
`
