/**
 * Input component wrapper.
 *
 *   - Can be used as react-final-form field if "name" prop is defined
 *   - Can be used as controlled input if both "value" and "onChange" are defined
 *   - Can be used with local state and listened to if only "onChange" is defined
 */

import React from 'react'
import { Field, FieldRenderProps } from 'react-final-form'
import { injectIntl, InjectedIntlProps, FormattedMessage } from 'react-intl'
import { observer } from 'mobx-react'
import { Input as AntdInput } from 'antd'
import makeDebounce from 'lodash/debounce'

import { InputProps, TextAreaProps } from 'antd/es/input'

import { FieldLabel } from '~/components/ui/FieldLabel'
import { FieldError } from '~/components/ui/FieldError'
import { TTheme } from '~/constants'
import { InputWrapper, Wrapper } from '~/components/ui/InputWrappers'
import {
  styled,
  css,
  createGlobalStyle,
  ICSSProps,
  required as requiredValidator,
  getCSSProps,
  uuid,
  renderMessage
} from '~/utils'

type TInputFieldRenderProps = FieldRenderProps<string, HTMLInputElement>
type TFieldOnChange = TInputFieldRenderProps['input']['onChange']
type TOnChangeEvent = React.ChangeEvent<HTMLInputElement>

type TExtendProps = Omit<InputProps, 'placeholder' | 'onChange'> & InjectedIntlProps & ICSSProps

interface ICommonInputProps {
  // ===================================
  // Interaction
  // ===================================
  // use as react-final-form field
  name?: string
  // use as controlled input (if onChange also provided)
  value?: string
  // parent listener
  onChange?: (value: string) => any
  // Validate and render required asterisk
  required?: boolean
  // debounce onChange listener
  debounce?: boolean
  // configure debounce delay
  debounceMs?: number

  // ===================================
  // Content
  // ===================================
  // i18n placeholder
  placeholder?: FormattedMessage.MessageDescriptor
  // i18n label
  label?: FormattedMessage.MessageDescriptor
  // DOM / Cypress identifier
  id?: string

  // ===================================
  // Styling
  // ===================================
  // use with search icon
  isSearch?: boolean
  // use as textarea
  isTextarea?: boolean
  // rows count for textarea variant
  rows?: number
  // use as password field
  isPassword?: boolean
  // Render label inline
  labelInline?: boolean
  // Inline rendered label width
  labelInlineWidth?: number
}

export interface IInputProps extends TExtendProps, ICommonInputProps {
  // use as controlled input (if onChange also provided)
  value?: string
}

interface IInputState {
  value: string
}

// Class component is needed for creating deboucned onChange handler only once
class InputComponent extends React.Component<IInputProps, IInputState> {
  public state = {
    value: ''
  }

  static defaultProps = {
    debounceMs: 500
  }

  private onChange?: (value: string) => any

  constructor(props: IInputProps) {
    super(props)

    this.state = { value: '' }

    const { onChange, debounce, debounceMs } = props

    // debounced onChange...
    // retrospectively doing debounce here was nonsense...
    this.onChange = onChange && debounce ? makeDebounce(onChange, debounceMs) : onChange
  }

  handleChange = (event: TOnChangeEvent, fieldOnChange?: TFieldOnChange) => {
    const value = event.target.value

    if (fieldOnChange) fieldOnChange(value)

    if (this.onChange) this.onChange(value)

    this.setState({ value })
  }

  getValue = (fieldProps?: TInputFieldRenderProps) => {
    if (fieldProps) return fieldProps.input.value

    if (typeof this.props.value !== 'undefined') return this.props.value

    return this.state.value
  }

  renderInput = (fieldProps?: TInputFieldRenderProps) => {
    const {
      intl,
      isPassword,
      isSearch,
      isTextarea,
      label,
      labelInline,
      labelInlineWidth,
      placeholder,
      size = 'large',
      required,
      id = uuid(),
      ...rest
    } = this.props

    let Component = StyledInput
    if (isSearch) Component = StyledSearchInput
    if (isTextarea) Component = StyledTextarea
    if (isPassword) Component = StyledPassword

    const meta = fieldProps ? fieldProps.meta : {}
    const input = fieldProps ? fieldProps.input : {}

    const onChangeFinal = fieldProps ? fieldProps.input.onChange : undefined

    const hasError = meta && meta.touched && meta.error
    const placeholderFinal = renderMessage(placeholder, intl)

    return (
      <Wrapper labelInline={labelInline} cssProps={getCSSProps(rest)} data-cy={id} id={id}>
        <Styles size={size} />

        {label && (
          <FieldLabel
            message={label}
            isInline={labelInline}
            width={labelInlineWidth}
            size={size}
            required={required}
          />
        )}

        <InputWrapper labelInline={labelInline}>
          <Component
            {...rest}
            {...input}
            onChange={(event: TOnChangeEvent) => this.handleChange(event, onChangeFinal)}
            value={this.getValue(fieldProps)}
            placeholder={placeholderFinal}
            hasError={hasError}
            size={size}
          />
        </InputWrapper>

        <FieldError isVisible={hasError}>{meta.error}</FieldError>
      </Wrapper>
    )
  }

  render() {
    const { name, required } = this.props

    const validator = required ? requiredValidator : () => undefined

    if (name) return <Field name={name} render={this.renderInput} validate={validator} />

    return this.renderInput()
  }
}

interface ICustomProps {
  hasError?: boolean
  placeholder?: any
  theme: TTheme
}

const ICON_SIZE = '16px'

type TSize = 'default' | 'large' | 'small' | undefined

const Styles = createGlobalStyle<{ size: TSize }>`
  ${props => css`
    ${props.size === 'large' &&
      css`
        i.ant-input-clear-icon {
          width: ${ICON_SIZE};
          height: ${ICON_SIZE};

          svg {
            width: ${ICON_SIZE};
            height: ${ICON_SIZE};
          }
        }

        i.ant-input-search-icon {
          width: ${ICON_SIZE};
          height: ${ICON_SIZE};
          margin-top: 1px;

          svg {
            width: ${ICON_SIZE};
            height: ${ICON_SIZE};
          }
        }
      `}
  `}
`

const sharedStyles = (props: InputProps & ICustomProps) => css`
  && {
    ${props.size === 'large' &&
      css`
        font-size: 16px;
      `}

    ${props.hasError &&
      css`
        border-color: ${props.theme.color.error};

        &:hover {
          border-color: ${props.theme.color.error};
        }

        &:focus {
          box-shadow: rgba(245, 34, 45, 0.2) 0px 0px 0px 2px;
          border-color: ${props.theme.color.error};
        }
      `}
  }
`

/**
 * Prevent custom props we introduced from getting passed to the DOM element.
 * Styled-components does this automatically but not for 3rd party components.
 */
const withValidProps = (Component: any) => ({ hasError, debounce, debounceMs, ...props }: any) => (
  <Component {...props} />
)

const StyledInput = styled(withValidProps(AntdInput))<InputProps & ICustomProps>`
  ${sharedStyles}
`

const StyledTextarea = styled(withValidProps(AntdInput.TextArea))<TextAreaProps & ICustomProps>`
  ${sharedStyles}
`

const StyledPassword = styled(withValidProps(AntdInput.Password))<InputProps & ICustomProps>`
  ${sharedStyles}
`

const StyledSearchInput = styled(withValidProps(AntdInput.Search))<InputProps & ICustomProps>`
  ${sharedStyles}
`

export const Input = injectIntl(observer(InputComponent))
