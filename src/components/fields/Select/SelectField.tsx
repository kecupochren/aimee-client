import React, { ReactNode } from 'react'
import { FieldRenderProps } from 'react-final-form'
import { useObserver } from 'mobx-react'
import { ModeOption } from 'antd/es/select'

import { SelectComponent } from './SelectComponent'

interface ISelectFieldProps<TOption> extends FieldRenderProps<TOption, any> {
  mode: ModeOption
  children?: ReactNode
}

export function SelectField<TOption = string>(props: ISelectFieldProps<TOption>) {
  return useObserver(() => {
    const { input, meta, children, mode, ...rest } = props

    return (
      <SelectComponent
        {...rest}
        value={input.value || (mode === 'multiple' ? [] : '')}
        onChange={input.onChange}
        onFocus={input.onFocus}
        children={children}
        error={meta.error}
        errorVisible={meta.touched && meta.error}
        mode={mode}
      />
    )
  })
}
