import React from 'react'
import { useObserver } from 'mobx-react'

import { Select as AntdSelect } from 'antd'

import { FieldLabel } from '~/components/ui/FieldLabel'
import { FieldError } from '~/components/ui/FieldError'
import { Icon } from '~/components/ui/Icon'
import { useRootStore } from '~/store/rootStore'
import { styled, css, applyCSSProps, ICSSProps, createGlobalStyle, getCSSProps } from '~/utils'

import { IBaseSelectProps } from './Select'

interface ISelectComponentProps<TOption> extends IBaseSelectProps<TOption> {
  children: React.ReactNode
  value?: TOption
  onChange: (value?: TOption) => void
}

export function SelectComponent<TOption = string>(props: ISelectComponentProps<TOption>) {
  const { helpers } = useRootStore()

  return useObserver(() => {
    const {
      value,
      onChange,
      children,
      labelInline,
      labelInlineWidth,
      label,
      size = 'large',
      placeholder,
      required,
      mode,
      errorVisible,
      error,
      stopPropagation,
      ...rest
    } = props

    const handleClear = () => {
      if (onChange) onChange(undefined)
    }

    const handleWrapperClick = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
      if (stopPropagation) {
        event.stopPropagation()
      }
    }

    const placeholderFinal = placeholder ? helpers.intl.formatMessage(placeholder) : ''

    let valueFinal = value
    if (mode === 'multiple' && value) {
      valueFinal = (value as any).length ? value : undefined
    }

    const hasValue = Boolean(typeof value !== 'undefined' && value)
    const canClear = hasValue && onChange && mode !== 'multiple' && !required

    return (
      <Wrapper labelInline={labelInline} cssProps={getCSSProps(rest)} onClick={handleWrapperClick}>
        <Styles hasValue={hasValue} />

        {label && (
          <FieldLabel
            message={label}
            isInline={labelInline}
            width={labelInlineWidth}
            size={size}
            required={required}
          />
        )}

        <SelectWrapper>
          <StyledSelect
            {...rest}
            size={size}
            mode={mode}
            value={valueFinal}
            onChange={onChange}
            placeholder={placeholderFinal}
          >
            {children}
          </StyledSelect>

          {canClear && (
            <ClearButton onClick={handleClear}>
              <Icon type="close-circle" theme="filled" style={{ fontSize: 16 }} />
            </ClearButton>
          )}
        </SelectWrapper>

        <FieldError isVisible={errorVisible as boolean}>{error}</FieldError>
      </Wrapper>
    )
  })
}

interface IWrapperProps {
  cssProps: ICSSProps
  labelInline?: boolean
}

const Wrapper = styled.div<IWrapperProps>`
  display: flex;
  flex-direction: column;
  position: relative;

  ${props => css`
    min-width: 160px;

    ${props.labelInline &&
      css`
        flex-direction: row;
      `}
  `}

  ${props => applyCSSProps(props.cssProps)}
`

// Props we don't care about
type TOmittedProps = 'placeholder' | 'intl'

// Props we pass in transformed from original
interface ITransformedProps {
  children: any
  placeholder?: string
}

type TStyledSelectProps = Omit<IBaseSelectProps<any>, TOmittedProps> & ITransformedProps

const StyledSelect = styled((props: TStyledSelectProps) => <AntdSelect<any> {...props} />)`
  width: 100%;

  && {
    ${props => css`
      ${props.size === 'large' &&
        css`
          font-size: 16px;
        `}

      ${props.errorVisible &&
        css`
          > div {
            border-color: ${props.theme.color.error};

            &:hover {
              border-color: ${props.theme.color.error};
            }

            &:focus {
              box-shadow: rgba(245, 34, 45, 0.2) 0px 0px 0px 2px;
              border-color: ${props.theme.color.error};
            }
          }
        `}
    `}
  }
`

const SelectWrapper = styled.div`
  box-shadow: ${props => props.theme.boxShadow.default};
  flex: 1 0 auto;
  position: relative;
`

const ClearButton = styled.div`
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
  right: 34px;

  i {
    font-size: 16px;
    color: rgba(0, 0, 0, 0.25);
  }

  &:hover i {
    cursor: pointer;
    color: rgba(0, 0, 0, 0.45);
  }
`

const Styles = createGlobalStyle<{ hasValue: boolean }>`
  span.ant-select-selection__clear {
    width: 16px;
    height: 16px;
    margin-top: -8px;
    right: 33px;

    ${props => css`
      ${props.hasValue &&
        css`
          opacity: 1;
        `}
    `}

    svg {
      width: 16px;
      height: 16px;
    }
  }
`
