import React, { useState, useEffect } from 'react'
import { Field } from 'react-final-form'
import { useObserver, observer } from 'mobx-react'
import { FormattedMessage } from 'react-intl'
import debounce from 'lodash/debounce'

import { Select as AntdSelect } from 'antd'
import { SelectProps as AntdSelectProps } from 'antd/es/select'
import { IconProps as AntdIconProps } from 'antd/es/icon'

import { Spinner } from '~/components/ui/Spinner'
import { Icon } from '~/components/ui/Icon'
import { RepositoryCollection } from '~/store/modules'
import { useRootStore, RootStore } from '~/store/rootStore'
import { messages } from '~/messages'
import { ICSSProps, styled, css, required } from '~/utils'

import { SelectComponent } from './SelectComponent'
import { SelectField } from './SelectField'

export const Option = observer(AntdSelect.Option)

// ===================================
// Control type props
// ===================================
// Use as RFF Field, inside Formd
interface IFieldProps {
  type: 'field'
  name: string
}

// Use as typical React controlled input
interface IControlledProps<TOption = string> {
  type: 'controlled'
  value?: TOption
  onChange: (value?: TOption) => any
}

// Use when you want to just observe selected value
interface IObservedProps<TOption = string> {
  type: 'observed'
  onChange: (value?: TOption) => any
}

// ===================================
// Options source props
// ===================================
export interface ICollectionSelectProps {
  optionsFrom: 'collection'
  collection: RepositoryCollection<any, any, any>
}

export interface IBasicSelectProps {
  optionsFrom: 'children'
  children: React.ReactNode
}

// ===================================
// Main props
// ===================================
interface IProps {
  // i18n placeholder
  placeholder?: FormattedMessage.MessageDescriptor

  // i18n label
  label?: FormattedMessage.MessageDescriptor

  // Render label inline
  labelInline?: boolean

  // Inline rendered label width
  labelInlineWidth?: number

  // Show required asterisk
  required?: boolean

  // Error message
  error?: string

  // Should show error message
  errorVisible?: boolean

  // Hide selected value
  noValue?: boolean

  // Stop onClick event propagation
  stopPropagation?: boolean
}

export type IBaseSelectProps<TOption = string> = IProps & AntdSelectProps<TOption> & ICSSProps

export type ISelectProps<TOption = string> = IBaseSelectProps<TOption> &
  (IFieldProps | IControlledProps<TOption> | IObservedProps<TOption>) &
  (ICollectionSelectProps | IBasicSelectProps)

// ===================================
// Main
// ===================================
export function Select<TOption = string>(props: ISelectProps<TOption>) {
  const [value, setValue] = useState<TOption>()
  const rootStore = useRootStore()

  useEffect(() => {
    if (props.optionsFrom === 'collection' && !props.collection.loaded) {
      props.collection.handleFetch()
    }
  }, [])

  return useObserver(() => {
    const optionsProps = getOptionsProps(props, rootStore)

    if (props.type === 'field' && props.name) {
      const validator = props.required ? required : () => undefined

      return (
        <Field<TOption>
          {...props}
          {...optionsProps}
          name={props.name}
          component={fieldProps => <SelectField {...fieldProps} mode={props.mode!} />}
          validate={option => validator(option as any)}
        />
      )
    }

    if (props.type === 'controlled') {
      return (
        <SelectComponent
          {...props}
          {...optionsProps}
          value={props.value}
          onChange={props.onChange}
        />
      )
    }

    if (props.type === 'observed') {
      const handleChange = (selected?: TOption) => {
        setValue(selected)
        props.onChange(selected)
      }

      return (
        <SelectComponent
          {...props}
          {...optionsProps}
          value={!props.noValue ? value : undefined}
          onChange={handleChange}
        />
      )
    }

    return null
  })
}

function getOptionsProps<TOption>(props: ISelectProps<TOption>, rootStore: RootStore) {
  if (props.optionsFrom === 'children') {
    return { children: props.children }
  }

  if (props.optionsFrom === 'collection') {
    const { searching, search, asSelectOptions } = props.collection

    const noResultsMessage = rootStore.helpers.intl.formatMessage(messages.NoResultsFound)

    return {
      children: asSelectOptions.map(x => (
        <Option key={x.value} value={x.value}>
          {x.title}
        </Option>
      )),
      showSearch: true,
      showArrow: true,
      filterOption: false,
      loading: searching,
      notFoundContent: searching ? <Spinner size="small" /> : noResultsMessage,
      suffixIcon: <StyledIcon type="search" size={props.size === 'large' ? 18 : undefined} />,
      onSearch: debounce((text: string) => search(text), 500)
    }
  }

  return { children: null }
}

const StyledIcon = styled(Icon)<AntdIconProps>`
  && svg {
    ${props => css`
      color: ${props.theme.color.textLigher};
      transform: none !important;
    `}
  }
`
