import React, { useCallback } from 'react'
import { useDropzone, DropzoneInputProps, FileWithPath } from 'react-dropzone'
import { Button } from '~/components/ui/Button'
import { FormattedMessage } from 'react-intl'

export interface IFileUploadProps extends Partial<Omit<DropzoneInputProps, 'onDrop'>> {
  message?: FormattedMessage.MessageDescriptor
  onDrop: (files: FileWithPath[]) => void
}

export const FileUpload = (props: IFileUploadProps) => {
  const { message, onDrop, ...restProps } = props
  const [isLoading, setIsLoading] = React.useState<boolean>(false)

  const handleFileIsLoadedState = (acceptedFiles: FileWithPath[]) => {
    setIsLoading(true)

    acceptedFiles.forEach(file => {
      const reader = new FileReader()

      reader.onabort = () => setIsLoading(false)
      reader.onerror = () => setIsLoading(false)
      reader.onload = () => {
        setIsLoading(false)
      }
      reader.readAsArrayBuffer(file)
    })
  }

  const customOnDrop = useCallback((acceptedFiles: FileWithPath[]) => {
    if (props.onDrop) {
      handleFileIsLoadedState(acceptedFiles)
      props.onDrop(acceptedFiles)
    }
  }, [])

  const { getRootProps, getInputProps } = useDropzone({
    onDrop: customOnDrop,
    ...restProps
  })

  const inputProps: DropzoneInputProps = {
    ...getInputProps()
  }

  return (
    <span {...getRootProps()}>
      <input {...inputProps} />
      <Button message={message} disabled={props.disabled} loading={isLoading} />
    </span>
  )
}
