import React from 'react'
import { fireEvent } from '@testing-library/react'
import { SynchronizedHistory } from 'mobx-react-router'

import { render, getByClassName } from '~/utils/testing'
import { createRootStore, RootStore } from '~/store/rootStore'

import { Datepicker } from './Datepicker'

let store: { store: RootStore; history: SynchronizedHistory }

const DATE_ISO = '2020-03-05T00:00:00.000Z'
const DATE_ISO_END = '2020-03-05T23:59:59.999Z'
const DATE_DEFAULT = '05.03.2020'
const INPUT_TEST_ID = 'datepicker-input'

beforeEach(() => {
  store = createRootStore()
})

it('handles ISO string date format as initial value', () => {
  const { onChange, getValue } = mockState(DATE_ISO)

  const result = render(<Datepicker value={getValue()} onChange={onChange} />, store)
  const input = result.getByTestId(INPUT_TEST_ID)

  expect(input.getAttribute('value')).toBe(DATE_DEFAULT)
})

it('handles default date format as initial value', () => {
  const { onChange, getValue } = mockState(DATE_DEFAULT)

  const result = render(<Datepicker value={getValue()} onChange={onChange} />, store)
  const input = result.getByTestId(INPUT_TEST_ID)

  expect(input.getAttribute('value')).toBe(DATE_DEFAULT)
})

it('can set date by typing input', () => {
  const { onChange, getValue } = mockState()

  const result = render(<Datepicker value={getValue()} onChange={onChange} />, store)
  const input = result.getByTestId(INPUT_TEST_ID)

  fireEvent.input(input, { target: { value: DATE_DEFAULT } })

  expect(getValue()).toBe(DATE_ISO)
})

it('can clear date by deleting input', () => {
  const { onChange, getValue } = mockState(DATE_ISO)

  const result = render(<Datepicker value={getValue()} onChange={onChange} />, store)
  const input = result.getByTestId(INPUT_TEST_ID)

  fireEvent.input(input, { target: { value: ' ' } })

  expect(getValue()).toBe('')
})

it('clears invalid date from input on blur', async () => {
  const { onChange, getValue } = mockState(DATE_ISO)

  const result = render(<Datepicker value={getValue()} onChange={onChange} />, store)
  const input = result.getByTestId(INPUT_TEST_ID)

  fireEvent.focus(input)
  fireEvent.input(input, { target: { value: '2018-02' } })
  fireEvent.blur(input)

  expect(getValue()).toBe('')
  expect(input.getAttribute('value')).toBe('')
})

it('skips on blur check when there is no change to input', async () => {
  const { onChange, getValue } = mockState(DATE_ISO)

  const result = render(<Datepicker value={getValue()} onChange={onChange} />, store)
  const input = result.getByTestId(INPUT_TEST_ID)

  fireEvent.focus(input)
  fireEvent.blur(input)

  expect(getValue()).toBe(DATE_ISO)
  expect(input.getAttribute('value')).toBe(DATE_DEFAULT)
})

it('calls parent blur handler', async () => {
  const { onChange, getValue } = mockState(DATE_ISO)

  const onBlur = jest.fn()

  const result = render(
    <Datepicker value={getValue()} onChange={onChange} onBlur={onBlur} />,
    store
  )
  const input = result.getByTestId(INPUT_TEST_ID)

  fireEvent.blur(input)

  expect(onBlur).toBeCalled()
})

it('calls parent focus handler', () => {
  const { onChange, getValue } = mockState(DATE_ISO)

  const onFocus = jest.fn()

  const result = render(
    <Datepicker value={getValue()} onChange={onChange} onFocus={onFocus} />,
    store
  )
  const input = result.getByTestId(INPUT_TEST_ID)

  fireEvent.focus(input)
  expect(onFocus).toBeCalled()
})

it('uses end of day moment when props.useEndOfDay', () => {
  const { onChange, getValue } = mockState()

  const result = render(<Datepicker value={getValue()} onChange={onChange} useEndOfDay />, store)
  const input = result.getByTestId(INPUT_TEST_ID)

  fireEvent.input(input, { target: { value: DATE_DEFAULT } })

  expect(getValue()).toBe(DATE_ISO_END)
})

it('can be disabled', () => {
  const { onChange, getValue } = mockState(DATE_DEFAULT)

  const result = render(<Datepicker value={getValue()} onChange={onChange} disabled />, store)
  const input = result.getByTestId(INPUT_TEST_ID)

  expect(input.getAttribute('disabled')).toBe('')
})

it('can be auto-focused', () => {
  const { onChange, getValue } = mockState(DATE_DEFAULT)

  const result = render(<Datepicker value={getValue()} onChange={onChange} autoFocus />, store)
  const input = result.getByTestId(INPUT_TEST_ID)

  expect(document.activeElement).toBe(input)
})

it('has popup fixed on the right side when props.alignRight', () => {
  const { onChange, getValue } = mockState(DATE_DEFAULT)

  const result = render(<Datepicker value={getValue()} onChange={onChange} alignRight />, store)
  const popup = getByClassName(result.container, 'rdtPicker')

  expect(popup).toHaveStyle({ right: 0 })
})

it('renders alternative small version when props.small', () => {
  const { onChange, getValue } = mockState(DATE_DEFAULT)

  const result = render(<Datepicker value={getValue()} onChange={onChange} small />, store)
  const popup = getByClassName(result.container, 'rdtPicker')

  expect(popup).toHaveStyle({ width: '215px', boxSizing: 'border-box' })
})

it('uses date format as default placeholder', () => {
  const { onChange, getValue } = mockState()

  const dateFormat = 'YYYY-MM-DD'

  const comp = <Datepicker value={getValue()} onChange={onChange} dateFormat={dateFormat} />
  const result = render(comp)
  const input = result.getByTestId(INPUT_TEST_ID)

  expect(input.getAttribute('placeholder')).toBe(dateFormat)
})

// TODO: Move to Input.test
it('renders placeholder when empty', () => {
  const { onChange, getValue } = mockState()

  const placeholder = 'foobar'

  const comp = <Datepicker value={getValue()} onChange={onChange} placeholder={placeholder} />
  const result = render(comp)
  const input = result.getByTestId(INPUT_TEST_ID)

  expect(input.getAttribute('placeholder')).toBe(placeholder)
})

const mockState = (initialValue = '') => {
  let value: string | undefined = initialValue

  const onChange = (val?: string) => (value = val)
  const getValue = () => value

  return { getValue, onChange }
}
