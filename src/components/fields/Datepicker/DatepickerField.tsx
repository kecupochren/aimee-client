import React, { FC } from 'react'
import { observer } from 'mobx-react-lite'
import { Field, FieldRenderProps } from 'react-final-form'

import * as validators from '~/utils/validation'
import { omitSpacingProps } from '~/utils'

import { Datepicker, IDatepickerProps } from './Datepicker'
import { BaseField, IBaseFieldProps } from '../BaseField'

type TExtendProps = Omit<IBaseFieldProps, 'component' | 'componentName'> &
  Omit<IDatepickerProps, 'value' | 'onChange'>

// Control value/onChange using react-final-form field
export interface IDatepickerFieldProps extends TExtendProps {
  // field key
  name: string
  // onChange listener
  onChange?: (value: string) => any
}

export const DatepickerField: FC<IDatepickerFieldProps> = observer(props => {
  const { id, name, required, ...rest } = props

  const renderField = (fieldProps: FieldRenderProps<string, HTMLElement>) => {
    const { input, meta } = fieldProps

    const error = meta.touched && meta.error ? meta.error : ''

    return (
      <BaseField
        id={id}
        component={() => <Datepicker {...omitSpacingProps(rest)} {...input} />}
        componentName="datepicker"
        required={required}
        error={error}
        {...rest}
      />
    )
  }

  const validator = required ? validators.required : undefined

  return <Field name={name} render={renderField} validate={validator} />
})
