import { createGlobalStyle } from '~/utils'

export const PexDatepickerStyle = createGlobalStyle`
  input[data-testid="datepicker-input"]:focus ~ .field-message-info {
    color: ${props => props.theme.color.primary};
  }

  .pex-datepicker {
    width: 100%;
  }

  .pex-datepicker .rdtPicker {
    padding: 0;
    border: 1px solid ${props => props.theme.color.border};
    box-shadow: ${props => props.theme.boxShadow.softer};
    border-radius: 4px;
    width: 240px;
    top: 43px;

    tbody {
      margin: -1px;
    }
  }

  .pex-datepicker .rdtDays {
    margin-left: -1px;
  }

  .pex-datepicker--small .rdtPicker .rdtDay {
    height: 28px;
  }

  .pex-datepicker .rdtPrev span,
  .pex-datepicker .rdtNext span {
    font-size: 25px;
    margin-bottom: 3px;
  }

  .pex-datepicker .rdtPicker th {
    border-bottom: 0;
  }

  .pex-datepicker .rdtPicker .rdtActive {
    background-color: ${props => props.theme.color.primary};
  }

  .pex-datepicker .rdtPicker .rdtPrev,
  .pex-datepicker .rdtPicker .rdtNext {
    width: 40px;
  }

  .pex-datepicker .rdtPicker .rdtToday:before {
    right: 0;
    bottom: 0;
  }

  .pex-datepicker .rdtPicker td.rdtDay:hover,
  .pex-datepicker .rdtPicker td.rdtHour:hover,
  .pex-datepicker .rdtPicker td.rdtMinute:hover,
  .pex-datepicker .rdtPicker td.rdtSecond:hover,
  .pex-datepicker .rdtPicker thead tr:first-child th:hover,
  .pex-datepicker .rdtPicker .rdtTimeToggle:hover {
    color: ${props => props.theme.color.primaryLight};
  }

  .pex-datepicker .rdtPicker .rdtSwitch {
    font-weight: 500;
    font-size: 16px;
  }

  .pex-datepicker .rdtPicker .rdtDay {
    height: 32px;
  }

  .pex-datepicker .rdtPicker .rdtDay,
  .pex-datepicker .rdtPicker .rdtMonth,
  .pex-datepicker .rdtPicker .rdtYear {
    border: 1px solid ${props => props.theme.color.borderLight};
  }

  .pex-datepicker .rdtPicker td,
  .pex-datepicker .rdtPicker th {
    vertical-align: middle;
  }

  .pex-datepicker .rdtPicker thead tr:first-child {
    height: 50px;
  }

  .pex-datepicker .rdtPicker thead tr:last-child,
  .pex-datepicker .rdtPicker tbody {
    font-size: 13px;
  }

  .pex-datepicker .rdtPicker thead tr:first-child th {
    vertical-align: middle;
  }

  .pex-datepicker--alignRight .rdtPicker {
    right: 0;
  }

  .pex-datepicker--small .rdtPicker {
    width: 215px;
    box-sizing: border-box;
  }
`
