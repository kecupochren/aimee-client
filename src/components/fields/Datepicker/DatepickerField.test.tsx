import React from 'react'
import { fireEvent } from '@testing-library/react'
import { SynchronizedHistory } from 'mobx-react-router'
import { Form } from 'react-final-form'

import { render } from '~/utils/testing'
import { createRootStore, RootStore } from '~/store/rootStore'

import { DatepickerField, IDatepickerFieldProps } from './DatepickerField'

let store: { store: RootStore; history: SynchronizedHistory }

const DATE_ISO = '2020-03-05T00:00:00.000Z'
const DATE_DEFAULT = '05.03.2020'

const DATE_OTHER_ISO = '2020-03-02T00:00:00.000Z'
const DATE_OTHER_DEFAULT = '02.03.2020'

const INPUT_TEST_ID = 'datepicker-input'
const FORM_TEST_ID = 'foo'

beforeEach(() => {
  store = createRootStore()
})

it('controls Datepicker state using react-final-form', () => {
  const name = 'foo'
  const onSubmit = jest.fn()

  const result = renderInForm(onSubmit, { name })
  const input = result.getByTestId(INPUT_TEST_ID)
  const form = result.getByTestId(FORM_TEST_ID)

  expect(input.getAttribute('value')).toBe(DATE_DEFAULT)

  fireEvent.input(input, { target: { value: DATE_OTHER_DEFAULT } })
  fireEvent.submit(form)

  expect(onSubmit).toBeCalledWith({ [name]: DATE_OTHER_ISO })
})

it.skip('validates value present if props.required', () => {
  const name = 'foo'
  const onSubmit = jest.fn()

  const result = renderInForm(onSubmit, { name, required: true })
  const input = result.getByTestId(INPUT_TEST_ID)

  expect(input.getAttribute('value')).toBe(DATE_DEFAULT)

  fireEvent.input(input, { target: { value: '' } })
  fireEvent.blur(input)

  // TODO: Verify using max-height
  expect(result.getByText('Required')).toBeDefined()
})

function renderInForm<IFormValues>(
  onSubmit: (values: any) => any,
  { name, ...rest }: IDatepickerFieldProps
) {
  return render(
    <Form<IFormValues>
      onSubmit={values => onSubmit(values)}
      initialValues={({ [name]: DATE_ISO } as unknown) as IFormValues}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit} data-testid={FORM_TEST_ID}>
          <DatepickerField name={name} {...rest} />
        </form>
      )}
    />,
    store
  )
}
