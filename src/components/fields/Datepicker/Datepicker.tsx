import React, { FC, useEffect, useState, FocusEvent } from 'react'
import { observer } from 'mobx-react-lite'
import { Moment } from 'moment'
import { FormattedMessage } from 'react-intl'
import RCDateTime from 'react-datetime'
import 'react-datetime/css/react-datetime.css'

import { DATE_FORMATS } from '~/constants'
import { Input } from '~/components/fields/Input'
import { useRootStore } from '~/store/rootStore'
import { isValidISODate, isValidDefaultDate, isValidDate } from '~/utils/dates'
import { ISpacingProps, applySpacingProps } from '~/utils/spacingProps'
import { noop, renderMessage } from '~/utils'

import { PexDatepickerStyle } from './Datepicker.elements'
import styled from 'styled-components'

export const DATEPICKER_INPUT = 'datepicker-input'

export interface IDatepickerProps extends ISpacingProps {
  // Date value - ISO string
  value?: string
  // Set value
  onChange: (value: string, moment?: Moment) => any

  // Focus handler
  onFocus?: (event: FocusEvent<any>) => any
  // Blur handler
  onBlur?: () => any
  // Input placeholder
  placeholder?: FormattedMessage.MessageDescriptor | string
  // Set rendered date format, defaults to YYYY-MM-DD
  dateFormat?: string
  // If true, the internal date format will use end of the day
  useEndOfDay?: boolean
  // If true, the datepicker cannot be interacted with
  disabled?: boolean
  // If true, the input will get focused on mount
  autoFocus?: boolean
  // If true, a red asterisk will be rendered if input is empty
  required?: boolean
  // // If true, the datepicker popover will be fixed on the right side of the input
  alignRight?: boolean
  // If true, alternative smaller version will be rendered
  small?: boolean
}

export const Datepicker: FC<IDatepickerProps> = observer(props => {
  const store = useRootStore()

  const {
    alignRight,
    autoFocus,
    dateFormat = DATE_FORMATS.DEFAULT,
    disabled,
    onBlur = noop,
    onChange,
    onFocus = noop,
    placeholder = dateFormat,
    required,
    small,
    useEndOfDay,
    value,
    ...spacingProps
  } = props

  // Use local state to sanely keep track of internal moment value and input value
  const [moment, setMoment] = useState<Moment>()
  const [input, setInput] = useState('')

  // Sync parent value with internal moment value
  useEffect(() => {
    const date = isValidDate(value)
    if (date) {
      setMoment(date)
    }

    const isoDate = isValidISODate(value)
    if (isoDate) {
      setMoment(isoDate)
    }

    const defaultDate = isValidDefaultDate(value)
    if (defaultDate) {
      setMoment(defaultDate)
    }
  }, [value])

  // Sync internal moment value with parent value
  useEffect(() => {
    let isoString = ''

    if (moment) {
      isoString = (useEndOfDay ? moment.endOf('day') : moment).toISOString()
    }

    onChange(isoString, moment)
  }, [moment])

  const handleChange = (val: string | Moment) => {
    if (typeof val === 'string') {
      return handleInput(val)
    }

    setMoment(val)
    setInput('')
  }

  const handleInput = (inputVal: string) => {
    setInput(inputVal)

    if (inputVal.trim() === '') {
      setMoment(undefined)
    }
  }

  const handleBlur = () => {
    // Prevent storing invalid date value in state
    if (input && !isValidDefaultDate(input)) {
      setMoment(undefined)
      setInput('')
    }

    onBlur()
  }

  const getClassNames = () => {
    let className = `pex-datepicker`

    if (alignRight) {
      className += ' pex-datepicker--alignRight'
    }

    if (small) {
      className += ' pex-datepicker--small'
    }

    return className
  }

  const renderInput = (inputProps: any) => {
    const placeholderFinal = renderMessage(placeholder, store.helpers.intl.intlProvider)

    return (
      <Input
        {...inputProps}
        data-testid={DATEPICKER_INPUT}
        onChange={inputValue => inputProps.onChange({ target: { value: inputValue } })}
        disabled={disabled}
        autoFocus={autoFocus}
        placeholder={placeholderFinal}
        required={required}
        mb={0}
      />
    )
  }

  // Value can be either from input or state.
  // Input has preference since we want to be able to type dates in.
  const valueFinal = input || moment || undefined

  return (
    <Wrapper className={getClassNames()} {...spacingProps}>
      <PexDatepickerStyle />

      <RCDateTime
        utc
        timeFormat={false}
        dateFormat={dateFormat}
        value={valueFinal}
        onChange={handleChange}
        onFocus={onFocus}
        onBlur={handleBlur}
        closeOnSelect
        // Events passed here so they are synced with react-datetime
        inputProps={{
          onFocus,
          onBlur: handleBlur
        }}
        // @ts-ignore - prop missing from typings
        renderInput={renderInput}
      />
    </Wrapper>
  )
})

const Wrapper = styled.div`
  ${props => applySpacingProps(props)}
`
