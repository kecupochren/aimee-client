import React from 'react'
import { FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl'
import { Field, FieldRenderProps } from 'react-final-form'
import { observer } from 'mobx-react-lite'

import { Checkbox as AntdCheckbox } from 'antd'
import { CheckboxProps } from 'antd/es/checkbox'
import { ICSSProps, renderMessage } from '~/utils'

export interface ICheckboxProps extends CheckboxProps, InjectedIntlProps, ICSSProps {
  // react-final-form field name
  name?: string
  // i18n label
  label?: FormattedMessage.MessageDescriptor | string
}

const CheckboxComponent: React.FC<ICheckboxProps> = props => {
  const renderField = (fieldProps?: FieldRenderProps<any, any>) => {
    const input = fieldProps ? fieldProps.input : {}

    const onChange = (e: any) => {
      if (fieldProps) {
        fieldProps.input.onChange(e)
      }

      if (props.onChange) {
        props.onChange(e)
      }
    }

    return (
      <AntdCheckbox {...input} {...props} onChange={onChange}>
        {renderMessage(props.label)}
      </AntdCheckbox>
    )
  }

  if (props.name) {
    return <Field name={props.name} render={renderField} type="checkbox" />
  }

  return renderField()
}

export const Checkbox = injectIntl(observer(CheckboxComponent))
