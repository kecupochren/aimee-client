import React, { FC } from 'react'
import { observer } from 'mobx-react-lite'

import { render } from '~/utils/testing'
import { ISpacingProps } from '~/utils/spacingProps'

import { BaseField } from './BaseField'

const compID = 'input-id'
const compName = 'input'
const compNode = <input data-testid={compID} />
const Comp: FC = () => compNode
const CompObserved: FC = observer(() => compNode)

it('renders field component defined as FC', () => {
  const result = render(<BaseField component={Comp} componentName={compName} />)
  const field = result.getByTestId(compID)
  expect(field).toBeDefined()
})

it('renders field component defined as observed FC', () => {
  const result = render(<BaseField component={CompObserved} componentName={compName} />)
  const field = result.getByTestId(compID)
  expect(field).toBeDefined()
})

it('renders field component defined as ReactNode', () => {
  const result = render(<BaseField component={compNode} componentName={compName} />)
  const field = result.getByTestId(compID)
  expect(field).toBeDefined()
})

it('renders label', () => {
  const label = 'Foobar'
  const result = render(<BaseField component={Comp} componentName={compName} label={label} />)
  expect(result.getByText(label)).toBeDefined()
})

it('renders error', () => {
  const error = 'Error foo'
  const result = render(<BaseField component={Comp} componentName={compName} error={error} />)
  expect(result.getByText(error)).toBeDefined()
})

it('renders note', () => {
  const note = 'Info message'
  const result = render(<BaseField component={Comp} componentName={compName} note={note} />)
  expect(result.getByText(note)).toBeDefined()
})

it('sets identifiers', () => {
  const id = 'foo'
  const result = render(<BaseField component={Comp} componentName={compName} id={id} />)
  expect(result.wrapper.getAttribute('id')).toBe(id)
  expect(result.wrapper.getAttribute('data-testid')).toBe(`${compName}-${id}`)
  expect(result.wrapper.getAttribute('data-cy')).toBe(`${compName}-${id}`)
})

it('sets spacing props', () => {
  const mb = 10
  const mr = 5
  const spacingProps: ISpacingProps = { mb, mr }

  const result = render(<BaseField component={Comp} componentName={compName} {...spacingProps} />)

  expect(result.wrapper).toHaveStyle({ 'margin-bottom': `${mb}px` })
  expect(result.wrapper).toHaveStyle({ 'margin-right': `${mr}px` })
})
