import React, { FC, ReactNode, ComponentType } from 'react'
import { observer } from 'mobx-react-lite'
import styled, { css } from 'styled-components'
import { FormattedMessage } from 'react-intl'

import { FieldLabel } from '~/components/ui/FieldLabel'
import { FieldMessage } from '~/components/ui/FieldMessage'
import { ISpacingProps, applySpacingProps } from '~/utils/spacingProps'

export interface IBaseFieldProps extends ISpacingProps {
  // Identifier, also used for data-testId/data-cy
  id?: string

  // Field component render fn
  component: ReactNode | (() => ReactNode)
  // Component name, used as prefix for identifiers
  componentName: string

  // Field label
  label?: FormattedMessage.MessageDescriptor | string
  // Render field label inline
  inline?: boolean

  // Err message rendered under the component
  error?: string
  // If true, the error will not be rendered
  hideError?: boolean
  // Message rendered under the component
  note?: string
  // If true, asterisk indicating required field will be rendered
  required?: boolean
}

export const BaseField: FC<IBaseFieldProps> = observer(props => {
  const {
    id,
    componentName,
    inline,
    label,
    error,
    note,
    hideError,
    required,
    ...spacingProps
  } = props

  const testId = `${componentName}-${id}`
  const cypressId = `${componentName}-${id}`

  return (
    <Wrapper
      id={id}
      data-testid={testId}
      data-cy={cypressId}
      inline={Boolean(inline)}
      {...spacingProps}
    >
      {label && <FieldLabel message={label} isInline={inline} required={required} />}

      <FieldWrapper>
        <Field {...props} />
        <FieldMessage message={note} visible={Boolean(note) && !hideError} type="info" />
        <FieldMessage message={error} visible={Boolean(error)} />
      </FieldWrapper>
    </Wrapper>
  )
})

const Field: FC<IBaseFieldProps> = observer(({ component }) => {
  if (!component) {
    return null
  }

  // Primitive values
  let content = component

  // FC
  if (typeof component === 'function') {
    content = component()
  }

  // ReactNode
  else if (typeof component === 'object' && 'props' in component) {
    content = component
  }

  // Observed FC
  else {
    const Comp = component as ComponentType
    content = <Comp />
  }

  return <>{content}</>
})

const Wrapper = styled.div<{ inline: boolean } & ISpacingProps>`
  ${props => css`
    ${applySpacingProps(props)}

    ${props.inline &&
      css`
        display: flex;
        align-items: flex-start;
        justify-content: center;

        > label {
          width: 120px;
          flex: 1 0 auto;
          margin-top: 8px;
        }
      `}
  `}
`

const FieldWrapper = styled.div`
  flex: 1 1 100%;
`
