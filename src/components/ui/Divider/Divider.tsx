import React from 'react'

import { Divider as AntdDivider } from 'antd'
import { DividerProps } from 'antd/es/divider'

import { styled, css } from '~/utils'

export interface IDividerProps extends DividerProps {
  foo?: String
}

export const Divider = (props: IDividerProps) => <StyledAntdDivider {...props} />

const StyledAntdDivider = styled(AntdDivider)<IDividerProps>`
  && {
    ${props => css`
      ${props.type === 'horizontal' &&
        css`
          margin-left: -30px;
          margin-right: -30px;
          width: calc(100% + 60px);
          margin-top: 34px;
          margin-bottom: 26px;
        `}
    `}
  }
`
