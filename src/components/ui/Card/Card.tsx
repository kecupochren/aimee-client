import React from 'react'
import { observer } from 'mobx-react-lite'

import { Card as AntdCard } from 'antd'
import { CardProps } from 'antd/es/card'

import { TTheme } from '~/constants'
import { styled, applySpacingProps, ISpacingProps, css } from '~/utils'

type TExtendProps = CardProps & ISpacingProps

export interface ICardProps extends TExtendProps {
  bodySpacing?: ISpacingProps
  noBorder?: boolean
}

const CardComponent: React.FC<ICardProps> = props => <StyledCard {...props} />

// Exclude invalid DOM props from getting passed to 3rd party component
const StyledCard = styled(({ bodySpacing, noBorder, ...rest }) => <AntdCard {...rest} />)<
  ICardProps & TTheme
>`
  && {
    box-shadow: ${props => props.theme.boxShadow.default};
    border-radius: ${props => props.theme.borderRadius};

    ${props => css`
      ${applySpacingProps(props)}

      ${props.noBorder &&
        css`
          border: 0;
        `}
    `}

    > .ant-card-body {
      padding: 28px;
      ${props => applySpacingProps(props.bodySpacing || {})}
    }

    .ant-card-head-title {
      font-size: 15px;
      font-weight: 400;
      padding: 12px 0;
    }

    .ant-card-head {
      color: inherit;
      min-height: 46px;
    }
  }
`

export const Card = observer(CardComponent)
