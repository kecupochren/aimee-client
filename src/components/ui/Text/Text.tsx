import React from 'react'
import { observer } from 'mobx-react-lite'

import { Message } from '~/components/ui/Message'
import { styled, css, ISpacingProps, applySpacingProps } from '~/utils'

export interface ITextProps extends ISpacingProps {
  // ===================================
  // Content
  // ===================================
  children?: any
  message?:
    | {
        id: string
        defaultMessage?: string
      }
    | string

  // ===================================
  // Styling
  // ===================================
  fontSize?: number
  inlineBlock?: boolean
  gray?: boolean
  grayer?: boolean
  grayest?: boolean
  color?: string
  bold?: boolean
  bolder?: boolean
}

export const TextComponent: React.FC<ITextProps> = (props: ITextProps) => {
  return (
    <StyledSpan {...props}>
      <Message {...props} />
    </StyledSpan>
  )
}

const StyledSpan = styled.span<ITextProps>`
  ${props => css`
    ${applySpacingProps(props)}

    ${props.fontSize &&
      css`
        font-size: ${props.fontSize}px;
      `}

    ${props.bold &&
      css`
        font-weight: 500;
      `}

    ${props.bolder &&
      css`
        font-weight: 600;
      `}

    ${props.gray &&
      css`
        color: ${props.theme.color.textLight};
      `}

    ${props.grayer &&
      css`
        color: ${props.theme.color.textLigher};
      `}

    ${props.grayest &&
      css`
        color: #ddd;
      `}

    ${props.color &&
      css`
        color: ${props.color};
      `}

    ${props.inlineBlock &&
      css`
        display: inline-block;
      `}
  `}
`

export const Text = observer(TextComponent)
