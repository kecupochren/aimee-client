import React, { FC } from 'react'
import { FormattedMessage } from 'react-intl'
import { observer } from 'mobx-react-lite'

import { Tooltip as AntdTooltip } from 'antd'
import { TooltipProps } from 'antd/lib/Tooltip'
import { Message } from '../Message'

/**
 * Type is used here because an interface cannot extend a union type.
 * More info in section 4. here https://stackoverflow.com/a/52682220
 */
export type ITooltipProps = Omit<TooltipProps, 'overlay'> & {
  message?: FormattedMessage.MessageDescriptor
  content?: React.ReactElement
  overlay?: TooltipProps['overlay']
}

export const Tooltip: FC<ITooltipProps> = observer(props => {
  let overlayFinal = props.overlay || <div />

  if (props.message) {
    overlayFinal = <Message message={props.message} />
  }
  if (props.content) {
    overlayFinal = props.content
  }

  return <AntdTooltip {...props} overlay={overlayFinal} />
})
