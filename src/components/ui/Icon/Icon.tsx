// https://ant.design/components/icon/

import React, { FC } from 'react'

import { Icon as AntdIcon } from 'antd'
import { IconProps } from 'antd/es/Icon'
import 'antd/es/icon/style'

import { Box, IBoxProps } from '~/components/ui/Box'
import { ISpacingProps, getSpacingProps } from '~/utils'

export interface IIconProps extends IconProps, ISpacingProps, IBoxProps {
  disabled?: boolean
  size?: number
  color?: string
  inline?: boolean
}

export const Icon: FC<IIconProps> = ({ size, color, disabled, inline, inlineFlex, ...rest }) => {
  const style: any = { ...rest.style }

  if (size) {
    style.fontSize = `${size}px`
  }

  if (color) {
    style.color = color
  }

  return (
    <Box {...getSpacingProps(rest)} inlineFlex={inline || inlineFlex}>
      <AntdIcon {...rest} twoToneColor={disabled ? '#cacaca' : undefined} style={style} />
    </Box>
  )
}
