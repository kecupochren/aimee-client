import React from 'react'
import { useObserver } from 'mobx-react-lite'

import { Modal } from 'antd'
import { ModalProps } from 'antd/es/modal'

import { useStore } from '~/store'

export interface IModalProps extends ModalProps {
  id: string
  onClose?: () => any
}

export const ModalComponent: React.FC<IModalProps> = ({ id, ...props }) => {
  return useObserver(() => {
    const { modal } = useStore().helpers
    const isOpen = modal.openIDs.includes(id)

    return <Modal {...props} visible={isOpen} />
  })
}
