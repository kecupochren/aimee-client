import React from 'react'

import { logo } from '~/assets/img'
import { styled, css } from '~/utils/styled'
import { applySpacingProps, getSpacingProps, ISpacingProps } from '~/utils/spacingProps'

export interface ILogoProps extends ISpacingProps {
  size?: 'small' | 'medium'
}

export const Logo = (props: ILogoProps) => (
  <Wrapper {...props}>
    <a href="/">
      <img src={logo} alt="Volkswagen logo" />
    </a>
  </Wrapper>
)

const Wrapper = styled.div<ILogoProps>`
  ${props => applySpacingProps(getSpacingProps(props))}

  ${props => {
    let width = 'auto'

    if (props.size === 'small') {
      width = '236px'
    }

    if (props.size === 'medium') {
      width = '407px'
    }

    return css`
      ${width &&
        css`
          img {
            width: ${width};
            height: auto;
          }
        `}
    `
  }}
`
