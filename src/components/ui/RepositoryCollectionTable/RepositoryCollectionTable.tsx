/**
 * This component combines generic Table component with our Collection MobX module.
 */

import React from 'react'
import { useObserver } from 'mobx-react-lite'

import { IRepositoryModel, IRepositoryModelData, RepositoryCollection } from '~/store/modules'
import { useStore } from '~/store'

import { CollectionTable, ICollectionTableProps } from '../CollectionTable'

export interface IRepositoryCollectionTableProps<
  IDType extends string | number,
  IModel extends IRepositoryModelData<IDType>,
  Model extends IRepositoryModel<IDType, IModel>
> extends ICollectionTableProps<IDType, IModel, Model> {
  collection: RepositoryCollection<IDType, IModel, Model, any>
  withBaseActions?: boolean
  editPageRoute?: string
}

export function RepositoryCollectionTable<
  IDType extends string | number,
  IModel extends IRepositoryModelData<IDType>,
  Model extends IRepositoryModel<IDType, IModel>
>(props: IRepositoryCollectionTableProps<IDType, IModel, Model>) {
  return useObserver(() => {
    const store = useStore()

    const { collection, withBaseActions, editPageRoute } = props

    let handleDelete
    let handleEdit

    if (withBaseActions) {
      handleDelete = collection.handleDelete

      if (editPageRoute) {
        handleEdit = (item: Model) =>
          store.helpers.router.push(editPageRoute.replace(':id', item.id.toString()))
      }
    }

    return (
      <CollectionTable<IDType, IModel, Model>
        {...props}
        collection={collection}
        handleDelete={handleDelete}
        handleEdit={handleEdit}
        handleBulkDelete={collection.handleBulkDelete}
      />
    )
  })
}
