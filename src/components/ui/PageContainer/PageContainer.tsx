import { styled, css } from '~/utils'

export const PageContainer = styled.div<{ center?: boolean }>`
  max-width: ${props => props.theme.containerWidth};
  padding: 0 16px;
  box-sizing: border-box;
  width: 100%;
  margin: 0 auto;
  display: flex;

  ${props => css`
    ${props.center &&
      css`
        justify-content: center;
        align-items: center;
      `}
  `}
`
