import React, { FC } from 'react'
import { observer } from 'mobx-react-lite'

import { Pagination as AntdPagination } from 'antd'
import { PaginationProps } from 'antd/lib/Pagination'

export interface IPaginationProps extends PaginationProps {
  foo?: String
}

export const Pagination: FC<IPaginationProps> = observer(props => {
  return <AntdPagination {...props} />
})
