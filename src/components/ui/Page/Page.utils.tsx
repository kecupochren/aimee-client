import React from 'react'
import { Breadcrumb } from 'antd'

import { Link } from '~/components/ui/Link'
import { Icon } from '~/components/ui/Icon'
import { ROUTES, ROUTE_LABELS } from '~/constants'
import { messages } from '~/messages'

import { IPageProps } from './Page'

export function generateBreadcrumbs({ location }: Omit<IPageProps, 'title'>) {
  if (!location) {
    return null
  }

  const pathSnippets = location.pathname.split('/').filter(i => i)

  const homeBreadcrumb = (
    <Breadcrumb.Item key="home">
      <Link href={ROUTES.legacy.DASHBOARD}>
        <Icon type="home" />
      </Link>
    </Breadcrumb.Item>
  )

  const extraBreadcrumbs = pathSnippets.map((_, index) => {
    const url = `/${pathSnippets.slice(0, index + 1).join('/')}`
    let message = ROUTE_LABELS[url]

    const parts = url.split('/')
    const maybeParam = parts[2]
    if (!message && maybeParam) {
      message = messages[maybeParam === 'Add' ? 'Add' : 'Edit']
    }

    return (
      <Breadcrumb.Item key={url}>
        <Link to={url} message={message} />
      </Breadcrumb.Item>
    )
  })

  return [homeBreadcrumb].concat(extraBreadcrumbs)
}
