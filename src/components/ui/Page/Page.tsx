import React from 'react'
import { observer } from 'mobx-react-lite'
import { RouteComponentProps, withRouter } from 'react-router'
import { FormattedMessage } from 'react-intl'

import { styled, css } from '~/utils'
import { Spinner } from '~/components/ui/Spinner'
import { Text } from '~/components/ui/Text'
import { PERMISSIONS } from '~/constants'
import { messages } from '~/messages'
import { useStore } from '~/store'

import { PageContainer } from '../PageContainer'

type TPageWidth = 'sm' | 'md' | 'lg'

export interface IPageProps extends RouteComponentProps {
  // Page title
  title: FormattedMessage.MessageDescriptor
  titleVariable?: string

  // Actions, rendered in header
  actions?: ((props?: any) => Element) | React.ReactNode

  // Arbitary max width
  maxWidth?: number

  // Predefined max width
  width?: TPageWidth

  // Is page loading
  loading?: boolean

  // Waiting text for loading (if provided)
  loadingText?: string

  // Required permission to view this page
  requiredPermission?: PERMISSIONS
}

export const PageComponent: React.FC<IPageProps> = ({
  actions,
  children,
  maxWidth,
  width,
  loading,
  loadingText,
  title,
  titleVariable,
  requiredPermission,
  ...props
}) => {
  const store = useStore()

  const renderTitle = () => {
    if (title && titleVariable) {
      return (
        <span>
          <Text fontSize={17} message={title} />
          <Text fontSize={17} message={titleVariable} />
        </span>
      )
    }

    if (title) {
      return <Text fontSize={17} message={title} />
    }

    if (titleVariable) {
      return <Text fontSize={17} message={titleVariable} />
    }

    return null
  }

  const renderActions = () => {
    if (!actions) return null

    if (typeof actions === 'function') {
      return actions(props)
    }

    return actions
  }

  const hasViewPermission = store.helpers.user.hasPermission(requiredPermission)
  if (!hasViewPermission) {
    return (
      <PageContainer data-cy="page__container">
        <NoPermissions>
          <Text fontSize={14} mt={24} message={messages.MissingPermission} />
        </NoPermissions>
      </PageContainer>
    )
  }

  return (
    <div data-cy="page">
      {(title || actions) && (
        <Header data-cy="page__header">
          <PageContainer>
            {renderTitle()}
            {renderActions()}
          </PageContainer>
        </Header>
      )}

      <PageContainer data-cy="page__container">
        {loading ? (
          <Loading>
            <Spinner tip={loadingText} />
          </Loading>
        ) : (
          <Content {...props} maxWidth={maxWidth} width={width}>
            {children}
          </Content>
        )}
      </PageContainer>
    </div>
  )
}

const Header = styled.div`
  padding: 16px 0;
  background: #fff;
  border-bottom: 1px solid ${props => props.theme.color.border};
  margin-bottom: 24px;
  box-shadow: ${props => props.theme.boxShadow.default};

  > div {
    justify-content: space-between;
    align-items: center;
    height: 32px;
  }
`

const Content = styled.div<{ maxWidth?: number; width?: TPageWidth }>`
  margin: 0 auto;
  max-width: ${props => (props.maxWidth ? `${props.maxWidth}px` : 'none')};
  padding-bottom: 48px;
  width: 100%;

  ${props => css`
    ${props.maxWidth &&
      css`
        max-width: ${props.maxWidth}px;
      `}

    ${props.width &&
      css`
        ${props.width === 'sm' &&
          css`
            max-width: 768px;
          `}

        ${props.width === 'md' &&
          css`
            max-width: 992px;
          `}

        ${props.width === 'lg' &&
          css`
            max-width: 1140px;
          `}
      `}
  `}
`

const Loading = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 48px 0;
  width: 100%;
`

const NoPermissions = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  padding: 24px 0;
`

export const Page = withRouter(observer(PageComponent))
