import React, { FC } from 'react'
import { observer } from 'mobx-react-lite'

import { IconCaret } from '~/assets/icons'
import { Dropdown } from '~/components/ui/Dropdown'
import { Text } from '~/components/ui/Text'
import { messages } from '~/messages'
import { useStore } from '~/store'
import { styled } from '~/utils'

import { NavigationLink } from '../Layout.elements'

export const LanguageMenu: FC = observer(() => {
  const { setLocale, locale } = useStore().helpers.intl

  const options = [
    {
      id: 'lang-set-cz',
      message: messages['cs-CZ'],
      onClick: () => setLocale('cs-CZ'),
      iconClassName: 'flag-icon flag-icon-cz'
    },
    {
      id: 'lang-set-en',
      message: messages['en-US'],
      onClick: () => setLocale('en-US'),
      iconClassName: 'flag-icon flag-icon-gb'
    }
  ]

  return (
    <Dropdown options={options} placement="bottomRight">
      <NavigationLink
        alignRight
        icon={<IconCaret />}
        iconPosition="right"
        iconSpacing={{ ml: 5 }}
        data-cy="lang"
      >
        <LangIcon className={`flag-icon flag-icon-${locale === 'en-US' ? 'gb' : 'cz'}`} />
        <Text message={messages[locale]} />
      </NavigationLink>
    </Dropdown>
  )
})

const LangIcon = styled.span`
  margin-right: 6px;
  margin-left: 6px;
`
