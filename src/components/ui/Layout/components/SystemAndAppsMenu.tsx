import React, { FC } from 'react'
import { observer } from 'mobx-react-lite'

import { IconCaret } from '~/assets/icons'
import { Dropdown, TDropdownOptions } from '~/components/ui/Dropdown'
import { messages } from '~/messages'
import { ROUTES, PERMISSIONS } from '~/constants'

import { NavigationLink } from '../Layout.elements'

export const SystemAndAppsMenu: FC = observer(() => {
  const options: TDropdownOptions = [
    {
      message: messages.SystemRoles,
      requiredPermission: PERMISSIONS.canReadSystemRole,
      href: ROUTES.legacy.SYSTEM_ROLES
    },
    {
      message: messages.SystemPermissions,
      requiredPermission: PERMISSIONS.canReadSystemSecurityFlag,
      href: ROUTES.legacy.SYSTEM_PERMISSIONS
    },
    {
      message: messages.DataSets,
      requiredPermission: PERMISSIONS.canReadDataSet,
      href: ROUTES.legacy.DATA_SETS
    },
    {
      message: messages.SystemApplications,
      requiredPermission: PERMISSIONS.canReadSystemApplication,
      href: ROUTES.legacy.SYSTEM_APPS
    }
  ]

  return (
    <Dropdown options={options}>
      <NavigationLink
        color="white"
        message={messages.SystemAndApps}
        icon={<IconCaret />}
        iconPosition="right"
        iconSpacing={{ ml: 5 }}
        ml={10}
      />
    </Dropdown>
  )
})
