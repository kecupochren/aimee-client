import React, { FC } from 'react'
import { observer } from 'mobx-react-lite'

import { IconCaret } from '~/assets/icons'
import { Dropdown, TDropdownOptions } from '~/components/ui/Dropdown'
import { Icon } from '~/components/ui/Icon'
import { ENVIRONMENT_KEYS } from '~/constants'
import { useStore } from '~/store'
import { humanizeString } from '~/utils'

import { NavigationLink } from '../Layout.elements'

export const EnvMenu: FC = observer(() => {
  const { env } = useStore().helpers

  const isProduction = process.env.NODE_ENV === 'production'
  const isLocalProduction = process.env.LOCAL_PROD
  const options: TDropdownOptions = []
  let iconProps = {}

  if (!isProduction || isLocalProduction) {
    ENVIRONMENT_KEYS.forEach(key =>
      options.push({
        children: humanizeString(key),
        onClick: () => env.setEnv(key)
      })
    )

    iconProps = {
      icon: <IconCaret />,
      iconPosition: 'right',
      iconSpacing: { ml: 5 }
    }
  }

  return (
    <Dropdown options={options}>
      <NavigationLink alignRight static={options.length === 0} {...iconProps}>
        <Icon type="database" mr={6} ml={6} style={{ fontSize: 16 }} />
        {humanizeString(env.config.name)}
      </NavigationLink>
    </Dropdown>
  )
})
