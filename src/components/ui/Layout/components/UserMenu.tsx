import React, { FC } from 'react'
import { observer } from 'mobx-react-lite'

import { IconCaret } from '~/assets/icons'
import { Dropdown, TDropdownOptions } from '~/components/ui/Dropdown'
import { Icon } from '~/components/ui/Icon'
import { messages } from '~/messages'
import { useStore } from '~/store'

import { NavigationLink } from '../Layout.elements'

export const UserMenu: FC = observer(() => {
  const { user } = useStore().helpers

  const options: TDropdownOptions = []
  let iconProps = {}

  if (user.isImpersonated) {
    options.push({
      id: 'cancel-impersonate',
      message: messages.CancelImpersonate,
      onClick: user.cancelImpersonation
    })

    iconProps = {
      icon: <IconCaret />,
      iconPosition: 'right',
      iconSpacing: { ml: 5 }
    }
  }

  return (
    <Dropdown options={options}>
      <NavigationLink alignRight static={options.length === 0} {...iconProps} data-cy="user-name">
        <Icon type="user" mr={6} ml={6} style={{ fontSize: 16 }} />
        {user.name}
      </NavigationLink>
    </Dropdown>
  )
})
