import React, { FC } from 'react'
import { observer } from 'mobx-react-lite'

import { IconCaret } from '~/assets/icons'
import { Dropdown, TDropdownOptions } from '~/components/ui/Dropdown'
import { ROUTES, PERMISSIONS } from '~/constants'
import { messages } from '~/messages'

import { NavigationLink } from '../Layout.elements'

export const BusinessModelMenu: FC = observer(() => {
  const options: TDropdownOptions = [
    {
      message: messages.BusinessRoles,
      requiredPermission: PERMISSIONS.canReadBusinessRole,
      href: ROUTES.legacy.BUSINESS_ROLES
    },
    {
      message: messages.Labels,
      requiredPermission: PERMISSIONS.canReadLabel,
      to: ROUTES.LABELS
    },
    {
      message: messages.LabelRestrictedPairs,
      requiredPermission: PERMISSIONS.canReadLabelRestrictedPair,
      to: ROUTES.LABEL_RESTRICTED_PAIRS
    },
    {
      message: messages.Identities,
      requiredPermission: PERMISSIONS.canReadEmployeeAll,
      href: ROUTES.legacy.IDENTITIES
    },
    {
      message: messages.PreapprovedSodCompensations,
      requiredPermission: PERMISSIONS.canReadPreapprovedSodCompensation,
      href: ROUTES.legacy.PREAPPROVED_SOD_COMPENSATIONS
    },
    {
      message: messages.SodConflicts,
      requiredPermission: PERMISSIONS.canReadWorkflowSummary,
      href: ROUTES.legacy.SOD_CONFLICTS
    },
    {
      message: messages.JobPositions,
      requiredPermission: PERMISSIONS.canReadJobPosition,
      to: ROUTES.JOB_POSITIONS
    },
    {
      message: messages.Departments,
      requiredPermission: PERMISSIONS.canReadDepartment,
      href: ROUTES.legacy.DEPARTMENTS
    },
    {
      message: messages.Companies,
      requiredPermission: PERMISSIONS.canReadCompany,
      href: ROUTES.legacy.COMPANIES
    },
    {
      message: messages.OrgUnits,
      requiredPermission: PERMISSIONS.canReadOrganizationalUnit,
      to: ROUTES.ORGANIZATIONAL_UNITS
    },
    {
      message: messages.WorkflowReport,
      requiredPermission: PERMISSIONS.canReadWorkflowSummary,
      href: ROUTES.legacy.WORKFLOW_REPORT
    },
    {
      message: messages.Recertification,
      requiredPermission: PERMISSIONS.canReadRecertification,
      to: ROUTES.RECERTIFICATIONS
    },
    {
      message: messages.MyRecertificationUnits,
      requiredPermission: PERMISSIONS.canReadMyRecertificationUnit,
      href: ROUTES.legacy.MY_RECERTIFICATION_UNITS
    }
  ]

  return (
    <Dropdown options={options}>
      <NavigationLink
        color="white"
        message={messages.BusinessModel}
        icon={<IconCaret />}
        iconPosition="right"
        iconSpacing={{ ml: 5 }}
      />
    </Dropdown>
  )
})
