import React, { FC } from 'react'

import { Logo } from '~/components/ui/Logo'
import { styled, css } from '~/utils'

import { BusinessModelMenu, SystemAndAppsMenu, LanguageMenu, EnvMenu, UserMenu } from './components'

export const Layout: FC = ({ children }) => (
  <Wrapper>
    <Header>
      <PageContainer center>
        <Logo size="medium" />
      </PageContainer>
    </Header>

    <Navigation>
      <PageContainer>
        <NavigationLeft>
          <BusinessModelMenu />
          <SystemAndAppsMenu />
        </NavigationLeft>

        <NavigationRight>
          <EnvMenu />
          <UserMenu />
          <LanguageMenu />
        </NavigationRight>
      </PageContainer>
    </Navigation>

    <Main>{children}</Main>
  </Wrapper>
)

const HEADER_HEIGHT = '85px'
const NAV_HEIGHT = '50px'

const Wrapper = styled.div``

export const PageContainer = styled.div<{ center?: boolean }>`
  max-width: ${props => props.theme.containerWidth};
  padding: 0 16px;
  box-sizing: border-box;
  width: 100%;
  margin: 0 auto;
  display: flex;

  ${props => css`
    ${props.center &&
      css`
        justify-content: center;
        align-items: center;
      `}
  `}
`

const Header = styled.div`
  height: ${HEADER_HEIGHT};
  display: flex;

  img {
    max-width: 430px;
    height: auto;
  }
`

const Navigation = styled.div`
  height: ${NAV_HEIGHT};
  background: ${props => props.theme.color.primary};
  display: flex;
  /* margin: 0 -12px; */

  ${PageContainer} {
    justify-content: space-between;
    align-items: center;

    > div {
      display: flex;
      flex: 1;
    }
  }
`

const NavigationLeft = styled.div`
  height: ${NAV_HEIGHT};
  margin-left: -12px;
`

const NavigationRight = styled.div`
  height: ${NAV_HEIGHT};
  justify-content: flex-end;
  margin-right: -12px;
`

const Main = styled.div`
  background: ${props => props.theme.color.background};
  min-height: calc(100vh - ${NAV_HEIGHT} - ${HEADER_HEIGHT});

  ${PageContainer} {
    flex-direction: column;
  }
`
