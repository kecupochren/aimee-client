import { styled, css } from '~/utils'

import { Link } from '../Link'

export const NavigationLink = styled(Link)<{
  alignRight?: boolean
  static?: boolean
}>`
  && {
    box-sizing: border-box;
    padding-left: 12px;
    padding-right: 5px;
    transition: 0.2s ease;
    color: #fff;

    &:hover {
      background: rgba(255, 255, 255, 0.08);
      color: #fff;
    }

    ${props => css`
      ${props.alignRight &&
        css`
          padding-right: 12px;
          padding-left: 5px;
        `}

      ${props.static &&
        css`
          &:hover {
            background: none;
            cursor: default;

            > * {
              cursor: default;
            }
          }
        `}
    `}
  }
`
