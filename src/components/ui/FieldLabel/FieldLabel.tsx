import React from 'react'
import { FormattedMessage } from 'react-intl'

import { Text } from '~/components/ui/Text'
import { styled, css, ISpacingProps, applySpacingProps } from '~/utils'

export interface IFieldLabelProps extends ISpacingProps {
  message?: FormattedMessage.MessageDescriptor | string
  isInline?: boolean
  width?: number
  required?: boolean
  size?: 'small' | 'medium' | 'large' | 'default'
}

export const FieldLabel: React.FC<IFieldLabelProps> = ({ size = 'large', ...props }) => {
  if (!props.message) return null

  return (
    <Wrapper {...props} size={size}>
      <Text {...props} />
      {props.required && <Required>*</Required>}
    </Wrapper>
  )
}

const Wrapper = styled.div<IFieldLabelProps>`
  margin-bottom: 5px;

  ${props => css`
    ${props.size === 'large' &&
      css`
        font-size: 15px;
        margin-bottom: 8px;
      `}

    ${props.isInline &&
      css`
        margin-bottom: 0;
        margin-top: 6px;
        margin-right: 12px;

        ${props.size === 'large' &&
          css`
            margin-top: 8px;
          `}
      `}

    ${props.width &&
      css`
        width: ${props.width}px;
        flex-shrink: 0;
      `}

    ${applySpacingProps(props)}
  `}
`

const Required = styled.span`
  color: ${props => props.theme.color.primaryLight};
  margin-left: 5px;
`
