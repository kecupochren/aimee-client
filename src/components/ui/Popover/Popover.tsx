import React from 'react'

import { Popover as AntdPopover } from 'antd'
import { PopoverProps } from 'antd/es/popover'

export interface IPopoverProps extends PopoverProps {
  foo?: String
}

export const Popover: React.FC<IPopoverProps> = ({
  placement = 'bottom',
  trigger = 'click',
  ...props
}) => <AntdPopover placement={placement} trigger={trigger} {...props} />
