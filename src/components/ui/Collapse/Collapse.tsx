import React, { FC } from 'react'

import { Collapse as AntdCollapse } from 'antd'
import { CollapseProps } from 'antd/lib/Collapse'

export interface ICollapseProps extends CollapseProps {
  foo?: String
}

export const Collapse: FC<ICollapseProps> = props => {
  return <AntdCollapse {...props} />
}

export const Panel = AntdCollapse.Panel
