import React from 'react'

import { Spin } from 'antd'
import { SpinProps } from 'antd/es/spin'

export interface ISpinnerProps extends SpinProps {
  foo?: String
}

export const Spinner: React.FC<ISpinnerProps> = props => {
  const { isLoading, pastDelay, timedOut, retry, ...validProps } = props as any
  return <Spin {...validProps} />
}
