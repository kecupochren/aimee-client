import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Link as RouterLink } from 'react-router-dom'
import { observer } from 'mobx-react-lite'
import history from 'history'

import { Message } from '~/components/ui/Message'
import { Box } from '~/components/ui/Box'
import { theme } from '~/constants'
import {
  styled,
  css,
  ISpacingProps,
  getSpacingProps,
  applySpacingProps,
  IWithTheme,
  withValidProps
} from '~/utils'

export interface ILinkProps extends ISpacingProps {
  // ===================================
  // Content
  // ===================================
  // HTML content
  children?: any
  // i18n message content
  message?: FormattedMessage.MessageDescriptor
  // Icon
  icon?: React.ReactNode
  // DOM/Cypress identifier
  id?: string

  // ===================================
  // Interaction
  // ===================================
  // Link to external route
  href?: string
  // Link to internal route
  to?: history.LocationDescriptor
  // Target
  target?: '_blank' | '_self' | '_parent' | '_top'
  // Use as a link styled button
  onClick?: (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => any
  // Is link disabled
  disabled?: boolean

  // ===================================
  // Styling
  // ===================================
  // With permanent underline
  withUnderline?: boolean
  // With underline on hover
  withHoverUnderline?: boolean
  // Disable hover styling
  noHoverStyling?: boolean
  // Display as block
  asBlock?: boolean
  // Use 100% width
  fullWidth?: boolean
  // Set color
  color?: 'white' | 'gray' | 'red'
  // Control icon position relative to label
  iconPosition?: 'left' | 'right'
  // Adjust icon position
  iconSpacing?: ISpacingProps
}

export const LinkComponent: React.FC<ILinkProps> = props => {
  const { icon, iconPosition, to, disabled, onClick, href, message, children, id } = props

  let idProps = {}
  if (id) {
    idProps = { id, 'data-cy': id }
  }

  function renderIcon() {
    return <Box {...getSpacingProps(props.iconSpacing)}>{icon}</Box>
  }

  function renderContent() {
    if (!icon) return <Message {...props} />

    const onRight = iconPosition === 'right'
    const onLeft = !onRight

    return (
      <>
        {icon && onLeft && renderIcon()}
        <Message {...props} />
        {icon && onRight && renderIcon()}
      </>
    )
  }

  if (to) {
    return (
      <StyledRouterLink to={!disabled && to} {...props} {...idProps}>
        {renderContent()}
      </StyledRouterLink>
    )
  }

  if (onClick || href || message || children) {
    const handleClick = (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
      // event.preventDefault()

      if (disabled) {
        return
      }

      if (onClick) {
        onClick(event)
      }
    }

    return (
      <StyledAnchor href={href} {...props} onClick={handleClick} {...idProps}>
        {renderContent()}
      </StyledAnchor>
    )
  }

  return null
}

const sharedStyles = (props: ILinkProps & IWithTheme) => {
  let color

  if (props.color) {
    if (props.color === 'white') {
      color = '#fff'
    }

    if (props.color === 'gray') {
      color = theme.color.textLight
    }

    if (props.color === 'red') {
      color = theme.color.error
    }
  }

  return css`
    /*
     * Using && {} ensures our style override potential defaults from 3rd party components.
     * It generates the classname twice, like .foo.foo {}, increasing specificity.
     */
    && {
      ${applySpacingProps(getSpacingProps(props))}

      display: ${props.asBlock ? 'flex' : 'inline-flex'};
      align-items: center;
      justify-content: center;
      transition: .2s ease;
      box-sizing: border-box;

      ${!props.noHoverStyling &&
        css`
          &:hover {
            cursor: pointer;
            color: ${props.theme.color.primaryLight};

            > * {
              cursor: pointer;
            }
          }
        `}

      ${props.withUnderline &&
        css`
          text-decoration: underline;
        `}

      ${props.withHoverUnderline &&
        css`
          &:hover {
            text-decoration: underline;
          }
        `}

      ${color &&
        css`
          color: ${color};
        `}

      ${props.fullWidth &&
        css`
          width: 100%;
        `}
    }
  `
}

const customProps = ['withUnderline', 'fullWidth', 'noHoverStyling']

const StyledAnchor = styled.a<ILinkProps>`
  ${props => sharedStyles(props)}
`

const StyledRouterLink = styled(withValidProps(RouterLink, customProps))<ILinkProps>`
  ${props => sharedStyles(props)}
`

export const Link = observer(LinkComponent)
