import React, { FC } from 'react'

import { Spinner } from '~/components/ui/Spinner'
import { Logo } from '~/components/ui/Logo/Logo'
import { styled, css } from '~/utils/styled'

export interface IPageLoaderProps {
  fullscreen?: boolean
}

export const PageLoader: FC<IPageLoaderProps> = ({ fullscreen }) => {
  return (
    <Wrapper fullscreen={fullscreen}>
      {fullscreen && <Logo mb={40} size="medium" />}
      <Spinner size="large" />
    </Wrapper>
  )
}

const Wrapper = styled.div<IPageLoaderProps>`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  height: 50vh;

  ${props => css`
    ${props.fullscreen &&
      css`
        height: 100vh;
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
      `}
  `}
`
