import React from 'react'

import { Menu as AntdMenu } from 'antd'
import { MenuItemProps } from 'antd/es/menu/MenuItem'

import { styled } from '~/utils'

export interface IMenuItemProps extends MenuItemProps {
  foo?: String
}

export const MenuItem = (props: IMenuItemProps) => <StyledMenuItem {...props} />

const StyledMenuItem = styled(AntdMenu.Item)`
  && {
    transition: 0.2s ease;

    &:hover {
      background: rgba(0, 0, 0, 0.05);
    }
  }
`
