import React from 'react'

import { Menu as AntdMenu } from 'antd'
import { MenuProps } from 'antd/es/menu'

export interface IMenuProps extends MenuProps {
  children: any
}

export const Menu = (props: IMenuProps) => <AntdMenu {...props} />
