import React from 'react'

import { styled, css, ISpacingProps, applySpacingProps } from '~/utils'

export interface IButtonGroupProps extends ISpacingProps {
  align?: 'start' | 'center' | 'end'
}

export const ButtonGroup: React.FC<IButtonGroupProps> = ({ children, ...props }) => (
  <Wrapper {...props}>{children}</Wrapper>
)

const Wrapper = styled.div<IButtonGroupProps>`
  display: flex;
  align-items: center;
  justify-content: center;

  ${props => css`
    ${applySpacingProps(props)}

    ${props.align === 'start' &&
      css`
        justify-content: flex-start;
      `}

    ${props.align === 'end' &&
      css`
        justify-content: flex-end;
      `}
  `}
`
