import React from 'react'
import { observer } from 'mobx-react'
import { JustifyContentProperty, AlignItemsProperty } from 'csstype'

import { applySpacingProps, ISpacingProps, styled, css } from '~/utils'

export interface IBoxProps extends ISpacingProps {
  flex?: boolean
  inlineFlex?: boolean
  justifyContent?: JustifyContentProperty
  alignItems?: AlignItemsProperty
}

export const Box: React.FC<IBoxProps> = observer(({ children, ...rest }) => {
  return <Wrapper {...rest}>{children}</Wrapper>
})

const Wrapper = styled.div<IBoxProps>`
  ${props => css`
    ${applySpacingProps(props)}

    ${props.flex &&
      css`
        display: flex;
      `}

    ${props.inlineFlex &&
      css`
        display: inline-flex;
      `}

    ${props.justifyContent &&
      css`
        justify-content: ${props.justifyContent};
      `}

    ${props.alignItems &&
      css`
        align-items: ${props.alignItems};
      `}
  `}
`
