import React from 'react'
import { FormattedMessage } from 'react-intl'

import { styled, css } from '~/utils'

export interface IFieldErrorProps {
  children: any
  message?: FormattedMessage.MessageDescriptor
  isVisible: boolean
}

export const FieldError: React.FC<IFieldErrorProps> = ({ children, ...props }) => (
  <Wrapper {...props}>{children}</Wrapper>
)

const Wrapper = styled.div<IFieldErrorProps>`
  height: 0px;
  transition: 0.2s ease-in;
  overflow: hidden;
  color: ${props => props.theme.color.error};

  ${props => css`
    ${props.isVisible &&
      css`
        padding-top: 5px;
        padding-bottom: 20px;
        margin-bottom: -15px;
      `}
  `}
`
