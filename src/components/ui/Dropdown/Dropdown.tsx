import React from 'react'
import { observer } from 'mobx-react-lite'

import { Dropdown as AntdDropdown } from 'antd'
import { DropDownProps } from 'antd/es/dropdown'

import { Menu, MenuItem } from '~/components/ui/Menu'
import { Link, ILinkProps } from '~/components/ui/Link'
import { PERMISSIONS } from '~/constants'
import { styled } from '~/utils'
import { useStore } from '~/store'

export type TDropdownOption = ILinkProps & {
  iconClassName?: string
  requiredPermission?: PERMISSIONS
}

export type TDropdownOptions = TDropdownOption[]

export interface IDropdownProps extends Omit<DropDownProps, 'overlay'> {
  children: any
  options?: TDropdownOptions
  overlay?: React.ReactNode | (() => React.ReactNode)
}

export const Dropdown: React.FC<IDropdownProps> = observer(({ options, children, ...rest }) => {
  const store = useStore()

  if (options && options.length > 0) {
    const validOptions = options.filter(option => {
      if (!option.requiredPermission) {
        return true
      }

      return store.helpers.user.hasPermission(option.requiredPermission)
    })

    const overlay = (
      <Menu>
        {validOptions.map((option, index) => (
          <StyledMenuItem key={index}>
            {option.iconClassName && <DropdownIcon className={option.iconClassName} />}
            <StyledLink {...option} fullWidth />
          </StyledMenuItem>
        ))}
      </Menu>
    )

    return <AntdDropdown overlay={overlay} children={children} {...rest} />
  }

  return children
})

const StyledLink = styled(Link)`
  && {
    padding: 5px 12px;
    margin: 0;
    justify-content: flex-start;
  }
`

const StyledMenuItem = styled(MenuItem)`
  && {
    padding: 0;
  }
`

export const DropdownIcon = styled.span`
  margin-left: 12px;
`
