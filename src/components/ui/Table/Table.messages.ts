import { defineMessages } from 'react-intl'

import { sharedMessages } from '~/messages'

const layoutMessages = defineMessages({
  Selected: {
    id: 'Table.Selected',
    defaultMessage: 'selected'
  },
  SelectedSingle: {
    id: 'Table.SelectedSingle',
    defaultMessage: 'selected'
  },
  Results: {
    id: 'Table.Results',
    defaultMessage: 'results'
  },
  Result: {
    id: 'Table.Result',
    defaultMessage: 'result'
  }
})

export const messages = { ...sharedMessages, ...layoutMessages }
