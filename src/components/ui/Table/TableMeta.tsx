import React, { FC } from 'react'
import { observer } from 'mobx-react-lite'

import { Box } from '~/components/ui/Box'
import { Card } from '~/components/ui/Card'
import { Text } from '~/components/ui/Text'
import { Link } from '~/components/ui/Link'
import { styled, formatExactNumber, css } from '~/utils'

import { messages } from './Table.messages'
import { ITableProps } from './Table'

export const TableMeta: FC<ITableProps<any> & { isUnderTable?: boolean }> = observer(
  ({ pagination, selection, selectionActions, isUnderTable }) => {
    if (!pagination && !selection) return null

    return (
      <MetaInfo
        data-cy="table__meta"
        mb={-1}
        bodySpacing={{ py: 9, px: 21 }}
        isUnderTable={isUnderTable}
      >
        {pagination && <Total total={pagination.total} />}
        {selection && <Selected selection={selection} selectionActions={selectionActions} />}
      </MetaInfo>
    )
  }
)

const Total: FC<{ total?: number }> = observer(({ total }) => {
  if (typeof total === 'undefined') return null

  const message = total === 0 || total > 1 ? messages.Results : messages.Result

  return (
    <div data-cy="table__total">
      <strong>{formatExactNumber(total)}</strong>
      {` `}
      <Text message={message} />
    </div>
  )
})

const Selected: FC<ITableProps<any>> = observer(({ selection, selectionActions = [] }) => {
  if (!selection || !selection.count) return null

  const msg = selection.count > 1 ? messages.Selected : messages.SelectedSingle

  return (
    <Box data-cy="table__selection" ml={12}>
      <strong>{formatExactNumber(selection.count)}</strong>
      {` `}
      <Text message={msg} />

      {selectionActions.map(({ message, onClick }) => (
        <Link key={message.id} message={message} ml={10} onClick={onClick} />
      ))}
    </Box>
  )
})

const MetaInfo = styled(({ isUnderTable, ...props }) => <Card {...props} />)<{
  isUnderTable?: boolean
}>`
  && {
    ${props => css`
      ${props.isUnderTable
        ? css`
            margin-top: -1px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
          `
        : css`
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
          `}
    `}

    > div {
      display: flex;
    }
  }
`
