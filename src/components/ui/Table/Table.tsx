// Usage: https://github.com/ant-design/ant-design/blob/master/components/table/index.en-US.md

export { ColumnProps as ITableColumnProps } from 'antd/lib/table'

import React from 'react'
import { observer } from 'mobx-react'
import { FormattedMessage } from 'react-intl'

import { Table as AntdTable } from 'antd'
import { TableProps } from 'antd/lib/table'
import 'antd/es/table/style'

import { Box } from '~/components/ui/Box'
import { Card } from '~/components/ui/Card'
import { Selection } from '~/store/modules'
import { createGlobalStyle, ISpacingProps, getSpacingProps, css, styled } from '~/utils'

import { TableMeta } from './TableMeta'

export interface ITableProps<T>
  extends Omit<TableProps<T>, 'dataSource' | 'columns'>,
    ISpacingProps {
  dataSource?: TableProps<T>['dataSource']
  columns?: TableProps<any>['columns']
  selection?: Selection<T>
  selectionActions?: { message: FormattedMessage.MessageDescriptor; onClick: () => void }[]
  noPagination?: boolean
  withBorder?: boolean
  withBackground?: boolean
  virtualized?: boolean
}

export const Table = observer(<T extends {}>(props: ITableProps<T>) => {
  const {
    noPagination,
    dataSource,
    rowKey = 'id',
    withBorder,
    withBackground,
    size,
    virtualized,
    pagination,
    onRowClick,
    ...rest
  } = props

  const length = dataSource ? dataSource.length : 0

  return (
    <Card noBorder bodySpacing={{ p: 0 }} {...getSpacingProps(rest)}>
      <TableMeta pagination={pagination} {...rest} />

      <Wrapper
        withBorder={withBorder}
        withBackground={withBackground}
        withShadow={size === 'small'}
        data-cy="table"
      >
        <Styles hasRowClick={Boolean(onRowClick)} />

        <AntdTable
          {...rest}
          size={size}
          dataSource={dataSource}
          rowKey={rowKey}
          pagination={noPagination ? false : pagination}
          onRowClick={onRowClick}
        />
      </Wrapper>

      {length > 15 && <TableMeta isUnderTable pagination={pagination} {...rest} />}
    </Card>
  )
})

const Styles = createGlobalStyle<{ hasRowClick: boolean }>`
  /* Fix checkbox tick centering */
  .ant-table-tbody .ant-checkbox-inner::after {
    width: 6px;
    height: 9px;
    left: 3px;
  }

  .ant-table-pagination.ant-pagination {
    padding-right: 16px;
  }

  .ant-table-selection-column {
    padding: 12px 22px !important;
  }

  .ant-pagination {
    position: absolute;
    z-index: 10;
    top: -48px;
    right: -4px;
  }

  ${props => css`
    ${props.hasRowClick &&
      css`
        .ant-table-tbody td:hover {
          cursor: pointer;
        }
      `}
  `}
`

type TWrapperProps = ISpacingProps & {
  withBorder?: boolean
  withBackground?: boolean
  withShadow?: boolean
}

const Wrapper = styled(Box)<TWrapperProps>`
  ${props => css`
    ${props.withBackground &&
      css`
        background: #fff;
      `}

    ${props.withShadow &&
      css`
        box-shadow: ${props.theme.boxShadow.softer};
      `}

    ${props.withBorder &&
      css`
        border: 1px solid ${props.theme.color.border};
        border-bottom: 0;
      `}
  `}
`
