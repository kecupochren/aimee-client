import { styled, css, applyCSSProps, ICSSProps } from '~/utils'

interface IWrapperProps {
  cssProps: ICSSProps
  labelInline?: boolean
}

export const Wrapper = styled.div<IWrapperProps>`
  display: flex;
  flex-direction: column;

  ${props => css`
    ${props.labelInline &&
      css`
        flex-direction: row;
      `}
  `}

  ${props => applyCSSProps(props.cssProps)}
`

export const InputWrapper = styled.div<{ labelInline?: boolean }>`
  box-shadow: ${props => props.theme.boxShadow.default};
  flex: 1 0 auto;
`
