import React from 'react'
import { createGlobalStyle } from 'styled-components'
import { FormattedMessage } from 'react-intl'
import { observer } from 'mobx-react-lite'

import { Button as AntdButton } from 'antd'
import { ButtonProps } from 'antd/es/button'
import 'antd/es/button/style'

import { Message } from '~/components/ui/Message'
import { theme, PERMISSIONS } from '~/constants'
import { styled, ISpacingProps, applySpacingProps, css, withValidProps } from '~/utils'
import { useStore } from '~/store'

type TExtendProps = ButtonProps & ISpacingProps

export interface IButtonProps extends TExtendProps {
  // i18n message
  message?: FormattedMessage.MessageDescriptor
  // use as link
  to?: string
  // required permission to use this button
  requiredPermission?: PERMISSIONS
  // use as return butotn
  returnButton?: boolean
}

const ButtonComponent: React.FC<IButtonProps> = props => {
  const store = useStore()

  const { router } = store.helpers

  const handleClick = (event: React.MouseEvent<any, MouseEvent>) => {
    if (props.returnButton) {
      router.goBack()
    }

    if (props.onClick) {
      props.onClick(event)
    }

    if (props.to) {
      router.push(props.to)
    }
  }

  const getIsDisabled = () => {
    if (!props.requiredPermission) {
      return props.disabled
    }

    return !store.helpers.user.hasPermission(props.requiredPermission)
  }

  return (
    <StyledButton {...props} onClick={handleClick} disabled={getIsDisabled()}>
      <Styles />
      <Message {...props} />
    </StyledButton>
  )
}

const primaryBtnStyles = css`
  /* color: #fff;
  background-color: ${theme.color.primary};
  border-color: ${theme.color.primary};

  &:hover {
    color: #fff;
    background-color: ${theme.color.primaryLighter};
    border-color: ${theme.color.primaryLighter};
  } */
`

const Styles = createGlobalStyle`
  button.ant-btn.ant-btn {
    box-shadow: ${theme.boxShadow.default};
  }

  button.ant-btn-primary[disabled] {
    ${primaryBtnStyles}
    opacity: .4;
  }
`

const StyledButton = styled(withValidProps(AntdButton))`
  && {
    ${props => applySpacingProps(props)}
  }
`

export const Button = observer(ButtonComponent)
