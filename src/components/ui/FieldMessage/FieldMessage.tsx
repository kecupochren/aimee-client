import React, { FC } from 'react'
import { observer } from 'mobx-react-lite'
import styled, { css } from 'styled-components'
import { FormattedMessage } from 'react-intl'

import { theme } from '~/constants'
import { ISpacingProps, applySpacingProps, renderMessage } from '~/utils'

export type TFieldMessageType = 'error' | 'info'

export interface IFieldMessageProps extends ISpacingProps {
  // The message to be rendered
  message?: FormattedMessage.MessageDescriptor | string
  // Is mesessage visible
  visible: boolean

  // Identifier, for data-testid
  id?: string
  // Message type
  type?: TFieldMessageType
  // Override styling
  className?: string
}

export const FieldMessage: FC<IFieldMessageProps> = observer(props => {
  const { visible, message, className = '', type = 'error', id, ...spacingProps } = props

  const name = `field-message-${type}`
  const testId = id ? `${name}-${id}` : name
  const classNameFinal = `${className} ${name}`

  return (
    <Wrapper
      id={id}
      className={classNameFinal}
      data-testid={testId}
      visible={visible}
      type={type}
      {...spacingProps}
    >
      {renderMessage(message)}
    </Wrapper>
  )
})

type TWrapperProps = Pick<IFieldMessageProps, 'visible' | 'type'> & ISpacingProps

const Wrapper = styled.div<TWrapperProps>`
  color: ${theme.color.error};
  height: auto;
  max-height: ${({ visible }) => (visible ? '150px' : '0px')};
  margin: 4px 0 -4px;
  font-size: 14px;
  font-weight: 400;
  transition: max-height 1s ease;
  overflow: hidden;

  ${props => css`
    ${applySpacingProps(props)}

    ${props.type === 'info' &&
      css`
        color: ${props.theme.color.textLight};
      `}
  `}
`
