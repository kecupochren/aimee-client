import { defineMessages } from 'react-intl'

import { sharedMessages } from '~/messages'

const layoutMessages = defineMessages({
  Selected: {
    id: 'CollectionTable.Selected',
    defaultMessage: 'selected'
  },
  SelectedSingle: {
    id: 'CollectionTable.SelectedSingle',
    defaultMessage: 'selected'
  },
  Results: {
    id: 'CollectionTable.Results',
    defaultMessage: 'results'
  },
  Result: {
    id: 'CollectionTable.Result',
    defaultMessage: 'result'
  }
})

export const messages = { ...sharedMessages, ...layoutMessages }
