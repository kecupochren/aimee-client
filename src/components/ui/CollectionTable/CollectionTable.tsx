/**
 * This component combines generic Table component with our Collection MobX module.
 */

import React from 'react'
import { useObserver } from 'mobx-react-lite'
import { ColumnProps } from 'antd/es/table'
import get from 'lodash/get'

import { Divider } from '~/components/ui/Divider'
import { Icon } from '~/components/ui/Icon'
import { Link } from '~/components/ui/Link'
import { Table, ITableProps } from '~/components/ui/Table'
import { Text } from '~/components/ui/Text'
import { Collection, IRepositoryModel, IRepositoryModelData } from '~/store/modules'
import { formatDate, formatDateLong } from '~/utils'

import { messages } from './CollectionTable.messages'
import { Tooltip } from '../Tooltip'

export interface ICollectionTableProps<
  IDType extends ID,
  IModel extends IRepositoryModelData<IDType>,
  Model extends IRepositoryModel<IDType, IModel>
> extends ITableProps<Model> {
  collection: Collection<IDType, IModel, Model>
  columns: ColumnProps<Model>[]
  ofSelectedItems?: boolean
  noPagination?: boolean
  noMeta?: boolean
  noSelect?: boolean
  withBaseColumns?: boolean
  handleEdit?: (item: Model) => void
  handleDelete?: (item: Model) => void
  handleBulkDelete?: () => void
}

export function CollectionTable<
  IDType extends ID,
  IModel extends IRepositoryModelData<IDType>,
  Model extends IRepositoryModel<IDType, IModel>
>(props: ICollectionTableProps<IDType, IModel, Model>) {
  return useObserver(() => {
    const {
      collection,
      columns,
      ofSelectedItems,
      withBaseColumns,
      handleEdit,
      handleDelete,
      handleBulkDelete,
      selectionActions = [],
      noMeta,
      noSelect,
      scroll,
      ...rest
    } = props

    const dataSource = ofSelectedItems ? collection.selected : collection.results

    let columnsFinal = [...columns]
    if (withBaseColumns) {
      columnsFinal = columnsFinal.concat(baseColumns)
    }

    if (handleEdit || handleDelete) {
      const hasEditAction = Boolean(handleEdit)
      const hasDeleteAction = Boolean(handleDelete)

      if (hasEditAction || hasDeleteAction) {
        columnsFinal.push({
          title: <Text message={messages.Action} />,
          key: 'action',
          width: 100,
          fixed: Boolean(scroll?.x) ? 'right' : undefined,
          render: (_text, item) => (
            <span style={{ minWidth: '75px', display: 'block' }}>
              {hasEditAction && (
                <Tooltip message={messages.Edit}>
                  <Link
                    id="btn-edit"
                    noHoverStyling
                    // disabled={!item.canBeEdited} edit button should be visible always, because user has permissions to read
                    // if he didn't have this permission, he wouldn't see this item
                    onClick={() => handleEdit && handleEdit(item)}
                  >
                    <Icon type="edit" theme="twoTone" />
                  </Link>
                </Tooltip>
              )}

              {hasEditAction && hasDeleteAction && <Divider type="vertical" />}

              {hasDeleteAction && (
                <Tooltip message={messages.Delete}>
                  <Link
                    id="btn-delete"
                    noHoverStyling
                    disabled={!item.canBeDeleted}
                    onClick={() => handleDelete && handleDelete(item)}
                  >
                    <Icon type="delete" theme="twoTone" disabled={!item.canBeDeleted} />
                  </Link>
                </Tooltip>
              )}
            </span>
          )
        })
      }
    }

    // Set default sort order from collection
    const { orderBy, direction } = collection.pagination
    columnsFinal = columnsFinal.map(col => {
      if (col.key === orderBy) {
        return { ...col, defaultSortOrder: direction }
      }

      return col
    })

    // Sync selected rows to collection state
    const rowSelection = {
      selectedRowKeys: collection.selection.ids,
      onChange: (_keys: string[] | number[], items: Model[]) => collection.selection.set(items)
    }

    const selectionActionsFinal = [...selectionActions]
    if (handleBulkDelete) {
      selectionActionsFinal.push({ message: messages.Delete, onClick: handleBulkDelete })
    }

    return (
      <Table<Model>
        // React prop-types will complain that ObservableArray is not actually Array but Object.
        // Converting to Array here would break data being observed/reactive, so nothing we can do.
        dataSource={dataSource}
        columns={columnsFinal}
        rowKey={(entity: Model) => entity.id.toString()}
        pagination={collection.pagination.tablePaginationProps}
        rowSelection={noSelect ? undefined : rowSelection}
        onChange={collection.handleTableChange}
        loading={collection.loading || collection.searching}
        selection={collection.selection}
        selectionActions={selectionActionsFinal}
        size="middle"
        withBackground
        withBorder
        scroll={scroll}
        {...rest}
      />
    )
  })
}

export const baseColumns = [
  {
    title: <Text message={messages.CreatedBy} />,
    dataIndex: 'createdBy',
    key: 'createdBy',
    align: 'right' as any,
    // width: 120,
    sorter: true
  },
  {
    title: <Text message={messages.Created} />,
    dataIndex: 'created',
    key: 'created',
    sorter: true,
    width: 120,
    render: (_: any, entity: any) => renderTimestamp(get(entity, 'created'))
  },
  {
    title: <Text message={messages.ChangedBy} />,
    dataIndex: 'changedBy',
    sorter: true,
    align: 'right' as any,
    // width: 120,
    key: 'changedBy'
  },
  {
    title: <Text message={messages.Changed} />,
    dataIndex: 'changed',
    key: 'changed',
    width: 120,
    sorter: true,
    render: (_: any, entity: any) => renderTimestamp(get(entity, 'changed'))
  }
]

const renderTimestamp = (timestamp: string) => {
  if (timestamp) {
    return <Tooltip overlay={formatDateLong(timestamp)}>{formatDate(timestamp)}</Tooltip>
  }

  return null
}

export const renderDateValue = (date: string) => {
  return renderTimestamp(date)
}
