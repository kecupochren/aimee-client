import { FC } from 'react'
import { observer } from 'mobx-react-lite'
import { FormattedMessage } from 'react-intl'

import { useRootStore } from '~/store/rootStore'
import { renderMessage } from '~/utils'

export interface IMessageProps {
  message?: FormattedMessage.MessageDescriptor | string
}

// @ts-ignore
export const Message: FC<IMessageProps> = observer(props => {
  const rootStore = useRootStore()

  if (props.children) {
    return props.children
  }

  if (!props.message) {
    return null
  }

  if (typeof props.message !== 'string' && props.message.id) {
    // Touch message here to let observer know we use it and hence it can rerender relevant parts
    // as necessary.
    // @ts-ignore
    const message = rootStore.helpers.intl.dictionary[props.message.id]
  }

  return renderMessage(props.message)
})
