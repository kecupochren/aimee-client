import React from 'react'

import { Tag as AntdTag } from 'antd'
import { TagProps } from 'antd/es/tag'

export interface ITagProps extends TagProps {
  foo?: String
}

export const Tag: React.FC<ITagProps> = props => <AntdTag {...props} />
