const theme = {
  borderRadius: '5px',
  containerWidth: '1400px',

  color: {
    primary: '#004563',
    primaryLight: '#0b92cb',
    primaryLighter: '#00a1e6',

    text: 'rgba(0, 0, 0, .85)',
    textLigher: 'rgba(0, 0, 0, .45)',
    textLight: 'rgba(0, 0, 0, .65)',

    border: '#e8e8e8',
    borderLight: '#e8e8e8',

    background: '#f0f2f5',

    info: '#1590ff',
    warning: '#faad15',
    success: '#53c41a',
    error: '#f52e3a'
  },

  boxShadow: {
    softer: '1px 7px 8px rgba(0,0,0,0.02)',
    default: '1px 7px 12px rgba(0,0,0,0.03)'
  },

  font: {
    family: {
      // Taken from "AntD Pro" dashboard css, produces similar looking, pretty fonts on all platforms
      regular: `
        -apple-system,
        BlinkMacSystemFont,
        'Segoe UI',
        'PingFang SC',
        'Hiragino Sans GB',
        'Microsoft YaHei',
        'Helvetica Neue',
        Helvetica,
        Arial,
        sans-serif,
        'Apple Color Emoji',
        'Segoe UI Emoji',
        'Segoe UI Symbol' !important
      `
    },
    size: {
      default: '13px',
      small: '12px'
    }
  }
}

module.exports = { theme }
