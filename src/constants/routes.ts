import { messages } from '~/messages'

export const ROUTES = {
  /**
   * Business model
   */
  LABELS: '/spa/labels',
  LABEL_EDIT: '/spa/labels/edit/:id',
  LABEL_NEW: '/spa/labels/new',

  LABEL_RESTRICTED_PAIRS: '/spa/label-restricted-pairs',
  LABEL_RESTRICTED_PAIR_EDIT: '/spa/label-restricted-pairs/edit/:id',
  LABEL_RESTRICTED_PAIR_NEW: '/spa/label-restricted-pairs/new',

  ORGANIZATIONAL_UNITS: '/spa/organizational-units',
  ORGANIZATIONAL_UNIT_EDIT: '/spa/organizational-units/edit/:id',
  ORGANIZATIONAL_UNIT_NEW: '/spa/organizational-units/new',

  JOB_POSITIONS: '/spa/job-positions',
  JOB_POSITION_EDIT: '/spa/job-positions/edit/:id',
  JOB_POSITION_NEW: '/spa/job-positions/new',

  RECERTIFICATIONS: '/spa/recertifications',
  RECERTIFICATION_EDIT: '/spa/recertifications/edit/:id',
  RECERTIFICATION_NEW: '/spa/recertifications/new',

  RECERTIFICATION_UNITS: '/spa/recertifications/:id/recertification-units',
  RECERTIFICATION_UNIT_EDIT: '/spa/recertification-units/:id',

  /**
   * Legacy app routes
   */
  legacy: {
    DASHBOARD: '/',
    BUSINESS_ROLES: '/BusinessCodebooks/BusinessRoles#/List',
    BUSINESS_ROLE: '/BusinessCodebooks/BusinessRoles#/Edit/:id',
    IDENTITIES: '/BusinessCodebooks/Identities#/List',
    IDENTITY: '/BusinessCodebooks/Identities#/Edit/:id',
    PREAPPROVED_SOD_COMPENSATIONS: '/BusinessCodebooks/PreapprovedSodCompensation#/List',
    SOD_CONFLICTS: '/BusinessCodebooks/SodCompensation#/List',
    DEPARTMENTS: '/BusinessCodebooks/Departments#/List',
    DEPARTMENT: '/BusinessCodebooks/Departments#/Edit/:id',
    COMPANIES: '/BusinessCodebooks/Companies#/List',
    COMPANY: '/BusinessCodebooks/Companies#/Edit/:id',
    WORKFLOW_REPORT: '/BusinessCodebooks/WorkflowDocumentReport#/List',
    SYSTEM_ROLES: '/SystemCodebooks/SystemRoles#/List',
    SYSTEM_ROLE: '/SystemCodebooks/SystemRoles#/Edit/:id',
    SYSTEM_PERMISSIONS: '/SystemCodebooks/SystemSecurityFlags#/List',
    SYSTEM_PERMISSION: '/SystemCodebooks/SystemSecurityFlags#/Edit/:id',
    DATA_SETS: '/SystemCodebooks/DataSets#/List',
    SYSTEM_APPS: '/SystemCodebooks/SystemApplications#/List',
    SYSTEM_APP: '/SystemCodebooks/SystemApplications#/Edit/:id',
    MY_RECERTIFICATION_UNITS: '/Home/MyRecertificationUnits'
  }
}

// TODO: Used by Breadcrumbs, which are not used atm but will be
export const ROUTE_LABELS = {
  [ROUTES.LABELS]: messages.Labels,
  [ROUTES.LABEL_EDIT]: messages.Label,
  [ROUTES.LABEL_NEW]: messages.Label,

  [ROUTES.LABEL_RESTRICTED_PAIRS]: messages.LabelRestrictedPairs,
  [ROUTES.LABEL_RESTRICTED_PAIR_EDIT]: messages.LabelRestrictedPair,
  [ROUTES.LABEL_RESTRICTED_PAIR_NEW]: messages.LabelRestrictedPair,

  [ROUTES.ORGANIZATIONAL_UNITS]: messages.OrgUnits,
  [ROUTES.ORGANIZATIONAL_UNIT_EDIT]: messages.OrgUnit,
  [ROUTES.ORGANIZATIONAL_UNIT_NEW]: messages.OrgUnit,

  [ROUTES.RECERTIFICATIONS]: messages.RecertificationList,

  [ROUTES.JOB_POSITIONS]: messages.JobPositions,
  [ROUTES.JOB_POSITION_EDIT]: messages.JobPosition,
  [ROUTES.JOB_POSITION_NEW]: messages.JobPosition,

  [ROUTES.legacy.DASHBOARD]: messages.Dashboard,
  [ROUTES.legacy.BUSINESS_ROLES]: messages.BusinessRoles,
  [ROUTES.legacy.IDENTITIES]: messages.Identities,
  [ROUTES.legacy.PREAPPROVED_SOD_COMPENSATIONS]: messages.PreapprovedSodCompensations,
  [ROUTES.legacy.SOD_CONFLICTS]: messages.SodConflicts,
  [ROUTES.legacy.DEPARTMENTS]: messages.Departments,
  [ROUTES.legacy.COMPANIES]: messages.Companies,
  [ROUTES.legacy.WORKFLOW_REPORT]: messages.WorkflowReport,
  [ROUTES.legacy.SYSTEM_ROLES]: messages.SystemRoles,
  [ROUTES.legacy.SYSTEM_PERMISSIONS]: messages.SystemPermissions,
  [ROUTES.legacy.DATA_SETS]: messages.DataSets,
  [ROUTES.legacy.SYSTEM_APPS]: messages.SystemApplications
}
