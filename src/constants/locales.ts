// API format
export type TLocale = 'cs-CZ' | 'en-US'

// react-intl format
export type TLocaleShort = 'cs' | 'en'

export const LOCALES: TLocale[] = ['cs-CZ', 'en-US']

export const LOCALE_NAMES = {
  'cs-CZ': 'Česky',
  'en-US': 'English'
}
