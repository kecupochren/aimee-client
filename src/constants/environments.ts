export interface IEnvironment {
  name: string
  apiURL: string
}

export type TEnv = 'server' | 'env' | 'mock' | 'stage'

export const ENVIRONMENT_KEYS: TEnv[] = ['server', 'env', 'mock', 'stage']

export const ENVIRONMENTS: { [key in TEnv]: IEnvironment } = {
  // read from window.serverData
  server: {
    name: '',
    apiURL: ''
  },

  // read from .env
  env: {
    name: 'ENV',
    apiURL: process.env.API_URL || ''
  },

  mock: {
    name: 'MOCK',
    apiURL: 'mock'
  },

  stage: {
    name: 'STAGE',
    apiURL: 'https://125-aim-webapi.stage.havit.local'
  }
}
