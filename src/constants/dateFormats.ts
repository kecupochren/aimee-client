export const DATE_FORMATS = {
  DEFAULT: 'DD.MM.YYYY',
  LONG: 'DD.MM.YYYY HH:mm:ss'
}
