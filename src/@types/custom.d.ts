declare module '*.png'
declare module '*.jpg'
declare module '*.svg'

interface IAnyObject {
  [key: string]: any
}

// Type helpers
type Diff<T extends keyof any, U extends keyof any> = ({ [P in T]: P } &
  { [P in U]: never } & { [x: string]: never })[T]

type Overwrite<T, U> = Pick<T, Diff<keyof T, keyof U>> & U

type ID = string | number
