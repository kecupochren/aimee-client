/* tslint:disable interface-name */
import { ITheme } from '~/constants'

declare module 'styled-components' {
  interface DefaultTheme extends ITheme {}
}
