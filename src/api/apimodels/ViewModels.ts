﻿/* tslint:disable */

export interface BaseVM {
  canBeDeleted: boolean
  canBeEdited: boolean
  created: Date | string
  createdBy: string
  changed: Date | string
  changedBy: string
  id: number
}
export interface ListVM<TFilter, UOrderBy> {
  filter: TFilter
  orderByColumn: UOrderBy
  orderByDirection: ListOrderByDirection
  pageNumber: number
  pageSize: number
}
export interface ReturnListVM<T> {
  data: T[]
  total: number
}

/**
 * One workflow step (row) in audit trail
 */
export interface WorkflowAuditTrailVM {
  completed: Date | string
  created: Date | string
  createdBy: string
  currentWorkflowStep: string
  entityChangeSetId: number
  entityId: number
  name: string
  workflowOperationId: number
  workflowStepAction: WorkflowStepAction
}
export enum UOrderBy {}
export interface SystemApplicationFilterVM {
  /**
   * Filter by name
   */
  name: string
}
export interface SystemApplicationListVM {
  id: number
  name: string
}
export interface IRecertificationUnitRowVM {
  key: string
  values: IRecertificationUnitRowValueVM[]
}
export interface RecertificationDetailVM {
  canBeEdited: boolean
  created: Date | string
  createdBy: string
  finalizationDate: Date | string
  id: number
  name: string
  planOfMeasuresDate: Date | string
  recertificationState: RecertificationState
  referenceDate: Date | string
  systemApplications: RecertificationSystemApplicationVM[]
}
export interface RecertificationFilterVM {
  /**
   * Filter by completed flag
   */
  isCompleted: boolean

  /**
   * Filter by recertification state
   */
  recertificationState: RecertificationState

  /**
   * Filter by name
   */
  text: string
}
export interface RecertificationListVM {
  canBeEdited: boolean
  created: Date | string
  createdBy: string
  finalizationDate: Date | string
  id: number
  name: string
  planOfMeasuresDate: Date | string
  recertificationState: RecertificationState
  referenceDate: Date | string
}
export interface RecertificationUnitColumnDisplayVM extends IRecertificationUnitColumnVM {
  columnType: RecertificationUnitColumnType
  entityType: RecertificationEntityType
  headerTitle: string
  propertyName: string
  propertyType: PropertyType
}
export interface RecertificationUnitDetailVM {
  canBeConfirmed: boolean
  canBeEdited: boolean
  canBeUnlocked: boolean
  columnDefinitions: IRecertificationUnitColumnVM[]
  confirmed: Date | string
  confirmedBy: string
  created: Date | string
  createdBy: string
  id: number
  recertificationId: number
  recertificationName: string
  recertificationState: RecertificationState
  recertificationUnitState: RecertificationUnitState
  recertificationUnitTypeName: string
  rows: IRecertificationUnitRowVM[]
  title: string
  unlocked: Date | string
  unlockedBy: string
}
export interface RecertificationUnitFilterVM {
  departmentId: number
  recertificationId: number
  recertificationUnitState: RecertificationUnitState
  recertificationUnitTypeId: number
  systemApplicationId: number
  text: string
}
export enum RecertificationState {
  Initial = 0,
  Opened = 1,
  Confirmed = 2,
  Finalized = 3
}
export interface RecertificationSystemApplicationVM {
  areImportedDataValid: boolean
  hasImportedData: boolean
  imported: Date | string
  importedBy: string
  missingEmployeesDkx: string[]
  missingRoleCodes: string[]
  systemApplicationId: number
  systemApplicationName: string
}
export enum RecertificationEntityType {
  NotSupported = 0,
  Employee = -1,
  Department = -2,
  BusinessRole = -3,
  SystemRole = -4,
  SystemSecurityFlag = -5,
  SystemApplication = -6,
  Label = -7,
  Company = -8
}
export enum RecertificationUnitColumnType {
  Display = 0,
  Input = 1,
  Select = 2
}
export enum RecertificationUnitState {
  Initial = 0,
  Draft = 1,
  Confirmed = 2,
  Unlocked = 3
}
export interface IRecertificationUnitColumnVM {
  columnType: RecertificationUnitColumnType
  headerTitle: string
  propertyName: string
}
export interface IRecertificationUnitRowValueVM {
  propertyName: string
}
export interface UserImpersonationVM {
  userId: number
}

/**
 * Filter for organizational units in list
 */
export interface OrganizationalUnitFilterVM {
  /**
   * Fulltext search
   */
  text: string
}
export interface OrganizationalUnitForSaveVM {
  labelIds: number[]
  name: string
}
export interface OrganizationalUnitLabelVM {
  id: number
  name: string
}

/**
 * Organizational unit viewmodel (client -> server)
 */
export interface OrganizationalUnitVM extends BaseVM {
  labels: OrganizationalUnitLabelVM[]
  name: string
}

/**
 * Filter for labels in list
 */
export interface LabelFilterVM {
  /**
   * Fulltext search
   */
  text: string
}

/**
 * Label viewmodel for saving client -> server
 */
export interface LabelForSaveVM {
  description: string
  isSoD: boolean
  name: string
}

/**
 * Label viewmodel for server -> client
 */
export interface LabelVM extends BaseVM {
  description: string
  isSoD: boolean
  name: string
}
export interface InternalLabelConflictVM {
  /**
   * Popis prvního konfliktního labelu
   */
  firstLabelDescription: string

  /**
   * Jméno prvního konfliktního labelu
   */
  firstLabelName: string

  /**
   * Popis druhého konfliktního labelu
   */
  secondLabelDescription: string

  /**
   * Jméno druhého konfliktního labelu
   */
  secondLabelName: string
}

/**
 * Filter for LabelRestrictedPairs in list
 */
export interface LabelRestrictedPairFilterVM {
  /**
   * Fulltext search
   */
  text: string
}

/**
 * LabelRestrictedPair viewmodel for saving client -> server
 */
export interface LabelRestrictedPairForSaveVM {
  description: string
  firstLabelId: number
  isCompensable: boolean
  secondLabelId: number
}

/**
 * Label ids ViewModel
 */
export interface LabelRestrictedPairLabelIdsVM {
  /**
   * Array of ids of labels to check
   */
  labelIds: number[]
}

/**
 * LabelRestrictedPair viewmodel for server -> client
 */
export interface LabelRestrictedPairVM extends BaseVM {
  description: string
  firstLabelId: number
  firstLabelName: string
  isCompensable: boolean
  secondLabelId: number
  secondLabelName: string
}

/**
 * Bussiness role assigned to Job position (server -> client)
 */
export interface JobPositionBusinessRoleVM {
  /**
   * Business role id
   */
  businessRoleId: number

  /**
   * Name of department
   */
  department: string

  /**
   * Description of Business role
   */
  description: string

  /**
   * Name of Business role
   */
  name: string
}
export interface JobPositionFilterVM {
  /**
   * Id of department. Optional
   */
  departmentId: number

  /**
   * Fulltext search
   */
  text: string
}
export interface JobPositionForSaveVM {
  /**
   * Attached Business Roles
   */
  businessRoleIds: number[]

  /**
   * Id of Department
   */
  departmentId: number

  /**
   * Name of job position
   */
  name: string

  /**
   * Workflow note (note for workflow inserted by user when saving JobPosition)
   */
  workflowNote: string
}

/**
 * Identity assigned to Job position (server -> client)
 */
export interface JobPositionIdentityVM {
  dkxExternalCode: string
  givenName: string
  identityType: string
  surname: string
}

/**
 * Job position list viewmodel (server -> client)
 */
export interface JobPositionListVM extends BaseVM {
  departmentId: number
  departmentName: string
  name: string
  workflowStatus: WorkflowDocumentStatus
}

/**
 * Job position viewmodel (server -> client)
 */
export interface JobPositionVM extends BaseVM {
  auditTrails: WorkflowAuditTrailVM[]
  businessRoles: JobPositionBusinessRoleVM[]
  departmentId: number
  identities: JobPositionIdentityVM[]
  name: string
}
export interface DepartmentFilterVM {
  /**
   * Id of Company. Optional
   */
  companyId: number

  /**
   * Fulltext search
   */
  text: string
}
export interface DepartmentListVM extends BaseVM {
  companyId: number
  name: string
  shortcut: string
}

/**
 * Department VM (server -> client)
 */
export interface DepartmentVM extends BaseVM {
  companyId: number
  deleted: boolean
  name: string
  shortcut: string
}
export interface BusinessRoleListVM extends BaseVM {
  departmentId: number
  departmentName: string
  description: string
  name: string
  workflowStatus: WorkflowDocumentStatus
}
export enum ListOrderByDirection {
  Ascending = 0,
  Descending = 1
}
export enum WorkflowDocumentStatus {
  NotStarted = 0,
  InProgress = 1,
  Approved = 2,
  Rejected = 3
}
export enum WorkflowStepAction {
  JobPositionCreate = 1,
  JobPositionModify = 2,
  JobPositionDelete = 3
}
export enum PropertyType {
  Array = 1,
  Number = 2,
  String = 4,
  DateTime = 8,
  Boolean = 16
}
