﻿/* tslint:disable */

/**
 * Třída popisuje validační chybu WebAPI.
 */
export interface ValidationErrorModel {
  /**
		 * Chyby validace modelu.
            Null hodnota není do JSON serializována.
            Je vzájemně výlučné s Message. Buď je jedno, nebo druhé.
		 */
  errors: FieldValidationError[]

  /**
		 * Text chyby. Použito pro chyby vyhozené "ručně" výjimkou OperationFailedException (ev. jiné).
            Null hodnota není do JSON serializována.
            Je vzájemně výlučné s Errors. Buď je jedno, nebo druhé.
		 */
  message: string

  /**
   * Stack trace výjimky. Použito pro chyby vyhozené "ručně" výjimkou OperationFailedException (ev. jiné). Jen pro aplikaci kompilovanou v DEBUGu!
   */
  stackTrace: string

  /**
   * HTTP Status code. Vždy 422 - Unprocessable Entity.
   */
  statusCode: number

  /**
		 * HTTP Sub status code (nemusí být vyplněn). 
            Příklad v IIS
		 */
  subStatusCode: number
}
export interface FieldValidationError {
  field: string
  message: string
}
