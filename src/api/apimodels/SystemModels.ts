﻿/* tslint:disable */

export interface SystemRecertificationUnitDetailVM extends RecertificationUnitDetailVM {
  systemApplicationName: string
}
export interface IRecertificationUnitRowVM {
  key: string
  values: IRecertificationUnitRowValueVM[]
}
export interface RecertificationUnitDetailVM {
  canBeConfirmed: boolean
  canBeEdited: boolean
  canBeUnlocked: boolean
  columnDefinitions: IRecertificationUnitColumnVM[]
  confirmed: Date | string
  confirmedBy: string
  created: Date | string
  createdBy: string
  id: number
  recertificationId: number
  recertificationName: string
  recertificationState: RecertificationState
  recertificationUnitState: RecertificationUnitState
  recertificationUnitTypeName: string
  rows: IRecertificationUnitRowVM[]
  title: string
  unlocked: Date | string
  unlockedBy: string
}
export enum RecertificationUnitColumnType {
  Display = 0,
  Input = 1,
  Select = 2
}
export enum RecertificationUnitState {
  Initial = 0,
  Draft = 1,
  Confirmed = 2,
  Unlocked = 3
}
export interface IRecertificationUnitColumnVM {
  columnType: RecertificationUnitColumnType
  headerTitle: string
  propertyName: string
}
export interface IRecertificationUnitRowValueVM {
  propertyName: string
}
export enum RecertificationState {
  Initial = 0,
  Opened = 1,
  Confirmed = 2,
  Finalized = 3
}
