﻿/* tslint:disable */

export enum RecertificationUnitColumnType {
  Display = 0,
  Input = 1,
  Select = 2
}
export enum RecertificationUnitState {
  Initial = 0,
  Draft = 1,
  Confirmed = 2,
  Unlocked = 3
}
export interface IRecertificationUnitColumnVM {
  columnType: RecertificationUnitColumnType
  headerTitle: string
  propertyName: string
}
export interface IRecertificationUnitRowValueVM {
  propertyName: string
}
export interface RecertificationUnitColumnInputVM extends IRecertificationUnitColumnVM {
  columnType: RecertificationUnitColumnType
  headerTitle: string
  propertyName: string
  propertyType: PropertyType
}
export interface RecertificationUnitColumnOptionVM {
  text: string
  value: number
}
export interface RecertificationUnitColumnSelectVM extends IRecertificationUnitColumnVM {
  availableOptions: RecertificationUnitColumnOptionVM[]
  columnType: RecertificationUnitColumnType
  headerTitle: string
  propertyName: string
}
export interface RecertificationUnitListVM {
  canBeConfirmed: boolean
  canBeEdited: boolean
  canBeUnlocked: boolean
  confirmedBy: string
  created: Date | string
  createdBy: string
  departmentId: number
  departmentName: string
  id: number
  lastConfirmed: Date | string
  recertificationUnitState: RecertificationUnitState
  recertificationUnitTypeId: number
  recertificationUnitTypeName: string
  systemApplicationId: number
  systemApplicationName: string
  unlocked: Date | string
  unlockedBy: string
}
export interface RecertificationUnitRowDisplayValueVM extends IRecertificationUnitRowValueVM {
  current: string
  entityId: number
  generated: string
  propertyName: string
  saved: string
}
export interface RecertificationUnitRowForSaveVM {
  inputs: RecertificationUnitRowInputForSaveVM[]
  key: string
  statements: RecertificationUnitRowStatementForSaveVM[]
}
export interface RecertificationUnitRowInputForSaveVM {
  propertyName: string
  value: string
}
export interface RecertificationUnitRowInputValueVM extends IRecertificationUnitRowValueVM {
  propertyName: string
  value: string
}
export interface RecertificationUnitRowSelectValueVM extends IRecertificationUnitRowValueVM {
  propertyName: string
  value: RecertificationStatementOption
}
export interface RecertificationUnitRowStatementForSaveVM {
  propertyName: string
  selectedOption: RecertificationUnitColumnOptionVM
}
export enum PropertyType {
  Array = 1,
  Number = 2,
  String = 4,
  DateTime = 8,
  Boolean = 16
}
export enum RecertificationStatementOption {
  NotSet = 0,
  Correct = -1,
  NotCorrect = -2,
  CanBeRemoved = -3,
  NormalRole = -4,
  CriticalRole = -5,
  MyEmplyee = -6,
  NotMyEmployee = -7,
  ChangeOfDepartment = -8
}
