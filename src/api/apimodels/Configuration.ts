﻿/* tslint:disable */

export interface AspPrerenderDataViewModel {
  authorization: AuthorizationViewModel
  enableImpersonation: boolean
  environmentName: string
  impersonatedUserId: number
  isImpersonated: boolean
  language: string
  resources: string
  userDisplayAs: string
  userName: string
  vwfsSubsidiaryName: string
  webApiUrl: string
}
export interface AuthorizationViewModel {
  canConfirmRecertificationUnit: boolean
  canCreateDepartment: boolean
  canCreateJobPosition: boolean
  canCreateLabel: boolean
  canCreateLabelRestrictedPair: boolean
  canCreateOrganizationalUnit: boolean
  canCreateRecertification: boolean
  canCreateSystemSecurityFlag: boolean
  canFullExportReports: boolean
  canImportVCD: boolean
  canReadBusinessRole: boolean
  canReadCompany: boolean
  canReadDataSet: boolean
  canReadDepartment: boolean
  canReadEmployee: boolean
  canReadEmployeeAll: boolean
  canReadIdentityOrganizationalUnitReport: boolean
  canReadJobPosition: boolean
  canReadLabel: boolean
  canReadLabelRestrictedPair: boolean
  canReadOrganizationalUnit: boolean
  canReadPreapprovedSodCompensation: boolean
  canReadRecertification: boolean
  canReadRecertificationUnit: boolean
  canReadReportApplicationsWithRoleSecurityFlags: boolean
  canReadReportEmployeeSodConflicts: boolean
  canReadReportIdentityLabels: boolean
  canReadReports: boolean
  canReadSystemApplication: boolean
  canReadSystemRole: boolean
  canReadSystemSecurityFlag: boolean
  canReadWorkflowSummary: boolean
  canSaveRecertificationUnit: boolean
  canUnlockRecertificationUnit: boolean
}
