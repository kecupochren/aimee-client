﻿/* tslint:disable */

/**
 * Create recertification view model
 */
export interface RecertificationCreateVM {
  finalizationDate: Date | string
  name: string
  planOfMeasuresDate: Date | string
  referenceDate: Date | string
  systemApplicationIds: number[]
}
export interface RecertificationForSaveVM {
  finalizationDate: Date | string
  name: string
  planOfMeasuresDate: Date | string
  referenceDate: Date | string
  systemApplicationIds: number[]
}
export interface RecertificationSystemApplicationVM {
  areImportedDataValid: boolean
  hasImportedData: boolean
  imported: Date | string
  importedBy: string
  missingEmployeesDkx: string[]
  missingRoleCodes: string[]
  systemApplicationId: number
  systemApplicationName: string
}
