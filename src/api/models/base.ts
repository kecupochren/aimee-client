// Shared entity props across models
export interface IBaseVM {
  id: number

  created: Date
  createdBy: string

  changed?: Date
  changedBy?: string

  canBeDeleted: boolean
  canBeEdited: boolean
}

// Generate shared props for an entity
export const baseProps = (id = 1): IBaseVM => ({
  id,
  canBeDeleted: true,
  canBeEdited: true,
  created: new Date(2019, 0, id),
  createdBy: 'Mockserver',
  changed: new Date(2019, 0, id),
  changedBy: 'Mockserver'
})
