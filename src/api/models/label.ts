import { IBaseVM } from './base'

export interface IOrganizationalUnitLabelVM {
  id: number
  name: string
}

export interface ILabelFilterVM {
  Text: string
  DepartmentId?: number
}

export interface ILabelForSaveVM {
  name: string
  description?: string
  isSoD?: boolean
}

export interface ILabelVM extends IBaseVM {
  name: string
  description: string
  isSoD: boolean
}

export interface ILabel extends ILabelVM {}
