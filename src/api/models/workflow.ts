export enum WorkflowStatus {
  NotStarted = 0,
  InProgress = 1,
  Approved = 2,
  Rejected = 3
}

export enum WorkflowStepAction {
  JobPositionCreate = 1,
  JobPositionModify = 2,
  JobPositionDelete = 3
}

export enum WorkflowOperation {
  AcceptId = -1,
  RejectId = -2,
  ForwardId = -3,
  ReturnId = -4,
  AcknowledgeId = -5
}

export interface IWorkflowAuditTrailVM {
  id: number
  completed: Date
  created: Date
  createdBy: string
  currentWorkflowStep: string
  entityChangeSetId: number
  entityId: number
  name: string
  workflowOperationId: number
  workflowStepAction: WorkflowStepAction
}

export interface IWorkflowAuditTrail extends IWorkflowAuditTrailVM {}
