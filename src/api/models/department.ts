import { IBaseVM } from './base'

export interface IDepartmentVM extends IBaseVM {
  name: string
  shortcut: string
  companyId: number
  deleted: boolean
}

export interface IDepartment extends IDepartmentVM {}
