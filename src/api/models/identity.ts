export interface IJobPositionIdentityVM {
  dkxExternalCode: string
  givenName: string
  surname: string
  identityType: string
}

export interface IIdentity extends IJobPositionIdentityVM {}
