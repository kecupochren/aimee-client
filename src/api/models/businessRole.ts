import { IBaseVM } from './base'
import { WorkflowStatus } from './workflow'

export interface IJobPositionBusinessRoleVM {
  businessRoleId: number
  department: string
  name: string
  description: string
}

export interface IBusinessRoleListVM extends IBaseVM {
  name: string
  departmentId: number
  departmentName: string
  description: string
  workflowStatus: WorkflowStatus
}

export interface IBusinessRole extends IBusinessRoleListVM, IJobPositionBusinessRoleVM {}
