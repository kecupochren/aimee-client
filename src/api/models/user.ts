export interface IUser {
  userName: string
  userDisplayAs: string
  isImpersonated: boolean
  impersonatedUserId?: string
}
