import { IBaseVM } from './base'
import { ILabel } from './label'

export interface IOrganizationalUnitFilterVM {
  Text: string
}

export interface IOrganizationalUnitForSaveVM {
  labelIds?: number[]
  name: string
}

export interface IOrganizationalUnitVM extends IBaseVM {
  labels: ILabel[]
  name: string
}

export interface IOrganizationalUnit extends IOrganizationalUnitVM {}
