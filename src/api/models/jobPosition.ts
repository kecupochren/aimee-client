import { IBaseVM } from './base'

import { IJobPositionBusinessRoleVM } from './businessRole'
import { IJobPositionIdentityVM } from './identity'
import { IWorkflowAuditTrailVM, WorkflowStatus } from './workflow'

export interface IJobPositionFilterVM {
  Text: string
  DepartmentId?: number
}

export interface IJobPositionForSaveVM {
  name: string
  departmentId: number
  businessRoleIds?: number[]
  workflowNote: string
}

export interface IJobPositionListVM extends IBaseVM {
  name: string
  departmentId: number
  departmentName: string
  workflowStatus: WorkflowStatus
}

export interface IJobPositionVM extends IBaseVM {
  name: string
  departmentId: number
  businessRoles: IJobPositionBusinessRoleVM[]
  identities: IJobPositionIdentityVM[]
  auditTrails: IWorkflowAuditTrailVM[]
}

export interface IJobPosition extends IJobPositionVM, IJobPositionListVM {}
