import { IBaseVM } from './base'

export interface ILabelRestrictedPairFilterVM {
  Text: string
}

export interface ILabelRestrictedPairForSaveVM {
  description: string
  isCompensable: boolean
  firstLabelId: number
  secondLabelId: number
}

export interface ILabelRestrictedPairVM extends IBaseVM {
  description: string
  isCompensable: boolean
  firstLabelId: number
  firstLabelName: string
  secondLabelId: number
  secondLabelName: string
}

export interface ILabelRestrictedPair extends ILabelRestrictedPairVM {}
