import { IApi } from '../api.interface'

import * as handlers from './handlers'
import { resolve } from './utils'

export const resolvers: IApi = {
  impersonate: (userId: number) => resolve({ userId }),
  unimpersonate: () => resolve({}),
  switchLanguage: () => resolve({}),

  getBusinessRoles: query => resolve(handlers.getBusinessRoles(query)),

  getDepartments: query => resolve(handlers.getDepartments(query)),

  getSystemApplications: query => resolve(handlers.getSystemApplications(query)),

  getRecertificationUnits: query => resolve(handlers.getRecertificationUnits(query)),
  getRecertificationUnit: id => resolve(handlers.getRecertificationUnit(id) as any),
  getRecertificationUnitExportChanges: (id: string) => resolve(handlers.exportRecertificationUnitChanges(Number(id))), // prettier-ignore

  getRecertifications: query => resolve(handlers.getRecertifications(query)),
  getRecertification: id => resolve(handlers.getRecertification(id)),
  createRecertification: props => resolve(handlers.createRecertification(props)),
  editRecertification: (id, props) => resolve(handlers.editRecertification(Number(id), props)),
  uploadSystemAppFile: data => resolve(handlers.editRecertification(1, data)),
  openRecertification: (id: string) => resolve(handlers.openRecertification(Number(id))),
  finalizeRecertification: (id: string) => resolve(handlers.finalizeRecertification(Number(id))),
  getRecertificationExportChanges: (id: string) => resolve(handlers.getRecertificationExportChanges(Number(id))), // prettier-ignore
  getRecertificationExportSummary: (id: string) => resolve(handlers.getRecertificationExportChanges(Number(id))), // prettier-ignore

  getJobPositions: query => resolve(handlers.getJobPositions(query)),
  getJobPosition: id => resolve(handlers.getJobPosition(id)),
  createJobPosition: props => resolve(handlers.createJobPosition(props)),
  editJobPosition: (id, props) => resolve(handlers.editJobPosition(id, props)),
  deleteJobPosition: id => resolve(handlers.deleteJobPosition(id)),

  getLabelRestrictedPairs: query => resolve(handlers.getLabelRestrictedPairs(query)),
  getLabelRestrictedPair: id => resolve(handlers.getLabelRestrictedPair(id)),
  createLabelRestrictedPair: props => resolve(handlers.createLabelRestrictedPair(props)),
  editLabelRestrictedPair: (id, props) => resolve(handlers.editLabelRestrictedPair(id, props)),
  deleteLabelRestrictedPair: id => resolve(handlers.deleteLabelRestrictedPair(id)),

  getLabels: query => resolve(handlers.getLabels(query)),
  getLabel: id => resolve(handlers.getLabel(id)),
  createLabel: props => resolve(handlers.createLabel(props)),
  editLabel: (id, props) => resolve(handlers.editLabel(id, props)),
  deleteLabel: id => resolve(handlers.deleteLabel(id)),

  getOrganizationalUnits: query => resolve(handlers.getOrganizationalUnits(query)),
  getOrganizationalUnit: id => resolve(handlers.getOrganizationalUnit(id)),
  createOrganizationalUnit: props => resolve(handlers.createOrganizationalUnit(props)),
  editOrganizationalUnit: (id, props) => resolve(handlers.editOrganizationalUnit(id, props)),
  deleteOrganizationalUnit: id => resolve(handlers.deleteOrganizationalUnit(id))
}
