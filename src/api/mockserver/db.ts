/**
 * Poor man database implementation for having mocked data somewhat functional
 */

import {
  RecertificationState,
  RecertificationUnitState,
  SystemApplicationListVM
} from '../apimodels/ViewModels'

import times from 'lodash/times'
import set from 'lodash/set'

import { IRecertification } from './../../store/models/Recertification.model'
import {
  baseProps,
  IBusinessRoleListVM,
  IDepartmentVM,
  IJobPosition,
  ILabelVM,
  ILabelRestrictedPairVM,
  IOrganizationalUnitVM
} from '../models'
import { IRecertificationUnit } from '~/store/models'

const COUNT = 5

/**
 * Prepare DB tables
 */
const db = {
  businessRoles: { id: COUNT, data: [] as IBusinessRoleListVM[] },
  departments: { id: COUNT, data: [] as IDepartmentVM[] },
  jobPositions: { id: COUNT, data: [] as IJobPosition[] },
  labelRestrictedPairs: { id: COUNT, data: [] as ILabelRestrictedPairVM[] },
  labels: { id: COUNT, data: [] as ILabelVM[] },
  organizationalUnits: { id: COUNT, data: [] as IOrganizationalUnitVM[] },
  recertifications: { id: COUNT, data: [] as IRecertification[] },
  recertificationUnits: { id: COUNT, data: [] as IRecertificationUnit[] },
  systemApplications: { id: COUNT, data: [] as SystemApplicationListVM[] }
}

/**
 * Generate initial data
 */
times(COUNT).forEach((_, _id) => {
  const id = _id + 1

  db.businessRoles.data.push({
    ...baseProps(id),
    name: `Business role #${id}`,
    departmentId: 1,
    departmentName: '',
    description: '',
    workflowStatus: -1
  })

  db.departments.data.push({
    ...baseProps(id),
    name: `Department #${id}`,
    shortcut: `Department shortcut #${id}`,
    companyId: 1,
    deleted: false
  })

  db.jobPositions.data.push({
    ...baseProps(id),
    name: `Job position #${id}`,
    businessRoles: [
      {
        name: 'Business role #1',
        businessRoleId: 1,
        department: 'Department #1',
        description: 'Business role description #1'
      }
    ],
    identities: [],
    auditTrails: [],
    departmentId: 1,
    departmentName: 'Department #1',
    workflowStatus: -1
  })

  db.labelRestrictedPairs.data.push({
    ...baseProps(id),
    description: `Label restricted pair #${id}`,
    isCompensable: true,
    firstLabelId: 1,
    firstLabelName: 'Label #1',
    secondLabelId: 2,
    secondLabelName: 'Label #2'
  })

  db.labels.data.push({
    ...baseProps(id),
    name: `Label #${id}`,
    description: `Label description #${id}`,
    isSoD: id > 3
  })

  db.organizationalUnits.data.push({
    ...baseProps(id),
    name: `Organizational unit #${id}`,
    labels: []
  })

  db.recertifications.data.push({
    ...baseProps(id),
    name: `Recertification #${id}`,
    // isCompleted: false,
    referenceDate: new Date(2021, 0, id),
    planOfMeasuresDate: new Date(2021, 0, id + 1),
    systemApplications: [
      {
        systemApplicationId: 1,
        areImportedDataValid: true,
        hasImportedData: true,
        imported: new Date(2021, 0, id),
        importedBy: 'Josef Noha',
        missingEmployeesDkx: [],
        missingRoleCodes: [],
        systemApplicationName: 'System app #1'
      }
    ],
    finalizationDate: new Date(2021, 0, id + 2),
    recertificationState: id === 1 ? RecertificationState.Confirmed : RecertificationState.Initial
  })

  db.recertificationUnits.data.push({
    ...baseProps(id),
    // isCompleted: false,
    recertificationId: id,
    canBeConfirmed: true,
    canBeUnlocked: true,
    confirmedBy: 'Josef Noha',
    departmentId: 1,
    departmentName: `Oddělení #${id}`,
    lastConfirmed: new Date().toISOString(),
    recertificationUnitState: RecertificationUnitState.Confirmed,
    recertificationUnitTypeId: 1,
    recertificationUnitTypeName: `Jméno jednotky #${id}`,
    systemApplicationId: 1,
    systemApplicationName: `Syst. aplikace #${id}`,
    unlocked: new Date().toISOString(),
    unlockedBy: 'Mockserver',
    canBeEdited: true,
    columnDefinitions: [],
    confirmed: new Date().toISOString(),
    recertificationName: 'Mocke recert',
    rows: [],
    title: 'title'
  })

  db.systemApplications.data.push({
    id,
    name: `System app #${id}`
  })
})

// ===================================
// Expose helper to update tables
// ===================================
export const updateTable = (table: string, data: IDatabaseTable) => {
  set(db, table, data)
}

export interface IDatabaseTable {
  id: number
  data: any[]
}

export { db }
