import Router from 'koa-router'

import { labels } from './labels'

export function routes(router: Router) {
  labels(router)

  return router
}
