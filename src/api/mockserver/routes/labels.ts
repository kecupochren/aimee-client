import Router from 'koa-router'

import { resolvers } from '../resolvers'

export function labels(router: Router) {
  router.get('/Label', async ctx => {
    const { data, status } = await resolvers.getLabels(ctx.querystring)
    ctx.body = data
    ctx.status = status
  })

  router.get('/Label/:id', async ctx => {
    const { data, status } = await resolvers.getLabel(ctx.params.id)
    ctx.body = data
    ctx.status = data ? status : 422
  })

  router.post('/Label', async ctx => {
    const { data, status } = await resolvers.createLabel(ctx.body)
    ctx.body = data
    ctx.status = status
  })
}
