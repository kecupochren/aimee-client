import qs from 'query-string'
import merge from 'lodash/merge'

import { IResponse } from '../api.interface'
import { baseProps } from '../models'

import { IDatabaseTable, updateTable } from './db'

/**
 * Parse query, simulate searching/sorting/pagination on some list
 */
export const mockListQuery = (data: any[], query: string, primaryField = 'name') => {
  // Parse query
  const params = merge({}, defaultListParams, qs.parse(query))

  // Prepare results
  let results = [...data]

  // Handle searching
  const searchQuery = params[QueryParams.search]
  if (searchQuery) {
    results = results.filter(x => x[primaryField].includes(searchQuery))
  }

  // Handle sorting
  const direction = params[QueryParams.direction]
  const order = params[QueryParams.order] as string
  if (order) {
    results = results.sort((a, b) => {
      if (a[order] < b[order]) return -1
      if (a[order] > b[order]) return 1
      return 0
    })

    if (direction === 'Descending') {
      results.reverse()
    }
  }

  // Handle pagination
  const page = Number((params[QueryParams.page] as string) || '1') - 1
  const pageSize = Number((params[QueryParams.pageSize] as string) || '30')
  if (page) {
    const start = page * pageSize
    results = results.slice(start, start + pageSize)
  }

  return {
    data: results,
    total: results.length
  }
}

/**
 * Find and return entity, null if not found
 */
export const mockOneQuery = (data: any[], predicate: any, key = 'id') => {
  return data.find(x => x[key].toString() === predicate.toString()) || null
}

export const mockCreateMutation = (tableName: string, table: IDatabaseTable, props: any) => {
  const entity = {
    ...baseProps(table.id),
    ...props
  }

  table.id++
  table.data.push(entity)

  updateTable(tableName, table)

  return entity
}

/**
 * Simulate edit mutation
 */
export const mockEditMutation = (
  tableName: string,
  table: IDatabaseTable,
  id: number,
  props: any
) => {
  const index = table.data.findIndex(x => x.id === id)

  if (typeof index !== 'undefined') {
    table.data[index] = { ...table.data[index], ...props }
    updateTable(tableName, table)
  }

  return table.data[index]
}

export const mockDeleteMutation = (tableName: string, table: IDatabaseTable, id: number) => {
  const index = table.data.findIndex(x => x.id === Number(id))

  if (typeof index !== 'undefined') {
    table.data.splice(index, 1)
    updateTable(tableName, table)
  }

  return Boolean(index)
}

/**
 * Resolve some data same as Axios would
 */
export const resolve = <T>(data: T, httpStatus: number = 200): IResponse<T> => {
  const status = data === null ? 404 : httpStatus

  const ok = status >= 200 && status < 300
  const isRedirect = status >= 300 && status < 400
  const isClientErr = status >= 400 && status < 500
  const statusText = ok ? 'ok' : isRedirect ? 'redirect' : isClientErr ? 'clientErr' : 'serverErr'

  return Promise.resolve({
    data,
    status,
    statusText,
    ok,
    headers: {},
    config: {}
  })
}

enum QueryParams {
  search = 'Filter.Text',
  order = 'OrderByColumn',
  direction = 'OrderByDirection',
  page = 'PageNumber',
  pageSize = 'PageSize'
}

const defaultListParams = {
  [QueryParams.search]: '',
  [QueryParams.order]: 'name',
  [QueryParams.direction]: 'Ascending',
  [QueryParams.page]: '1',
  [QueryParams.pageSize]: '30'
}
