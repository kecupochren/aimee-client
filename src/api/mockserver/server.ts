/* tslint:disable no-implicit-dependencies */

/**
 * HTTP server to serve mockApi fixtures for Cypress tests.
 *
 * We can't use HTTP server in runtime since the app is served as static html.
 * In runtime we just use mockApi directly and call its method to get data.
 *
 * Normally we run Cypress against actual API server to have true e2e tests.
 * We can also run them against our mocks though to have integration tests.
 * This way we can determine whether something broke on the client or the server.
 * This also allows us to not use Cypress built-in fixtures, which can't be typed.
 */

import Koa from 'koa'
import Router from 'koa-router'
import BodyParser from 'koa-bodyparser'

import { routes } from './routes'

const app = new Koa()
const router = new Router()

routes(router)

app
  .use(BodyParser())
  .use(router.routes())
  .use(router.allowedMethods())
  .listen(1337)
