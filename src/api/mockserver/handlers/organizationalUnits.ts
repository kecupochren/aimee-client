import { IOrganizationalUnitForSaveVM } from '~/api/models'

import { db } from '../db'
import * as utils from '../utils'

const table = 'organizationalUnits'

export const getOrganizationalUnits = (query: string) => {
  return utils.mockListQuery(db.organizationalUnits.data, query)
}

export const getOrganizationalUnit = (id: number) => {
  return utils.mockOneQuery(db.organizationalUnits.data, id)
}

export const createOrganizationalUnit = (data: IOrganizationalUnitForSaveVM) => {
  return utils.mockCreateMutation(table, db.organizationalUnits, data)
}

export const editOrganizationalUnit = (id: number, data: IOrganizationalUnitForSaveVM) => {
  return utils.mockEditMutation(table, db.organizationalUnits, id, data)
}

export const deleteOrganizationalUnit = (id: number) => {
  return utils.mockDeleteMutation(table, db.organizationalUnits, id)
}
