// import { IJobPositionForSaveVM } from '~/api/models'

import { db } from '../db'
import * as utils from '../utils'
import recertificationUnit from '../mockdata/recertification-unit.json'

// const table = 'recertificationUnits'

export const getRecertificationUnits = (query: string) => {
  return utils.mockListQuery(db.recertificationUnits.data, query)
}

export const getRecertificationUnit = (_id: number) => {
  return recertificationUnit
}

export const exportRecertificationUnitChanges = (_id: number) => {
  return {}
}
