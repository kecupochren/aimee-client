import { db } from '../db'
import * as utils from '../utils'

export const getBusinessRoles = (query: string) => {
  return utils.mockListQuery(db.businessRoles.data, query)
}
