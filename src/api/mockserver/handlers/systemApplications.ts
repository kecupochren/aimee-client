// import { IJobPositionForSaveVM } from '~/api/models'

import { db } from '../db'
import * as utils from '../utils'

// const table = 'recertifications'

export const getSystemApplications = (query?: string) => {
  return utils.mockListQuery(db.systemApplications.data, query || '')
}
