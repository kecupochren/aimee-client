import { ILabelRestrictedPairForSaveVM } from '~/api/models'

import { db } from '../db'
import * as utils from '../utils'

const table = 'labelRestrictedPairs'

export const getLabelRestrictedPairs = (query: string) => {
  return utils.mockListQuery(db.labelRestrictedPairs.data, query)
}

export const getLabelRestrictedPair = (id: number) => {
  return utils.mockOneQuery(db.labelRestrictedPairs.data, id)
}

export const createLabelRestrictedPair = (data: ILabelRestrictedPairForSaveVM) => {
  return utils.mockCreateMutation(table, db.labelRestrictedPairs, data)
}

export const editLabelRestrictedPair = (id: number, data: ILabelRestrictedPairForSaveVM) => {
  return utils.mockEditMutation(table, db.labelRestrictedPairs, id, data)
}

export const deleteLabelRestrictedPair = (id: number) => {
  return utils.mockDeleteMutation(table, db.labelRestrictedPairs, id)
}
