import { ILabelForSaveVM } from '~/api/models'

import { db } from '../db'
import * as utils from '../utils'

const table = 'labels'

export const getLabels = (query: string) => {
  return utils.mockListQuery(db.labels.data, query)
}

export const getLabel = (id: number) => {
  return utils.mockOneQuery(db.labels.data, id)
}

export const createLabel = (data: ILabelForSaveVM) => {
  return utils.mockCreateMutation(table, db.labels, data)
}

export const editLabel = (id: number, data: ILabelForSaveVM) => {
  return utils.mockEditMutation(table, db.labels, id, data)
}

export const deleteLabel = (id: number) => {
  return utils.mockDeleteMutation(table, db.labels, id)
}
