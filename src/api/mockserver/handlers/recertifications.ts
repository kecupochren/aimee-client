// import { IJobPositionForSaveVM } from '~/api/models'

import { db } from '../db'
import * as utils from '../utils'
import { RecertificationForSaveVM } from '~/api/apimodels/Recertifications'

const table = 'recertifications'

export const getRecertifications = (query: string) => {
  return utils.mockListQuery(db.recertifications.data, query)
}

export const getRecertification = (id: number) => {
  return utils.mockOneQuery(db.recertifications.data, id)
}

export const createRecertification = (data: RecertificationForSaveVM) => {
  return utils.mockCreateMutation(table, db.recertifications, data)
}

export const editRecertification = (id: number, data: RecertificationForSaveVM) => {
  return utils.mockEditMutation(table, db.recertifications, id, data)
}

export const openRecertification = (id: number) => {
  return utils.mockOneQuery(db.recertificationUnits.data, id)
}

export const finalizeRecertification = (id: number) => {
  return utils.mockOneQuery(db.recertificationUnits.data, id)
}

export const getRecertificationExportChanges = (_id: number) => {
  return {}
}
