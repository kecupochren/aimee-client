import { db } from '../db'
import * as utils from '../utils'

export const getDepartments = (query: string) => {
  return utils.mockListQuery(db.departments.data, query)
}
