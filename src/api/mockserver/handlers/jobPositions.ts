import { IJobPositionForSaveVM } from '~/api/models'

import { db } from '../db'
import * as utils from '../utils'

const table = 'jobPositions'

export const getJobPositions = (query: string) => {
  return utils.mockListQuery(db.jobPositions.data, query)
}

export const getJobPosition = (id: number) => {
  return utils.mockOneQuery(db.jobPositions.data, id)
}

export const createJobPosition = (data: IJobPositionForSaveVM) => {
  return utils.mockCreateMutation(table, db.jobPositions, data)
}

export const editJobPosition = (id: number, data: IJobPositionForSaveVM) => {
  return utils.mockEditMutation(table, db.jobPositions, id, data)
}

export const deleteJobPosition = (id: number) => {
  return utils.mockDeleteMutation(table, db.jobPositions, id)
}
