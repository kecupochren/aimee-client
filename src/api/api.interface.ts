import { AxiosResponse } from 'axios'

import {
  IBusinessRoleListVM,
  IDepartmentVM,
  IJobPositionForSaveVM,
  IJobPositionListVM,
  IJobPositionVM,
  ILabelForSaveVM,
  ILabelRestrictedPairForSaveVM,
  ILabelRestrictedPairVM,
  ILabelVM,
  IOrganizationalUnitForSaveVM,
  IOrganizationalUnitVM
} from './models'
import { TLocale } from '~/constants'
import {
  RecertificationListVM,
  RecertificationDetailVM,
  SystemApplicationListVM,
  RecertificationSystemApplicationVM,
  RecertificationUnitDetailVM
} from '~/api/apimodels/ViewModels'
import { RecertificationCreateVM, RecertificationForSaveVM } from '~/api/apimodels/Recertifications'
import { RecertificationUnitListVM } from './apimodels/RecertificationUnits'

// prettier-ignore
export interface IApi {
  impersonate: (userId: number) => IResponse<any>
  unimpersonate: () => IResponse<any>
  switchLanguage: (locale: TLocale) => IResponse<any>

  getBusinessRoles: (query: string) => IResponse<IList<IBusinessRoleListVM>>

  getDepartments: (query: string) => IResponse<IList<IDepartmentVM>>

  getSystemApplications: (query?: string) => IResponse<IList<SystemApplicationListVM>>

  getRecertificationUnits: (query: string) => IResponse<IList<RecertificationUnitListVM>>
  getRecertificationUnit: (id: number) => IResponse<RecertificationUnitDetailVM>
  getRecertificationUnitExportChanges: (id: string) => IResponse<any>

  getRecertifications: (query: string) => IResponse<IList<RecertificationListVM>>
  getRecertification: (id: number) => IResponse<RecertificationDetailVM>
  createRecertification: (data: RecertificationCreateVM) => IResponse<RecertificationDetailVM>
  editRecertification: (id: string, data: RecertificationForSaveVM) => IResponse<RecertificationDetailVM>
  uploadSystemAppFile: (data: any) => IResponse<RecertificationSystemApplicationVM>
  openRecertification: (id: string) => IResponse<RecertificationDetailVM>
  finalizeRecertification: (id: string) => IResponse<RecertificationDetailVM>
  getRecertificationExportChanges: (id: string) => IResponse<any>
  getRecertificationExportSummary: (id: string) => IResponse<any>

  getJobPositions: (query: string) => IResponse<IList<IJobPositionListVM>>
  getJobPosition: (id: number) => IResponse<IJobPositionVM>
  createJobPosition: (data: IJobPositionForSaveVM) => IResponse<IJobPositionVM>
  editJobPosition: (id: number, data: IJobPositionForSaveVM ) => IResponse<IJobPositionVM>
  deleteJobPosition: (id: number) => IResponse<boolean>

  getLabelRestrictedPairs: (query: string) => IResponse<IList<ILabelRestrictedPairVM>>
  getLabelRestrictedPair: (id: number) => IResponse<ILabelRestrictedPairVM>
  createLabelRestrictedPair: (data: ILabelRestrictedPairForSaveVM) => IResponse<ILabelRestrictedPairVM>
  editLabelRestrictedPair: (id: number, data: ILabelRestrictedPairForSaveVM) => IResponse<ILabelRestrictedPairVM>
  deleteLabelRestrictedPair: (id: number) => IResponse<boolean>

  getLabels: (query: string) => IResponse<IList<ILabelVM>>
  getLabel: (id: number) => IResponse<ILabelVM>
  createLabel: (data: ILabelForSaveVM) => IResponse<ILabelVM>
  editLabel: (id: number, data: ILabelForSaveVM) => IResponse<ILabelVM>
  deleteLabel: (id: number) => IResponse<boolean>

  getOrganizationalUnits: (query: string) => IResponse<IList<IOrganizationalUnitVM>>
  getOrganizationalUnit: (id: number) => IResponse<IOrganizationalUnitVM>
  createOrganizationalUnit: (data: IOrganizationalUnitForSaveVM) => IResponse<IOrganizationalUnitVM>
  editOrganizationalUnit: (id: number, data: IOrganizationalUnitForSaveVM) => IResponse<IOrganizationalUnitVM>
  deleteOrganizationalUnit: (id: number) => IResponse<boolean>
}

export type IResponse<T> = Promise<AxiosResponse<T>>

// Shape of generic list response returned by the .NET API
export interface IList<T> {
  data: T[]
  total: number
}
