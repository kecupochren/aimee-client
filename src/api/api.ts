import axios from 'axios'

import { IApi } from './api.interface'
import { resolvers } from './mockserver/resolvers'

export function createApi(baseURL?: string): IApi {
  // For runtime mode we only use mockserver's resolves since
  // we can't run actual HTTP server in a client rendered app.
  if (baseURL === 'mock') {
    return resolvers
  }

  const client = axios.create({
    baseURL,
    withCredentials: true,
    headers: {
      'Cache-Control': 'no-cache',
      Pragma: 'no-cache'
    }
  })

  return {
    impersonate: userId => client.post('/User/impersonate', { userId }),
    unimpersonate: () => client.post('/User/unimpersonate'),
    switchLanguage: locale => client.put(`/Localization?code=${locale}`),

    getBusinessRoles: query => client.get(`/BusinessRole?${query}`),

    getDepartments: query => client.get(`/Department?${query}`),

    getSystemApplications: query => client.get(`/SystemApplication?${query}`),

    getRecertificationUnits: query => client.get(`/RecertificationUnit?${query}`),
    getRecertificationUnit: id => client.get(`/RecertificationUnit/${id}`),
    getRecertificationUnitExportChanges: id =>
      client.get(`/RecertificationUnit/${id}/exportchanges`, { responseType: 'blob' }),

    getRecertifications: query => client.get(`/Recertification?${query}`),
    getRecertification: id => client.get(`/Recertification/${id}`),
    createRecertification: data => client.post('/Recertification', data),
    editRecertification: (id, data) => client.put(`/Recertification/${id}`, data),
    uploadSystemAppFile: data => {
      const config = {
        headers: {
          'Content-Type': 'multipart/form-data',
          'Cache-Control': 'no-cache',
          Pragma: 'no-cache'
        }
      }
      return client.post('/Recertification/importSystemApplicationData', data, config)
    },
    openRecertification: id => client.put(`/Recertification/${id}/open`),
    finalizeRecertification: id => client.put(`/Recertification/${id}/finalize`),
    getRecertificationExportChanges: id =>
      client.get(`/Recertification/${id}/exportchanges`, { responseType: 'blob' }),
    getRecertificationExportSummary: id =>
      client.get(`/Recertification/${id}/exportsummary`, { responseType: 'blob' }),

    getJobPositions: query => client.get(`/JobPosition?${query}`),
    getJobPosition: id => client.get(`/JobPosition/${id}`),
    createJobPosition: data => client.post('/JobPosition', data),
    editJobPosition: (id, data) => client.put(`/JobPosition/${id}`, data),
    deleteJobPosition: id => client.delete(`/JobPosition/${id}`),

    getLabelRestrictedPairs: query => client.get(`/LabelRestrictedPair?${query}`),
    getLabelRestrictedPair: id => client.get(`/LabelRestrictedPair/${id}`),
    createLabelRestrictedPair: data => client.post('/LabelRestrictedPair', data),
    editLabelRestrictedPair: (id, data) => client.put(`/LabelRestrictedPair/${id}`, data),
    deleteLabelRestrictedPair: id => client.delete(`/LabelRestrictedPair/${id}`),

    getLabels: query => client.get(`/Label?${query}`),
    getLabel: id => client.get(`/Label/${id}`),
    createLabel: data => client.post('/Label', data),
    editLabel: (id, data) => client.put(`/Label/${id}`, data),
    deleteLabel: id => client.delete(`/Label/${id}`),

    getOrganizationalUnits: query => client.get(`/OrganizationalUnit?${query}`),
    getOrganizationalUnit: id => client.get(`/OrganizationalUnit/${id}`),
    createOrganizationalUnit: data => client.post('/OrganizationalUnit', data),
    editOrganizationalUnit: (id, data) => client.put(`/OrganizationalUnit/${id}`, data),
    deleteOrganizationalUnit: id => client.delete(`/OrganizationalUnit/${id}`)
  }
}
