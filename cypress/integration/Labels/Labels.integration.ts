import qs from 'query-string'

import * as mocks from '../../../src/api/mockserver/handlers'
import { IAPIPagination, TAPIDirection } from '../../../src/store/modules/Pagination'
import { ROUTES } from '../../../src/constants'

import { ui, MISSING_PERMISSIONS_ERROR } from '../../support/constants'

describe('Labels - integration', () => {
  beforeEach(() => cy.login())

  it('handles missing read permission', () => {
    cy.server()
    cy.route({
      method: 'GET',
      url: '**/Label**',
      response: mocks.getLabels(buildAPIQuery())
    })

    cy.login({ authorization: { canReadLabel: false } })
    cy.visit(ROUTES.LABELS)

    cy.get(ui.PAGE_HEADER).should('not.exist')
    cy.get(ui.PAGE_CONTAINER).should('contain', MISSING_PERMISSIONS_ERROR)
  })

  it('renders empty state', () => {
    cy.server()
    cy.route({
      method: 'GET',
      url: '**/Label**',
      response: { data: [], total: 0 }
    })

    cy.visit(ROUTES.LABELS)

    cy.get(ui.TABLE).should('contain', 'No Data')
    cy.get(ui.TABLE_TOTAL).should('contain.text', '0 results')
  })

  it('renders data and syncs state to URL', () => {
    loadPage()

    cy.get(ui.TABLE_TOTAL).should('contain.text', '5 results')
    cy.get(ui.TABLE_ROWS).should('have.length', 5)
    cy.get(ui.TABLE_FIRST_ROW).should('contain', 'Label #1')
    cy.get(ui.TABLE_LAST_ROW).should('contain', 'Label #5')

    cy.url().then(url => expect(url).to.include(buildQuery()))
  })

  it('accepts params from URL', () => {
    const params = {
      OrderByColumn: 'isSod',
      OrderByDirection: 'Descending' as any,
      PageNumber: 2,
      PageSize: 2
    }

    const apiQuery = buildAPIQuery(params)
    const query = buildQuery(params)

    cy.server()
    cy.route({
      method: 'GET',
      url: '**/Label**',
      onRequest: ({ url }) => expect(url).to.include(apiQuery),
      response: mocks.getLabels(apiQuery)
    }).as('search')

    cy.visit(`${ROUTES.LABELS}${query}`)

    cy.get(ui.TABLE_TOTAL).should('contain.text', '2 results')
    cy.get(ui.TABLE_ROWS).should('have.length', 2)
    cy.get(ui.TABLE_FIRST_ROW).should('contain', 'Label #3')
    cy.get(ui.TABLE_LAST_ROW).should('contain', 'Label #2')
  })

  it('can search', () => {
    loadPage()

    const query = 'Label #3'

    cy.route({
      method: 'GET',
      url: '**/Label**',
      onRequest: ({ url }) => expect(url).to.include(query),
      response: mocks.getLabels(buildAPIQuery({ 'Filter.Text': query }))
    }).as('search')

    cy.get(ui.INPUT_SEARCH).type(query)
    cy.wait('@search')

    cy.get(ui.TABLE_TOTAL).should('contain.text', '1 result')
    cy.get(ui.TABLE_ROWS).should('have.length', 1)
    cy.get(ui.TABLE_FIRST_ROW).should('contain', 'Label #3')
  })

  it('can change sort order', () => {
    loadPage()

    const query = buildAPIQuery({ OrderByColumn: 'created' })
    cy.route({
      method: 'GET',
      url: '**/Label**',
      onRequest: ({ url }) => expect(url).to.include(`Label${query}`),
      response: mocks.getLabels(query)
    }).as('fetchSorted')

    cy.get(ui.TABLE_HEAD)
      .find('> th:nth-child(5)')
      .click()
  })

  // TODO
  it.skip('can change sort direction', () => {
    loadPage()

    const params = { OrderByColumn: 'name', OrderByDirection: 'Descending' as any }
    const apiQuery = buildAPIQuery(params)
    const query = buildQuery(params)

    cy.route({
      method: 'GET',
      url: '**/Label**',
      onRequest: ({ url }) => expect(url).to.include(`Label${apiQuery}`),
      response: mocks.getLabels(query)
    }).as('fetchSorted')

    cy.get(ui.TABLE_HEAD)
      .find('> th:nth-child(2)')
      .click()

    // cy.get(ui.TABLE_FIRST_ROW).should('contain', 'Label #5')
  })

  it('can select/deselect all', () => {
    loadPage()

    cy.get(ui.TABLE_SELECT_ALL_CHECKBOX).click()
    cy.get(ui.TABLE_SELECTION).should('contain.text', '5 selected')
    cy.get(ui.TABLE_FIRST_ROW)
      .find(ui.CHECKBOX)
      .should('have.class', 'ant-checkbox-wrapper-checked')

    cy.get(ui.TABLE_SELECT_ALL_CHECKBOX).click()
    cy.get(ui.TABLE_SELECTION).should('not.exist')
  })

  it('can select one', () => {
    loadPage()

    cy.get(ui.TABLE_FIRST_ROW)
      .find(ui.CHECKBOX)
      .click()
      .should('have.class', 'ant-checkbox-wrapper-checked')

    cy.get(ui.TABLE_SELECTION).should('contain.text', '1 selected')
  })

  // it('can filter', () => {})
})

// ===================================
// Helpers
// ===================================
const loadPage = () => {
  cy.server()

  const query = buildAPIQuery()
  cy.route({
    method: 'GET',
    url: '**/Label**',
    onRequest: ({ url }) => expect(url).to.include(`Label${query}`),
    response: mocks.getLabels(query)
  }).as('fetch')

  cy.visit(ROUTES.LABELS)
  cy.wait('@fetch')
}

type TParams = Partial<IAPIPagination> & { [key: string]: string | number }

const buildAPIQuery = (params: TParams = {}) => {
  const { OrderByColumn, OrderByDirection, PageNumber = '1', PageSize = '30', ...rest } = params

  const query = qs.stringify(
    { OrderByColumn, OrderByDirection, PageNumber, PageSize, ...rest },
    { sort: false }
  )

  return `?${query}`
}

const buildQuery = (params: TParams = {}) => {
  // Pick and set defaults
  const { OrderByColumn, OrderByDirection, PageNumber = '1', PageSize = '30', ...rest } = params

  let direction

  if (OrderByDirection === 'Descending') {
    direction = 'descend'
  }

  if (OrderByDirection === 'Ascending') {
    direction = 'ascend'
  }

  // Transform naming from API to client
  const paramsFinal = {
    orderBy: OrderByColumn,
    direction,
    page: PageNumber,
    pageSize: PageSize,
    ...rest
  }

  const query = qs.stringify(paramsFinal, { sort: false })

  return `?${query}`
}
