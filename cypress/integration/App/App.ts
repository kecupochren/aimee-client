import { ROUTES } from '../../../src/constants'

import { ui, getDefaultUser } from '../../support/constants'

describe('App', () => {
  beforeEach(() => {
    // Always mock data request since it's irrelevant
    cy.server()
    cy.route({
      method: 'GET',
      url: '**/Label**',
      response: { data: [], total: 0 }
    })
  })

  it('can login user', () => {
    cy.login()
    cy.visit(ROUTES.LABELS)

    cy.get(ui.user.NAME).should('contain', getDefaultUser().userDisplayAs)
    cy.get(ui.PAGE_HEADER).should('contain', 'Labely')
  })

  it('can impersonate user', () => {
    cy.route({
      method: 'POST',
      url: '**/User/impersonate**',
      response: {},
      onRequest: ({ request }) => expect(request.body.userId).to.eq(impersonatedUserId)
    })

    cy.login(impersonateUser)
    cy.visit(ROUTES.LABELS)

    cy.get(ui.user.NAME).should('contain', userDisplayAs)
  })

  it('can cancel impersonation', () => {
    cy.route({
      method: 'POST',
      url: '**/User/impersonate**',
      response: {}
    })
    cy.route({
      method: 'POST',
      url: '**/User/unimpersonate**',
      response: {}
    }).as('unimpersonate')

    cy.login(impersonateUser)
    cy.visit(ROUTES.LABELS)

    cy.get(ui.user.NAME).trigger('mouseover')
    cy.get(ui.user.CANCEL_IMPERSONATE).trigger('click')

    // Assert unimpersonate API request called
    cy.wait('@unimpersonate').then(() => expect(1).to.eq(1))

    // Assert we're being redirected to legacy app to finish unimpersonation
    cy.url().then(url => expect(url).to.contain('/Impersonate/Unset'))
  })

  it('can load language', () => {
    cy.login()
    cy.visit(ROUTES.LABELS)
    cy.get(ui.lang.ACTIVE).should('contain', 'English')
  })

  it('can change language', () => {
    cy.route({
      method: 'PUT',
      url: '**/Localization**',
      response: {}
    }).as('changeLang')

    cy.login()
    cy.visit(ROUTES.LABELS)

    cy.get(ui.lang.ACTIVE).trigger('mouseover')
    cy.get(ui.lang.SET_CZ).trigger('click')

    cy.wait('@changeLang').then(({ url }) => expect(url).to.contain('/Localization?code=cs-CZ'))
  })
})

const userDisplayAs = 'The Real Slim Shady'
const impersonatedUserId = '1'
const impersonateUser = {
  isImpersonated: true,
  userDisplayAs,
  impersonatedUserId
}
