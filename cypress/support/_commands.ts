// /* tslint:disable interface-name interface-over-type-literal */

// /// <reference types="Cypress" />

// // import get from 'lodash/get'

// // import { saveSession } from '../../src/utils/auth'
// // import { IAccessLevel } from '../../src/generated'
// // import { ROUTES } from '../../src/constants'

// // @ts-ignore
// import { ui } from './ui'

// // ===================================
// // Extend Cypress interface
// // ===================================
// declare global {
//   namespace Cypress {
//     interface Chainable {
//       /**
//        * Logs in Cypress test user, saves sessions to store so all
//        * subsequent requests fired of by the application use auth token
//        *
//        * @memberof Chainable
//        *
//        * @example
//        * ```
//        * cy.login()
//        * ```
//        */
//       login: typeof login

//       /**
//        * Creates a new collection
//        *
//        * @memberof Chainable
//        *
//        * @example
//        * ```
//        * cy.createCollection('My test collection')
//        * ```
//        */
//       createCollection: typeof createCollection

//       /**
//        * Deletes a collection by title
//        *
//        * @memberof Chainable
//        *
//        * @example
//        * ```
//        * cy.deleteCollection('My test collection')
//        * ```
//        */
//       deleteCollection: typeof deleteCollection
//     }
//   }
// }

// // ===================================
// // Constants
// // ===================================
// const API_URL = Cypress.env('API_URL')

// // ===================================
// // Private
// // ===================================
// // function getBaseRoute(route: string) {
// //   // prettier-ignore
// //   return route.replace('/:id(\\d+)?', '')
// // }

// // function graphqlRequest(body: { query: string; variables: { [key: string]: any } }) {
// //   return login().then(session => cy.request('POST', `${API_URL}/graphql`, { ...body, ...session }))
// // }

// // ===================================
// // Public
// // ===================================
// function login() {
//   return cy
//     .request('POST', `${API_URL}/auth`, {
//       email: Cypress.env('CYPRESS_LOGIN'),
//       password: Cypress.env('CYPRESS_PASSWORD')
//     })
//     .then(res => {
//       // saveSession(res.body)
//       return res.body
//     })
// }

// function createCollection(title: string) {
//   return Promise.resolve(title)
//   // return graphqlRequest({
//   //   query: `
//   //     mutation collectionCreate($input: CollectionCreateInput!) {
//   //       collectionCreate(input: $input) {
//   //         id
//   //       }
//   //     }
//   //   `,
//   //   variables: { input: { title } }
//   // })
// }

// function deleteCollection(title: string) {
//   return Promise.resolve(title)
//   // return graphqlRequest({
//   //   query: `
//   //     query collectionsSearch($query: String!, $limit: Int!) {
//   //       collectionsSearch(query: $query, limit: $limit) {
//   //         id
//   //       }
//   //     }
//   //   `,
//   //   variables: { query: title, limit: 1 }
//   // }).then(res => {
//   //   const id = get(res, 'body.data.mastersSearch[0].id')
//   //   if (id) {
//   //     return graphqlRequest({
//   //       query: `
//   //         mutation collectionDelete($id: ID!) {
//   //           collectionDelete(id: $id)
//   //         }
//   //       `,
//   //       variables: { id }
//   //     })
//   //   }

//   //   return Promise.resolve()
//   // })
// }
// // ===================================
// // Bind public commands
// // ===================================
// Cypress.Commands.add('login', login)
// Cypress.Commands.add('createCollection', createCollection)
// Cypress.Commands.add('deleteCollection', deleteCollection)
