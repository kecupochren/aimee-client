/* tslint:disable interface-name */

import 'cypress-ntlm-auth/src/commands'
import merge from 'lodash/merge'

import { mockServerData } from '../../../src/constants'

import { TUser, getDefaultUser } from '../constants'

declare global {
  namespace Cypress {
    interface Chainable {
      /**
       * Login
       *
       * @memberof Chainable
       *
       * @example
       * ```
       * cy.login()
       * cy.login({ userName: 'Dude' })
       * ```
       */
      login: typeof login

      /**
       * Simulates client side login
       *
       * @memberof Chainable
       *
       * @example
       * ```
       * cy.login()
       * ```
       */
      mockUser: typeof mockUser
    }
  }
}

const login = (user: TUser = {}): Cypress.Chainable<any> => {
  const userFinal = merge({}, getDefaultUser(), user)

  // Login to API using Windows credentials
  // @ts-ignore
  cy.ntlmReset().ntlm(
    Cypress.env('WEB_API'),
    Cypress.env('NTLM_READONLY_USERNAME'),
    Cypress.env('NTLM_READONLY_PASSWORD'),
    Cypress.env('NTLM_READONLY_DOMAIN')
  )
  cy.log('Login success')

  // "Login" to client side app
  mockUser(userFinal)

  return cy
}

const mockUser = (user?: TUser) => {
  const userFinal = merge({}, getDefaultUser(), user)

  // Merge default mock data with provided
  const data = merge({}, JSON.parse(mockServerData), merge(userFinal, user))

  // Write data to localStorage; app is setup to read it from there if defined
  window.localStorage.setItem('serverData', JSON.stringify(data))
}

Cypress.Commands.add('login', login)
Cypress.Commands.add('mockUser', mockUser)
