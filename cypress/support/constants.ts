import { UserModel } from '../../src/store/models'
import { Permissions } from '../../src/store/helpers/user'

export const ui = {
  user: {
    NAME: '[data-cy="user-name"]',
    CANCEL_IMPERSONATE: '[data-cy="cancel-impersonate"]'
  },

  lang: {
    ACTIVE: '[data-cy="lang"]',
    SET_EN: '[data-cy="lang-set-en"]',
    SET_CZ: '[data-cy="lang-set-cz"]'
  },

  PAGE: '[data-cy="page"]',
  PAGE_HEADER: '[data-cy="page__header"]',
  PAGE_CONTAINER: '[data-cy="page__container"]',

  TABLE: '[data-cy="table"]',
  TABLE_META: '[data-cy="table__meta"]',
  TABLE_TOTAL: '[data-cy="table__total"]',
  TABLE_SELECTION: '[data-cy="table__selection"]',
  TABLE_HEAD: '.ant-table-thead > tr',
  TABLE_BODY: '.ant-table-tbody',
  TABLE_ROWS: '.ant-table-tbody > tr',
  TABLE_FIRST_ROW: '.ant-table-tbody > tr:first-child',
  TABLE_LAST_ROW: '.ant-table-tbody > tr:last-child',
  TABLE_SELECT_ALL_CHECKBOX: '.ant-table-thead > tr > th:first-child .ant-checkbox-wrapper',

  INPUT_SEARCH: '[data-cy="input-search"]',

  CHECKBOX: '.ant-checkbox-wrapper'
}

export const MISSING_PERMISSIONS_ERROR =
  'Your account does not have necessary permission to view this page'

export type TUser = Partial<UserModel> & { authorization?: Partial<Permissions> } & {
  language?: string
}

export const getDefaultUser = (): TUser => ({
  userName: 'Cypress',
  userDisplayAs: 'Cypress user',
  isImpersonated: false,
  impersonatedUserId: undefined,
  language: 'en-US',
  authorization: {
    canCreateDepartment: true,
    canCreateJobPosition: true,
    canCreateLabel: true,
    canCreateLabelRestrictedPair: true,
    canCreateOrganizationalUnit: true,
    canCreateRecertification: true,
    canCreateSystemSecurityFlag: true,
    canFullExportReports: true,
    canReadBusinessRole: true,
    canReadCompany: true,
    canReadDataSet: true,
    canReadDepartment: true,
    canReadEmployee: true,
    canReadEmployeeAll: true,
    canReadIdentityOrganizationalUnitReport: true,
    canReadJobPosition: true,
    canReadLabel: true,
    canReadLabelRestrictedPair: true,
    canReadOrganizationalUnit: true,
    canReadPreapprovedSodCompensation: true,
    canReadRecertification: true,
    canReadRecertificationUnit: true,
    canReadReportApplicationsWithRoleSecurityFlags: true,
    canReadReportEmployeeSodConflicts: true,
    canReadReportIdentityLabels: true,
    canReadReports: true,
    canReadSystemApplication: true,
    canReadSystemRole: true,
    canReadSystemSecurityFlag: true,
    canReadWorkflowSummary: true
  }
})
