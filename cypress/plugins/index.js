// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

const wp = require('@cypress/webpack-preprocessor')
const ntlmAuth = require('cypress-ntlm-auth/src/plugin')

const paths = require('../../webpack/paths')

require('dotenv').config()

module.exports = (on, config) => {
  config.env.API_URL = process.env.API_URL
  config.env.USE_MOCK = process.env.USE_MOCK

  config.env.NTLM_READONLY_USERNAME = process.env.NTLM_READONLY_USERNAME
  config.env.NTLM_READONLY_PASSWORD = process.env.NTLM_READONLY_PASSWORD
  config.env.NTLM_READONLY_DOMAIN = process.env.NTLM_READONLY_DOMAIN

  const options = {
    webpackOptions: {
      resolve: {
        extensions: ['.ts', '.tsx', '.js'],
        alias: {
          '~': paths.src
        }
      },

      module: {
        rules: [
          {
            test: /\.tsx?$/,
            loader: 'ts-loader',
            options: { transpileOnly: true }
          }
        ]
      }
    }
  }

  on('file:preprocessor', wp(options))

  config = ntlmAuth.initNtlmAuth(config)

  return config
}
