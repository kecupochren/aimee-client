# 125.AIM ClientApp

## 1. Development

Install dependencies

```
npm install
```

Run Webpack + TS/TSLint

```
npm run dev
```

Open browser on http://localhost:8080

## 2. Environments

The application offers a way to change environment in runtime. There is a toggle in header for this.
It is useful if you want to run your local version against production DB or mockserver.

## 3. Mockserver

Ideally you should be developing against mockserver. This lets you develop stuff ahead of backend and forces you to understand the data in greater detail.

## 4. Using react-intl

The application uses `react-intl` to handle i18n.
The process for using it is as follows:

- Define message through `defineMessages` imported from `react-intl`. Give it an `id` and `defaultMessage` (_in English_). By convention, define these messages in `MyPage.messages.ts`
- Use it in your page by importing it and pass it to either
  - `FormattedMessage` imported from `react-intl`
  - `Message` imported from `~/components`
  - Any other component that accepts message prop, such as `Input`, `Button`, `Link` etc
- Run `npm run trans` to output JSON file containing all messages used in source code
- Add Czech translation in this JSON file, located in `~/translations/locales/cs.json`
- This JSON file is what's actually used in runtime to you should see your message translated.

**Important**

The API outputs server defined dictionaries to window.serverData.
These have a bit different format:

```
{
  "SomePage": {
    "SomeKey": "Some Czech translation"
  }
}
```

The application is setup to process these, transform them to the same format as default JSON file outputted by `react-intl`:

```
"SomePage.SomeKey": "Some czech Translation"
```

This allows us to use these phrases in `react-intl`, just by using `SomePage.SomeKey` as `id` in `defineMessages`.
Since the transformed API dictionary is merged with original JSON (JSON overwrites it), it lets us use most of the messages from the server, **while also letting us define custom messages as we need them, or overwrite server defined phrase**.

## 5. Troubleshooting MobX

#### Component doesn't rerender when data changes

Each component in the render tree that uses such data needs to be wrapped in observer, imported from `mobx-react-lite`.

This also applies to render functions in class components. Though ideally you should avoid using class components altogether, there is no reason to use them today over functional components.
