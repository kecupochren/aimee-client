require('dotenv').config()

const webpack = require('webpack')

const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
// const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')
const CircularDependencyPlugin = require('circular-dependency-plugin')

const { theme } = require('../src/constants/theme')

const paths = require('./paths')

const config = {
  mode: 'development',

  entry: paths.entry,

  output: {
    path: paths.output,
    filename: '[name].bundle.js',
    publicPath: '/'
  },

  module: {
    rules: [
      {
        test: /\.(js|ts|tsx)$/,
        loader: 'babel-loader',
        exclude: /@babel(?:\/|\\{1,2})runtime|core-js/,
        options: {
          presets: ['@babel/preset-env']
        }
      },

      {
        // Traverse all rules until one matches, else use file-loader as fallback
        oneOf: [
          // Customize Antd theme https://ant.design/docs/react/customize-theme
          {
            test: /\.less$/,
            exclude: '/node_modules/',
            use: [
              { loader: 'style-loader' },
              { loader: 'css-loader' },
              {
                loader: 'less-loader',
                options: {
                  modifyVars: {
                    'primary-color': theme.color.primaryLight,
                    'link-color': theme.color.primaryLight,
                    'border-radius-base': '5px'
                  },
                  javascriptEnabled: true
                }
              }
            ]
          },

          {
            test: /\.css$/,
            exclude: '/node_modules/',
            use: ['style-loader', 'css-loader', 'less-loader']
          },

          {
            test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
            exclude: '/node_modules/',
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: 'static/media/[name].[hash:8].[ext]'
            }
          },

          // Fallback to file loader for any other files
          {
            loader: 'file-loader',
            exclude: '/node_modules/',
            exclude: [/\.(js|mjs|ejs|jsx|ts|tsx)$/, /\.html$/, /\.json$/],
            options: {
              name: 'static/media/[name].[hash:8].[ext]'
            }
          }
        ]
      }
    ]
  },

  resolve: {
    extensions: ['.js', '.json', '.ts', '.tsx', '.svg', '.ejs'],
    alias: {
      '~': paths.src
    }
  },

  plugins: [
    new webpack.EnvironmentPlugin({
      API_URL: process.env.API_URL,
      LOCAL_PROD: process.env.LOCAL_PROD || false
    }),

    new CleanWebpackPlugin(),

    // new ForkTsCheckerWebpackPlugin({
    //   memoryLimit: 8192,
    //   tsconfig: paths.tsconfig,
    //   tslint: paths.tslint,
    //   // for some reason this rule setting is not picked up from tslint.json
    //   ignoreLints: ['object-literal-sort-keys'],
    //   checkSyntacticErrors: true
    // }),

    new CircularDependencyPlugin({
      exclude: /node_modules/,
      failOnError: true,
      allowAsyncCycles: false,
      cwd: process.cwd()
    })
  ],

  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: { name: false, chunks: 'all' }
      }
    }
  },

  node: {
    fs: 'empty',
    module: 'empty'
  }
}

if (process.env.WEBPACK_ANALYZE) {
  config.plugins.push(new BundleAnalyzerPlugin())
}

module.exports = config
