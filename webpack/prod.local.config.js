/**
 * Production configuration for standalone testing without .NET
 */

const merge = require('webpack-merge')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const paths = require('./paths')

module.exports = merge(require('./prod.config'), {
  output: {
    path: paths.output,
    publicPath: paths.publicPathLocalProd
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: paths.htmlTemplate
    })
  ]
})
