const merge = require('webpack-merge')
const TerserPlugin = require('terser-webpack-plugin')
const WorkboxPlugin = require('workbox-webpack-plugin')

const paths = require('./paths')

const config = merge(require('./base.config'), {
  mode: 'production',

  output: {
    path: paths.outputProd,
    publicPath: paths.publicPathProd
  },

  optimization: {
    minimizer: [new TerserPlugin({})]
  }
})

if (!process.env.LOCAL_PROD) {
  config.plugins.push(
    new WorkboxPlugin.GenerateSW({
      clientsClaim: true,
      skipWaiting: true
    })
  )
}

module.exports = config
