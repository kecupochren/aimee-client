const path = require('path')
const merge = require('webpack-merge')
const _ = require('lodash')

const baseConfig = require('./base.config')

module.exports = function({ config }) {
  // Extend default rules with our custom
  _.set(config, 'module.rules', [...config.module.rules, ...baseConfig.module.rules])

  // Resolve ~/components imports
  _.set(config, 'resolve.alias', baseConfig.resolve.alias)

  // Resolve typescript files
  _.set(config, 'resolve.extensions', [...config.resolve.extensions, '.tsx'])

  return config
}
