const path = require('path')

const resolve = file => path.resolve(__dirname, file)

module.exports = {
  src: resolve('../src'),
  entry: resolve('../src/index.tsx'),

  output: resolve('../build'),
  outputProd: resolve('../../Web/assets/bundles'),

  publicPathProd: '/assets/bundles/',
  publicPathLocalProd: '/',

  htmlTemplate: resolve('../src/index.html'),
  favicon: resolve('../src/assets/img/favicon.ico'),

  tsconfig: resolve('../tsconfig.json'),
  tslint: resolve('../tslint.json')
}
