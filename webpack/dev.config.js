const webpack = require('webpack')
const merge = require('webpack-merge')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const paths = require('./paths')

module.exports = merge(require('./base.config'), {
  devtool: 'eval-source-map',

  plugins: [
    new webpack.ProgressPlugin(),

    new HtmlWebpackPlugin({ inject: true, template: paths.htmlTemplate })
  ],

  devServer: {
    hot: true,
    inline: true,
    contentBase: paths.src,
    publicPath: '/',
    historyApiFallback: true,
    port: process.env.PORT || '8080'
  }
})
